#!/usr/bin/env bash
#
# Demandware Build Shell Script
# Copyright (c) 2015 PixelMedia, Inc
#

set -e
#set -x

if [ ! -f build_settings.sh ]; then
  echo "Error: 'build_settings.sh' deployment settings file not found"
  exit 1
fi

source build_settings.sh

echo ${CODE_VERSION}

if [ ! -n "${CODE_VERSION}" ]; then
  echo "Error: Must specify a code version in 'build_settings.sh'";
  exit 1;
fi

mkdir -p "build/${CODE_VERSION}"

_CARTRIDGES=$(find ${PWD}/* -name '.project' | xargs -n 1 dirname | xargs -n 1 basename)
_CARTRIDGES_DIRS=$(find ${PWD}/* -name '.project' | xargs -n 1 dirname)
_MIGRATION_DIR="${PWD}/migrations"

echo "Collecting cartridges..."
for _CARTRIDGE_DIR in $(echo $_CARTRIDGES_DIRS); do
  _skip=0
  _cart_name=$(echo $_CARTRIDGE_DIR | xargs -n 1 basename)
  for e in "${EXCLUDE_CARTRIDGES[@]}"; do
    if [ "$e" = "${_cart_name}" ]; then
      echo "SKIPPING CARTRIDGE: ${_cart_name}";
      _skip=1
      break
    fi
  done

  if [ $_skip -eq 0 ]; then
    echo "CARTRIDGE: ${_cart_name}"
    cp -r $_CARTRIDGE_DIR "build/${CODE_VERSION}";
  fi
done

echo "Zipping cartridges..."

pushd build > /dev/null
rm -f "${CODE_VERSION}.zip"
zip --quiet -r "${CODE_VERSION}.zip" "${CODE_VERSION}"

if [ -d $_MIGRATION_DIR ]; then
  echo "Zipping migrations..."
  cp -r "${_MIGRATION_DIR}" migrations
  zip --quiet -r "${CODE_VERSION}_migrations.zip" migrations/
fi

popd > /dev/null
echo "Cleaning up..."
rm -rf build/migrations
rm -rf build/${CODE_VERSION}

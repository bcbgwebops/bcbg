/**
* Search
*
* @input ProductSearchResult : dw.catalog.ProductSearchModel
*
*/
function execute( args : PipelineDictionary ) : Number
{
	var track = require( './track.js' );
	try{
		track.Search(args.ProductSearchResult);
	} catch (error) {}

   return PIPELET_NEXT;
}

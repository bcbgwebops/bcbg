/**
* checkout
*
* @input Order : dw.order.Order
*
*/
function execute( args : PipelineDictionary ) : Number
{
	var track = require( './track.js' );
	try{
		track.Checkout(args.Order);
	} catch (error) {}

   return PIPELET_NEXT;
}

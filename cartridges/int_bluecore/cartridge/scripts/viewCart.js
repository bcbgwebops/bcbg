/**
* viewCart
*
* @input Cart : dw.order.Basket
*
*/
function execute( args : PipelineDictionary ) : Number
{
	var track = require( './track.js' );
	try{
		track.ViewCart(args.Cart);
	} catch (error) {}

   return PIPELET_NEXT;
}

/**
* removeFromCart
*
* @input ProductLineItem : dw.order.ProductLineItem
*
*/
function execute( args : PipelineDictionary ) : Number
{
	var track = require( './track.js' );
	try{
		track.RemoveFromCart(args.ProductLineItem);
	} catch (error) {}

   return PIPELET_NEXT;
}

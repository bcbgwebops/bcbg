/**
* addToWishlist
*
*/
function execute( args : PipelineDictionary ) : Number
{
	var track = require( './track.js' );
	try{
		track.AddToWishlist(args.Product);
	} catch (error) {}

   return PIPELET_NEXT;
}

/**
* addToCart
*
*/
function execute( args : PipelineDictionary ) : Number
{
	var track = require( './track.js' );
	try{
		track.AddToCart();
	}
	catch (error) {}

   return PIPELET_NEXT;
}

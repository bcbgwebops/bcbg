/**
 * getCategoryBreadcrumbs
 *
 * @param {dw.catalog.Category} category - Category
 * @returns {Object} - list of category names
 */
function getCategoryBreadcrumbs(category : dw.catalog.Category) {
    var breadcrumbs = [];
    if (category) {
        var lookup_category = category;
        while(lookup_category && !lookup_category.isRoot()){
            breadcrumbs.unshift(lookup_category.getDisplayName());
            lookup_category = lookup_category.getParent();
        }
    }
    return breadcrumbs;
}

/**
 * getImageURLFromProduct
 *
 * @param {dw.catalog.Product} product - A product
 * @returns {String} imageURL - URL of first prioritized image found for product
 */
function getImageURLFromProduct(product : dw.catalog.Product) {
    var imageSize,
        mediaFile = undefined,
        // Image options by priority
        imageOptions = ['large', 'medium', 'hi-res', 'small'];

    for(var i = 0; i < imageOptions.length && !mediaFile; i++) {
        imageSize = imageOptions[i];
        mediafile = product.getImage(imageSize, 0);
        if (mediafile) {
            return String(mediafile.getHttpURL());
        }
    };

    return mediaFile;
}

/**
 * getCategoryFromProduct
 *
 * @param {dw.catalog.Product} product - A product
 * @returns {dw.catalog.Categor} category - Primary category for product
 */
function getCategoryFromProduct(product : dw.catalog.Product) {
    var category = product.getPrimaryCategory();

    if (!category && product.isVariant()) {
        category = product.getMasterProduct().getPrimaryCategory()
    }

    return category || undefined;
}

/**
 * parseProductInfo
 *
 * @param {dw.catalog.Product} product - A product
 * @param {Boolean} includeVariants
 * @returns {Object} - parsed product info
 */
function parseProductInfo(product : dw.catalog.Product, includeVariants : Boolean) {
    includeVariants = includeVariants || false;

    var category = getCategoryFromProduct(product),
        breadcrumbs = getCategoryBreadcrumbs(category),
        URLUtils = require('dw/web/URLUtils'),
        priceModel = product.getPriceModel(),
        priceVal = Number(priceModel.getPrice().getDecimalValue()) || undefined,
        productAvailability = product.getAvailabilityModel(),
        productInventoryRecord = productAvailability.getInventoryRecord();

    var parsedProduct = {
        // root_id / variant_id
        id: product.isVariant() ? String(product.getMasterProduct().getID()) : String(product.getID()),
        skuId: product.isVariant() ? String(product.getID()) : undefined,

        name: product.getName(),
        url: String(URLUtils.abs('Product-Show', 'pid', product.getID())) || undefined,
        brand: product.getBrand() || undefined,
        price: product.isMaster() ? Number(priceModel.getMinPrice().getDecimalValue()) : priceVal,
        minPrice: product.isMaster() ? Number(priceModel.getMinPrice().getDecimalValue()) : undefined,
        maxPrice: product.isMaster() ? Number(priceModel.getMaxPrice().getDecimalValue()) : undefined,
        //image: getImageURLFromProduct(product),
        image: product.isMaster() ? String(URLUtils.imageURL(URLUtils.CONTEXT_CATALOG, dw.catalog.CatalogMgr.getSiteCatalog().ID, '/images/large/'+product.ID.replace(/\-/g, '_')+'.jpg', {scaleWidth:1268,scaleHeight:1992,scaleMode:'fit'})): String(URLUtils.imageURL(URLUtils.CONTEXT_CATALOG, dw.catalog.CatalogMgr.getSiteCatalog().ID, '/images/large/'+product.masterProduct.ID.replace(/\-/g, '_')+'.jpg', {scaleWidth:1268,scaleHeight:1992,scaleMode:'fit'})),
        outOfStock: !productAvailability.isInStock(),
        inventory: productInventoryRecord ? productInventoryRecord.getAllocation().getValue() : undefined,
        category: category ? category.getDisplayName() : undefined,
        breadcrumbs: breadcrumbs.length > 0 ? breadcrumbs : undefined,

        // product flags
        isMaster: product.isMaster(),
        isProduct: product.isProduct(),
        isProductSet: product.isProductSet(),
        isVariant: product.isVariant()
    };

    var attributeVariations = [];

    // get possible custom attribute variations
    (product.getVariationModel().getProductVariationAttributes().toArray() || []).forEach(function(variationAttr) {
        var variationId = variationAttr.getAttributeID();
        attributeVariations.push(variationId);
        // add variation properties
        parsedProduct[variationId] = product.getCustom()[variationId];
    });
    // add list of variations
    parsedProduct.attributeVariations = attributeVariations;

    if (includeVariants) {
        var variants = [],
            master = product.isVariant() ? product.getMasterProduct() : product;

        // get variant products and fetch variation values
        (master.getVariants().toArray() || []).forEach(function(productVariant) {
            var variantAvailability = productVariant.getAvailabilityModel(),
                inventoryRecord = variantAvailability.getInventoryRecord();
            var variantData = {
                id: String(productVariant.getID()),
                outOfStock: !variantAvailability.isInStock(),
                image: product.isMaster() ? String(URLUtils.imageURL(URLUtils.CONTEXT_CATALOG, dw.catalog.CatalogMgr.getSiteCatalog().ID, '/images/large/'+product.ID.replace(/\-/g, '_')+'.jpg', {scaleWidth:1268,scaleHeight:1992,scaleMode:'fit'})): String(URLUtils.imageURL(URLUtils.CONTEXT_CATALOG, dw.catalog.CatalogMgr.getSiteCatalog().ID, '/images/large/'+product.masterProduct.ID.replace(/\-/g, '_')+'.jpg', {scaleWidth:1268,scaleHeight:1992,scaleMode:'fit'})),
                price: Number(productVariant.getPriceModel().getPrice().getDecimalValue()) || undefined,
                inventory: inventoryRecord ? inventoryRecord.getAllocation().getValue() : undefined
            };

            // add variation properties
            (parsedProduct.attributeVariations).forEach(function(variationId) {
                variantData[variationId] = productVariant.custom[variationId];
            });

            variants.push(variantData);
        });
        // add list of variant products with variations
        parsedProduct.variants = variants;
    }

    return parsedProduct;
}

/**
 * renderEventData
 *
 * @param {String} functionName - Function in partner integration
 * @param {String} functionData - data to pass to integration function
 */
function renderEventData(functionName : String, functionData : String) {
    var ISML = require('dw/template/ISML');
    functionData = encodeURI(JSON.stringify(functionData || {}));

    var template = 'intbluecore/track';
    ISML.renderTemplate(template, {
        functionName: functionName,
        functionData: functionData
    });
}

/**
 * ViewProduct
 *
 * @param {dw.catalog.Product} product - Product
 */
function viewProduct(product : dw.catalog.Product) {
    var isVisible = product.hasOwnProperty('isVisible') ? product.isVisible() : true;
    if (product && isVisible) {
        // only track public products
        renderEventData('trackProductView', parseProductInfo(product, true));
    }
}

/**
 * addToCart
 *
 */
function addToCart() {
    var addToCartEvent = function(product) {
        var productData = {
            // root_id / variant_id
            id: product.isVariant() ? String(product.getMasterProduct().getID()) : String(product.getID()),
            skuId: product.isVariant() ? String(product.getID()) : undefined
        };

        renderEventData('trackAddToCart', productData);
    };
    var rootProduct;
    var params = request.httpParameterMap;
    var ProductMgr = require('dw/catalog/ProductMgr');
    var ProductListMgr = require('dw/customer/ProductListMgr');


    if (params.plid.stringValue) {
        // Adds a product from a product list.
        var productList = ProductListMgr.getProductList(params.plid.stringValue);
        if (productList) {
            rootProduct = productList.getItem(params.itemid.stringValue);
            addToCartEvent(rootProduct);
        }
    } else {
        // Adds a root product.
        rootProduct = ProductMgr.getProduct(params.pid.stringValue);
        if (rootProduct.isProductSet()) {
            var childPids = params.childPids.stringValue.split(',');
            for (var i = 0; i < childPids.length; i++) {
                var childProduct = ProductMgr.getProduct(childPids[i]);
                if (childProduct && !childProduct.isProductSet()) {
                    addToCartEvent(childProduct);
                }
            }
        } else {
            addToCartEvent(rootProduct);
        }
    }
}

/**
 * view cart
 *
 * @param {dw.order.Basket} cart - Cart
 */
function viewCart(cart : dw.order.Basket) {

    // detect remove from cart activity
    var removeFromCartProductString = session.custom.bluecoreRemoveFromCart;
    if (removeFromCartProductString) {
        var productData;
        try {
            productData = JSON.parse(removeFromCartProductString);
        } catch (e) {}

        if (productData && productData.id) {
            renderEventData('trackRemoveFromCart', productData);
        }
        session.custom.bluecoreRemoveFromCart = undefined;
    }

    if (cart) {
        var cartData = {
            productIds: getProductIdsFromBasket(cart)
        };

        renderEventData('trackAddToCart', cartData);
    }
}

/**
 * removeFromCart
 *
 * @param {dw.order.ProductLineItem} productLineItem
 */
function removeFromCart(productLineItem : dw.order.ProductLineItem) {
    var product = productLineItem.getProduct(),
        productData = {
            id: product.isVariant() ? String(product.getMasterProduct().getID()) : String(product.getID()),
            skuId: product.isVariant() ? String(product.getID()) : undefined
        };
    session.custom.bluecoreRemoveFromCart = JSON.stringify(productData);
}

/**
 * getProductIdsFromBasket
 *
 * @param {dw.order.Basket} cart - Cart
 * @returns {Object} - list of objects containing product IDs
 */
function getProductIdsFromBasket(basket : dw.order.Basket) {
    var productIds = [];
    (basket.getAllProductLineItems().toArray() || []).forEach(function(orderLineItem) {
        var product = orderLineItem.getProduct();
        if (product) {
            productIds.push({
                id: product.isVariant() ? String(product.getMasterProduct().getID()) : String(product.getID()),
                skuId: product.isVariant() ? String(product.getID()) : undefined
            });
        }
    });
    return productIds;
}

/**
 * addToWishlist
 *
 * @param {dw.catalog.Product} product - Product
 */
function addToWishlist(product : dw.catalog.Product) {
    var params = request.httpParameterMap,
        ProductMgr = require('dw/catalog/ProductMgr');
    product = product || ProductMgr.getProduct(params.pid.stringValue);

    if (product) {
        var productData = {
            id: product.isVariant() ? String(product.getMasterProduct().getID()) : String(product.getID()),
            skuId: product.isVariant() ? String(product.getID()) : undefined
        };
        session.custom.bluecoreAddToWishlist = JSON.stringify(productData);
    }
}

/**
 * viewWishlist
 *
 */
function viewWishlist() {
    var addToWishlistProductString = session.custom.bluecoreAddToWishlist;
    if (addToWishlistProductString) {
        var productData;
        try {
            productData = JSON.parse(addToWishlistProductString);
        } catch (e) {}

        if (productData && productData.id) {
            renderEventData('trackWishlist', productData);
        }
        session.custom.bluecoreAddToWishlist = undefined;
    }
}

/**
 * search
 *
 * @param {dw.catalog.ProductSearchModel} productSearchModel
 */
function search(productSearchModel : dw.catalog.ProductSearchModel) {
    // must be a ProductSearchModel
    if (productSearchModel instanceof(dw.catalog.ProductSearchModel)) {
        var searchTerm, category, breadcrumbs = [];

        if (productSearchModel.categorySearch) {
            // category browse
            category = productSearchModel.getCategory(),
            breadcrumbs = getCategoryBreadcrumbs(category);
        } else if (productSearchModel.getCount()) {
            // free text search
            searchTerm = productSearchModel.getSearchPhrase() || '';
        }

        var searchData = {
            searchTerm: searchTerm,
            category: category ? category.getDisplayName() :undefined,
            breadcrumbs: breadcrumbs.length ? breadcrumbs : undefined
        };

        if (searchData.category || searchData.searchTerm) {
            renderEventData('trackSearch', searchData);
        }
    }
}

/**
 * checkout
 *
 * @param {dw.order.Order} order - Completed user order
 */
function checkout(order : dw.order.Order) {
    if (order) {
        var orderProductIds = getProductIdsFromBasket(order);
        var orderData = {
            orderId: order.getInvoiceNo(),
            orderTotalNet: Number(order.getMerchandizeTotalNetPrice().getDecimalValue()) || undefined,
            orderTotalGross: Number(order.getMerchandizeTotalGrossPrice().getDecimalValue()) || undefined,
            email: order.getCustomerEmail(),
            productIds: orderProductIds
        };

        renderEventData('trackCheckout', orderData);
    }
}

/**
 * debug
 *
 * @param {object} props
 */
function debug(props) {
    renderEventData('trackDebug', props);
}

// debug / development
exports.Debug = debug
exports.Debug.public = true;

exports.ViewProduct = viewProduct;
exports.ViewProduct.public = true;

exports.Search = search;
exports.Search.public = true;

exports.AddToCart = addToCart;
exports.AddToCart.public = true;

exports.RemoveFromCart = removeFromCart;
exports.RemoveFromCart.public = true;

exports.ViewCart = viewCart;
exports.ViewCart.public = true;

exports.AddToWishlist = addToWishlist;
exports.AddToWishlist.public = true;

exports.ViewWishlist = viewWishlist;
exports.ViewWishlist.public = true;

exports.Checkout = checkout;
exports.Checkout.public = true;
/**
* ViewProduct
*
* @input Product : dw.catalog.Product
*/
function execute( args : PipelineDictionary ) : Number
{
    var productMgr = dw.catalog.ProductMgr,
        track = require( './track.js' ),
        pid = request.httpParameterMap.pid.stringValue;

    var product = args.Product || productMgr.getProduct(pid);
    try{
        track.ViewProduct(product);
    } catch (error) {}

   return PIPELET_NEXT;
}
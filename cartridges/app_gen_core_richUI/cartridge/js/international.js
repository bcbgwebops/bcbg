'use strict';

var ajax = require('./ajax'),
	page = require('./page'),
	util = require('./util'),
	dialog = require('./dialog');

var countryCode = '';

exports.init = function () {
	
	$(document).on('click touchend', '.intl_cancel', function() {
		dialog.close();
	});

	$('.internationalShip').on('click', function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		var locale = $(this).attr('locale');
		dialog.open({
			url: url,
			options: {
				width: 320,
				dialogClass: "boldclose countryselector",
			}
		});		
	});
	 
	$(document).on('click', '.intl_continue', function(e) {
		document.cookie = 'Lang=' + countryCode + '; expires=Fri, 31 Dec 2100 23:59:59 GMT';
	});
	
	$(document).on('click', '.intl_update', function(e) {
		e.preventDefault();
		var changeShip = ($('#intl_shipto').val() != '-1');
		var changeLang = ($('#intl_country').val() != '-1');

		if (changeShip) {
			var parts = $('#intl_shipto').val().split('_');
			if (parts.length > 1) {
				var url = $(this).attr('href') + '&currencyCountry=' + parts[0] + '&currency=' + parts[1];
				var tourl = window.location.href;
				if (tourl.indexOf('International-') == -1) {
					url += '&from=' + encodeURI(window.location.href);
				}
				if (changeLang == false) {
					window.location = url;
					return;
				} else {
					$.ajax({
						url:url
					}).done(function() {
						var l = $('#intl_country').val();
						$('a.internationalLink').each(function(idx, obj) {
							var curLocale = $(this).attr('curlocale');
							if (curLocale == 'default') curLocale = 'en';
							if ($(this).attr('locale') == l) {
								window.location = $(this).attr('href').replace('/'+curLocale+'/', '/'+l+'/');
								return;
							}
						});						
					});
				}
			}
			return;
		} 
		if (changeLang) {
			var l = $('#intl_country').val();
			$('a.internationalLink').each(function(idx, obj) {
				var curLocale = $(this).attr('curlocale');
				if (curLocale == 'default') curLocale = 'en';				
				if ($(this).attr('locale') == l) {
					window.location = $(this).attr('href').replace('/'+curLocale+'/', '/'+l+'/');
					return;
				}
			});
		}	
	});
};


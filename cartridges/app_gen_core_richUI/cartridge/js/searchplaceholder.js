'use strict';

/**
 * @private
 * @function
 * @description Binds event to the place holder (.blur)
 */
function initializeEvents() {
	$('#q').focus(function () {
		$(this).attr('placeholder');
	});
	$('#q').blur(function () {
		var placeholder = $(this).attr('defaultText');
		$(this).attr('placeholder', placeholder);
	});

	$(".menu-utility-user .searchIcon").click(function(){
		if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(".header-search").slideUp();
		} else{
			$(this).addClass("active");
			$(".header-search").slideDown();
		 $(".header-search").css({height : '180px'})
		}
		$(".header-search #q").focus();
	})
	$(".header-search .close").click(function(){
		$(".menu-utility-user .searchIcon").removeClass("active");
		$(".header-search").slideUp();
		$(".header-search #q").val('');
	})
	
}

exports.init = initializeEvents;

'use strict';

var progress = require('../../progress'),
	util = require('../../util'),
	dialog = require('../../dialog'),
	validator = require('../../validator'),
	onepage = require('./onepage'),
	address = require('../checkout/address'),
	billing = require('../checkout/billing'),
	afterpay = require('./afterpay');

var buttonClick = function(){
	$('#dwfrm_billing').data('button', this.name);
}

var opcFormSubmit = function(e){
	var paymentMethod = $("input[name='dwfrm_billing_paymentMethods_selectedPaymentMethodID']:checked").val();
	
	if (paymentMethod == "Sofort" || paymentMethod == "iDeal" || paymentMethod == "QIWI" || paymentMethod == "Alipay" || paymentMethod == "PAYPAL_CHECKOUT" || paymentMethod == "PAYPAL_PAYMENT" || paymentMethod == "UNIONPAY_CREDIT" || paymentMethod == "PayPal" || paymentMethod == "SafetyPay" || paymentMethod == "Interac") {
		util.createCookie('checkoutStep','summary',0);
		util.createCookie('checkoutStepFrom','billing',0);
	} else {
		e.preventDefault();

		var thisForm = $(this);

		$("#dwfrm_billing_billingAddress_addressFields_country").prop('disabled', false);

		onepage.clearFormError(thisForm);

		var sendObj = {};
		sendObj.thisForm = thisForm;
		sendObj.formBtn = $(this).data('button') || $('button[type="submit"]', thisForm).get(0).name;

		sendObj.successCallback = function(data) {
			onepage.clearFormError(thisForm);
			progress.hide();

			var billingError = thisForm.find(".bf-billing-error");
			
			if (billingError.is(":visible")) {
				data.success = false;
			}

			if (data.success == true) {
				if (data.apToken) {
					afterpay.init(data.apURL, data.apToken);	
				} else {
					onepage.refreshBilling();
				}
			} else {
				if (data.errorMsg) {
					var errorLocation = thisForm.children(".billing-buttons");
					onepage.showFormError(errorLocation, data.errorMsg);
				}
			}
		};

		if ($('#isInternational').val() == 'true') {
			$('.bf-billing-error').hide();
			// Let's look and see which option is selected.

			if (paymentMethod == 'CREDIT_CARD') {
				if ($('#dwfrm_billing').valid()) {
					e.preventDefault();
					// walk through our options and see which one is selected.
					var url = '';
					var bfType = $(this).attr('bf_type');
					var selectedOption = $('#dwfrm_billing_paymentMethods_creditCard_type option:selected').val();
					$('#dwfrm_billing_paymentMethods_creditCard_type option').each(function(i) {
						if ($(this).val() == selectedOption) {
							url = $(this).attr('bf_url');
						} 
					});

					// build our payload
					var payload = {};
					payload.cardNumber = $('#dwfrm_billing_paymentMethods_creditCard_number').val();
					payload.expirationMonth = parseInt($('#dwfrm_billing_paymentMethods_creditCard_month').val());
					payload.expirationYear = parseInt($('#dwfrm_billing_paymentMethods_creditCard_year').val());
					payload.cvn = $('#dwfrm_billing_paymentMethods_creditCard_cvn').val();
					
					$.ajax({		 
						url: url,
						data: JSON.stringify(payload),
						contentType: 'application/json',
						type:'POST',
						dataType: 'json'
					}).done(function(data) {
						$('#bfToken').val(data.token);
						$('#bfTokenType').val($('.input-radio:checked').val());

						progress.show($("#onepage_billing_content"));
						onepage.loadJSON(sendObj);
					}).error(function(data) {
						$('.bf-billing-error').show();
					});
				}
			}
		} else {
			if (thisForm.valid()) {
				progress.show($("#onepage_billing_content"));
				onepage.loadJSON(sendObj);
			}
		}
	}
}

var opcPrevious = function(e){
	e.preventDefault();
	onepage.updateUrl('shipping','billing');
	$("#edit_billing").addClass("visible");
}

var opcRemoveCoupon = function(e){
	e.preventDefault();
	onepage.removeCoupon($(this).attr("data-code"),$('#dwfrm_billing'));
}

var opcShippingBilling = function(e){
	var fieldset = $("#onepage_billing_fieldset");

	$("#dwfrm_billing_billingAddress_addressFields_firstName").val(shippingFields.firstName != '' ? shippingFields.firstName.replace(/&quot;/g,'"') : '');
	$("#dwfrm_billing_billingAddress_addressFields_lastName").val(shippingFields.lastName != '' ? shippingFields.lastName.replace(/&quot;/g,'"') : '');
	$("#dwfrm_billing_billingAddress_addressFields_address1").val(shippingFields.address1 != '' ? shippingFields.address1.replace(/&quot;/g,'"') : '');
	$("#dwfrm_billing_billingAddress_addressFields_address2").val(shippingFields.address2 != '' ? shippingFields.address2.replace(/&quot;/g,'"') : '');
	$("#dwfrm_billing_billingAddress_addressFields_city").val(shippingFields.city != '' ? shippingFields.city.replace(/&quot;/g,'"') : '');

	$("#dwfrm_billing_billingAddress_addressFields_states_state").val(shippingFields.state != '' ? shippingFields.state.replace(/&quot;/g,'"') : '');

	$("#dwfrm_billing_billingAddress_addressFields_states_stateCA").val(shippingFields.stateCA != '' ? shippingFields.stateCA.replace(/&quot;/g,'"') : '');
	$("#dwfrm_billing_billingAddress_addressFields_region").val(shippingFields.region != '' ? shippingFields.region.replace(/&quot;/g,'"') : '');
	$("#dwfrm_billing_billingAddress_addressFields_postal").val(shippingFields.postal != '' ? shippingFields.postal.replace(/&quot;/g,'"') : '');

	$("#dwfrm_billing_billingAddress_addressFields_country").val(shippingFields.country != '' ? shippingFields.country.replace(/&quot;/g,'"') : '');

	$("#dwfrm_billing_billingAddress_addressFields_phone").val(shippingFields.phone != '' ? shippingFields.phone.replace(/&quot;/g,'"') : '');

	if ($(this).is(":checked")) {
		$(".opcField",fieldset).addClass("opcHidden");
		$(".email",fieldset).addClass("opcWider");
	} else {
		$(".opcField",fieldset).removeClass("opcHidden");
		$(".email",fieldset).removeClass("opcWider");
	}
}

/**
 * @function
 * @description Billing Functions
 */
exports.init = function () {
	address.init();
	billing.init();

	$("#dwfrm_billing_couponCode").val('');

	$("#onepage_billing").off("click", "#shipAsBill", opcShippingBilling);
	$("#onepage_billing").on("click", "#shipAsBill", opcShippingBilling);

	$("#onepage_billing").off("click", ".couponCodeLink", opcRemoveCoupon);
	$("#onepage_billing").on("click", ".couponCodeLink", opcRemoveCoupon);

	$("#onepage_billing").off('click', 'button[type="submit"]', buttonClick);
	$("#onepage_billing").on('click', 'button[type="submit"]', buttonClick);

	if ($('#isInternational').val() == 'true') {
		$("#dwfrm_billing").unbind("submit");
	}
	$("#onepage_billing").off("submit", "#dwfrm_billing", opcFormSubmit);
	$("#onepage_billing").on("submit", "#dwfrm_billing", opcFormSubmit);

	$("#onepage_billing").off("click", ".billing-prev-button", opcPrevious);
	$("#onepage_billing").on("click", ".billing-prev-button", opcPrevious);
};

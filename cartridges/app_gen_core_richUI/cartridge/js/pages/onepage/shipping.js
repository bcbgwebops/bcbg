'use strict';

var progress = require('../../progress'),
	util = require('../../util'),
	dialog = require('../../dialog'),
	validator = require('../../validator'),
	onepage = require('./onepage'),
	address = require('../checkout/address'),
	shipping = require('../checkout/shipping');

var buttonClick = function(e){
	$('#dwfrm_singleshipping_shippingAddress').data('button', this.name);
}

var opcFormSubmit = function(e){
	e.preventDefault();

	var thisForm = $(this);

	$("#dwfrm_singleshipping_shippingAddress_addressFields_country").prop('disabled', false);

	onepage.clearFormError(thisForm);

	var sendObj = {};
	sendObj.thisForm = thisForm;
	sendObj.formBtn = $(this).data('button') || $('button[type="submit"]', thisForm).get(0).name;

	if (thisForm.valid()) {
		sendObj.successCallback = function(data) {
			onepage.clearFormError(thisForm);
			progress.hide();
			
			if (data.success == true) {
				onepage.refreshShipping();
			} else {
				onepage.showFormError(thisForm, data.errorMsg);
			}
		};

		progress.show($("#onepage_shipping_content"));
		onepage.loadJSON(sendObj);
	}
}

var opcPrevious = function(e){
	e.preventDefault();
	onepage.updateUrl('account','shipping');
	$("#edit_shipping").addClass("visible");
}

var changeClick = function(e){
	e.preventDefault();
	var url = $(this).attr('href');

	dialog.open({
		url: url,
		options: {
			width: 620,
			dialogClass: "no-close",
		}
	});
}

/**
 * @function
 * @description Shipping Functions
 */
exports.init = function () {
	address.init();
	shipping.init();

	$("#onepage_shipping").off('click', '#changeFromShipping a', changeClick);
	$("#onepage_shipping").on('click', '#changeFromShipping a', changeClick);

	$("#onepage_shipping").off('click', 'button[type="submit"]', buttonClick);
	$("#onepage_shipping").on('click', 'button[type="submit"]', buttonClick);

	$("#onepage_shipping").off("submit","#dwfrm_singleshipping_shippingAddress", opcFormSubmit);
	$("#onepage_shipping").on("submit","#dwfrm_singleshipping_shippingAddress", opcFormSubmit);

	$("#onepage_shipping").off("click",".shipping-prev-button", opcPrevious);
	$("#onepage_shipping").on("click",".shipping-prev-button", opcPrevious);

	$("#shipping-method-list").on('click', '.form-row', function(){
		$("#shipping-method-list .form-row").removeClass("active");
		$("#shipping-method-list .description").removeClass("active");
		$(this).addClass("active").children("input").prop("checked", true);
	});
};

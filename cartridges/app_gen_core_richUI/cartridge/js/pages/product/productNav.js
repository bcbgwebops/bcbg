'use strict';

var ajax = require('../../ajax'),
    util = require('../../util');

/**
 * @description loads product's navigation
 **/
module.exports = function () {
    var $pidInput = $('.pdpForm input[name="pid"]').last(),
        $navContainer = $('#product-nav-container');
    // if no hash exists, or no pid exists, or nav container does not exist, return
    if (window.location.hash.length <= 1 || $pidInput.length === 0 || $navContainer.length === 0) {
        return;
    }

    // Remove previous pid params from hash
    var params = window.location.hash.substr(1).split('&');
    for (var i = 0; i < params.length; i++) {
        if (params[i].indexOf('pid=') != -1) {
            params.splice(i, 1);
        }
    }

    var pid = $pidInput.val(),
        hash = params.join('&'),
        url = util.appendParamToURL(Urls.productNav + '?' + hash, 'pid', pid);

    ajax.load({
        url: url,
        target: $navContainer
    });
};

'use strict';
var dialog = require('../../dialog'),
	util = require('../../util');

/**
 * @description Replaces the images in the image container, for eg. when a different color was clicked.
 */
var replaceImages = function () {
	var $newImages = $('#update-images'),
		$imageContainer = $('#pdpMain .product-image-container-inner');
	if ($newImages.length === 0) { return; }

	$imageContainer.html($newImages.html());
	$newImages.remove();
};

/* @module image
 * @description this module handles the primary image viewer on PDP
 **/

/**
 * @description by default, this function sets up zoom and event handler for thumbnail click
 **/
module.exports = function () {
	// handle product thumbnail click event
	$('#pdpMain').on('click', '.productthumbnail', function () {
		// switch indicator
		if (!$(this).closest('.thumb').hasClass('selected')) {
	        var utag_data = $('.add-wishlist').data('utag');
	        utag_data['event_type'] = 'product_alt_image';
	        utag.link(utag_data);
	    }
		$(this).closest('.product-thumbnails').find('.thumb.selected').removeClass('selected');
		$(this).closest('.thumb').addClass('selected');
	});	
};

module.exports.replaceImages = replaceImages;

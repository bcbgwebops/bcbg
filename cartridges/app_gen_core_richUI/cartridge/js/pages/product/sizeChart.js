'use strict';

var dialog = require('../../dialog');

function initSizeChart() {

    $(document).on('click', '.size-chart', function (e) {
        e.preventDefault();
        dialog.open({
            url: Urls.getSizeChart,
        });
    });
};

module.exports = initSizeChart;
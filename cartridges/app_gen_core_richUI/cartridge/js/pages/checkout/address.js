'use strict';

var util = require('../../util');
var shipping = require('./shipping');

/**
 * @function
 * @description Selects the first address from the list of addresses
 */
exports.init = function () {
	$('.countryValue').on('change', function () {
		var c = $('.countryValue').val().substring(0,2);
		$('.state,.state select').removeClass('required');
		$('.stateCA,.stateCA select').removeClass('required');
		$('.region,.region input').removeClass('required');
		if (c == 'CA') {
			$('.stateCA').show();
			$('.stateCA,.stateCA select').addClass('required');
			$('.region').hide();
			$('.state').hide();
		} else if (c == 'US') {
			$('.state').show();
			$('.state,.state select').addClass('required');
			$('.stateCA').hide();
			$('.region').hide();
		} else {
			//$('.region,.region input').addClass('required');
			$('.region').show();
			$('.stateCA').hide();
			$('.state').hide();
		}
	});

	var $form = $('.address');
	// select address from list
	$('select[name$="_addressList"]', $form).on('change', function () {
		var selected = $(this).children(':selected').first();
		var selectedAddress = $(selected).data('address');
		if (!selectedAddress) { return; }
		util.fillAddressFields(selectedAddress, $form);
		shipping.updateShippingMethodList();
		// re-validate the form
		//$form.validate().form();
	});

	// update state options in case the country changes
	$('select[id$="_country"]', $form).on('change', function () {
		util.updateStateOptions($form);
	});

	var $select = $('select', $form);
	$select.each(function() {
		if($(this).children(':selected').val() != ''){
	    	$(this).addClass("nonDefault");
	    }
	});
	$select.on('change', function(ev) {
	    $(this).removeClass('nonDefault');
	    if($(this).children(':selected').val() != ''){
	    	$(this).addClass("nonDefault");
	    }
	});
};

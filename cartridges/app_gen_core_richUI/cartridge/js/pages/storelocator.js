'use strict';
var dialog = require('../dialog'),
    ajax = require('../ajax'),
    util = require('../util');

exports.init = function () {
    $(".back-storelocator").click(function(e) {
        var storeResults = $("#storeresults");
        var storeforms = $("#storeforms");
        var storeData = $("#storeresults tbody");

        storeforms.show();
        storeResults.hide();
        $("tr.storeresult",storeData).remove();
    });

    ajax.load({
        url: "/on/demandware.store/Sites-BCBG-Site/default/Stores-countryJSON",
        callback: function (data) {
            $("#dwfrm_storelocator_country").children(":gt(0)").remove();
            $("#dwfrm_storelocator_country").append(data);
        }
    });

    ajax.load({
        url: "/on/demandware.store/Sites-BCBG-Site/default/Stores-stateJSON",
        callback: function (data) {
            $("#dwfrm_storelocator_address_states_stateUSCA").children(":gt(0)").remove();
            $("#dwfrm_storelocator_address_states_stateUSCA").append(data);
            $("#dwfrm_storelocator_address_states_stateUSCA").children().each(function(i){
                if ($(this).text() == "" || $(this).text() == "x") {
                    $(this).remove();
                }
            });
        }
    });

    $("#intSubmit").on('click', function(e) {
        e.preventDefault();

        var loading = $(this).siblings(".loader-indicator");

        var country = $("#dwfrm_storelocator_country").val();

        if (country.length > 0) {
            var countryURL = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";

            var url = util.appendParamToURL(countryURL, 'brand', storebrand);
            url = util.appendParamToURL(url, 'country', country);
            url = util.appendParamToURL(url, 'type', 'international');

            loading.show();
            ajax.load({
                url: url,
                callback: function (data) {
                    storeData(data);
                    loading.hide();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            });
        }
    });

    $("#usCaStateSubmit").click(function(e) {
        e.preventDefault();

        var loading = $(this).siblings(".loader-indicator");

        var stateProvStr = $("#dwfrm_storelocator_address_states_stateUSCA").val();
        var caArray = ["AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"];

        if (stateProvStr.length > 0) {
            var stateProvinceURL = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";

            var url = util.appendParamToURL(stateProvinceURL, 'brand', storebrand);

            if (caArray.indexOf(stateProvStr) > -1) {
                url = util.appendParamToURL(url, 'country', "CANADA");
            } else {
                url = util.appendParamToURL(url, 'country', "USA");
            }

            url = util.appendParamToURL(url, 'state', stateProvStr);
            url = util.appendParamToURL(url, 'type', 'state');

            loading.show();
            ajax.load({
                url: url,
                callback: function (data) {
                    storeData(data);
                    loading.hide();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            });
        }
    });

    $("#usCaZipSubmit").click(function(e) {
        e.preventDefault();

        var loading = $(this).siblings(".loader-indicator");

        var radiusStr = $("#dwfrm_storelocator_maxdistance").val();
        var zipStr = $("#dwfrm_storelocator_postalCode").val();

        if (zipStr.length == 6) {
            var part1 = zipStr.substr(0, 3);
            var part2 = zipStr.substr(3, 3);
            zipStr = part1 + " " + part2;
        }

        if (radiusStr.length > 0 && zipStr.length > 0) {
            var zipUrl = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";

            var url = util.appendParamToURL(zipUrl, 'brand', storebrand);

            var reg = new RegExp('^[0-9]+$');

            if (reg.test(zipStr)) {
                url = util.appendParamToURL(url, 'country', "USA");
            } else {
                url = util.appendParamToURL(url, 'country', "CANADA");
            }

            url = util.appendParamToURL(url, 'radius', radiusStr);
            url = util.appendParamToURL(url, 'zip', zipStr);

            url = util.appendParamToURL(url, 'type', 'zip');


            loading.show();
            ajax.load({
                url: url,
                callback: function (data) {
                    storeData(data);
                    loading.hide();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }
            });
        }
    });


    function storeData(data) {
        var obj = jQuery.parseJSON(data);

        var storeT = $("#storetemplate");
        var storeResults = $("#storeresults");
        var storeData = $("#storeresults tbody");
        var storeforms = $("#storeforms");

        var foundData = false;

        $.each(obj, function(key,value) {
             var storeTR = storeT.clone();
             storeTR.attr("id","");

             foundData = true;

             var storename = value.storename;
             if (value.storename == "null" || value.storename == null) {
                 storename = "";
             }

             var storephone = value.phone;
             if (value.phone == "null" || value.phone == null) {
                 storephone = "";
             }

             var storefax = value.phone2;
             if (value.phone2 == "null" || value.phone2 == "" || value.phone2 == null) {
                 storefax = "";
             } else {
                 storefax = " | " + storefax;
             }
             var storeaddress = value.address;
             if (value.address == "null" || value.address == null) {
                 storeaddress = "";
             }
             var storecity = value.city;
             if (value.city == "null" || value.city == null) {
                 storecity = "";
             }
             var storestate = value.state;
             if (value.state == "null" || value.state == null) {
                 storestate = "";
             }
             var storezip = value.zip;
             if (value.zip == "null" || value.zip == null) {
                 storezip = "";
             }
             var storecountry = value.country;
             if (value.country == "null" || value.country == null) {
                 storecountry = "";
             }

             var qAttr = "";

             if (storeaddress != "") qAttr += storeaddress + ", ";
             if (storecity != "") qAttr += storecity + ", ";
             if (storezip != "") qAttr += storezip + ", ";
             if (storestate != "") qAttr += storestate + ", ";
             if (storecountry != "") qAttr += storecountry;

             //store-map
             var storelink = "http://maps.google.com/maps?hl=en&f=q&q=" + encodeURI(qAttr);
             var storemap = "<a class='google-map' href='"+ storelink +"' target='_blank'>Map</a>";

             if ((storecity != "" && storecity != "null") && (storestate != "" && storestate != "null")) {
                 storecity = storecity + ", ";
             }

             $(".storename", storeTR).html(storename);
             $(".storephone", storeTR).html(storephone);
             $(".storefax", storeTR).html(storefax);

             $(".storeaddress", storeTR).html(storeaddress);
             $(".storecity", storeTR).html(storecity);
             $(".storestate", storeTR).html(storestate);
             $(".storezip", storeTR).html(storezip);
             $(".storecountry", storeTR).html(storecountry);

             $(".store-map", storeTR).html(storemap);

             storeTR.attr("style","").addClass("storeresult");
             storeData.append(storeTR);
        });

        $(".store-locator-no-results").hide();

        if (foundData) {
            storeResults.show();
            storeforms.hide();
        } else {
            $(".store-locator-no-results").show();
        }
    }
};
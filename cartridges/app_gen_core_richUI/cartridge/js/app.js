/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var ajax =  require('./ajax'),
	countries = require('./countries'),
	dialog = require('./dialog'),
	international = require('./international'),
	minicart = require('./minicart'),
	multicurrency = require('./multicurrency'),
	page = require('./page'),
	rating = require('./rating'),
	responsys = require('./responsys'),
	searchplaceholder = require('./searchplaceholder'),
	searchsuggest = require('./searchsuggest'),
	searchsuggestbeta = require('./searchsuggest-beta'),
	tooltip = require('./tooltip'),
	util = require('./util'),
	validator = require('./validator'),
	quickview = require('./quickview');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
	var s = document.createElement('script');
	s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
	s.setAttribute('type', 'text/javascript');
	document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
//require('picturefill');

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function initializeEvents() {
	var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

	//slider for header-banner (promos in header)
	$('.promo-slides').slick({
	  dots: false,
	  infinite: true,
	  speed: 500,
	  fade: true,
	  arrows: false,
	  autoplay: true,
	  cssEase: 'linear'
	});

	$('input.phone').blur(function(){
		var value = $(this).val();
		$(this).val(value.replace(/\D/g,''));
	});

	var cookiepopup = $("#cookiepopup");

	if (cookiepopup.length > 0) {
		$("#cookieclose").click(function(e){
			e.preventDefault();
			
			var cookiepopup = $("#cookiepopup");

			cookiepopup.fadeOut();

			createCookie('bcbg_cookie_policy', '1', 365);
		});

		$("a",cookiepopup).click(function(e){
			var cookiepopup = $("#cookiepopup");

			cookiepopup.fadeOut();

			createCookie('bcbg_cookie_policy', '1', 365);
		});
	}

	$('body')
		.on('keydown', 'textarea[data-character-limit]', function (e) {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length;

				if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
					e.preventDefault();
				}
		})
		.on('change keyup mouseup', 'textarea[data-character-limit]', function () {
			var text = $.trim($(this).val()),
				charsLimit = $(this).data('character-limit'),
				charsUsed = text.length,
				charsRemain = charsLimit - charsUsed;

			if (charsRemain < 0) {
				$(this).val(text.slice(0, charsRemain));
				charsRemain = 0;
			}

			$(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
        });

	//count the number of columns each L1 dropdown will have to determine its class to determine the width

    $('.level-1 > li').each(function () {

        var columnCount = $(this).find("ul").length;

        $(this).addClass("columns-" + columnCount);

    });

	// 'Back to Top' button
	$(window).scroll(function() {
		var a=$(window).scrollTop();
		a > 200 ? $("#backtotop").fadeIn() : $("#backtotop").fadeOut();
	});

	$("#backtotop").on("click", function() {
		$("html,body").animate(
			{scrollTop:0},
			"slow"
		);
	});

	$(document).on('click', ".quickViewOpenWithColor", function (e) {
        e.preventDefault();
		var pid = $(this).attr('product');
		var productHref = $(this).attr('href');

		ajax.getJson({
			url: util.appendParamsToUrl(Urls.getAvailability, {
				pid: pid,
				Quantity: 1
			}),
			hideParseAlert: true,
			callback: function(data){
				if (data != null && data.levels.NOT_AVAILABLE == 0) {
					quickview.show({
						callback: function() {
							window.setTimeout(function () {
								MagicZoom.start();
							}, 1000);
						},
						url: productHref,
						source: 'quickview'
					});
				}
				else{
					alert("Item not available.");
				}
			}
		});
    });

	$(document).on('click', ".quickViewOpen", function (e) {
		e.preventDefault();

		var pid = $(this).attr('product');
		var url = util.appendParamsToUrl(Urls.getProductUrl, {pid: pid});

		ajax.getJson({
			url: util.appendParamsToUrl(Urls.getAvailability, {
				pid: pid,
				Quantity: 1
			}),
			hideParseAlert: true,
			callback: function(data){
				if (data != null && data.levels.NOT_AVAILABLE == 0) {
					quickview.show({
						callback: function() {
							window.setTimeout(function () {
								MagicZoom.start();
							}, 1000);
						},
						url: url,
						source: 'quickview'
					});
				}
				else{
					alert("Item not available.");
				}
			}
		});
	});

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
	var $searchContainer = $('.header-search');
	if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
		searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
	} else {
		searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
	}

	// print handler
	$('.print-page').on('click', function () {
		window.print();
		return false;
	});

	//This is for guest services toggle content
	$(function() {
		$('.guest_services_content .togglebox').hide();
	    $('.guest_services_content  h2').click(function () {
	    	$('.guest_services_content  h2 > img').attr('src', 'http://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif');
	    	$(this).children('img').toggleClass('arrow_image below_arrow');
	        $(this).next('div.togglebox').slideToggle('slow');
	        $(this).parent().siblings().children().next().slideUp();

	        return false;
	    });
	});

	// subscribe email box
	var $subscribeEmail = $('.subscribe-email');
	if ($subscribeEmail.length > 0)	{
		$subscribeEmail.focus(function () {
			var val = $(this.val());
			if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
				return; // do not animate when contains non-default value
			}

			$(this).animate({color: '#999999'}, 500, 'linear', function () {
				$(this).val('').css('color', '#333333');
			});
		}).blur(function () {
			var val = $.trim($(this.val()));
			if (val.length > 0) {
				return; // do not animate when contains value
			}
			$(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
				.css('color', '#999999')
				.animate({color: '#333333'}, 500, 'linear');
		});
	}

	$('.privacy-policy').on('click', function (e) {
		e.preventDefault();
		dialog.open({
			url: $(e.target).attr('href'),
			options: {
				height: 600
			}
		});
	});

	// main menu toggle
	$('.menu-toggle, .close-nav').on('click', function () {
		$('body').toggleClass('menu-active');
		$('.menu-toggle .hamburger').toggleClass('is-active');
	});
	$('.navigation-mobile .menu-category li').on('click', function (e) {
		e.preventDefault();
		if ($(this).has('ul').length > 0) {
			$('.navigation-mobile .menu-category li').removeClass('active');
			$(this).addClass('active');
			return;
		} else {
			window.location = $(this).children('a').attr('href');
		}
	});
	$("body").on('click', '.menu-active .level-1 > li > a', function(e){
		e.preventDefault();
		if($(this).parent().find("li").length > 0){
			$(this).parent().toggleClass("active");
		} else{
			location.href = $(this).attr('href');
		}
	});
	$('.user-account').on('click', function (e) {
		e.preventDefault();
		$(this).parent('.user-info').toggleClass('active');
    });
    $('.searchIcon').on('click', function (e) {
        e.preventDefault();
        $('.header-search').toggleClass('search-visible');
    });

	$(document).on('submit', '#dwfrm_login', function(e) {
		if ($('#dwfrm_login').valid() == false) {
			e.preventDefault();
		} else {
			$.ajax({
				url: Urls.tealiumGetCustomerID,
				data: "email=" + $(this).find(".email-input").val(),
				contentType: 'application/json',
				dataType: 'json'
			}).done(function(data) {
				var utag_data = new Object();
				utag_data['customer_email'] = $("#dwfrm_login").find(".email-input").val();
				utag_data['customer_id'] = data.ID;
				utag_data['event_type'] = 'user_login';
				utag.link(utag_data);
			}).error(function(data) {
			});
		}
	});

	$('.countryValue').on('change', function () {
		var c = $('.countryValue').val().substring(0,2);
		$('.state,.state select').removeClass('required');
		$('.stateCA,.stateCA select').removeClass('required');
		$('.region,.region input').removeClass('required');
		if (c == 'CA') {
			$('.stateCA').show();
			$('.stateCA,.stateCA select').addClass('required');
			$('.region').hide();
			$('.state').hide();
		} else if (c == 'US') {
			$('.state').show();
			$('.state,.state select').addClass('required');
			$('.stateCA').hide();
			$('.region').hide();
		} else {
			$('.region,.region select').addClass('required');
			$('.region').show();
			$('.stateCA').hide();
			$('.state').hide();
		}
	});
	
	$(document).on('click', '.add-wishlist', function(e) {
    	var utag_data = $(this).data("utag");
		utag_data['event_type'] = 'wishlist_add';
		utag.link(utag_data);
	});
	
	$('footer .social a').on('click', function() {
	    utag_data['event_type'] = 'social_share';
	    utag.link(utag_data);
	});
	
	$(document).on('submit', '#send-to-friend-form', function() {
		if ($(this).valid()) {
		    utag_data['event_type'] = 'show_your_friend';
	        utag.link(utag_data);
		}
		return true;
	});
	
	$(window).load(function() {
	    if ($('#utag_recently_registered').val()) {
            var utag_data = {"customer_id" : $('#utag_customer_id').val(), "customer_email" : $('#utag_customer_email').val(), "event_type" : "user_register"};
            utag.link(utag_data);
        }
	    if ($('#utag_recently_login').val()) {
            var utag_data = {"customer_id" : $('#utag_customer_id').val(), "customer_email" : $('#utag_customer_email').val(), "event_type" : "user_login"};
            utag.link(utag_data);
        }
    });

    $(".header-banner .modal").click(function(e) {
		e.preventDefault();
		var id = $(this).attr("href");
		
		$(id).dialog({
			maxWidth: 670,
			autoOpen: false,
			modal: true
		});
		$(id).dialog('open');
	});

	$(document).on('click', '.afterpay-link', function (e) {
		e.preventDefault();
		dialog.open({
            url: $(e.target).attr('href'),
            options: {
                dialogClass: 'afterpay-dialog'
            }
		});
    });
	
	//*************************Responsive Manipulation*************************//
	/*$(window).resize(function() {
		if(screen.width < 768) {
			// Need to move stuff around in the cart
			$('#cart-table .cart-row').each(function(idx, value) {
				// move quantity
				var appendto = $('.item-total', value); 
				$('.item-quantity', value).remove().insertAfter(appendto);
				// move cart actions
				$('.item-total .cart-action-menu', value).remove().insertAfter($('.color', value));
			});
			if ($('.step5-mobile').length > 0) {
				$('.step5-mobile').html('CONFIRM');
			}
		} else { 
			// move cart stuff back
			$('#cart-table .cart-row').each(function(idx, value) {
				// move quantity
				$('.item-quantity', value).remove().insertAfter($('.size', value));
				// move cart actions
				$('.cart-action-menu', value).remove().appendTo($('.item-total', value));
			});		
		}
	}).resize();*/
	//*************************************************************************//

	$('.mobile-sign-up .close').click(function (e) {
		e.preventDefault();
		$('.mobile-sign-up').slideUp();
	});
}

	
/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
	// add class to html for css targeting
	$('html').addClass('js');
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$('html').addClass('infinite-scroll');
	}
	// load js specific styles
	util.limitCharacters();
}

var pages = {
	account: require('./pages/account'),
	cart: require('./pages/cart'),
	checkout: require('./pages/checkout'),
	compare: require('./pages/compare'),
	product: require('./pages/product'),
	registry: require('./pages/registry'),
	search: require('./pages/search'),
	storefront: require('./pages/storefront'),
	wishlist: require('./pages/wishlist'),
	storelocator: require('./pages/storelocator'),
	orderconfirmation: require('./pages/orderconfirmation'),
	onepage: require('./pages/onepage')
};

var app = {
	init: function () {
		if (document.cookie.length === 0) {
			$('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
		}
		initializeDom();
		initializeEvents();

		// init specific global components
		countries.init();
		tooltip.init();
		minicart.init();
		validator.init();
		rating.init();
		searchplaceholder.init();
		multicurrency.init();
		international.init();
		responsys.init();

		// execute page specific initializations
		$.extend(page, window.pageContext);
		
		var ns = page.ns;
		if (ns && pages[ns] && pages[ns].init) {
			pages[ns].init();
		}
	}
};

// general extension functions
(function () {
	String.format = function () {
		var s = arguments[0];
		var i, len = arguments.length - 1;
		for (i = 0; i < len; i++) {
			var reg = new RegExp('\\{' + i + '\\}', 'gm');
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
})();

// initialize app
$(document).ready(function () {
	app.init();
});
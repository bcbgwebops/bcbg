/**
* 	Saves the validated values of the translationconfig form.
*
*   @input TransForm : Object
*   
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.object );

function execute( args : PipelineDictionary ) : Number
{
	var obj = args.TransForm;
	var serviceURL : String = obj.translationEndpointURL.htmlValue;
	var pdURL: String = "";

	// save configuration attributes
	// TODO: what about direct check against defined PD URLS, see getConfigs.ds -> getFreshData()
	try {
		if (serviceURL != null) {
		var idx = serviceURL.indexOf("/PD/");
		if (idx >= 0) {
			pdURL = serviceURL.substring(0, idx+4);
		}else{
			//check for /PD only endings too
			idx = serviceURL.indexOf("/PD");
			pdURL = serviceURL.substring(0, idx+3);
		}
		if(pdURL.length > 0){
			Site.getCurrent().getPreferences().getCustom()['translation_endpointURL'] = pdURL;
		}
		Site.getCurrent().getPreferences().getCustom()['translation_username'] = obj.translationUsername.htmlValue;
		Site.getCurrent().getPreferences().getCustom()['translation_password'] = obj.translationPassword.htmlValue;
	}

	}catch(e){
		var errMsg = e.message;
		Logger.error("[saveConfigs.ds] Error: save site preference", e.message);
	}
	var sourceLang : String = obj.translationSourceLanguage.htmlValue;
	var targetLang : String = obj.translationLanguage.htmlValue;
	if ((sourceLang != null) && (sourceLang.length > 0)) { 
		var sourceLangs : Array = sourceLang.split(" (");
		if ((sourceLangs != null) && (sourceLangs.length == 2)) {
			sourceLang = sourceLangs[0];
		}
	}
	
	if ((targetLang != null) && (targetLang.length > 0)) { 
		var targetLangs : Array = targetLang.split(" (");
		if ((targetLangs != null) && (targetLangs.length == 2)) {
			targetLang = targetLangs[0];
		}
	}
	
	Site.getCurrent().getPreferences().getCustom()['translation_language'] = targetLang;
	Site.getCurrent().getPreferences().getCustom()['translation_source_language'] = sourceLang;
	if (obj.translationCatalogEnabled.htmlValue == "true" ){
		Site.getCurrent().getPreferences().getCustom()['translation_catalog_enabled'] = true;
		Site.getCurrent().getPreferences().getCustom()['translation_catalog_shortcode'] = obj.translationCatalogShortcode.htmlValue;
	} else {
		Site.getCurrent().getPreferences().getCustom()['translation_catalog_enabled'] = false; 
		Site.getCurrent().getPreferences().getCustom()['translation_catalog_shortcode'] = "";
	}

	Site.getCurrent().getPreferences().getCustom()['translation_export_catalog_metadata'] = (obj.translationExportCatalogMetadata.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_export_recommendation'] = (obj.translationExportRecommendation.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_export_variation'] = (obj.translationExportVariation.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_export_display_name_option'] = (obj.translationExportDisplayNameOption.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_export_product_online_option'] = obj.translationExportProductOnlineOption.htmlValue;
	Site.getCurrent().getPreferences().getCustom()['translation_export_preprocess_product'] = (obj.translationExportPreprocessProduct.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_export_searchRefinement'] = (obj.translationExportSearchRefinement.htmlValue == "true");
	
	Site.getCurrent().getPreferences().getCustom()['translation_file_nodes_number'] = Number(obj.translationCatalogFileNodesNumber.value);
	Site.getCurrent().getPreferences().getCustom()['translation_file_size'] = Number(obj.translationCatalogFileSize.value);
	Site.getCurrent().getPreferences().getCustom()['translation_product_is_export_ref'] = (obj.translationProductIsExportRef.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_product_ref_id'] = obj.translationProductRefId.htmlValue;

	if( obj.translationContentEnabled.htmlValue == "true" ){
		Site.getCurrent().getPreferences().getCustom()['translation_content_enabled'] = true;
		Site.getCurrent().getPreferences().getCustom()['translation_content_shortcode'] = obj.translationContentShortcode.htmlValue;
	} else {
		Site.getCurrent().getPreferences().getCustom()['translation_content_enabled'] = false;
		Site.getCurrent().getPreferences().getCustom()['translation_content_shortcode'] = "";
	}
	Site.getCurrent().getPreferences().getCustom()['translation_export_content_metadata'] = (obj.translationExportContentMetadata.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_export_content_online_option'] = obj.translationExportContentOnlineOption.htmlValue;
	Site.getCurrent().getPreferences().getCustom()['translation_content_is_export_ref'] = (obj.translationContentIsExportRef.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_content_ref_id'] = obj.translationContentRefId.htmlValue;
	
	
	Site.getCurrent().getPreferences().getCustom()['translation_content_file_nodes_number'] = Number(obj.translationContentFileNodesNumber.value);
	Site.getCurrent().getPreferences().getCustom()['translation_content_file_size'] = Number(obj.translationContentFileSize.value);
	
	if( obj.translationPromotionEnabled.htmlValue == "true" ){
		Site.getCurrent().getPreferences().getCustom()['translation_promotion_enabled'] = true;
		Site.getCurrent().getPreferences().getCustom()['translation_promotion_shortcode'] = obj.translationPromotionShortcode.htmlValue;
	} else {
		Site.getCurrent().getPreferences().getCustom()['translation_promotion_enabled'] = false;
		Site.getCurrent().getPreferences().getCustom()['translation_promotion_shortcode'] = "";
	}
	Site.getCurrent().getPreferences().getCustom()['translation_export_promotion_metadata'] = (obj.translationExportPromotionMetadata.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_slot_is_export_ref']= (obj.translationSlotIsExportRef.htmlValue == "true");
	Site.getCurrent().getPreferences().getCustom()['translation_slot_ref_id'] = obj.translationSlotRefId.htmlValue;

	if( obj.translationShippingEnabled.htmlValue == "true" ){
		Site.getCurrent().getPreferences().getCustom()['translation_shipping_enabled'] = true;
		Site.getCurrent().getPreferences().getCustom()['translation_shipping_shortcode'] = obj.translationShippingShortcode.htmlValue;
	} else {
		Site.getCurrent().getPreferences().getCustom()['translation_shipping_enabled'] = false;
		Site.getCurrent().getPreferences().getCustom()['translation_shipping_shortcode'] = "";
	}
	Site.getCurrent().getPreferences().getCustom()['translation_is_allow_import_disabled_locale'] = (obj.translationIsAllowImportDisabledLocale.htmlValue == "true");
	
	Site.getCurrent().getPreferences().getCustom()['translation_error_notification_email'] = obj.translationErrorNotificationEmail.htmlValue;
	Site.getCurrent().getPreferences().getCustom()['translation_submission_prefix'] = obj.translationSubmissionPrefix.htmlValue;

    return PIPELET_NEXT;
}

/**
* SystemMetadataExporterModel.ds
*
*	This is designed to read from full contend exported file and write files for PD
*
*/

importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.io );
importPackage( dw.order );
importPackage( dw.object );


importScript( "int_translation:lib/libUtils.ds" );
importScript( "int_translation:lib/libTranslation.ds" );
importScript( "int_translation:foldersStructure/FoldersStructureModel.ds" );

function systemMetadataExporter() { 
	
	var TranslationHelper = getTranslationHelper();
	var fileSizeLimit : Bytes = TranslationHelper.getFileMaxSize();
	var isReset : Boolean;
		
	var sourceFileName : String;
	var sourceFile : File;
	var targetFileName : String;
	var targetFile : File;	
	var targetFileCounter : Number = 1;
	var targetDir : File = new File(File.IMPEX+getFoldersStructure()['export_translateRequest']);
	
	var errorCode : Number = PIPELET_NEXT;
	var errorMsg : String = "";
	
	var xmlFileReader : FileReader;
	var xmlReader : XMLStreamReader;
	var ns : Namespace = new Namespace("http://www.demandware.com/xml/impex/metadata/2006-10-31");
	
	var _filecontent : XML = new XML();
	var _systemMetadatas : XML = new XML();
		
	var _systemMetadata : XML = new XML();	
	
	var _currentSystemMetadataSource : XML = new XML();
	
	var _oldSystemMetadatas : XML = new XML();
		
	var jobTgtLangPD : String;
	var jobTargetLocalePair: String;
	var jobSrcLangPD : String;
	var jobTargetLocaleDW : String;
	var dwSrcLang : String = getSourceLocale(true);
	var TP_LANGUAGE_PREFIX : String = "TP_DW_PREFIX_";
	var _projectType : String;
	
	// constructor
	this.create = function(exportSystemMetadata : String, isResetMethod : Boolean, jobLocalePD : String, jobTargetLocale : String, srcLangPD : String, projectType : String) {		
		this.setSourceFile(exportSystemMetadata);
		this.setProjectType(projectType);
		this.setIsReset(isResetMethod);
		this.setJobLocales(jobLocalePD, jobTargetLocale, srcLangPD);
	};
	
	//init functions
	this.init = function() {
		this._initSystemMetadatas();		
		this._initReader();
	};

	this._initSystemMetadatas = function() {
		_systemMetadatas = <type-extensions></type-extensions>;
	};
	
	this._initReader = function() {
		sourceFile = new File(dw.io.File.IMPEX + getFoldersStructure()['export_metadata'] + sourceFileName);
		xmlFileReader = new FileReader(sourceFile);
		xmlReader = new XMLStreamReader(xmlFileReader);
	};

	this.closeReader = function() {
		xmlReader.close();
		xmlFileReader.close();
	};
	
	// basic xml builderes	
	this._createSystemMetadataSkeleton = function() {
    
	    var request : XML = <request></request>;
	    request.appendChild(this._initHeader());
	    if ((_projectType != null) && (_projectType.equals("Catalog"))) {
		    request.appendChild(<project-name resourcetype="1">Catalog</project-name>);
		    request.appendChild(<project-shortcode resourcetype="1">{TranslationHelper.getProjectShortCode("Catalog")}</project-shortcode>);
	    }  
	    else if ((_projectType != null) && (_projectType.equals("Content"))) {
		    request.appendChild(<project-name resourcetype="1">Content</project-name>);
		    request.appendChild(<project-shortcode resourcetype="1">{TranslationHelper.getProjectShortCode("Content")}</project-shortcode>);
	    }  
	    else if ((_projectType != null) && (_projectType.equals("Promotion"))) {
		    request.appendChild(<project-name resourcetype="1">Promotion</project-name>);
		    request.appendChild(<project-shortcode resourcetype="1">{TranslationHelper.getProjectShortCode("Promotion")}</project-shortcode>);
	    }  
	    else if ((_projectType != null) && (_projectType.equals("Shipping"))) {
		    request.appendChild(<project-name resourcetype="1">Shipping</project-name>);
		    request.appendChild(<project-shortcode resourcetype="1">{TranslationHelper.getProjectShortCode("Shipping")}</project-shortcode>);
	    }            
	    _filecontent = request;
	};
	
	this._initHeader = function() {
    
	    var targetLanguage : String = TranslationHelper.getTargetLanguage();
	       
	    var header : XML = 
	        <header>
	    		<clientrequest-id>{TranslationHelper.getUsername()}</clientrequest-id>
	    		<source-language>{jobSrcLangPD}</source-language>
	    		<target-language type="SINGLE">{jobTgtLangPD}</target-language>        
	        </header>;
		
	    return header;        
	};
	
	// THE MAIN FUNCTION
	this.doExport = function() {
		this.init();
		
		while (xmlReader.hasNext()) {
			try {	  
            	xmlReader.next();	
                if (xmlReader.getEventType() == XMLStreamConstants.START_ELEMENT) {
                						
					switch (xmlReader.getLocalName()) {
						case "type-extension":							
							_currentSystemMetadataSource = xmlReader.getXMLObject();
							var id : String = (_currentSystemMetadataSource.attribute("type-id")).toString();
							this._doSystemMetadata();
							break;

						default:
							break;
					}						
				}
            } catch(e) {
            	var errMsg = e.message;
            	errorMsg = e.message;
            	errorCode = PIPELET_ERROR;
                return;
            }
      	}
      	this.closeReader();
	};
	

	this._doSystemMetadata = function() {
		// create xml
		this._prepareSystemMetadataXML();
				
		if(_systemMetadata != "" && _systemMetadata){
			var _oldSystemMetadatas : XML = new XML(_systemMetadatas);
			
			// add xml to export
			this._addSystemMetadata();
			this._exportSystemMetadatas();
			
			// write?
			var isFileOverSizeLimit : Boolean = (targetFile != null) ? (fileSizeLimit < targetFile.length()) : false;
			if (isFileOverSizeLimit) {
				_systemMetadatas = new XML(_oldSystemMetadatas);
				this._exportSystemMetadatas();
				this._initSystemMetadatas();			
				this._addSystemMetadata();
				targetFileCounter++;						
			}
		}
	};
	
	
	this._addSystemMetadata = function() {
		if(_systemMetadata != "" && _systemMetadata){
			_systemMetadatas.appendChild(_systemMetadata);
		}
	};
	
	// ========================== export functions ====================== //
	
	this._exportSystemMetadatas = function() {		
		this._exportEntities("type-extension");
	};
		
	this._exportEntities = function(entityType : String) {
		var object : dw.object.ExtensibleObject;
		switch (entityType) {            
            case "type-extension":
                object = _systemMetadatas;
                break;
            default:
                break;
        }
        
        if (object && object.children().length() > 0) {
            this._createSystemMetadataSkeleton();
            _filecontent.appendChild(object);
            this._prepareFile(entityType);
            writeFile(targetFile, _filecontent, "UTF-8");   
        }
	};
	
	// ========================== prepare functions ====================== //
	
	this._prepareFile = function(type : String) {		
		if (_projectType.equals("Promotion"))
			targetFile = new File(targetDir, 'TranslationOnlineMarketingSystemMetadataRequest_' + jobTargetLocalePair + '.xml');
		else
			targetFile = new File(targetDir, 'Translation' + _projectType + 'SystemMetadataRequest_' + jobTargetLocalePair + '.xml');
	};
	

	
	this._isEmpty = function(elements : XMLList) {
		var result : Boolean = true;
		if(elements != null && elements.length() > 0){
			for each(var element : XML in elements){
				if (cdata(<test></test>, element)) {
					result = false;
				}
			}
		}
		return result;
	};
	
	this._prepareSystemMetadataXML = function(){
	    _systemMetadata = "";
	    var typeId : String = _currentSystemMetadataSource.attribute("type-id");
	    
	    if ( ((typeId != null) && (_projectType.equals("Catalog")) && 
	    	  (typeId.equals("Catalog") ||
	    	   typeId.equals("Product") ||
	    	   typeId.equals("Category") ||
	    	   typeId.equals("Recommendation"))) ||
	    	 ((typeId != null) && (_projectType.equals("Content")) && 
	    	  (typeId.equals("Content") ||
	    	   typeId.equals("Folder"))) ||
	    	 ((typeId != null) && (_projectType.equals("Promotion")) && 
	    	  (typeId.equals("Promotion") ||
	    	   typeId.equals("SlotConfiguration"))) ||
	    	 ((typeId != null) && (_projectType.equals("Shipping")) && 
	    	  (typeId.equals("ShippingMethod"))) ) {

		    	_systemMetadata = <type-extension id={_currentSystemMetadataSource.attribute("type-id")}></type-extension>;
		    	if(_currentSystemMetadataSource.ns::['custom-attribute-definitions']!= null && _currentSystemMetadataSource.ns::['custom-attribute-definitions'].ns::['attribute-definition'].length()){
		 			_systemMetadata.appendChild(this._prepareCustomAttributes(_currentSystemMetadataSource.ns::['custom-attribute-definitions']));
		 		} 		
	    }
	    this._ifSystemMetadataEmpty();
	};
	
	// ========================== checking for empty xml functions ====================== //
	
	this._ifSystemMetadataEmpty = function() {
		if (_systemMetadata != "" && !_systemMetadata.children().length()) {
			_systemMetadata = "";
		}
	};
	
	
	
	this._prepareCustomAttributes = function(object : XML) {
		var TP_LANGUAGE_PREFIX : String = "TP_DW_PREFIX_";
		var attrLang : String;
		var srclang : String = dwSrcLang;
		var joblang : String = jobTargetLocaleDW;
		
		var attributeDefinitionsXML : XML = <custom-attribute-definitions></custom-attribute-definitions>;
		var attributeDefinitionXML : XML = null;
		
	    for each(var custom : XML in object.ns::['attribute-definition']){		
	    	attributeDefinitionXML = null;
	    	//attributeDefinitionXML = <attribute-definition attribute-id={custom.attribute('attribute-id')} resourcetype="0" translate="false"></attribute-definition>;
			var localizableFlag : String = null;
			var type : String = null;
			
			if (custom.ns::['type'] != null)
				type = (custom.ns::['type']).toString();
			if (custom.ns::['localizable-flag'] != null)
				localizableFlag = (custom.ns::['localizable-flag']).toString();	
			if ((localizableFlag != null) && (type != null) && (localizableFlag.equals("true")) && 
				((type.equals("enum-of-int")) || (type.equals("enum-of-string")))) {
				if (custom.ns::['value-definitions'] != null) {
					var valueDefinitionsXML : XML = <value-definitions></value-definitions>;
					var valueDefinitions : XML = custom.ns::['value-definitions'];
					for each(var valueDefinition : XML in valueDefinitions.ns::['value-definition']){
    					if ((valueDefinition !=null) && (valueDefinition.length() > 0)) {
    						var valueDefinitionXML : XML = <value-definition></value-definition>;
    						var value : XML = valueDefinition.ns::['value'];
							if ((value !=null) && (value.length() > 0) && (value.toString().length >0)) {
								var valueString : String = getXMLElementString(value);
								valueDefinitionXML.appendChild(cdata_string(<value resourcetype="0" translate="false"></value>, valueString));	    						
								for each(var display : XML in valueDefinition.ns::['display']){
	    							if ((display !=null) && (display.length() > 0) && (display.toString().length >0)) {
	    								var displayString : String = getXMLElementString(display);
	    								//var attrDWLang : String = display.attribute('xml:lang');
	    								var aAttrs : XMLList = display.attributes();
										for each(var da : String in aAttrs){
											if(da.equals(srclang)) {
												valueDefinitionXML.appendChild(cdata_string(<display resourcetype="0" translate="true"></display>, displayString));
											} 
										}
	    							}
	    							//valueDefinitionsXML.appendChild(valueDefinitionXML);
								}
							}
							if ((valueDefinitionXML.children() != null) && (valueDefinitionXML.children().length() > 0)) {
								valueDefinitionsXML.appendChild(valueDefinitionXML);
							}								
    					}
					} //each value-definition
					if ((valueDefinitionsXML.children() != null) && (valueDefinitionsXML.children().length() > 0)) {
						attributeDefinitionXML = <attribute-definition attribute-id={custom.attribute('attribute-id')} type={type} localizable-flag={localizableFlag} resourcetype="0" translate="false"></attribute-definition>;						
						//extract description
						var descriptionsDefinitionXML : XML = "";
						var descriptionsList : XMLList = custom.ns::['description'];
						for each(var description : XML in descriptionsList) {
							if ((description !=null) && (description.length() > 0) && (description.toString().length >0)) {
								var descriptionString : String = getXMLElementString(description);
								var attrs : XMLList = description.attributes();
								for each(var attr : String in attrs){
									var dLang : String = TP_LANGUAGE_PREFIX + attr.toString();
									attributeDefinitionXML.appendChild(cdata_string(<description xml:lang={dLang} resourcetype="0" translate="false"></description>, descriptionString));
								}
							}
						}						
    					attributeDefinitionXML.appendChild(valueDefinitionsXML);	
					}					
				}
	    	}
	    	if ((attributeDefinitionXML != null) && (attributeDefinitionXML.children() != null) && (attributeDefinitionXML.children().length() > 0))
	    		attributeDefinitionsXML.appendChild(attributeDefinitionXML);
	    }
	    return (attributeDefinitionsXML.toString().length) ? attributeDefinitionsXML : "";
	};	
		
		
	// setters and getters
	
	this.setSourceFile = function(filename : String) {
		sourceFileName = filename;
	};

	this.setProjectType = function(type : String) {
		_projectType = type;
	};	
	
	this.setJobLocales = function(locale : String, jobTargetLocale : String, srcLangPD : String) {
		jobTgtLangPD = locale;
		jobTargetLocalePair = jobTargetLocale;
		jobSrcLangPD = srcLangPD;
		if (jobTargetLocalePair != null) {
			var locale : Array = jobTargetLocalePair.split("--");
			jobTargetLocaleDW = locale[0];
			jobTargetLocaleDW = jobTargetLocaleDW.replace("_", "-");
		}
	};
	
	
	this.setIsReset = function(is : Boolean) {
		isReset = is;
	};
	
	this.getErrorMsg = function() {
		return errorMsg;
	};
	
	this.getErrorCode = function() {
		return errorCode;
	}

}
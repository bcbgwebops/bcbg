/**
* PromotionExporterObject.ds
*
*	This is designed to read from full contend exported file and write files for PD
*
*/

importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.io );
importPackage( dw.campaign );
importPackage( dw.object );


importScript( "int_translation:lib/libUtils.ds" );
importScript( "int_translation:lib/libTranslation.ds" );
importScript( "int_translation:foldersStructure/FoldersStructureModel.ds" );
importScript( "int_translation:model/PromotionObject.ds" );

function promotionExporter() { 
	
	var TranslationHelper = getTranslationHelper();
	var nodesQuantity : Number = TranslationHelper.getContentFileNodesNumber();
	var fileSizeLimit : Bytes = TranslationHelper.getContentFileMaxSize();
	var isReset : Boolean;
		
	var sourceFileName : String;
	var sourceFile : File;
	var targetFileName : String;
	var targetFile : File;	
	var targetFileCounter : Number = 1;
	var targetDir : File = new File(File.IMPEX+getFoldersStructure()['export_translateRequest']);
	
	var errorCode : Number = PIPELET_NEXT;
	var errorMsg : String = "";
	
	var xmlFileReader : FileReader;
	var xmlReader : XMLStreamReader;
	var ns : Namespace = new Namespace("http://www.demandware.com/xml/impex/promotion/2008-01-31");
	
	var _filecontent : XML = new XML();
	var _promotions : XML = new XML();
	var _currentPromotionSource : XML = new XML();
	var _oldPromotions : XML = new XML();
	
	var _promotionItem : Promotion;
	var _date : Date = new Date();
	var _lastM : Date = null;
	var _lastMC : Date = null;
	var _lastMEC : Date = null;	
	var _lastSentTXLocale : Date = null;	
	var _lastDate : String = null;
	var _timeStamp : String = "";
	
	var jobTgtLangPD : String;
	var jobTargetLocalePair: String;
	var jobSrcLangPD : String;
	var jobTargetLocaleDW : String;
	
	var promotionSysDef : ObjectTypeDefinition= SystemObjectMgr.describe("Promotion");
	var promotionAttributeMap = new dw.util.HashMap();
		
	// constructor
	this.create = function(exportPromotion : String, isResetMethod : Boolean, jobLocalePD : String, jobTargetLocale : String, srcLangPD : String) {		
		this.setSourceFile(exportPromotion);
		this.setIsReset(isResetMethod);		
		this.setJobLocales(jobLocalePD, jobTargetLocale, srcLangPD);
	};
	
	//init functions
	this.init = function() {
		this._initPromotions();		
		this._initReader();
	};

	
	this._initPromotions = function() {
		_promotions = <promotions></promotions>;
	};
	
	this._initReader = function() {
		sourceFile = new File(dw.io.File.IMPEX + getFoldersStructure()['export_onlineMarketing'] + sourceFileName);
		xmlFileReader = new FileReader(sourceFile);
		xmlReader = new XMLStreamReader(xmlFileReader);
	};

	this.closeReader = function() {
		xmlReader.close();
		xmlFileReader.close();
	};
	
	// basic xml builderes	
	this._createPromotionSkeleton = function() {
    
	    var request : XML = <request></request>;
	    request.appendChild(this._initHeader());
	    request.appendChild(<project-name resourcetype="1">Promotion</project-name>);
	    request.appendChild(<project-shortcode resourcetype="1">{TranslationHelper.getProjectShortCode("Promotion")}</project-shortcode>);
	                
	    _filecontent = request;
	};
	
	this._initHeader = function() {
    
	    var targetLanguage : String = TranslationHelper.getTargetLanguage();
	       
	    var header : XML = 
	        <header>
	    		<clientrequest-id>{TranslationHelper.getUsername()}</clientrequest-id>
	    		<source-language>{jobSrcLangPD}</source-language>
	    		<target-language type="SINGLE">{jobTgtLangPD}</target-language>        
	        </header>;
		
	    return header;        
	};
	
	// THE MAIN FUNCTION
	this.doExport = function() {
		this.init();
		
		while (xmlReader.hasNext()) {
			try {	  
            	xmlReader.next();	
                if (xmlReader.getEventType() == XMLStreamConstants.START_ELEMENT) {
                						
					switch (xmlReader.getLocalName()) {
							
						case "promotion":							
							_currentPromotionSource = xmlReader.getXMLObject();
							this._doPromotion();
							break;

						default:
							break;
					}						
				}
            } catch(e) {
            	var errMsg : String = e.message;
            	errorMsg = e.message;
            	errorCode = PIPELET_ERROR;
                return;
            }
      	}
      	this.closeReader();
	};
	

	this._doPromotion = function() {
		// create xml
		this._preparePromotionXML();
		
		if(_promotion != "" && _promotion){
			var _oldPromotions : XML = new XML(_promotions);
						
			// add xml to export
			this._addPromotion();
			this._exportPromotions();
			
			// write?
			var isFileOverSizeLimit : Boolean = (targetFile != null) ? (fileSizeLimit < targetFile.length()) : false;
			if ((_promotions.children().length() == nodesQuantity) ||isFileOverSizeLimit) {
				if (isFileOverSizeLimit) {
					_promotions = new XML(_oldPromotions);
					this._exportPromotions();
					this._initPromotions();			
					this._addPromotion();
											
				} else {
					this._initPromotions();
				}
				targetFileCounter++;
			}
		}
	};
	
	// ========================== add functions ====================== //
	
	this._addPromotion = function() {
		if(_promotion != "" && _promotion){
			_promotions.appendChild(_promotion);
		}
	};
	
	// ========================== export functions ====================== //
		
	this._exportPromotions = function() {		
		this._exportEntities("promotion");
	};
		
	this._exportEntities = function(entityType : String) {
		var object : dw.object.ExtensibleObject;
		switch (entityType) {            
            case "promotion":
                object = _promotions;
                break;
            default:
                break;
        }
        
        if (object && object.children().length() > 0) {
            this._createPromotionSkeleton();
            _filecontent.appendChild(object);
            this._prepareFile(entityType);
            writeFile(targetFile, _filecontent, "UTF-8");   
        }
	}
	
	// ========================== prepare functions ====================== //
	
	this._prepareFile = function(type : String) {		
		targetFile = new File(targetDir, 'TranslationOnlineMarketingRequest_promotion' + "_" + jobTargetLocalePair + "-" + targetFileCounter + '.xml');		
	};
	
	
	this._isEmpty = function(elements : XMLList) {
		var result : Boolean = true;
		if(elements != null && elements.length() > 0){
			for each(var element : XML in elements){
				if (cdata(<test></test>, element)) {
					result = false;
				}
			}
		}
		return result;
	};
	
	this._preparePromotionXML = function(){
	    _promotion = "";
	    var isExcludeTx : String = getCustomAttribute(_currentPromotionSource.ns::['custom-attributes'], "isExcludeTranslate", ns);
		if ((isExcludeTx == null) || (!isExcludeTx.equals("true"))) {
	    
		    if(_currentPromotionSource.ns::['enabled-flag']!= null && _currentPromotionSource.ns::['enabled-flag'].toString().equals("true") ) {
		    
		 	    var promotionID : String = _currentPromotionSource.attribute("promotion-id");
	    		_promotionItem = dw.campaign.PromotionMgr.getPromotion(promotionID);
	    		if (_promotionItem != null) {
					_lastM = null;
	 				_lastMC = null;
	 				_lastMEC = null;
	 				_lastSentTXLocale = null;    			
	    			
	    			_lastM = _promotionItem.getLastModified();
					_lastDate = getCustomAttribute(_currentPromotionSource.ns::['custom-attributes'], "lastModifiedCartridge", ns);
	 				_lastMC = getDate(_lastDate);
	  					
	 				_lastDate = getCustomAttribute(_currentPromotionSource.ns::['custom-attributes'], "lastModifiedExcludeCartridge", ns);
	 				_lastMEC = getDate(_lastDate);
	 					
	 				_lastDate = getCustomAttributeByTarget(_currentPromotionSource.ns::['custom-attributes'], "lastSentForTranslationTarget", jobTargetLocaleDW, ns);
			    	_lastSentTXLocale = getDate(_lastDate);
			    					    				
	 				_lastMEC = getObjectLastMECDate(_lastM, _lastMC, _lastMEC);    			
			    	
			    	if ((isReset) || 
			    	    (isItemTranslationRequiredDate(_lastM, _lastMC, _lastMEC, _lastSentTXLocale))) {
			    		_timeStamp = StringUtils.formatCalendar(new Calendar(_lastMEC), "yyyy-MM-dd'T'HH:mm:ss'.896+0000'");	    		
				       	_promotion = <promotion id={_currentPromotionSource.attribute("promotion-id")} lastMEC={_timeStamp}></promotion>;
				    	if(_currentPromotionSource.ns::['name']!= null && _currentPromotionSource.ns::['name'].length() > 0){
				    		_promotion.appendChild(cdata(<name resourcetype="0" translate="true"></name>, _currentPromotionSource.ns::['name']));
				    	}
				    	
				    	if(_currentPromotionSource.ns::['callout-msg']!= null && _currentPromotionSource.ns::['callout-msg'].length() > 0){
				    		_promotion.appendChild(cdata(<callout-msg resourcetype="0" translate="true"></callout-msg>, _currentPromotionSource.ns::['callout-msg']));
				    	}
				    	
				    	if(_currentPromotionSource.ns::['details']!= null && _currentPromotionSource.ns::['details'].length() > 0){
				    		_promotion.appendChild(cdata(<details resourcetype="0" translate="true"></details>, _currentPromotionSource.ns::['details']));
				    	}
				    	
				    	if(_currentPromotionSource.ns::['custom-attributes']!= null && _currentPromotionSource.ns::['custom-attributes'].ns::['custom-attribute'].length()){
				 			_promotion.appendChild(this._prepareCustomAttributes(_currentPromotionSource.ns::['custom-attributes']));
				 		}
			    	}
	    		}
		    }
		}
	    
	    this._ifPromotionEmpty();
	};
	
	// ========================== checking for empty xml functions ====================== //
	 	
	this._ifPromotionEmpty = function() {
		if (_promotion != "" && !_promotion.children().length()) {
			_promotion = "";
		}
	};
	
	// ========================== custom attribute xml writer functions ====================== //
		
	this._prepareCustomAttributes = function(object : XML) {
		var customAttributes : XML = <custom-attributes></custom-attributes>;
		var isTranslatable : Number = 0;
		var cid : String = null;
	    for each(var custom : XML in object.ns::['custom-attribute']){		
	    	if ((custom.attribute('attribute-id') != "lastSentForTranslationTarget") &&
	    		(custom.attribute('attribute-id') != "lastModifiedCartridge") &&
	    		(custom.attribute('attribute-id') != "lastModifiedExcludeCartridge")) {		
	    			isTranslatable = 0;
	    			cid = (custom.attribute('attribute-id')).toString();
	    			isTranslatable = promotionAttributeMap.get(cid);
	    			if (isTranslatable == null) {
	    				isTranslatable = isTranslatableAttributeDatatype(promotionSysDef, cid);
	    				promotionAttributeMap.put(cid, isTranslatable);
	    			} 
	    			if (isTranslatable == 1) 
	        			customAttributes.appendChild(cdata(<custom-attribute attribute-id={custom.attribute('attribute-id')} resourcetype="0" translate="true" embedded="true"></custom-attribute>, custom));
	    			else if ((isTranslatable == 2) || (isTranslatable == 3))
	        			customAttributes.appendChild(cdata(<custom-attribute attribute-id={custom.attribute('attribute-id')} resourcetype="0" translate="false" embedded="true"></custom-attribute>, custom));
	    		}
	    }

	    return (customAttributes.toString().length) ? customAttributes : "";

	};
    
	
	function filterObject(object : dw.object.ExtensibleObject, isReset : Boolean) {    
	    var type : String = object.describe().getID().toLowerCase();
	    if (isReset || (!isReset && _isObjectTranslationRequired(object, 7000))) {
	        var type : String = object.describe().getID().toLowerCase();
	        
	        if (object.getDisplayName()) {
	            return object;
	        }
	    }
	   
	    return null;
	}
	
	// setters and getters
	
	this.setSourceFile = function(filename : String) {
		sourceFileName = filename;
	};
	
	this.setIsReset = function(is : Boolean) {
		isReset = is;
	};
	
	this.setJobLocales = function(locale : String, jobTargetLocale : String, srcLangPD : String) {
		jobTgtLangPD = locale;
		jobTargetLocalePair = jobTargetLocale;
		jobSrcLangPD = srcLangPD;
		if (jobTargetLocalePair != null) {
			var locale : Array = jobTargetLocalePair.split("--");
			jobTargetLocaleDW = locale[0];
			jobTargetLocaleDW = jobTargetLocaleDW.replace("_", "-");
		}
	};
	

	
	this.getErrorMsg = function() {
		return errorMsg;
	};
	
	this.getErrorCode = function() {
		return errorCode;
	}

}
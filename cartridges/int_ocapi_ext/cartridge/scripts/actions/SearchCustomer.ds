/**
* Action/Wrapper Script for Customer Actions
*
*
*  @input EmailId : String Email Id of Customer.
*  @input FirstName : String First Name of the Customer.
*  @input LastName : String Last Name of the Customer.
*  @output ErrorJson : String JSON string of the Customer object
*  @output CustomerList : dw.util.List results from customer search
*
*/
importPackage( dw.system );
importPackage(dw.util);

importScript("api/EACustomer.ds");
importScript("api/EAStatus.ds");
importScript("util/JsonSerializer.ds");

function execute( args : PipelineDictionary ) : Number
{
	var log : Log = Logger.getLogger("instore-audit-trail");
	log.info("SearchCustomer: entering script");		

	var eaStatus = new EAStatus();

	//check authorization
	if (!session.custom.agent || !('allowLOBO' in session.custom.agent) || !session.custom.agent.allowLOBO ) {
		eaStatus.findMessage("EA_CUSTOMER_401");
		args.ErrorJson = eaStatus.stringify();
		log.info("SearchCustomer: error EA_CUSTOMER_401");		
		
	} else {
		log.info("SearchCustomer: associate " + session.custom.agent + " is searching for a customer" + 
			args.EmailId + ", " +
			args.FirstName + ", " +
			args.LastName);		
	
		//search customer
		var customer = new EACustomer();
		var firstname=args.FirstName.replace("'","?");		// replace single quotes with ? to aid search string generation
		var lastname=args.LastName.replace("'","?");		// replace single quotes with ? to aid search string generation
		var customerItr : SeekableIterator = customer.searchCustomer(args.EmailId, firstname, lastname);
		var outputStr : String = null;	
		
		var hitMax = 250;
		if ('eaCustomerSearchLimit' in dw.system.Site.current.preferences.custom && !empty(dw.system.Site.current.preferences.custom.eaCustomerSearchLimit)) {
			hitMax = dw.system.Site.current.preferences.custom.eaCustomerSearchLimit;
		}
		
		if (!customer.isError && customerItr.count < hitMax) {
			var customerList : List = customerItr.asList();
			args.CustomerList = customerList;
		
		} else {
			(customerItr.count >= hitMax) ? eaStatus.findMessage("EA_CUSTOMER_4001") : eaStatus.findMessage(customer.eaStatusCode);
			args.ErrorJson = eaStatus.stringify();
		}	
	}
	
	log.info("SearchCustomer: exiting script");		
	return PIPELET_NEXT;
}

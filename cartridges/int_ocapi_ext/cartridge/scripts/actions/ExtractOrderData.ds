/**
* Demandware Script File
* To define input and output parameters, create entries of the form:
*
* @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
*
* where
*   <paramUsageType> can be either 'input' or 'output'
*   <paramName> can be any valid parameter name
*   <paramDataType> identifies the type of the parameter
*   <paramComment> is an optional comment
*
* For example:
*
*   @input Orders : dw.util.Iterator 
*   @output JSONResponse : Object
*
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.order );
importScript("api/EAStatus.ds");

function execute( args : PipelineDictionary ) : Number
{
	var log : Log = Logger.getLogger("instore-audit-trail");
	log.info("ExtractOrderData: entering script");
	
	var orders : Iterator = args.Orders;
	var output = [];

	var maxOrders = Site.current.preferences.custom.eaOrdersReturnedLimit;
	
	var orderCount = 0;
	if (orders.hasNext()) {
		while (orders.hasNext() && (orderCount++ < maxOrders)) {
			var order : Order = orders.next();
			log.info("ExtractOrderData: processing order " + order.getOrderNo());
			
			// Get nicely formatted date
			var date = order.getCreationDate();
			var cal : Calendar = new Calendar(date);
			var niceDate : String = StringUtils.formatCalendar(cal, request.getLocale(), Calendar.LONG_DATE_PATTERN);
			
			// get the first product's image
			var imageURL = order.defaultShipment.productLineItems[0].product.getImage("small").getAbsURL().toString();
			
			// get totalNetPrice
			var price = order.totalGrossPrice.value;
			
			// get the status (This login taken from Site Genesis!)
			var status = "";
			if ((order.getStatus() == dw.order.Order.ORDER_STATUS_NEW || order.getStatus() == dw.order.Order.ORDER_STATUS_OPEN) && 
				(order.getShippingStatus() == dw.order.Order.SHIPPING_STATUS_SHIPPED) || 
				(order.getStatus() == dw.order.Order.ORDER_STATUS_COMPLETED)) {
					status = "Shipped";
			} else if ((order.getStatus() == dw.order.Order.ORDER_STATUS_NEW || order.getStatus() == dw.order.Order.ORDER_STATUS_OPEN) && 
				       (order.getShippingStatus() != dw.order.Order.SHIPPING_STATUS_SHIPPED)) {
				    status = "Being Processed";
			} else {
				status = order.status.displayValue;	
			} 
			
			output.push( {
				'creationDate':niceDate,
				'orderNo': order.orderNo,
				'totalNetPrice': price,
				'status': status,
				'imageURL' : imageURL
			});	
		}
	}

	args.JSONResponse = {'httpStatus': 200, 'orders': output};
	log.info("ExtractOrderData: exiting script without error");
   	return PIPELET_NEXT;
}

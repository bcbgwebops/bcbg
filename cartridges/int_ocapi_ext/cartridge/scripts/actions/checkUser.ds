/**
* 	Wrapper to check user authentication
*
*  @input EmployeeId : String 
*  @input Passcode : String  
*  @output authorized: Boolean
*  @output status : String
*  @output output : String
*
*/
importPackage( dw.system );
importPackage( dw.object );
importScript("api/Authorize.ds");
function execute( args : PipelineDictionary ) : Number
{ 
	var log : Log = Logger.getLogger("instore-audit-trail");
	log.info("checkUser: entering script");

	var authorize = new Authorize();

	if (empty(args.EmployeeId) || empty(args.Passcode) || empty(session.custom.storeId)) {
		args.status = "bad_params";
		log.info("checkUser: exiting script with bad_params error");
		return PIPELET_ERROR;
	}
	log.info("checkUser: verifying credentials for associate "+ args.EmployeeId + " in store" + session.custom.storeId);

	//Verify Associate's credentials and authorization
	authorize.authorize(args.EmployeeId, args.Passcode, session.custom.storeId); //empid, passcode, store id

	if (authorize.httpStatus != 200) {
		var output = authorize.stringify();
		args.output = output;
		args.status = authorize.status;
		args.authorized = false;		
		log.info("checkUser: exiting script with authorization error");
		return PIPELET_ERROR;
	}
	
	//Set Associate's data in session
	session.custom.agent = {
		employeeId : args.EmployeeId,
		storeId : session.custom.storeId,
		allowLOBO : authorize.allowLOBO,
		permissionGroupId : authorize.associateInfo.permissionGroupId
	};

	
	//JSON output
	var output = authorize.stringify();
	args.output = output;
	args.authorized = authorize.isAuthorized() ? true : false;

	log.info("checkUser: exiting script without error");
   	return PIPELET_NEXT;
}

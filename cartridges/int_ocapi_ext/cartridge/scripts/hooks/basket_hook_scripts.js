/*  basket_hook_scripts.js  */

importPackage( dw.order );
importPackage( dw.system );

var updateProductLineItem = require('./updateProductLineItem.ds');
var basketPatch = require('./basketPatch.ds');
var setShippingMethod = require('./setShippingMethod.ds');

/*
 * the afterAdd product to a basket hook
 */
exports.afterAdd = function(basket, productItem) {
	var log = Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info(" basket afterAdd: entering script");
		// now update the product item
		var status = updateProductLineItem.updateProductItem(basket, productItem, attributes.agent.employeeId);
		if (status !== PIPELET_NEXT) {
			log.info("basket afterAdd: error on updateProductLineItem");
			return new Status(Status.ERROR);
		}
		log.info("basket afterAdd: exiting script");
	}
	// now call calculate again since the options on an item may have changed and the basket needs recalculating
	dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
	
	return new Status(Status.OK);
};

/*
 * the beforeUpdate basket hook. This gets called before a basket patch operation.
 */
exports.beforeUpdate = function(basket, update) {
	var log = Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info("basket beforeUpdate: entering script");
		// call the patch script. if that returns an error, return that error
		var status = basketPatch.execute(basket, update);
		if (status !== PIPELET_NEXT) {
			log.info("basket beforeUpdate: error on update");
			return new Status(Status.ERROR);
		}
		// make sure to call the calculate hook
		dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
		log.info("basket beforeUpdate: exiting script");
	}
	
	return new Status(Status.OK);
};

/*
 * the beforeSetShippingMethod hook. This is used to add a shipping price override.
 */
exports.beforeSetShippingMethod = function(basket, shippingMethod) {
	var log = Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info("basket beforeSetShippingMethod: entering script");
		// call the customization script
		var status = setShippingMethod.execute(basket, shippingMethod);
		// if that has an error, return error
		if (status !== PIPELET_NEXT) {
			log.info("basket beforeSetShippingMethod: error on setShippingMethod");
			return new Status(Status.ERROR);
		}
		// make sure to call the calculate hook
		dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
		log.info("basket beforeSetShippingMethod: exiting script");
	}
	return new Status(Status.OK);
};

/*
 * the afterSetShippingMethod hook. This is used to add a gift message
 */
exports.afterSetShippingMethod = function(basket, shippingMethod) {
	var log = Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info("basket afterSetShippingMethod: entering script");
		// call the customization script
		var status = setShippingMethod.afterSetShippingMethod(basket, shippingMethod);
		// if that has an error, return error
		if (status !== PIPELET_NEXT) {
			log.info("basket afterSetShippingMethod: error on setShippingMethod");
			return new Status(Status.ERROR);
		}
		// make sure to call the calculate hook
		dw.system.HookMgr.callHook( "dw.ocapi.shop.basket.calculate", "calculate", basket );
		log.info("basket beforeSetShippingMethod: exiting script");
	}
	return new Status(Status.OK);
};



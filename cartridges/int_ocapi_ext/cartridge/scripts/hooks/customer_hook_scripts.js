var customerAddress = require('./customerAddress.ds');
var createCustomerAccount = require('./createCustomerAccount.ds');

exports.afterRegister = function(profile, registration) {
	var log = Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info(" customer afterRegister: entering script");
		var status = createCustomerAccount.execute(profile, registration);
		log.info("afterRegister: exiting script");
		return status.status == PIPELET_NEXT ? new Status(Status.OK) : new Status(Status.ERROR, status.message);
	}
	return new Status(Status.OK);
};

/**
 * getPREligiblity.ds
 * This script determines whether a cart is eligible for ShopRunner Express Checkout using PayRunner.
 * Only one item in the cart needs to be eligible to make the cart eligible.
 *
 *	@input  Basket : dw.order.Basket
 *	@input  SRToken : String
 *  @output isBasketPREligible : Boolean
 *
 */
importPackage(dw.system);
importPackage(dw.order);
importPackage(dw.util);
importPackage(dw.value);
importPackage(dw.catalog);
importPackage(dw.net);
importPackage(dw.web);
importScript("util/srProductUtils.ds");
importScript("checkout/Utils.ds");

function execute(args: PipelineDictionary): Number {
	var basket: Basket = args.Basket;
	var isEligible: Boolean = false;
	// check if the basket is not empty
	if (!empty(basket)) {
		var basketLineItems: Collection = basket.allProductLineItems;
		var basketItems: Iterator = basketLineItems.iterator();

		// declare variables that we need to call the functions from srProductUtils and Utils.ds 
		var signin: Boolean = false;
		var srToken: String = args.SRToken;

		try {
			// include Express Checkout: Cart Buy Now condition
			var srEligibleProducts: Number = 0;

			while (basketItems.hasNext()) {
				var productLineItem: ProductLineItem = basketItems.next();
				var product: Product = productLineItem.getProduct();
				if (product != null) {
					var sr_eligible = product.getCustom().sr_eligible.value;
					if(sr_eligible == false || sr_eligible == 'false'){
						sr_eligible = 'false';
					} else {
						sr_eligible = 'true';
					}
					if (sr_eligible == true || sr_eligible == 'true') {
						isEligible = true;
						srEligibleProducts++;
					}
					// check if the productBasePrice is N/A or empty
					if (productLineItem.getBasePrice() == 'N/A' || empty(productLineItem.getBasePrice())) {
						isEligible = false;
						break;
					}
				}
			}
			if (srEligibleProducts <= 0) {
				isEligible = false;
			}

			// get the status from checkCartEligibility(MIXED/ALL_SR/NO_SR)
			var status: String = SRProductUtils.checkCartEligibility(basketLineItems);

			// check if the srToken has value in order to set the value to signin variable
			var signin: Boolean = empty(srToken) ? false : true;
			// set the default shipping method
			setDefaultShippingMedhod(basket, status);

			// call the function from Utils to set the srgroundfree value in custom.session
			setShippingGroundFreeSession(status, signin);
		} catch (e) {
			Logger.getLogger('ShopRunner', 'ShopRunner').error('ShopRunner: getPREligibilityCart.ds general error: ' +
				e.message);
			return PIPELET_ERROR;
		}
		args.isBasketPREligible = isEligible;
	}
	return PIPELET_NEXT;
}
/**
 * This script sets the shipping method of the given shipment to
 * the passed method id.
 * 
 * @input Shipment: dw.order.Shipment The shipment to update the shipping method for.
 * @input ProductLineItems : dw.util.Collection Product line items.
 * @input BasketSR : String Represents the current ShopRunner basket
 * @input Context : String The pipeline node where the script is called from
 *
 */
importPackage( dw.order );
importPackage( dw.system );
importPackage( dw.util );
importScript( "util/srProductUtils.ds" );

function execute( pdict : PipelineDictionary ) : Number
{
	var shipment : Shipment = pdict.Shipment;
	var basketSR : String = pdict.BasketSR;
	var plitems : Collection = pdict.ProductLineItems;
	var context : String = pdict.Context;
	 
	var basketSRjson : Object = JSON.parse( basketSR );
	var selectedShipMethId : String = getSelectedShipMethId(basketSRjson).replace('shippingMethodID-', '');
	
	var	shippingMethods : Collection;
	if (shipment != null)
	{
		shippingMethods = ShippingMgr.getShipmentShippingModel(shipment).getApplicableShippingMethods();
	}
	else
	{
		shippingMethods = ShippingMgr.getAllShippingMethods();
	}
	// check the mixed cart status
	var srStatus : String = SRProductUtils.checkCartEligibility(plitems);
	
	if (shippingMethods.length > 0) 
	{
		// Set the shipment shipping method to the passed one.
		var shippingMethodsIter : Iterator = shippingMethods.iterator();
		while (shippingMethodsIter.hasNext())
		{
			var method : ShippingMethod = shippingMethodsIter.next();			
			if (method.ID == 'shoprunner' && selectedShipMethId == 'shoprunner')
			{
				shipment.setShippingMethod( method );
				break;
			}
			if (method.ID == 'shoprunner' && srStatus == 'ALL_SR')
			{
				shipment.setShippingMethod(method);
				break;
			}		
			if (!method.displayName.equals(selectedShipMethId) && selectedShipMethId != 'shoprunner') 
			{
				continue;
			}
			else if (!method.displayName.equals(selectedShipMethId) && selectedShipMethId == 'shoprunner')
			{
				// do something
			}
			// set this shipping method
			shipment.setShippingMethod(method);
		}
	} 
	else 
	{
		return PIPELET_ERROR;
	}
	return PIPELET_NEXT;
}

function getSelectedShipMethId(srBasket) 
{
	var selectedShipMeth : String = '';
	if (!empty(srBasket))
	{
		for each (var shippingGroup in srBasket.shippingGroups) 
		{
			for each (var shipMeth in shippingGroup.shipping) 
			{
				if (shipMeth.selected) 
				{
					selectedShipMeth = shipMeth.method;
					break;	
				}
			}	
		}
		if (empty(selectedShipMeth)) 
		{
			selectedShipMeth = 'shoprunner';	
		}
	}
	return selectedShipMeth;
}
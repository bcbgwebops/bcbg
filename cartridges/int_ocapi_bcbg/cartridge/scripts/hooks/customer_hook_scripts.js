importPackage( dw.system );

var customerAddress = require('./customerAddress.ds');
var createCustomerAccount = require('./createCustomerAccount.ds');

exports.afterRegister = function(profile, registration) {
	var log = dw.system.Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info(" customer afterRegister: entering script");
		var status = createCustomerAccount.execute(profile, registration);
		log.info("afterRegister: exiting script");
		return status.status == PIPELET_NEXT ? new dw.system.Status(dw.system.Status.OK) : new dw.system.Status(dw.system.Status.ERROR, status.message);
	}
	return new dw.system.Status(dw.system.Status.OK);
};

exports.beforeUpdate = function(profile,update) {
	var log = dw.system.Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info(" customer beforeUpdate: entering script");
		var status = customerAddress.execute(profile, update);
		log.info(" customer beforeUpdate: exiting script");
		return status.status == PIPELET_NEXT ? new dw.system.Status(dw.system.Status.OK) : new dw.system.Status(dw.system.Status.ERROR, status.message);
	}
	return new dw.system.Status(dw.system.Status.OK);
};

importPackage( dw.system );

var customerAddress = require('./customerAddress.ds');

exports.afterUpdate = function(profile,address, update) {
	var log = Logger.getLogger("instore-audit-trail");
	var attributes = session.getCustom();
	log.info("customer address afterUpdate very beginning");
	// only call this code for DSS if there's an agent
	if (attributes.agent) {
		log.info(" customer address afterUpdate: entering script");
		var status = customerAddress.execute(profile, address, update);
		log.info(" customer address afterUpdate: exiting script");
		return status.status == PIPELET_NEXT ? new Status(Status.OK) : new Status(Status.ERROR, status.message);
	}
	return new Status(Status.OK);
};

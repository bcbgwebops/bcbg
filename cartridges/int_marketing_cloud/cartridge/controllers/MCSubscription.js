'use strict';

/**
 * @module controllers/MCSubscription
 */

// TODO: Content asset account-nav-unregistered and account-nav-registered should be updated to add endpoints to nav

const curSite = require('dw/system/Site').current;
const ArrayList = require('dw/util/ArrayList');
const HookMgr = require('dw/system/HookMgr');

/* Script Modules */
var app = require('*/cartridge/scripts/app');
var guard = require('*/cartridge/scripts/guard');
var responseUtil = require('*/cartridge/scripts/util/Response');

function manage() {
    var subscriber = require('int_marketing_cloud').subscriber(customer);
    var subscribedLists = subscriber.currentSubscriptions;
    var whitelist = (new ArrayList(curSite.getCustomPreferenceValue('mcMailingListsWhitelist'))).toArray();
    var availableLists = subscriber.fetchAvailableLists().filter(function(list){
        return whitelist.length <= 0 || whitelist.indexOf(list.ID) !== -1;
    });

    app.getForm('mcpreferences').clear();
    app.getForm('mcpreferences.lists').copyFrom(new (require('dw/util/ArrayList'))(availableLists));
    app.getView({
        ContinueURL: require('dw/web/URLUtils').https('MCSubscription-SavePreferences'),
        subscribedLists: subscribedLists,
        availableLists: availableLists
    }).render('marketingcloud/preferences');
}

function savePreferences() {
    var subscriptionForm = app.getForm('mcpreferences');

    var subscriptionResult = subscriptionForm.handleAction({
        save: function (formgroup) {
            var subscriber = require('int_marketing_cloud').subscriber(customer);
            for each (var list in formgroup.lists) {
                if (list.enabled.checked) {
                    subscriber.assignLists(list.listID.value);
                } else {
                    subscriber.unassignLists(list.listID.value);
                }
            }
            return subscriber.commit();
        },
        error: function (formgroup) {
            return null;
        }
    });

    response.redirect(require('dw/web/URLUtils').https('MCSubscription-Manage'));
}

//Render the modal subscribe form
function showSubscribe() {
    var form = app.getForm('mcsubscribe');
    app.getView({
        ContinueURL: require('dw/web/URLUtils').https('MCSubscription-SubscribeInfo'),
        formAction: 'subscribe',
        nodecorator: true
    }).render('marketingcloud/subscribe');
}

//Render the footer subscribe form
function showSubscribeFooter() {
    var form = app.getForm('mcsubscribe');
    app.getView({
        nodecorator: true,
        ContinueURL: require('dw/web/URLUtils').https('MCSubscription-SubscribeInfo'),
        formAction: 'subscribeFooter'
    }).render('marketingcloud/subscribe');
}

//Render the additional info form
function showInfoUpdate() {
	var form = app.getForm('mcsubscribe');
    app.getView({
        ContinueURL: require('dw/web/URLUtils').https('MCSubscription-Submit'),
        showView: 'info'
    }).render('marketingcloud/info');
}

//Render the unsubscribe form
function showUnsubscribe() {
    var form = app.getForm('mcsubscribe');
    if (form.object.isValid()) {
        // we're not clearing if invalid, so errors can be reflected to the user.
        app.getForm('mcsubscribe').clear();
    }
    app.getForm('mcsubscribe').object.unsubscribeemail.value = request.httpParameterMap.email.stringValue;
    app.getForm('mcsubscribe').object.action.value = "unsubscribe";
    app.getView({
        ContinueURL: require('dw/web/URLUtils').https('MCSubscription-Submit'),
        formAction: 'unsubscribe'
    }).render('marketingcloud/subscribe');
}

//Call API to check if subscriber exists
function checkSubscriber() {
    var form = app.getForm('mcsubscribe');
    if (form.object.isValid()) {
        // we're not clearing if invalid, so errors can be reflected to the user.
        app.getForm('mcsubscribe').clear();
    }
    app.getForm('mcsubscribe').object.email.value = request.httpParameterMap.email.stringValue;
    app.getForm('mcsubscribe').object.action.value = request.httpParameterMap.action.stringValue;
    var result = {};

    var hookID = 'app.mailingList.checkSubscriber';
    if (HookMgr.hasHook(hookID)) {
        result.response = HookMgr.callHook(
            hookID,
            hookID.slice(hookID.lastIndexOf('.') + 1),
            {
                email: request.httpParameterMap.email.stringValue
            }
        );
    }

	responseUtil.renderJSON(result);
}

//Update the form info
function updateFormInfo() {

    //set values on the form
    app.getForm('mcsubscribe').object.country.value = request.httpParameterMap.dwfrm_mcsubscribe_country.stringValue;
    app.getForm('mcsubscribe').object.dobday.value = request.httpParameterMap.dwfrm_mcsubscribe_dobday.intValue;
    app.getForm('mcsubscribe').object.dobmonth.value = request.httpParameterMap.dwfrm_mcsubscribe_dobmonth.intValue;
    app.getForm('mcsubscribe').object.dobyear.value = request.httpParameterMap.dwfrm_mcsubscribe_dobyear.intValue;
    app.getForm('mcsubscribe').object.firstname.value = request.httpParameterMap.dwfrm_mcsubscribe_firstname.stringValue;
    app.getForm('mcsubscribe').object.lastname.value = request.httpParameterMap.dwfrm_mcsubscribe_lastname.stringValue;
    app.getForm('mcsubscribe').object.postalcode.value = request.httpParameterMap.dwfrm_mcsubscribe_postalcode.stringValue;

    var result = {
		response: true
	}
	responseUtil.renderJSON(result);
}

/**
 * The handler for initial subscribe submit.
 */
function initialSubmit() {
    var subscriptionForm = app.getForm('mcsubscribe'),
    subscribed = false,
    noDecorator = false,
    submittedAction,
    subscriptionResult,
    date = new Date().toUTCString(),
    locale = request.getLocale(),
    profile = null,
    preferredAddress = null,
    hookID,
    req = request,
    action = subscriptionForm.object.action.value;
    
    if (locale == 'default') {
		locale = 'en';
	}
    
    if (!empty(session.getCustomer().getProfile())) {
    	profile = session.getCustomer().getProfile();
        preferredAddress = profile.getAddressBook().getPreferredAddress();
    }
    
    if (action == "subscribe") {
        subscribed = true;
        noDecorator = true;

        hookID = 'app.mailingList.callEvents';
        if (HookMgr.hasHook(hookID)) {
            subscriptionResult = HookMgr.callHook(
                hookID,
                hookID.slice(hookID.lastIndexOf('.') + 1),
                {
                    email: subscriptionForm.object.email.value,
                    contactKey: subscriptionForm.object.email.value,
                    locale: locale,
                    emailFormat: "H",
                    optin: "I",
                    optinDate: date,
                    contactSource: "modal"
                }
            );
        }
    } else if (action == "subscribeFooter") {
        subscribed = true;
        noDecorator = true;

        hookID = 'app.mailingList.callEvents';
        if (HookMgr.hasHook(hookID)) {
            subscriptionResult = HookMgr.callHook(
                hookID,
                hookID.slice(hookID.lastIndexOf('.') + 1),
                {
                    email: subscriptionForm.object.email.value,
                    contactKey: subscriptionForm.object.email.value,
                    locale: locale,
                    emailFormat: "H",
                    optin: "I",
                    optinDate: date,
                    contactSource: "footer"
                }
            );
        }
    }

    if (subscriptionResult === true) {
        app.getView({
            ContinueURL: require('dw/web/URLUtils').https('MCSubscription-Submit'),
            showView: 'info'
        }).render('marketingcloud/info');
    }

}

/**
 * The form handler for the sub/unsub forms.
 */
function submit() {
    var subscriptionForm = app.getForm('mcsubscribe');
    var subscribed = false;
    var noDecorator = false;
    var submittedAction;
    var subscriptionResult;

    var date = new Date().toUTCString();
    var locale = request.getLocale();
    if (locale == 'default') {
		locale = 'en';
	}
    var profile = null;
    var preferredAddress = null;
    if (!empty(session.getCustomer().getProfile())) {
    	var profile = session.getCustomer().getProfile();
        var preferredAddress = profile.getAddressBook().getPreferredAddress();
    }
    var hookID;
    var req = request;
    var action = subscriptionForm.object.action.value;

    if (action == "subscribe") {
        subscribed = true;
        noDecorator = true;

        hookID = 'app.mailingList.callEvents';
        if (HookMgr.hasHook(hookID)) {
            subscriptionResult = HookMgr.callHook(
                hookID,
                hookID.slice(hookID.lastIndexOf('.') + 1),
                {
                    email: subscriptionForm.object.email.value,
                    contactKey: subscriptionForm.object.email.value,
                    firstName: subscriptionForm.object.firstname.value,
                    lastName: subscriptionForm.object.lastname.value,
                    zipCode: subscriptionForm.object.postalcode.value,
                    birthDate: subscriptionForm.object.dobmonth.value + "/" + subscriptionForm.object.dobday.value + "/" + subscriptionForm.object.dobyear.value,
                    country: subscriptionForm.object.country.value,
                    address1: !empty(preferredAddress) ? preferredAddress.getAddress1() : "",
                    address2: !empty(preferredAddress) ? preferredAddress.getAddress2() : "",
                    city: !empty(preferredAddress) ? preferredAddress.getCity() : "",
                    state: !empty(preferredAddress) ? preferredAddress.getStateCode() : "",
                    locale: locale,
                    emailFormat: "H",
                    optin: "I",
                    optinDate: date,
                    contactSource: "modal"
                }
            );
        }
    } else if (action == "subscribeFooter") {
        subscribed = true;
        noDecorator = true;

        hookID = 'app.mailingList.callEvents';
        if (HookMgr.hasHook(hookID)) {
            subscriptionResult = HookMgr.callHook(
                hookID,
                hookID.slice(hookID.lastIndexOf('.') + 1),
                {
                    email: subscriptionForm.object.email.value,
                    contactKey: subscriptionForm.object.email.value,
                    firstName: subscriptionForm.object.firstname.value,
                    lastName: subscriptionForm.object.lastname.value,
                    zipCode: subscriptionForm.object.postalcode.value,
                    birthDate: subscriptionForm.object.dobmonth.value + "/" + subscriptionForm.object.dobday.value + "/" + subscriptionForm.object.dobyear.value,
                    country: subscriptionForm.object.country.value,
                    address1: !empty(preferredAddress) ? preferredAddress.getAddress1() : "",
                    address2: !empty(preferredAddress) ? preferredAddress.getAddress2() : "",
                    city: !empty(preferredAddress) ? preferredAddress.getCity() : "",
                    state: !empty(preferredAddress) ? preferredAddress.getStateCode() : "",
                    locale: locale,
                    emailFormat: "H",
                    optin: "I",
                    optinDate: date,
                    contactSource: "footer"
                }
            );
        }
    } else if (action == "unsubscribe") {
        //hookID = 'app.mailingList.unsubscribe';
        hookID = 'app.mailingList.callEvents';
        if (HookMgr.hasHook(hookID)) {
            subscriptionResult = HookMgr.callHook(
                hookID,
                hookID.slice(hookID.lastIndexOf('.') + 1),
                {
                    email: subscriptionForm.object.unsubscribeemail.value,
                    optin: "O",
                    optoutDate: date
                }
            );
        }
    }

    if (subscriptionResult === true) {
        if (action == "unsubscribe") {
            app.getView({
                nodecorator: noDecorator
            }).render('marketingcloud/unsubscribed');
            return;
        } else {
            app.getView({
            }).render('marketingcloud/subscribed');
            return;
        }
    }

    switch(submittedAction.split('_').pop()) {
        case 'subscribe':
            showSubscribe();
            break;
        case 'subscribeFooter':
            showSubscribeFooter();
            break;
        case 'unsubscribe':
            showUnsubscribe();
            break;
    }
}

/**
 * The handler for general use landing page subscribe.
 */
function lpSubscribe() {
    
	var subscriptionForm = app.getForm('mcsubscribe'),
    subscriptionResult,
    date = new Date().toUTCString(),
    locale = request.getLocale(),
    hookID,
    req = request,
    action = subscriptionForm.object.action.value;
    
    if (locale == 'default') {
		locale = 'en';
	}
    
    hookID = 'app.mailingList.callEvents';
    if (HookMgr.hasHook(hookID)) {
        subscriptionResult = HookMgr.callHook(
            hookID,
            hookID.slice(hookID.lastIndexOf('.') + 1),
            {
                email: subscriptionForm.object.email.value,
                contactKey: subscriptionForm.object.email.value,
                firstName: subscriptionForm.object.firstname.value,
                lastName: subscriptionForm.object.lastname.value,
                zipCode: subscriptionForm.object.postalcode.value,
                birthDate: subscriptionForm.object.dobmonth.value + "/" + subscriptionForm.object.dobday.value + "/" + subscriptionForm.object.dobyear.value,
                country: subscriptionForm.object.country.value,
                locale: locale,
                emailFormat: "H",
                optin: "I",
                optinDate: date,
                contactSource: "hiddenlp"
            }
        );
    }

    if (subscriptionResult === true) {
        finalResult = { response: 'submitted' }
    }else{
        finalResult = { response: 'error' }
    }

    responseUtil.renderJSON(finalResult);
}

/* Web exposed methods */

/** Renders the case overview.
 * @see {@link module:controllers/MCSubscription~manage} */
exports.Manage = guard.ensure(['https', 'loggedIn'], manage);
/** @see {@link module:controllers/MCSubscription~savePreferences} */
exports.SavePreferences = guard.ensure(['post', 'https', 'loggedIn'], savePreferences);
/** @see {@link module:controllers/MCSubscription~showSubscribe} */
exports.Subscribe = guard.ensure(['https'], showSubscribe);
/** @see {@link module:controllers/MCSubscription~showSubscribeFooter} */
exports.SubscribeFooter = guard.ensure([], showSubscribeFooter);
/** @see {@link module:controllers/MCSubscription~showUnsubscribe} */
exports.SubscribeInfo = guard.ensure(['https'], showInfoUpdate);
/** @see {@link module:controllers/MCSubscription~SubscribeInfo} */
exports.CheckSubscriber = guard.ensure(['https'], checkSubscriber);
/** @see {@link module:controllers/MCSubscription~CheckSubscriber} */
exports.UpdateFormInfo = guard.ensure(['https'], updateFormInfo);
/** @see {@link module:controllers/MCSubscription~UpdateFormInfo} */
exports.Unsubscribe = guard.ensure(['https'], showUnsubscribe);
/** @see {@link module:controllers/MCSubscription~InitialSubmit} */
exports.InitialSubmit = guard.ensure(['https'], initialSubmit);
/** @see {@link module:controllers/MCSubscription~submit} */
exports.Submit = guard.ensure(['https'], submit);
/** @see {@link module:controllers/MCSubscription~LPSubscribe} */
exports.LPSubscribe = guard.ensure(['https'], lpSubscribe);

'use strict';

/**
 * @type {dw.system.Site}
 */
const Site = require('dw/system/Site');
const ArrayList = require('dw/util/ArrayList');

var mailingListsEnabled = false;

var propertyMap = {
	email: "EMAIL_ADDRESS_",
	contactKey: "ContactKey",
	firstName: "FIRST_NAME",
	lastName: "LAST_NAME",
	zipCode: "POSTAL_CODE_",
	birthDate: "DATE_OF_BIRTH",
	country: "COUNTRY_",
	address1: "POSTAL_STREET_1_",
	address2: "POSTAL_STREET_2_",
	city: "CITY_",
	state: "STATE_",
	locale: "LANG_LOCALE",
	emailFormat: "EMAIL_FORMAT_",
	optinBCBG: "BCBG_EMAIL_PERM_STATUS",
	optinFAC: "BCBG_FACTORY_EMAIL_PERM_STATUS",
	optinGenerations: "GENERATION_EMAIL_PERM_STATUS",
	optinHerveLeger: "HERVE_LEGER_EMAIL_PERM_STATUS",
	optinDateBCBG: "BCBG_OPTIN_DATE",
	optinDateFAC: "BCBG_FACTORY_OPTIN_DATE",
	optinDateGenerations: "GENERATION_OPTIN_DATE",
	optinDateHerveLeger: "HERVE_LEGER_OPTIN_DATE",
	optoutDateBCBG: "BCBG_OPTOUT_DATE",
	optoutDateFAC: "BCBG_FACTORY_OPTOUT_DATE",
	optoutDateGenerations: "GENERATION_OPTOUT_DATE",
	optoutDateHerveLeger: "HERVE_LEGER_OPTOUT_DATE",
	contactSource: "CONTACT_SOURCE"
};

var siteID = Site.getCurrent().getID();

(function mcMailingListsInit(){
    mailingListsEnabled = !!(Site.current.getCustomPreferenceValue('mcEnableMailingLists'));
})();

function subscribe(subscriberData) {
    var defaultLists = (new ArrayList(Site.current.getCustomPreferenceValue('mcDefaultMailingLists'))).toArray();
    var subscriber = require('int_marketing_cloud').subscriber(subscriberData.email);
    var lists = empty(subscriberData.lists) ? defaultLists : lists;
    if (lists instanceof ArrayList) {
        lists = lists.toArray();
    }
    subscriber.assignLists(lists);
    return subscriber.commit();
}

function unsubscribe(subscriberData) {
    var defaultLists = (new ArrayList(Site.current.getCustomPreferenceValue('mcDefaultMailingLists'))).toArray();
    var subscriber = require('int_marketing_cloud').subscriber(subscriberData.email);
    var lists = empty(subscriberData.lists) ? defaultLists : lists;
    if (lists instanceof ArrayList) {
        lists = lists.toArray();
    }
    subscriber.unassignLists(lists);
    return subscriber.commit();
}

function addData(subscriberData) {
	
    var subscriber = require('int_marketing_cloud').subscriber(subscriberData.email);
	var dataExtKeys = !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('marketingCloudSubDataExtKey')) ? dw.system.Site.getCurrent().getCustomPreferenceValue('marketingCloudSubDataExtKey') : "";
	var updateData = {}
	
	for (var key in subscriberData){
		if (key == "optin" || key == "optinDate" || key == "optoutDate") {
			var keyID = key + siteID;
			updateData[propertyMap[keyID]] = subscriberData[key];
		} else {
			updateData[propertyMap[key]] = subscriberData[key];
		}
	}
	for (var i = 0; i < dataExtKeys.length; i++) {
		subscriber.adddata(updateData, dataExtKeys[i]);
	}
	
	return 
	
}

function callEvents(subscriberData) {
	
	
	var results = true;
    var subscriber = require('int_marketing_cloud').subscriber(subscriberData.email);
	var eventKeys = !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('marketingCloudDataExtKeyMap')) ? JSON.parse(dw.system.Site.getCurrent().getCustomPreferenceValue('marketingCloudDataExtKeyMap')) : "";
	
	for (var APIkey in eventKeys){
		//check for email in data extension
		var dataEntry = subscriber.getdata(subscriberData.email, eventKeys[APIkey]);
		var updateData = {}
		
		for (var key in subscriberData){
			if (key == "optin") {
				var keyID = key + siteID;
				updateData[propertyMap[keyID]] = subscriberData[key];
				if (siteID == "BCBG") {
					//optin/optout of FACTORY if the unsubscribe is coming in to BCBG
					var factoryID = key + 'FAC'
					updateData[propertyMap[factoryID]] = subscriberData[key];
				}
			} else if (key == "optinDate" || key == "optoutDate"){
				var keyID = key + siteID;
				var dateArr = subscriberData[key].split(" ");
				var formattedDate = [dateArr[1], dateArr[2], dateArr[3], dateArr[4]].join(" ");
				
				updateData[propertyMap[keyID]] = formattedDate;
				if (siteID == "BCBG") {
					//set optin/optout date of FACTORY if the unsubscribe is coming in to BCBG
					var factoryID = key + 'FAC'
					updateData[propertyMap[factoryID]] = formattedDate;
				}
			} else {
				updateData[propertyMap[key]] = subscriberData[key];
			}
		}
		
		if (dataEntry) {
			if (empty(eventKeys[APIkey])) {
				//if no data extension key is set, skip loop
				continue;
			}
			//update subscriber data
			results = subscriber.adddata(updateData, eventKeys[APIkey]);
		} else {
			if (empty(APIkey)) {
				//if no API key is set, skip loop
				continue;
			}
			if (subscriberData.optin == "O") {
				//user is not in data extension but trying to unsubscribe, skip
				results = true;
			} else {
				//write new subscriber in data extension
				var event = require('int_marketing_cloud').event(subscriberData.email, APIkey);
				for (var dataKey in updateData){
					event.setDataAttribute(dataKey, updateData[dataKey]);
				}
				var result = event.callEvent();
				if (!result) {
					results = false;
				}
			}
			
		}
	}
	//Do not block checkout even if there is an error
	if (subscriberData.contactSource == "billing") {
		results = true;
	}
	
	return results;
	
}

function checkSubscriber(subscriberData) {
	
	var results = true;
    var subscriber = require('int_marketing_cloud').subscriber(subscriberData.email);
	var eventKeys = !empty(dw.system.Site.getCurrent().getCustomPreferenceValue('marketingCloudDataExtKeyMap')) ? JSON.parse(dw.system.Site.getCurrent().getCustomPreferenceValue('marketingCloudDataExtKeyMap')) : "";
	
	for (var APIkey in eventKeys){
		//check for email in data extension
		var dataEntry = subscriber.getdata(subscriberData.email, eventKeys[APIkey]);
		if (dataEntry && results) {
			
		} else {
			//If there is at least one DE that the subscriber is not on, set results equal to false for this loop and all future loops
			results = false;
		}
		
	}
	
	return results;
	
}

// Ensure hooks only fire if enabled
if (mailingListsEnabled) {
    exports.subscribe = subscribe;
    exports.unsubscribe = unsubscribe;
    exports.addData = addData;
	exports.callEvents = callEvents;
	exports.checkSubscriber = checkSubscriber;
} else {
    exports.subscribe = function(){};
    exports.unsubscribe = function(){};
    exports.adddata = function(){};
    exports.callEvent = function(){};
}

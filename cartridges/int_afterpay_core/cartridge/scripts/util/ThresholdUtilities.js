var Transaction = require('dw/system/Transaction');

var configurationService = require("~/cartridge/scripts/logic/services/AfterpayConfigurationService");
var configurationType = require("~/cartridge/scripts/util/AfterpayConstants.js").CONFIGURATION_TYPE;

var SitePreferences = require("~/cartridge/scripts/util/AfterpayUtilities").getSitePreferencesUtilities();
var LogUtils = require('~/cartridge/scripts/util/LogUtils');
var Logger = LogUtils.getLogger("ThresholdUtilities");


var ThresholdUtilities = {
	setThresholdInSession: function() {

		var thresholdObj = ThresholdUtilities;
		//short circuit the call if it's already been made.
		if ('afterPayIsRangeAvailable' in session.custom && session.custom.afterPayIsRangeAvailable === true) {
			return;
		}

		var result = thresholdObj.getThresholdFromPreferences();
		if(result.error === true) {
			result = thresholdObj.getThresholdFromAfterpay();
		}
		return result;
	},
	getThresholdFromPreferences: function() {
		var minThreshold = SitePreferences.getMinThreshold();
		var maxThreshold = SitePreferences.getMaxThreshold();
		var afterPayIsRangeAvailable = (minThreshold > 0 && maxThreshold > 0);

		if (afterPayIsRangeAvailable) {
			session.custom.afterPayIsRangeAvailable = true;
			session.custom.afterPayMinAmount = parseFloat(minThreshold, 10);
			session.custom.afterPayMaxAmount = parseFloat(maxThreshold, 10);
			return {
				error : false
			};
		} else {
			return {
				error : true
			};
		}
	},
	getThresholdFromAfterpay: function() {
		configurationService.init();
		configurationService.generateRequest();
		var thresholdResponse;
		try {
			thresholdResponse = configurationService.getResponse();
			Logger.debug("service response to get the threshold amount :" + JSON.stringify(thresholdResponse));
		} catch (e) {
			Logger.debug("Exception occured to set the threshold amount in session :"+e)
			return {
				error : true
			};
			
		}
			
		var threshold;
		
		if(thresholdResponse.length > 0){
			for each(threshold in thresholdResponse){
				if(threshold.type == configurationType.PAY_BY_INSTALLMENT && (parseFloat(threshold.minimumAmount.amount, 10) - parseFloat(threshold.maximumAmount.amount, 10)) != 0.0){
					Transaction.begin();
					session.custom.afterPayIsRangeAvailable = true;
					session.custom.afterPayMinAmount = parseFloat(threshold.minimumAmount.amount, 10);
					session.custom.afterPayMaxAmount = parseFloat(threshold.maximumAmount.amount, 10);
					Transaction.commit();
					break;
				}
			}
		}
	},
	getThreshold: function() {
		return {
			isRangeAvailable: session.custom.afterPayIsRangeAvailable || false,
			minAmount: session.custom.afterPayMinAmount || 0.0,
			maxAmount: session.custom.afterPayMaxAmount || 0.0
		};
	}
};

module.exports = ThresholdUtilities;

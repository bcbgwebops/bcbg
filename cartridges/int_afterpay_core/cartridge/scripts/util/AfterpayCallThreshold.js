
importPackage( dw.system );

var LogUtils = require('~/cartridge/scripts/util/LogUtils');
var Logger = LogUtils.getLogger("AfterpayCallThreshold");

function execute( args : PipelineDictionary ) : Number
{
	var response = setThreshold();
	
	if(response.error){
		return PIPELET_ERROR;
	}
    	
   return PIPELET_NEXT;
}

function setThreshold(){
	
	try{
		var ThresholdUtilities = require('~/cartridge/scripts/util/ThresholdUtilities');
		var threshold = ThresholdUtilities.getThreshold();
		//check for session variables and range before proceeding.
		if(threshold.isRangeAvailable !== true) {
			ThresholdUtilities.setThresholdInSession();
		}
		return {error : false};	
	}catch (exception) {
		Logger.error("Exception to set threshold in session: "+exception);
		return {
			error : true
		};
	}
}


/*
 * Module exports
 */
module.exports = {
	SetThreshold: function(){
		return setThreshold();
	}
}
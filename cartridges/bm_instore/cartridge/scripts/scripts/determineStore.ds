/**
* determineStore.ds - for a given login id, this function returns the store that is associated with the login
*     if the BM user is found, and there is a corresponding store, the store is output
*     if the BM user is not found, the store output is set to NULL
*
*   @input LoginName : String 
*	@output Status : String
*   @output StoreId : String
*
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.object );

function execute( args : PipelineDictionary ) : Number
{
	var loginName : String = args.LoginName;
	
	if (empty(loginName)) {
		args.Status = "determineStore: login name is missing";
		return PIPELET_ERROR;
	}
	
	var storeCredentials : SeekableIterator = CustomObjectMgr.getAllCustomObjects("storeCredentials");	
	var storeId : String = "";
	
	while (storeCredentials.hasNext()) {
		var storeCredential : CustomObject = storeCredentials.next();
		if (storeCredential.custom.eaStoreUsername == loginName) {
			storeId = storeCredential.custom.storeId;
			break;
		}
	}
	
	args.StoreId = storeId;
   	return PIPELET_NEXT;
}

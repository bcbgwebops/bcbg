'use strict';

/**
 * @function
 * @description Initializes the tooltip-content and layout
 */
exports.init = function () {
	$(document).tooltip({
		items: '.tooltip',
		track: true,
		content: function () {
			return $(this).find('.tooltip-content').html();
		},
		position: {
			my: "right bottom-20",
			at: "right bottom",
			using: function( position, feedback ) {
				$( this ).css( position );
				$( "<div>" )
					.addClass( "arrow" )
					.addClass( feedback.vertical )
					.addClass( feedback.horizontal )
					.appendTo( this );
			}
		}
	});
};

'use strict';

var dialog = require('./dialog');
var util = require('./util');
var formPrepare = require('./pages/checkout/formPrepare.js');
exports.init = function () {
	var pageCount = parseInt($('#responsys_PageCount').val());
	var closeDate = parseInt($('#responsys_GuestCookieDuration').val());
	var registeredDate = parseInt($('#responsys_RegisteredCookieDuration').val());
	
	var showDialog = false;
	var cookie = util.getCookie('ResponsysSession');
	
	if (cookie=='') {
		if (pageCount == 0) {
			showDialog = true;
		}
		// set our page count to 1 viewing
		var expireDate = new Date(new Date().getTime() + (30 * 24 * 60 * 60 * 1000));
		document.cookie = 'ResponsysSession=1|0|0; expires='+ expireDate +'; path=/;';
	} else {
		var parts = cookie.split('|');
		var cookiePageCount = parseInt(parts[0]);
		var cookieCloseDate = parseInt(parts[1]);
		var cookieRegisteredDate = parseInt(parts[2]);
		
		if (cookiePageCount >= pageCount) {
			showDialog = true;
		}  
		
		// increment our viewing page count
		if (cookiePageCount != -1){
			cookiePageCount++;
		}
		
		// check to see if we have clicked to registered at some point
		if (showDialog == false && cookieRegisteredDate != -1) {
			if (new Date() > new Date(cookieRegisteredDate)) {
				showDialog = true;
			}
		// check to see if we have closed the dialog at some point
		} else if (showDialog == false && cookieCloseDate != -1) {
			var cdc = new Date(cookieCloseDate);
			if (new Date() > cdc) {
				showDialog = true;
			}
		}
	}

	if (showDialog) {
		var dwidth = 600;
		if ($('#wrapper').width() < 568) {
			dwidth = 300;
		} 

		dialog.open({
			url: Urls.mcSubscribeModal,
			options: {
				dialogClass: 'responsys-first-time',
				width: dwidth,
				open: function(evt,ui) {
					$('.non-cookie-subscribe input').addClass('valid');
					$('.non-cookie-subscribe input').attr("placeholder", "Enter Your Email");
					$('.non-cookie-subscribe input').blur(function(){
						var validEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
						if (validEmail.test(this.value)) {
							$('.non-cookie-subscribe button').click(function(){
								$('.non-cookie-subscribe input').removeClass('error');
							})
							$('.non-cookie-subscribe input').removeClass('error');
						} else {
							$('.non-cookie-subscribe button').click(function(){
								$('.non-cookie-subscribe input').addClass('error');
							})
							$('.non-cookie-subscribe input').addClass('error');
							$('.non-cookie-subscribe input').val("");
							$('.non-cookie-subscribe input').attr("placeholder", "Please enter a valid email");
						}
					})
					mcBindModal();
					$('.emailPopUpCloseArea').click(function() {
						dialog.close();
						
						$(".email-signup-form1 input").val('');
						
					});
				}
			}
		});

		// set our current cookiePageCount
		var expireDate = new Date(new Date().getTime() + (30 * 24 * 60 * 60 * 1000));
		document.cookie = 'ResponsysSession='+cookiePageCount+'|'+cookieCloseDate+'|'+cookieRegisteredDate+'; expires='+ expireDate +'; path=/;';
	}

	// check email validity and run modal if success
	$('.email-signup-form input').blur(function() {
		var validEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (validEmail.test(this.value)) {
			$('.mc-subscribe-start').click(function(){
				$('#emailSubscribe').removeClass('error');
			})
			$('.email-signup-form input').removeClass('error');
			$('.email-signup-form input').attr("placeholder", "Email Sign Up");
			if( $('.mc-subscribe-start').is('*') ){  

				$('body').on('click', '.mc-subscribe-start', function(){
					
					var email = $("#emailSubscribe").val(); 
					
					$.post( Urls.mcCheckSubscriber, {
						email: email,
						action: "subscribeFooter"
					}, function(data) {
						if (data.response == true) {
							$('.emailAlreadyUsed').css('display', 'block');
							return;
						//User is signed up
						} else {
							dialog.open({
								url: Urls.mcSubscribeInitialSubmit,
								options: { 
									dialogClass: 'responsys-first-time',
									width: dwidth,
									open: function(evt,ui) {
										mcBindInfo();
										$("#emailSubscribe").val('');
										$('.emailPopUpCloseArea').click(function() {
											dialog.close();
											$('#dwfrm_mcsubscribe_email').val(email);
											$("#emailSubscribe").val('');
											$(".email-signup-form input").val('');
											
										});
									}
								}
							});
						}
					}, 'json' 
					);
					
				})
				
			}
		} 
		else{
			$('.mc-subscribe-start').click(function(){
				$('#emailSubscribe').addClass('error');
			})
			$('.email-signup-form input').addClass('error');
			$('.email-signup-form input').val("");
			$('.email-signup-form input').attr("placeholder", "PLEASE ENTER A VALID EMAIL ADDRESS");
		}
	});

	$('.mc-subscribe-start').click(function(){
		if( !$('.email-signup-form input').val() ){
			$('.email-signup-form input').addClass('error');
			$('.email-signup-form input').attr("placeholder", "PLEASE ENTER A VALID EMAIL ADDRESS");
		}
	});

	$('.emailAlreadyUsed .close').click(function(){
		$('.emailAlreadyUsed').css('display', 'none');
	});
		// check email validity and run modal if success
		$('.socialCol .email-signup-form input').blur(function() {
			var validEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
			if (validEmail.test(this.value)) {
				$('.socialCol .email-signup-form .mc-subscribe-start').click(function(){
					$('.socialCol .email-signup-form input').removeClass('error');
				})
				$('.socialCol .email-signup-form input').removeClass('error');
				$('.socialCol .email-signup-form input').attr("placeholder", "Email Sign Up");
				if( $('.socialCol .mc-subscribe-start').is('*') ){  
					
					$('body').on('click', '.socialCol .mc-subscribe-start', function(){
						
						var email = $(".socialCol .email-signup-form input").val(); 
						
						$.post( Urls.mcCheckSubscriber, {
							email: email,
							action: "subscribeFooter"
						}, function(data) {
							if (data.response == true) {
								$('.socialCol .emailAlreadyUsed').css('display', 'block');
								return;
							//User is signed up
							} else {
								dialog.open({
									url: Urls.mcSubscribeInitialSubmit,
									options: { 
										dialogClass: 'responsys-first-time',
										width: dwidth,
										open: function(evt,ui) {
											mcBindInfo();
											$(".socialCol .email-signup-form input").val('');
											$('.emailPopUpCloseArea').click(function() {
												dialog.close();
												$('#dwfrm_mcsubscribe_email').val(email);
												$(".socialCol .email-signup-form input").val('');
												$(".socialCol .email-signup-form input").val('');
												
											});
										}
									}
								});
							}
						}, 'json' 
						);
						
					})
					
				}
			} 
			else{
				$('.socialCol .mc-subscribe-start').click(function(){
					$('.socialCol #emailSubscribe').addClass('error');
				})
				$('.socialCol .email-signup-form input').addClass('error');
				$('.socialCol .email-signup-form input').val("");
				$('.socialCol .email-signup-form input').attr("placeholder", "Please enter a valid email");
			}
		});
	
		$('.socialCol .mc-subscribe-start').click(function(){
			if( !$('.socialCol .email-signup-form input').val() ){
				$('.socialCol .email-signup-form input').addClass('error');
				$('.socialCol .email-signup-form input').attr("placeholder", "Please enter a valid email");
			}
		});
	
		$('.socialCol .emailAlreadyUsed .close').click(function(){
			$('.socialCol .emailAlreadyUsed').css('display', 'none');
		});
	function mcBindModal() {
		
		$('#MCSubscriptionForm').submit(function(e) {
			e.preventDefault(); 
			var email = $("#dwfrm_mcsubscribe_email").val(); 
					
			$.post( Urls.mcCheckSubscriber, {
				email: email,
				action: "subscribe"
			}, function(data) {
				if( data.response == true ) {
					//User is signed up
					$('#MCSubscriptionForm input').val('');
					$('#MCSubscriptionForm input').addClass('error');
					$('#MCSubscriptionForm input').attr("placeholder", "Email Already Subscribed");
					return;
				} else {
					dialog.open({
						url: Urls.mcSubscribeInitialSubmit,
						options: {
							dialogClass: 'responsys-first-time',
							width: dwidth,
							open: function(evt,ui) {
								mcBindInfo();
								$('.emailPopUpCloseArea').click(function() {
									dialog.close();
									
									$(".email-signup-form input").val('');
									
								});
							}
						} 
					});
				}
			}, 'json' 
			);
		});
	}

	function mcBindInfo () {
		$('#MCSubscriptionFormInfo').submit(function (e) {
			e.preventDefault(); 
			var fName = $('#dwfrm_mcsubscribe_firstname').val();
			var lName = $('#dwfrm_mcsubscribe_lastname').val();
			fName = escapeRegExp(fName);
			lName = escapeRegExp(lName);
			$.post( Urls.mcUpdateFormInfo, $('#MCSubscriptionFormInfo').serialize(), function(data) {
				if (data.response == true) {
					dialog.replace({
						url: Urls.mcSubscribeSubmit,
						options: {
							dialogClass: 'responsys-first-time',
							width: dwidth,
							open: function(evt,ui) {
								$('.emailPopUpCloseArea').click(function() {
									dialog.close();
									
									$(".email-signup-form input").val('');
									
								});
							}
						}
					});
				} else {
					
				}
			}, 'json' 
			);
		})
		formPrepare.init({
			continueSelector: '[id="mc-subscribe-submit-info"]',
			formSelector: 'form[id="MCSubscriptionFormInfo"]'
		});
		formPrepare.validateForm();
	}

	$('.responsys-first-time').on('dialogclose', function(event, ui) {
		// We've hit dialog close, so we need to add our site preference number of days to the current date
		var newDate = new Date(new Date().getTime() + (closeDate * 24 * 60 * 60 * 1000));
		document.cookie = 'ResponsysSession=-1|'+newDate.getTime()+'|-1; expires='+ newDate +'; path=/;';
		$(".email-signup-form1 input").val('');
	});
	
    $(document).on('submit', '#promotional-signup-email-container', function(e) {
    	e.preventDefault();
    	if ($(this).valid() == true) {
    		$.post($(this).attr('action'), $(this).serialize(), function(data) {
				$('.responsys-first-time').addClass('responsys-first-time-second-page');
    			$('#dialog-container').html(data);

    			$('.emailPopUpCloseArea').on('click', function() {
					dialog.close();
					
					$(".email-signup-form1 input").val('');
					
				});
    			
    			// We've hit dialog register button, so we need to add our site preference number of days to the current date
    			var newDate = new Date(new Date().getTime() + (registeredDate * 24 * 60 * 60 * 1000));
    			document.cookie = 'ResponsysSession=-1|'+newDate.getTime()+'|-1; expires='+ newDate +'; path=/;';
    		});
    	}  	
    });	
    
    $(document).on('submit', '#dwfrm_emailsubscription', function(e) {
    	$('.countryRequired').hide();
    	e.preventDefault();
    	if ($('#dwfrm_emailsubscription_country').val() == '-1') {
    		$('.countryRequired').show();
    		return;
    	}
    	if ($(this).valid() == true) {
    		$.post($(this).attr('action'), $(this).serialize(), function(data) {
    			$('#dialog-container').html(data);

    			$("#dialog-container").dialog("option", "position", {my: "center", at: "center", of: window});
    			
    			// We've hit dialog register button, so we need to add our site preference number of days to the current date
    			var newDate = new Date(new Date().getTime() + (registeredDate * 24 * 60 * 60 * 1000));
    			document.cookie = 'ResponsysSession=-1|'+newDate.getTime()+'|-1; expires='+ newDate +'; path=/;';
    			//dialog.close();
    			$('.emailPopUpCloseArea').on('click', function() {
					dialog.close();
					
					$(".email-signup-form1 input").val('');
					
				});
			    
				var utag_data = new Object();
			    if (localStorage.getItem("signupformheader") !== null) {
					if (Boolean(parseInt(localStorage.getItem("signupformheader")))) {
						utag_data['customer_email'] = $(".esformheader .email").val();
					} else {
						utag_data['customer_email'] = $(".esformfooter .email:not('#es-email-address-footer')").val();
					}
				}
				utag_data['event_type'] = 'email_signup';
				utag.link(utag_data);
    		});
    	}  	
    });	     
    
    $("form.email-signup-form1").validate({
	    onfocusin: false,
	    onfocusout:false,
	    onsubmit:false 
	});
    
    $(document).on('submit', '.email-signup-form1', function(e) {
    	e.preventDefault();
		

		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if($(".email").val() != ''){
			if(regex.test($(".email").val()) == false){
				$(".email").val('').attr("placeholder",Resources.FOOTER_EMAIL_VALIDATION).addClass("placeholder");
				return false;
			}
		}
		else {
			$(".email").val('').attr("placeholder",Resources.FOOTER_EMAIL_VALIDATION).addClass("placeholder");
			$("#es-email-address-error").hide();
			return false;
		}
		
		if ($(this).valid() == true) {
			$(".signup").slideToggle();
			$(".signup-overlay").toggle();
			dialog.open({
				url: util.appendParamToURL($(this).attr('action'), 'email', $('.email', this).val()),
				options: {
					dialogClass: 'responsys',
					width: "auto",					
					open: function(evt, ui) {
						$('.emailPopUpCloseArea').on('click', function() {
							dialog.close();
							
							$(".email-signup-form1 input").val('');
							
						});
						
					}
				}
			});
		}
		
    });	
    
    $(document).on('submit', '.email-signup-form', function(e) {
    	e.preventDefault();
	});	
	
	function fluidDialog() {
		var $visible = $(".ui-dialog:visible");
		$visible.each(function () {
		  var $this = $(this);
		  var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
		  var wWidth = $(window).width();
		  if (wWidth < (parseInt(dialog.options.maxWidth, 10) + 50)) {
			$this.css("max-width", "90%");
			$this.css("width", "auto");
		  } else {
			$this.css("max-width", dialog.options.maxWidth + "px");
		  }
		  dialog.option("position", dialog.options.position);
		});
	  }
	  $(window).resize(function () {
		fluidDialog();
	  });
	  $(document).on("dialogopen", ".ui-dialog", function () {
		fluidDialog();
	  });
    function escapeRegExp(string){
		return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
	}
    $(document).on('click', '.esformheader .email-signup-form1 input[type=submit]', function(e) {
		localStorage.setItem("signupformheader", 1);
	});
	
	$(document).on('click', '.esformfooter .email-signup-form1 input[type=submit]', function(e) {
		localStorage.setItem("signupformheader", 0);
	});
	
	$(document).on('click', '.mobile-sign-up .esformfooter .email-signup-form input[type=submit]', function(e) {
		var utag_data = new Object();
		utag_data['customer_email'] = $(".esformfooter #es-email-address-footer").val();
		utag_data['event_type'] = 'email_signup';
		utag.link(utag_data);
	});
}	

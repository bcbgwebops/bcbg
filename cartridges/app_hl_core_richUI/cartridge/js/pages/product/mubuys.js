'use strict';

/**
 * @description Creates product mybuys carousel using jQuery jcarousel plugin
 **/
module.exports = function () {
	var $carousel = $('.jcarousel-mb .MB_PROD1');
	if (!$carousel || $carousel.length === 0 || $carousel.children().length === 0) {
		return;
	}
	$carousel.jcarousel();
	$('.jcarousel-mb .jcarousel-control-prev-mb')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '-=1'
		});

	$('.jcarousel-mb .jcarousel-control-next-mb')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '+=1'
		});
};

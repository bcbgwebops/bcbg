'use strict';

/**
 * @description Creates product thumbnails carousel using jQuery jcarousel plugin
 **/
module.exports = function () {
	var $carousel = $('.product-thumbnails');
	if (!$carousel || $carousel.length === 0 || $carousel.children().length === 0) {
		return;
	}
	
	// this block added to replace MagicZoom auto-gallery bc it autoloads thumbnail zooms even if lazyZoom is set to true
	$('.thumbnail-link', $carousel).each(function(){
		$(this).on('click', function(e){
			e.preventDefault();
			MagicZoom.update('zoom', $(this).attr('href'), $(this).attr('data-image'));
		});
	});
	
	$(window).resize(function() {
	    if ($('#wrapper').width() < 768) {
	    	$carousel.jcarousel();
	    } else {
	    	$carousel.jcarousel({
	            vertical: true
	        });
	    }
	    $carousel.on('jcarousel:reload jcarousel:create', function() {
	    	$carousel = $(this);
	    });
	}).resize();

	$('.jcarousel-prev-image')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '-=1'
		});

	$('.jcarousel-next-image')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '+=1'
		});
};

'use strict';

/**
 * @description Creates product recommendation carousel using jQuery jcarousel plugin
 **/
module.exports = function () {
	var $carousel = $('div#carousel-recommendations');
	if (!$carousel || $carousel.length === 0 || $carousel.children().length === 0) {
		return;
	}
	$carousel.jcarousel();
	$('div#carousel-recommendations .jcarousel-prev')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '-=2'
		});

	$('div#carousel-recommendations .jcarousel-next')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '+=2'
		});


	var myElement = document.getElementById('carousel-recommendations');
	var mc = new Hammer(myElement);
		mc.on("panleft", function(ev) {
			    $("div#carousel-recommendations").jcarousel('scroll', '+=2');
		});		
		mc.on("panright", function(ev) {
			    $("div#carousel-recommendations").jcarousel('scroll', '-=2');
		});


    $('#carousel-recommendations')
        .on('jcarousel:reload jcarousel:create', function () {
            var carousel = $(this),
                width = carousel.innerWidth();
	        if (width > 300) {
	            width = width / 4;
	        } else {
	            width = width / 2;
	        }

            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
        })
        .jcarousel({
        }); 
        
			
};

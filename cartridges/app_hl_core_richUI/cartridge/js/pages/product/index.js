'use strict';

var dialog = require('../../dialog'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util'),
    addToCart = require('./addToCart'),
    availability = require('./availability'),
    image = require('./image'),
    productNav = require('./productNav'),
    productSet = require('./productSet'),
    thumbnails = require('./thumbnails'),
    sizeChart = require('./sizeChart'),
    recommendations = require('./recommendations'),
    mybuys = require('./mubuys'),
    variant = require('./variant');

/**
 * @description Initialize product detail page with reviews, recommendation and product navigation.
 */
function initializeDom() {
    // [BCB-381] The page attempts to load all the product images, then dynamically removes the ones that 404.
    // When MagicZoom is autoinitialized, this causes a race condition resulting in broken images in the
    // gallery popup. Delay MagicZoom initialization till after the DOM is loaded.
    var mobileHeroImgIndex = 0;
    var mobileHeroImgMax = $('#prodThumbnailCarousel li').length;
    if ($('.js-pdp-mobile-image').is('*')) {
        $(window).load(function () {
            MagicZoom.start();
        });
    } else {
        $('.js-pdp-alt-image').on('lazyloaded', function () {
            MagicZoom.start(this.parentNode);
        });
    }

    // these two blocks have been altered bc MagicZoom autoloads thumbnail zooms even if lazyZoom is set to true
    $(".mobile-prev-image-nav").click(function (event) {
        event.stopImmediatePropagation();
        //MagicZoom.prev('zoom');
        var next = (mobileHeroImgIndex - 1 > -1) ? mobileHeroImgIndex - 1 : mobileHeroImgMax - 1;
        var nextDataEl = $('#prodThumbnailCarousel li')[next];
        nextDataEl = $('a', $(nextDataEl));
        MagicZoom.update('zoom', nextDataEl.attr('href'), nextDataEl.attr('data-image'));
        mobileHeroImgIndex = next;
    });

    $(".mobile-next-image-nav").click(function (event) {
        event.stopImmediatePropagation();
        //MagicZoom.next('zoom');
        var next = (mobileHeroImgIndex + 1 < mobileHeroImgMax) ? mobileHeroImgIndex + 1 : 0;
        var nextDataEl = $('#prodThumbnailCarousel li')[next];
        nextDataEl = $('a', $(nextDataEl));
        MagicZoom.update('zoom', nextDataEl.attr('href'), nextDataEl.attr('data-image'));
        mobileHeroImgIndex = next;
    });

    $('.js-primary-image').imagesLoaded(function () {
        $('.js-mobile-image-arrow').removeClass('is-hidden');
    });

    $(".mobile-prev-image-nav").click(function (event) {
        event.stopImmediatePropagation();
        MagicZoom.prev('zoom');
    });

    $(".mobile-next-image-nav").click(function (event) {
        event.stopImmediatePropagation();
        MagicZoom.next('zoom');
    });

    $('#pdpMain .product-detail .product-tabs').tabs({
        activate: function (event, ui) {
            var utag_data = $('.add-wishlist').data('utag');
            utag_data['event_type'] = 'product_tab';
            utag.link(utag_data);
        }
    });
    productNav();
    thumbnails();
    recommendations();
    mybuys();
    tooltip.init();

    // begin of ECOM-586
    $("#reviewsAnswers").clone().appendTo("#reviewsAnswers-mobile");
    $(".size-chart-div").clone().appendTo("#size-chart-mobilewrap");
    $(".add-to-wishlist").clone().appendTo(".add-this-toolbox");

    /* Clone powerreviews stars for mobile pdp content*/
    $("#reviewsnip").clone().appendTo("#reviewsnip-mobile");
    $("#reviewsnip-mobile").children().first().removeAttr('id');

    /* clone price div to top on mobile */
    $(".main-price .price-standard:first").clone().appendTo(".price_for_mobile");
    $(".main-price .price-sales:first").clone().appendTo(".price_for_mobile");

    /* clone promotion-callout div to top on mobile */
    $(".promotion .promotion-callout").clone().appendTo(".price_for_mobile");
    // end of ECOM-586

    //If there is no active promotion, remove the element to close whitespace gap.
    if ($('.promotion .promotion-callout .promo-callout-message').is(':empty')) {
        $("#product-content .promotion").hide();
    }
}

/**
 * @description Initialize event handlers on product detail page
 */
function initializeEvents() {
    var $pdpMain = $('#pdpMain');

    addToCart();
    availability();
    variant();
    image();
    productSet();
    sizeChart();
    if (SitePreferences.STORE_PICKUP) {
        productStoreInventory.init();
    }

    // Add to Wishlist and Add to Gift Registry links behaviors
    $pdpMain.on('click', '[data-action="wishlist"], [data-action="gift-registry"]', function () {
        var data = util.getQueryStringParams($('.pdpForm').serialize());
        if (data.cartAction) {
            delete data.cartAction;
        }
        var url = util.appendParamsToUrl(this.href, data);
        this.setAttribute('href', url);
    });

    // product options
    $pdpMain.on('change', '.product-options select', function () {
        var salesPrice = $pdpMain.find('.product-add-to-cart .price-sales');
        var selectedItem = $(this).children().filter(':selected').first();
        salesPrice.text(selectedItem.data('combined'));
    });

    // prevent default behavior of add this Button
    $pdpMain.on('click', '.unselectable a', function (e) {
        e.preventDefault();
    });

    $('.size-chart-link a').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href')
        });
    });

    $(document).on('click', '.product-actions a.print', function () {
        var utag_data = $('.add-wishlist').data('utag');
        utag_data['event_type'] = 'product_print';
        utag.link(utag_data);
    });

    $(document).on('click', '.addthis_default_style > a > span', function () {
        var utag_data = $('.add-wishlist').data('utag');
        utag_data['event_type'] = 'product_social_share';
        utag.link(utag_data);
    });

    // begin of ECOM-586
    $('.facebook').click(function (e) {
        e.preventDefault();
        var loc = window.location;
        var title = encodeURIComponent($(this).attr('title'));
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + loc + '&text=' + title + '&', 'facebookwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    });


    $('.tweet').click(function (e) {
        e.preventDefault();
        var loc = window.location;
        var title = encodeURIComponent($(this).attr('title'));
        window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    });

    // end of ECOM-586
}

var product = {
    initializeEvents: initializeEvents,
    init: function () {
        initializeDom();
        initializeEvents();
    }
};

module.exports = product;




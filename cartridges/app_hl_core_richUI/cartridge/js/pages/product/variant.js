'use strict';

var addToCart = require('./addToCart'),
    ajax = require('../../ajax'),
    image = require('./image'),
    progress = require('../../progress'),
    productStoreInventory = require('../../storeinventory/product'),
    tooltip = require('../../tooltip'),
    util = require('../../util');


var variantHandleEstimateResponse = function (payment_estimate, a) {
    var dollars = payment_estimate.payment_string;

    var logo = '<img src="data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzEuNjQgMTY2LjA0Ij48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6IzIzMWYyMDt9PC9zdHlsZT48L2RlZnM+PHRpdGxlPmxvZ290eXBlX2JsazwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMjk4LjM5LTEuOTJBMTcuNTcsMTcuNTcsMCwxLDAsMzE2LDE1LjY1LDE3LjU5LDE3LjU5LDAsMCwwLDI5OC4zOS0xLjkyWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxLjkyKSIvPjxyZWN0IGNsYXNzPSJjbHMtMSIgeD0iMjgzLjMiIHk9IjQ2LjY4IiB3aWR0aD0iMjkuOTkiIGhlaWdodD0iMTE5LjMxIi8+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNNDAzLjQ3LDQxLjY5Yy0xNSwwLTMyLjI1LDEwLjgtMzcuOTMsMjQuMzRWNDQuNzZIMzM3LjA5VjE2NC4wOGgzMFYxMDguNjdjMC0yMy40NSw5LTM2LjU0LDI4LjYxLTM2LjkxbDE2Ljc1LTI5LjM4QTY0LjE3LDY0LjE3LDAsMCwwLDQwMy40Nyw0MS42OVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMS45MikiLz48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik01MjQsNDEuNzdjLTEyLjg3LDAtMjQuNDEsNC44NC0zMi41LDEzLjYybC0wLjQyLjQ1LTAuNDEtLjQ1Yy04LTguNzgtMTkuNDgtMTMuNjItMzIuMzYtMTMuNjItMjcuNTgsMC00Ny42LDIwLjExLTQ3LjYsNDcuODF2NzQuNTNoMjkuNTJWODguOTRjMC0xMS42Myw3LjEtMTkuMTQsMTguMDgtMTkuMTRzMTguMDksNy41MSwxOC4wOSwxOS4xNHY3NS4xN0g1MDZWODguOTRjMC0xMS42Myw3LjEtMTkuMTQsMTguMDktMTkuMTRzMTguMDksNy41MSwxOC4wOSwxOS4xNHY3NS4xN2gyOS41MVY4OS41OEM1NzEuNjQsNjEuODgsNTUxLjYyLDQxLjc3LDUyNCw0MS43N1oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMS45MikiLz48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik0yNDcsMzMuNzdjMC0zLjk0LjUzLTguNjUsMy45LTExLjI0LDMuNjktMi44Nyw5LjA5LTIuMzQsMTMuNDYtMi4xMmw1LjgyLTIxLjcyLTMuMTEtLjE2Yy0xMi41Mi0uNjctMjUuNjktMS42LTM2LjY3LDUuNjYtOS4zMSw2LjE1LTEzLjQ1LDE2Ljc0LTEzLjQ1LDI3LjYxdjEzSDE4MS4xOHYtMTFjMC0zLjkyLjUyLTguNTgsMy44Mi0xMS4yLDMuNjktMi45Myw5LjE2LTIuMzgsMTMuNTMtMi4xNmw1LjgyLTIxLjcyLTMuMTEtLjE2Yy0xMi42Mi0uNjgtMjUuOTItMS42LTM2LjkxLDUuODYtOS4xMiw2LjE4LTEzLjE2LDE2LjY4LTEzLjE2LDI3LjQydjEzSDEzNy42NVY2Ni40NWgxMy41M3Y5Ny42M2gzMFY2Ni40NWgzNS43NnY5Ny42M2gzMFY2Ni40NWgyMC43N1Y0NC43NkgyNDd2LTExWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxLjkyKSIvPjxwYXRoIGNsYXNzPSJjbHMtMSIgZD0iTTEyMi40MywxNjQuMDlsMC0xMDUuMzZhMTcuMzIsMTcuMzIsMCwwLDAtMTUuMzItMTdjLTUuNzMtLjM3LTExLjgyLDEuNzMtMTUuNDEsNi4zOEwwLDE2NC4wOEgyMi42YzksMCwxNi4xOC00LjY5LDIxLjYxLTExLjdMOTUsODguMTV2NzUuOTRoMjcuNDVaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDEuOTIpIi8+PC9zdmc+" style="height:1em; margin:0 .3em .15em;vertical-align:bottom;">';

    // Define the text surrounding the monthly payment price
    var content = "or as low as $" + dollars + "/mo with" + logo + "<br /><span>Learn more</span>";

    a.innerHTML = content;
    a.onclick = payment_estimate.open_modal;
    a.style.visibility = "visible";
};

/**
 * @description update product content with new variant from href, load new content to #product-content panel
 * @param {String} href - url of the new product variant
 **/
var updateContent = function (href) {
    var $pdpForm = $('.pdpForm'),
        qty = $pdpForm.find('input[name="Quantity"]').first().val(),
        params = {
            Quantity: isNaN(qty) ? '1' : qty,
            format: 'ajax',
            productlistid: $pdpForm.find('input[name="productlistid"]').first().val(),
            uuid: $pdpForm.find('input[name="uuid"]').first().val()
        };

    progress.show($('#pdpMain'));

    ajax.load({
        url: util.appendParamsToUrl(href, params),
        target: $('#product-content'),
        callback: function () {
            addToCart();
            if (SitePreferences.STORE_PICKUP) {
                productStoreInventory.init();
            }
            image.replaceImages();
            tooltip.init();
            sizeButtonWidths();

            var amtDiv = $('#product-content').find("#affirm-price");

            progress.hide();

            if (typeof (tfcapi) !== 'undefined' && tfcapi) { tfcapi('calculate'); }
            if (typeof (sr_updateMessages) !== 'undefined' && sr_updateMessages) { sr_updateMessages(); }

            if (typeof (affirm) !== 'undefined' && affirm) {
                var amount = amtDiv.attr("data-affirm-price");

                var a = document.getElementById('affirm-learn-more');

                // Only display as low as for items over $50 and less than $17500  
                if ((amount == null) || (amount > 1750000) || (amount < 50)) { return; }

                // Define payment estimate options
                var options = {
                    apr: "0.10", // percentage assumed APR for loan
                    months: 12, // can be 3, 6, or 12
                    amount: amount // USD cents
                }


                affirm.ui.payments.get_estimate(options, function (response) { variantHandleEstimateResponse(response, a); });
            }
        }
    });
};

function sizeButtonWidths(pdpMain) {
    // resize size button widths to match widest button width
    var w = 0;
    $('a.size-variation ', pdpMain).each(function () {
        w = $(this).outerWidth() > w ? $(this).outerWidth() : w;
    }).each(function () {
        $(this).outerWidth(w + 1);
    });
}


module.exports = function () {
    var $pdpMain = $('#pdpMain');

    sizeButtonWidths($pdpMain);

    // resize size button widths to match widest button width
    var w = 0;
    $('a.size-variation ', $pdpMain).each(function () {
        w = $(this).outerWidth() > w ? $(this).outerWidth() : w;
    }).each(function () {
        $(this).outerWidth(w + 1);
    });

    // hover on swatch - should update main image with swatch image
    $pdpMain.on('mouseenter mouseleave', '.swatchanchor', function () {
        var largeImg = $(this).data('lgimg'),
            $imgZoom = $pdpMain.find('.main-image'),
            $mainImage = $pdpMain.find('.primary-image');

        if (!largeImg) { return; }
        // store the old data from main image for mouseleave handler
        $(this).data('lgimg', {
            hires: $imgZoom.attr('href'),
            url: $mainImage.attr('src'),
            alt: $mainImage.attr('alt'),
            title: $mainImage.attr('title')
        });
        // set the main image
        image.setMainImage(largeImg);
    });

    // click on swatch - should replace product content with new variant
    $pdpMain.on('click', '.product-detail .swatchanchor', function (e) {
        e.preventDefault();
        if ($(this).parents('li').hasClass('unselectable')) { return; }
        updateContent(this.href);
    });

    // change drop down variation attribute - should replace product content with new variant
    $pdpMain.on('change', '.variation-select', function () {
        if ($(this).val().length === 0) { return; }
        updateContent($(this).val());
        window.universal_variable.events.push({
            "action": "UV:updated"
        });
    });

    // change the selected Size variation attribute - should replace product content with new variant
    $pdpMain.on('click', '.size-variation', function (e) {
        e.preventDefault();
        if ($(this).attr("href").length === 0) { return; }
        updateContent($(this).attr("href"));
        window.universal_variable.events.push({
            "action": "UV:updated"
        });
    });
};

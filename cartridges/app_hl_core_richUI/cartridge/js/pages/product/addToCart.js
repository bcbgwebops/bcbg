'use strict';

var dialog = require('../../dialog'),
	minicart = require('../../minicart'),
	page = require('../../page'),
	util = require('../../util'),
	TPromise = require('promise'),
	_ = require('lodash');

/**
 * @description Make the AJAX request to add an item to cart
 * @param {Element} form The form element that contains the item quantity and ID data
 * @returns {Promise}
 */
var addItemToCart = function (form) {
	var $form = $(form),
		$qty = $form.find('input[name="Quantity"]');
	if ($qty.length === 0 || isNaN($qty.val()) || parseInt($qty.val(), 10) === 0) {
		$qty.val('1');
	}
	return TPromise.resolve($.ajax({
		type: 'POST',
		url: util.ajaxUrl(Urls.addProduct),
		data: $form.serialize()
	}));
};

/**
 * @description Handler to handle the add to cart event
 */
var addToCart = function (e) {
	e.preventDefault();
	var $form = $(this).closest('form');
	var isSize = !($('#size-variation').css('display') == 'none');
	var isSizeSelected = $('.size-variation.selected').length;
	var szErrorDisplayed = ($('#sizeError').length > 0);
	var colorErrorDisplayed = ($('#colorError').length > 0);
	var isQvDialog = ($('#QuickViewDialog').length > 0);
	var isColor= !($('.swatches.color').css('display') == 'none');
	var colorText = $('.selected-value').text();
		
	if (isColor &&  colorText == ''){
		if (!colorErrorDisplayed) {
			jQuery('#swatchAttrib').append('<span id="colorError" class="error error-message">' + Resources.ADD_TO_CART_COLOR_FAIL + '</span>');
		}
		return;
	}
	
	if (isSize &&  !isSizeSelected){
		
		if (!szErrorDisplayed) {
			if (!isQvDialog){
				//$('<span id="sizeError" class="error error-message">' + Resources.ADD_TO_CART_SIZE_FAIL + '</span>').insertAfter(jQuery('.size-chart-div'));			
				$('<span id="sizeError" class="error error-message"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' + Resources.ADD_TO_CART_SIZE_FAIL + '</span>').insertAfter(jQuery('.size-label'));
			} else{
				$('<span id="sizeError" class="error error-message"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> ' + Resources.ADD_TO_CART_SIZE_FAIL + '</span>').insertAfter(jQuery('.size-label'));
			}
		}
	
		if(!isSizeSelected && $( '#sizeError' ).css('display') == 'block' ){
			$('html,body').animate({
			   scrollTop: $(".ul-size").offset().top - 300
			});
		}
		
		return;
	}

	addItemToCart($form).then(function (response) {
		var $uuid = $form.find('input[name="uuid"]');

		if ($uuid.length > 0 && $uuid.val().length > 0) {
			page.refresh();
		} else {
			// do not close quickview if adding individual item that is part of product set
			// @TODO should notify the user some other way that the add action has completed successfully
			if (!$(this).hasClass('sub-product-item')) {
				dialog.close();
			}
			minicart.show(response);
		}
		$.ajax({		 
			url: Urls.tealiumProductJson,
			data: "pid=" + $form.find("#pid").val(),
			contentType: 'application/json',
			dataType: 'json'
		}).done(function(data) {
			utag_data = data;
			utag_data['product_quantity'] = [$form.find("#Quantity").val()];
			utag_data['event_type'] = 'cart_add';
			utag.link(utag_data);

			var categories = data.product_category[0].split("-");
			if((typeof data.product_original_price[0] != "undefined") && (data.product_original_price[0] < data.product_price[0])){
				var subtotal = data.product_original_price[0] * data.product_quantity[0];
			} else{
				var subtotal = data.product_price[0] * data.product_quantity[0];
			}
			
			var event = {
				"action": "addToCart",
				"eventData":{
					"currency": universal_variable.basket.currency,
					"subtotal": universal_variable.basket.subtotal,
					"line_items":[{
						"quantity":data.product_quantity[0],
						"subtotal":subtotal,
						"product":{
							"id":data.product_id[0],
							"url":data.product_url[0],
							"image_url":data.product_image_url[0],
							"manufacturer":data.product_brand[0],
							"category":categories[categories.length-2],
							"subcategory":categories[categories.length-1],
							"currency":universal_variable.basket.currency,
							"unit_price":data.product_price[0],
							"unit_sale_price":data.product_original_price[0]
						}
					}]
				}
			}
			if(window.universal_variable){
				window.universal_variable.events.push(event);
			}
		}).error(function(data) {
			
		});
	}.bind(this));
};

/**
 * @description Handler to handle the add all items to cart event
 */
var addAllToCart = function (e) {
	e.preventDefault();
	var $productForms = $('#product-set-list').find('form').toArray();
	TPromise.all(_.map($productForms, addItemToCart))
		.then(function (responses) {
			dialog.close();
			// show the final response only, which would include all the other items
			minicart.show(responses[responses.length - 1]);
		});
};

/**
 * @function
 * @description Binds the click event to a given target for the add-to-cart handling
 * @param {Element} target The target on which an add to cart event-handler will be set
 */
module.exports = function (target) {
	$('.add-to-cart[disabled]').attr('title', $('.availability-msg').text());

	if (target) {
		target.on('click', '.add-to-cart', addToCart);
	} else {
		$('.add-to-cart').on('click', addToCart);
	}

	$('#add-all-to-cart').on('click', addAllToCart);
};

'use strict';

var account = require('./account'),
	bonusProductsView = require('../bonus-products-view'),
	quickview = require('../quickview'),
	dialog = require('../dialog'),
	util = require('../util'),
	cartStoreInventory = require('../storeinventory/cart');

function initPackagingDialog() {
	if ($('#basic').is(':checked') == false && $('#gift').is(':checked') == false) {
		$("#basic").prop("checked", true);
		disableRightColumn(true);
	}
	
	if ($('#gift').is(':checked')) {
		disableRightColumn(false);
	}
	
	

}
function disableRightColumn(disable) {
	if (disable) {
		$('.rightcolumn input').attr('disabled', true);
		$('.rightcolumn textarea').attr('disabled', true);
		$('.rightcolumn').css('opacity', .5);
		$('#giftMessage').val('');
	} else {
		$('.rightcolumn input').attr('disabled', false);
		$('.rightcolumn textarea').attr('disabled', false);
		$('.rightcolumn').css('opacity', 1);
	}
}
/**
 * @private
 * @function
 * @description Binds events to the cart page (edit item's details, bonus item's actions, coupon code entry)
 */
function initializeEvents() {
	$('#cart-table').on('click', '.item-edit-details a', function (e) {
		e.preventDefault();

		quickview.show({
			url: e.target.href,
			source: 'cart'
		});
	})
	if($('.cart-coupon-code .error').length > 0){
		$('.cart-coupon-code').addClass("active");
	}
	$('.cart-footer').on('click', '.cart-coupon-code label', function () {
		$('.cart-coupon-code').toggleClass("active");
	})
	.on('click', '.bonus-item-actions a, .item-details .bonusproducts a', function (e) {
		e.preventDefault();
		bonusProductsView.show(this.href);
	});

	$(document).on('click', '#proceedCheckout', function(e){
		e.preventDefault();
		$("#checkout-form").submit();
	})
	
	$(document).on('change', '.leftcolumn input[name=dwfrm_packaging_gift]', function() {
		if ($('#basic').is(':checked')) {
			disableRightColumn(true);
		} else {
			disableRightColumn(false);
		} 
	});	
	
	$(document).on('change', '.cartQuantity', function() {
		var new_quantity = $(this).val();
		
		if ($(this).closest("tr").find(".pid").val()) {
			$.ajax({		 
				url: Urls.tealiumProductJson,
				data: "pid=" + $(this).closest("tr").find(".pid").val(),
				contentType: 'application/json',
				dataType: 'json'
			}).done(function(data) {
				utag_data = data;
				var quantity_change = ["" + Math.abs(parseInt(utag_data.product_quantity[0]) - parseInt(new_quantity))];

				var categories = data.product_category[0].split("-");
				if((typeof data.product_original_price[0] != "undefined") && (data.product_original_price[0] < data.product_price[0])){
					var subtotal = data.product_original_price[0] * parseInt(new_quantity);
				} else{
					var subtotal = data.product_price[0] * parseInt(new_quantity);
				}

				if (parseInt(utag_data.product_quantity[0]) > parseInt(new_quantity)) {
				    utag_data.product_quantity = [parseInt(new_quantity)];
				    utag_data['event_type'] = 'cart_remove';
				    var event = {
						"action": "removeFromCart",
						"eventData":{
							"currency": universal_variable.basket.currency,
							"subtotal": universal_variable.basket.subtotal,
							"line_items":[{
								"quantity":data.product_quantity[0],
								"subtotal":subtotal,
								"product":{
									"id":data.product_id[0],
									"url":data.product_url[0],
									"image_url":data.product_image_url[0],
									"manufacturer":data.product_brand[0],
									"category":categories[categories.length-2],
									"subcategory":categories[categories.length-1],
									"currency":universal_variable.basket.currency,
									"unit_price":data.product_price[0],
									"unit_sale_price":data.product_original_price[0]
								}
							}]
						}
					}
				} else {
					utag_data.product_quantity = [parseInt(new_quantity)];
					utag_data['event_type'] = 'cart_add';
					var event = {
						"action": "addToCart",
						"eventData":{
							"currency": universal_variable.basket.currency,
							"subtotal": universal_variable.basket.subtotal,
							"line_items":[{
								"quantity":data.product_quantity[0],
								"subtotal":subtotal,
								"product":{
									"id":data.product_id[0],
									"url":data.product_url[0],
									"image_url":data.product_image_url[0],
									"manufacturer":data.product_brand[0],
									"category":categories[categories.length-2],
									"subcategory":categories[categories.length-1],
									"currency":universal_variable.basket.currency,
									"unit_price":data.product_price[0],
									"unit_sale_price":data.product_original_price[0]
								}
							}]
						}
					}
				}
				utag.link(utag_data);
				if(window.universal_variable){
					window.universal_variable.events.push(event);
				}
			}).error(function(data) {
				
			});
		}
		$('#cart-items-form').submit();
	});
	
	$(document).on('change', '.rightcolumn input[name=dwfrm_packaging_giftNote]', function() {
		if ($(this).val() == 'NN' || $(this).val() == 'BN') {
			$('#giftMessage').attr('disabled', true);
			$('#giftMessage').val('');
		} else {
			$('#giftMessage').attr('disabled', false);
		}
	});
	
	$(document).on('keypress', '#giftMessage', function() { 
		$('#personalizedGiftNote').attr('checked', true);
		var messageCnt = 150 - $(this).val().length;
		$('.giftPackagingTextArea .caption .count').html(messageCnt);
	});	

	// override enter key for coupon code entry
	$('form input[name$="_couponCode"]').on('keydown', function (e) {
		if (e.which === 13 && $(this).val().length === 0) { return false; }
	});
	
	$(document).on('click', 'button[name$="_deleteProduct"]', function(e) {
		if ($(this).closest("tr").find(".pid").val()) {
			$.ajax({		 
				url: Urls.tealiumProductJson,
				data: "pid=" + $(this).closest("tr").find(".pid").val(),
				contentType: 'application/json',
				dataType: 'json'
			}).done(function(data) {
				utag_data = data;
				utag_data['event_type'] = 'cart_remove';
				utag.link(utag_data);

				var categories = data.product_category[0].split("-");
				var event = {
					"action": "removeFromCart",
					"eventData":{
						"currency": universal_variable.basket.currency,
						"subtotal": universal_variable.basket.subtotal,
						"line_items":[{
							"quantity":0,
							"subtotal":0.00,
							"product":{
								"id":data.product_id[0],
								"url":data.product_url[0],
								"image_url":data.product_image_url[0],
								"manufacturer":data.product_brand[0],
								"category":categories[categories.length-2],
								"subcategory":categories[categories.length-1],
								"currency":universal_variable.basket.currency,
								"unit_price":data.product_price[0],
								"unit_sale_price":data.product_original_price[0]
							}
						}]
					}
				}
				if(window.universal_variable){
					window.universal_variable.events.push(event);
				}
			}).error(function(data) {
				
			});
		}
	});


	$('.contactUs').click(function(e) {
		$('#cscontact').show();
		e.stopPropagation();
	});

	$('body').click(function(e) {
		if ($('#cscontact').is(':visible')) {
			$('#cscontact').hide();
		}
	});

	$(".packagingSelect").on("click", function(e) {
		e.preventDefault();
		dialog.open({
			url: this.href,
			options: {
				open: initPackagingDialog(),
				width: 610,
				dialogClass: 'packaging-dialog'
			},
			callback: function () {
				if ($('#basic').is(':checked') == false && $('#gift').is(':checked') == false) {
					$("#basic").prop("checked", true);
					disableRightColumn(true);
				}
				
				if ($('#gift').is(':checked')) {
					disableRightColumn(false);
				}
				
				if ($('#personalizedGiftNote').is(':checked')){ $('#giftMessage').attr('disabled', false);}
				if ($('#blankGiftNote').is(':checked') ){$('#giftMessage').val('');$('#giftMessage').attr('disabled', true);}
				if ($('#noGiftNote').is(':checked')){$('#giftMessage').val('');$('#giftMessage').attr('disabled', true);}
			}
		
		});
	});
	$("body").on("click", ".opcInternationalShip", function(e) {
		e.preventDefault();
		var url = $(this).attr('href');

		dialog.open({
			url: url,
			options: {
				width: 620,
				dialogClass: "no-close",
			}
		});
	});

	/*
	$('#checkout-form').on('submit', function(e) {
		var url = $('.cart-checkout-button').attr('ihref');
		if ($('#isInternational').val() == 'true' && url != undefined && $('#useCheckoutGate').val() == 'true') {
			e.preventDefault();
			dialog.open({
				url: url,
				options: {
					width: 610,
					dialogClass: 'internationalDialog'
				},
				callback: function () {
					$("body #is_rightside a").each(function(i){
						$(this).attr("target","_blank");
					});

					$("body #is_leftside").attr("style","margin: 4px 0 0 0;");
					$("body #is_leftside input").attr("style","margin: 10px;");
				}
			});
		}
	});
	*/

	$(document).on('click', '#termsaccept', function(e) {
		if ($(this).is(":checked")) {
			$("#is_leftside").attr("style","margin: 4px 0 0 0;");
		}
	});

	$(document).on('click', '#termscontinue', function(e) {
		if ($('#termsaccept').is(":checked")) {
			$('.cart-checkout-button').removeAttr('ihref');
			$('.cart-checkout-button').click();
			$("#is_leftside").attr("style","margin: 4px 0 0 0;");
		} else {
			$("#is_leftside").attr("style","border: 1px solid #f00; margin: 4px 0 0 0;");
		}
	});

	$(document).on('click', '#termscancel', function(e) {
		$('#dialog-container').dialog('close');
	});	
	
	$(document).on('click', '.add-to-wishlist', function(e) {
    	var utag_data = $(this).data("utag");
		utag_data['event_type'] = 'wishlist_add';
		utag.link(utag_data);
	});
}

exports.init = function () {
	initializeEvents();
	if (SitePreferences.STORE_PICKUP) {
		cartStoreInventory.init();
	}

	util.createCookie('checkoutStep','account',0);
	util.createCookie('checkoutStepFrom','',0);

	account.initCartLogin();
};

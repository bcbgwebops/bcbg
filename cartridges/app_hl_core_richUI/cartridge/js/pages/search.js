'use strict';

var compareWidget = require('../compare-widget'),
	productTile = require('../product-tile'),
	progress = require('../progress'),
	util = require('../util');

function infiniteScroll() {
	// getting the hidden div, which is the placeholder for the next page
	var loadingPlaceHolder = $('.infinite-scroll-placeholder[data-loading-state="unloaded"]');
	// get url hidden in DOM
	var gridUrl = loadingPlaceHolder.attr('data-grid-url');

	if (loadingPlaceHolder.length === 1 && util.elementInViewport(loadingPlaceHolder.get(0), 250)) {
		// switch state to 'loading'
		// - switches state, so the above selector is only matching once
		// - shows loading indicator
		loadingPlaceHolder.attr('data-loading-state', 'loading');
		loadingPlaceHolder.addClass('infinite-scroll-loading');

		/**
		 * named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
		 */
		var fillEndlessScrollChunk = function (html) {
			loadingPlaceHolder.removeClass('infinite-scroll-loading');
			loadingPlaceHolder.attr('data-loading-state', 'loaded');
			$('div.search-result-content').append(html);
		};

		// old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
		// it was removed to temporarily address RAP-2649
		if (false) {
			// if we hit the cache
			fillEndlessScrollChunk(sessionStorage['scroll-cache_' + gridUrl]);
		} else {
			// else do query via ajax
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: gridUrl,
				success: function (response) {
					// put response into cache
					try {
						sessionStorage['scroll-cache_' + gridUrl] = response;
					} catch (e) {
						// nothing to catch in case of out of memory of session storage
						// it will fall back to load via ajax
					}
					// update UI
					fillEndlessScrollChunk(response);
					productTile.init();
				}
			});
		}
	}
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing() {
	var hash = location.href.split('#')[1];
	if (hash === 'results-content' || hash === 'results-products') { return; }
	var refineUrl;

	if (hash.length > 0) {
		refineUrl = window.location.pathname + '?' + hash;
	} else {
		return;
	}
	progress.show($('.search-result-content'));
	$('#main').load(util.appendParamToURL(refineUrl, 'format', 'ajax'), function () {
		compareWidget.init();
		productTile.init();
		progress.hide();
	});
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $('#main');
	
	$(".sortBySelect li.selected").prependTo(".sortBySelect");

	if(getQueryVariable("fo")){
		$("#sideRefinement").addClass('open');
	}

	// compare checked
	$main.on('click', 'input[type="checkbox"].compare-check', function () {
		var cb = $(this);
		var tile = cb.closest('.product-tile');

		var func = this.checked ? compareWidget.addProduct : compareWidget.removeProduct;
		var itemImg = tile.find('.product-image a img').first();
		func({
			itemid: tile.data('itemid'),
			uuid: tile[0].id,
			img: itemImg,
			cb: cb
		});

	});

	// handle toggle refinement blocks
	$main.on('click', '.refinement h3', function () {
		if ($(this).hasClass('expanded')) {
			$(this).removeClass('expanded');
            $(this).siblings('.scrollable').hide();
            $(this).siblings('.scrollable-div').hide();
		} else {
            $('.refinement h3').removeClass('expanded');
            $('.refinement .scrollable').hide();
            $('.refinement .scrollable-div').hide();
            $(this).addClass('expanded');
            $(this).siblings('.scrollable').show();
            $(this).siblings('.scrollable-div').show();
		}
	});

	$('body').on('click', '#sideRefinementHolder .close', function () {
		$("#sideRefinement").removeClass("open");
	});

	$main.on('click', '.attribute-sorting-refinements #openFilter', function () {
		$("#sideRefinement").addClass("open");
	});
	
	// handle events for updating grid
	$main.on('click', '.refinement a, .pagination a, .breadcrumb-refinement-value a', function () {
		if ($(this).parent().hasClass('unselectable')) { return; }
		var catparent = $(this).parents('.category-refinement');
		var folderparent = $(this).parents('.folder-refinement');

		//if the anchor tag is underneath a div with the class names & , prevent the double encoding of the url
		//else handle the encoding for the url
		if (catparent.length > 0 || folderparent.length > 0) {
			return true;
		} else {
			var uri = util.getQueryString(this.href);
			
			if (!!uri) {
				if (!!uri.query && uri.query.length > 1) {
					window.location.hash = uri.query.substring(1);
				} else {
					window.location.href = this.href;
				}
			}
			
			return false;
		}
	});

	$main.on('change', '#sortby-select-mobile', function () {
		if ($(this).parent().hasClass('unselectable')) { return; }
		var catparent = $(this).parents('.category-refinement');
		var folderparent = $(this).parents('.folder-refinement');

		//if the anchor tag is underneath a div with the class names & , prevent the double encoding of the url
		//else handle the encoding for the url
		if (catparent.length > 0 || folderparent.length > 0) {
			return true;
		} else {
			var uri = util.getQueryString(this.value);
			
			if (!!uri) {
				if (!!uri.query && uri.query.length > 1) {
					window.location.hash = uri.query.substring(1);
				} else {
					window.location.href = this.value;
				}
			}
			
			return false;
		}
	});
	
	$('body').on('click', '#refinement-apply-btn, #desktop-refinement-submit', function () {
		//first get the current url with no size or color
		var wl = window.location.href;
		var wl = removeParam("fo", wl);
		var wlnocolorp = removeParam("prefn1", wl);
		var wlnocolorv = removeParam("prefv1", wlnocolorp);
		var wlnosizep = removeParam("prefn2", wlnocolorv);
		var wlnosizev = removeParam("prefv2", wlnosizep);
		
		var newColorPrams = "";
		var newSizePrams = "";
		
		if($(this).hasClass("desktop")){
			$('#desktopColorRefinement input:checked').each(function() {
			    newColorPrams = newColorPrams + $(this).val() + "|";
			});
			var newColorPramsM = newColorPrams.slice(0,-1);

			$('#desktopSizeRefinement input:checked').each(function() {
			    newSizePrams = newSizePrams + $(this).val() + "|";
			});
			var newSizePramsM = newSizePrams.slice(0,-1);
		} else{
			$('#color-select option:selected').each(function() {
			    newColorPrams = newColorPrams + $(this).val() + "|";
			});
			var newColorPramsM = newColorPrams.slice(0,-1);

			$('#size-select option:selected').each(function() {
			    newSizePrams = newSizePrams + $(this).val() + "|";
			});
			var newSizePramsM = newSizePrams.slice(0,-1);
		}
		var newPrams = {};
		
		if(newColorPrams != ""){
			newPrams.prefn1 = "colorRefinement";
			newPrams.prefv1 = newColorPramsM;
		}
		
		if(newSizePrams != "" && newColorPrams != ""){
			newPrams.prefn2 = "size";
			newPrams.prefv2 = newSizePramsM;
		}else if(newSizePrams != ""){
			newPrams.prefn1 = "size";
			newPrams.prefv1 = newSizePramsM;
		}
		var finalurl = util.appendParamsToUrl(wlnosizev, newPrams);
		window.location.href = finalurl;
	});

	$('body').on('click', '#clearAllRefinements', function () {
		var currentURL =  window.location.href;
		var clearedURL = removeParam('prefn1', currentURL);
		clearedURL = removeParam('prefv1', clearedURL);
		clearedURL = removeParam('prefn2', clearedURL);
		clearedURL = removeParam('prefv2', clearedURL);
		clearedURL = updateUrlParameter(clearedURL, 'fo', 'true');
		window.location.href = clearedURL;
	});
	
	// handle events item click. append params.
	$main.on('click', '.product-tile a:not(".quickviewbutton")', function () {
		var a = $(this);
		// get current page refinement values
		var wl = window.location;
		var aParams  = util.getQueryStringParams(a[0].search.substr(1));

		var qsParams = (wl.search.length > 1) ? util.getQueryStringParams(wl.search.substr(1)) : {};
		var hashParams = (wl.hash.length > 1) ? util.getQueryStringParams(wl.hash.substr(1)) : {};
		
		// merge hash params with querystring params
		var params = $.extend(hashParams, qsParams);
		params = $.extend(params, aParams);

		if (!params.start) {
			params.start = 0;
		}

		// get the index of the selected item and save as start parameter
		var tile = a.closest('.product-tile');
		var idx = tile.parent().attr('data-product-index') ? + tile.parent().attr('data-product-index') : 0;

		// convert params.start to integer and add index
		params.start = (+params.start) + (idx + 1);
		// set the hash and allow normal action to continue
		a[0].hash = $.param(params);

	});

	// handle sorting change
	$main.on('change', '.sort-by select', function () {
		var refineUrl = $(this).find('option:selected').val();
		var queryString = util.getQueryString(refineUrl);
		window.location.hash = queryString;
		return false;
	})
	.on('change', '.items-per-page select', function () {
		var refineUrl = $(this).find('option:selected').val();
		var queryString = util.getQueryString(refineUrl);
		var params = util.getQueryStringParams(queryString);
		
		//qubit
		if(window.universal_variable){
			window.universal_variable.events.push( { "action": "UV:updated"});
		}

		if (refineUrl === 'INFINITE_SCROLL') {
			$('html').addClass('infinite-scroll').removeClass('disable-infinite-scroll');
		} else {
			$('html').addClass('disable-infinite-scroll').removeClass('infinite-scroll');
			var newUrl = updateUrlParameter(refineUrl, 'sz', params.sz);
			window.location.href = newUrl;
		}
		return false;
	});

	// handle hash change
	window.onhashchange = updateProductListing;
}

function updateUrlParameter(uri, key, value) {
	// remove the hash part before operating on the uri
    var i = uri.indexOf('#');
    var hash = i === -1 ? ''  : uri.substr(i);
         uri = i === -1 ? uri : uri.substr(0, i);

    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        uri = uri + separator + key + "=" + value;
    }
    return uri + hash;  // finally append the hash as well
}

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

exports.init = function () {
	compareWidget.init();
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$(window).on('scroll', infiniteScroll);
	}
	productTile.init();
	initializeEvents();
};

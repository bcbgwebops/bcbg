'use strict';

var ajax = require('../../ajax'),
	formPrepare = require('./formPrepare'),
	giftcard = require('../../giftcard'),
	util = require('../../util'),
	progress = require('../../progress'),
	onepage = require('../onepage/onepage');

/**
 * @function
 * @description Fills the Credit Card form with the passed data-parameter and clears the former cvn input
 * @param {Object} data The Credit Card data (holder, type, masked number, expiration month/year)
 */
function setCCFields(data) {
	var $creditCard = $('[data-method="CREDIT_CARD"]');
	$creditCard.find('input[name$="creditCard_owner"]').val(data.holder).trigger('change');
	$creditCard.find('select[name$="_type"]').val(data.type).trigger('change');
	$creditCard.find('input[name$="_number"]').val(data.maskedNumber).trigger('change');
	$creditCard.find('[name$="_month"]').val(data.expirationMonth).trigger('change');
	$creditCard.find('[name$="_year"]').val(data.expirationYear).trigger('change');
	$creditCard.find('input[name$="_cvn"]').val('').trigger('change');
}

/**
 * @function
 * @description Updates the credit card form with the attributes of a given card
 * @param {String} cardID the credit card ID of a given card
 */
function populateCreditCardForm(cardID) {
	// load card details
	var url = util.appendParamToURL(Urls.billingSelectCC, 'creditCardUUID', cardID);
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (!data) {
				window.alert(Resources.CC_LOAD_ERROR);
				return false;
			}
			setCCFields(data);
		}
	});
}

/**
 * @function
 * @description Changes the payment method form depending on the passed paymentMethodID
 * @param {String} paymentMethodID the ID of the payment method, to which the payment method form should be changed to
 */
function updatePaymentMethod(paymentMethodID) {
	var $paymentMethods = $('.payment-method');
	$paymentMethods.removeClass('payment-method-expanded');

	var $selectedPaymentMethod = $paymentMethods.filter('[data-method="' + paymentMethodID + '"]');
	if ($selectedPaymentMethod.length === 0) {
		$selectedPaymentMethod = $('[data-method="Custom"]');
	}
	$selectedPaymentMethod.addClass('payment-method-expanded');

	// ensure checkbox of payment method is checked
	$('input[name$="_selectedPaymentMethodID"]').removeAttr('checked');
	$('input[value=' + paymentMethodID + ']').prop('checked', 'checked');

	formPrepare.validateForm();
}

/**
 * @function
 * @description loads billing address, Gift Certificates, Coupon and Payment methods
 */
exports.init = function () {
	var $checkoutForm = $('.checkout-billing');
	var $addGiftCert = $('#add-giftcert');
	var $giftCertCode = $('input[name$="_giftCertCode"]');
	var $addCoupon = $('#add-coupon');
	var $couponCode = $('input[name$="_couponCode"]');
	var $selectPaymentMethod = $('.payment-method-options');
	var selectedPaymentMethod = $selectPaymentMethod.find(':checked').val();

	formPrepare.init({
		formSelector: 'form[id$="billing"]',
		continueSelector: '[name$="billing_save"]'
	});

	// default payment method to 'CREDIT_CARD'
	updatePaymentMethod((selectedPaymentMethod) ? selectedPaymentMethod : 'CREDIT_CARD');
	$selectPaymentMethod.on('click', 'input[type="radio"]', function () {
		updatePaymentMethod($(this).val());
	});

	// select credit card from list
	$('#creditCardList').on('change', function () {
		var cardUUID = $(this).val();
		if (!cardUUID) {return;}
		populateCreditCardForm(cardUUID);

		// remove server side error
		$('.required.error').removeClass('error');
		$('.error-message').remove();
	});

	$('.toggleSlide').on('click', function (e) {
		e.preventDefault();
		var slide = $(this).attr("href");
		$(slide).slideDown();
	});


	$('#check-giftcert').on('click', function (e) {
		e.preventDefault();
		var $balance = $('.balance');
		if ($giftCertCode.length === 0 || $giftCertCode.val().length === 0) {
			var error = $balance.find('span.error');
			if (error.length === 0) {
				error = $('<span>').addClass('error').appendTo($balance);
			}
			error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		giftcard.checkBalance($giftCertCode.val(), function (data) {
			if (!data || !data.giftCertificate) {
				$balance.html(Resources.GIFT_CERT_INVALID).removeClass('success').addClass('error');
				return;
			}
			$balance.html(Resources.GIFT_CERT_BALANCE + ' ' + data.giftCertificate.balance).removeClass('error').addClass('success');
		});
	});

	$addGiftCert.on('click', function (e) {
		e.preventDefault();
		var code = $giftCertCode.val(),
			$error = $checkoutForm.find('.giftcert-error'),
			$redemption = $checkoutForm.find('.redemption.giftcert');
		if (code.length === 0) {
			$error.html(Resources.GIFT_CERT_MISSING);
			return;
		}

		$error.html('');
		$redemption.html('');

		progress.show($("#coupon_gift_codes"));
		
		var url = util.appendParamsToUrl(Urls.redeemGiftCert, {giftCertCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			progress.hide();

			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			}
			if (fail) {
				$error.html(msg);
				return;
			} else {
				$redemption.html("<span class='success'>" + msg + "</span>");
			}

			if (data.success) {
				if ($("#onepage").length > 0) {
					onepage.refreshMiniSummary(true);
				}
			}
		});
	});

	$addCoupon.on('click', function (e) {
		e.preventDefault();
		var $error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val(),
			$redemption = $checkoutForm.find('.redemption.coupon');
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		$error.html('');
		$redemption.html('');

		progress.show($("#coupon_gift_codes"));

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			progress.hide();

			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			} else {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
			}
			if (fail) {
				$error.html(msg);
				return;
			} else {
				$redemption.html("<span class='success'>" + msg + "</span>");
			}

			if (data.success) {
				if ($("#onepage").length > 0) {
					onepage.refreshMiniSummary(true);
					var couponLink = $("<a />").html("Remove " + code.toUpperCase()).attr("href","#").attr("data-code",code.toUpperCase()).addClass("couponCodeLink");
					var couponDiv = $("<div />").addClass("couponCodeLine");
					couponDiv.append(couponLink);
					$("#coupon_gift_code_lines").append(couponDiv);
				}
			}
		});
	});

	// trigger events on enter
	$couponCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addCoupon.click();
		}
	});
	$giftCertCode.on('keydown', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			$addGiftCert.click();
		}
	});
	
	var allowSubmit = false;
	$('#dwfrm_billing').on('submit', function (e) {
		var paymentType = $('.payment-method-options .input-radio:checked').val();
		if (paymentType == 'CREDIT_CARD' && $('#isInternational').val() == 'false') {
			var cvn = $('input[name=dwfrm_billing_paymentMethods_creditCard_cvn]').val();
			var cctype = $('#dwfrm_billing_paymentMethods_creditCard_type').val();
			var errors = errors = $('.form-row.cvn').attr('data-required-text');
			if (cctype == 'Amex' && cvn.length != 4) {
				$('input[name=dwfrm_billing_paymentMethods_creditCard_cvn]').val('');
			}
			if (cctype != 'Amex' && cvn.length != 3) {
				$('input[name=dwfrm_billing_paymentMethods_creditCard_cvn]').val('');
			}
		}
		if ($('#isInternational').val() == 'true' && allowSubmit == false) {
			$('.bf-billing-error').hide();
			// Let's look and see which option is selected.

			if (paymentType == 'CREDIT_CARD') {
				if ($('#dwfrm_billing').valid()) {
					e.preventDefault();
					// walk through our options and see which one is selected.
					var url = '';
					var bfType = $(this).attr('bf_type');
					var selectedOption = $('#dwfrm_billing_paymentMethods_creditCard_type option:selected').val();
					$('#dwfrm_billing_paymentMethods_creditCard_type option').each(function(i) {
						if ($(this).val() == selectedOption) {
							url = $(this).attr('bf_url');
						} 
					});
					// build our payload
					var payload = {};
					payload.cardNumber = $('#dwfrm_billing_paymentMethods_creditCard_number').val();
					payload.expirationMonth = parseInt($('#dwfrm_billing_paymentMethods_creditCard_month').val());
					payload.expirationYear = parseInt($('#dwfrm_billing_paymentMethods_creditCard_year').val());
					payload.cvn = $('#dwfrm_billing_paymentMethods_creditCard_cvn').val();
					
					$.ajax({		 
						url: url,
						data: JSON.stringify(payload),
						contentType: 'application/json',
						type:'POST',
						dataType: 'json'
					}).done(function(data) {
						$('#bfToken').val(data.token);
						$('#bfTokenType').val($('.input-radio:checked').val());
						allowSubmit = true;
						$('#dwfrm_billing').submit();
					}).error(function(data) {
						$('.bf-billing-error').show();
					});
				}
			}
		} 
	});	
	$('.countryValue').trigger('change');
};

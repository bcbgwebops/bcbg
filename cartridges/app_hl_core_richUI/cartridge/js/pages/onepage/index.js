'use strict';

var dialog = require('../../dialog'),
	account = require('./account'),
	shipping = require('./shipping'),
	billing = require('./billing'),
	summary = require('./summary'),
	util = require('../../util'),
	progress = require('../../progress'),
	onepage = require('./onepage');

function getStep() {
	return document.URL.substr(document.URL.indexOf('#')+1);
}

var opcRemoveCoupon = function(e){
	e.preventDefault();
	onepage.removeCoupon($(this).attr("data-code"), $("#onepage_review"));
}

/**
 * @function Initializes the page events depending on the checkout stage (account/shipping/billing)
 */
exports.init = function () {
	$("#onepage_review").off("click", ".summaryCouponCodeLink", opcRemoveCoupon);
	$("#onepage_review").on("click", ".summaryCouponCodeLink", opcRemoveCoupon);

	var checkoutStep = getStep();
	
	if ('isAfterpay' in window && window.isAfterpay == true) {
		checkoutStep = 'billing';
	}
	onepage.setStep(checkoutStep);

	$(window).on('hashchange',function(){
		var checkoutStep = getStep();
		onepage.setStep(checkoutStep);
	});

	$("body").on("click",".editSection",function(e){
		e.preventDefault();
		var section = $(this).attr("id").replace("edit_","");
		onepage.updateUrl(section,'');
	});

	$("body").on("click",".opcInternationalShip",function(e){
		e.preventDefault();
		var url = $(this).attr('href');

		dialog.open({
			url: url,
			options: {
				width: 620,
				dialogClass: "no-close",
			}
		});
	});

	$("#onepage_review").on("click","#add-coupon-summary",function(e){
		e.preventDefault();

		var $couponCode = $('#onepage_review input[name$="_couponCode"]');
		var $checkoutForm = $('#onepage_review #onepage_review_cart');

		var $error = $checkoutForm.find('.coupon-error'),
			code = $couponCode.val(),
			$redemption = $checkoutForm.find('.redemption.coupon');
		if (code.length === 0) {
			$error.html(Resources.COUPON_CODE_MISSING);
			return;
		}

		$error.html('');
		$redemption.html('');

		progress.show($("#onepage_review #coupon_gift_codes"));

		var url = util.appendParamsToUrl(Urls.addCoupon, {couponCode: code, format: 'ajax'});
		$.getJSON(url, function (data) {
			progress.hide();

			var fail = false;
			var msg = '';
			if (!data) {
				msg = Resources.BAD_RESPONSE;
				fail = true;
			} else if (!data.success) {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
				fail = true;
			} else {
				msg = data.message.split('<').join('&lt;').split('>').join('&gt;');
			}
			if (fail) {
				$error.html(msg);
				return;
			} else {
				$redemption.html("<span class='success'>" + msg + "</span>");
			}

			if (data.success) {
				if ($("#onepage").length > 0) {
					onepage.refreshMiniSummary(true);
					var couponLink = $("<a />").html("Remove " + code).attr("href","#").attr("data-code",code).addClass("couponCodeLink");
					var couponDiv = $("<div />").addClass("couponCodeLine");
					couponDiv.append(couponLink);
					$(".toShow").append(couponDiv);
				}
			}
		});
	});

	// trigger events on enter
	$("#onepage_review").on('keydown', 'input[name$="_couponCode"]', function (e) {
		if (e.which === 13) {
			e.preventDefault();
			var $addCoupon = $('#add-coupon-summary');
			$addCoupon.click();
		}
	});

};

'use strict';

var util = require('../../util'),
	dialog = require('../../dialog'),
	onepage = require('./onepage'),
	page = require('../../page');

var qs = (function(a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
        var p=a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

var buttonClick = function(){
	$('#order_submit_form').data('button', this.name);
}

var opcPrevious = function(e){
	e.preventDefault();
	onepage.updateUrl('billing','summary');
}

/**
 * @function
 * @description Summary Functions
 */
exports.init = function () {
	$("#onepage_summary").on('click', 'button[type="submit"]', buttonClick);

	$("#onepage_summary").on("click", ".summary-prev-button", opcPrevious);
};

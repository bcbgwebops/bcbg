'use strict';

var progress = require('../../progress'),
	util = require('../../util'),
	dialog = require('../../dialog'),
	validator = require('../../validator'),
	onepage = require('./onepage'),
	account = require('../account');

var opcFormSubmit = function(e){
	e.preventDefault();

	var thisForm = $(this);

	onepage.clearFormError(thisForm);

	if (thisForm.valid()) {
		var sendObj = {};
		sendObj.thisForm = thisForm;
		sendObj.formBtn = $("button[type='submit']", thisForm).attr("name");
		sendObj.successCallback = function(data) {
			onepage.clearFormError(thisForm);
			progress.hide();

			if (data.success == true) {
				onepage.updateUrl('shipping',"");
				onepage.refreshHeaderCustomerInfo();
				$("#edit_account").addClass("visible");
			} else {
				if (data.guest == false) {
					switch(data.errorReasonCode) {
						case 100:
							onepage.showFormError(thisForm,Resources.ACCOUNT_CONFIRM);
							break;
						case 101:
							onepage.showFormError(thisForm,Resources.ACCOUNT_PASSWORD);
							break;
						case 102:
							onepage.showFormError(thisForm,Resources.ACCOUNT_EMAILEXISTS);
							break;
						case 103:
							onepage.showFormError(thisForm,Resources.ACCOUNT_EMAILEXISTSPASSWORD);
							break;
						case 104:
							onepage.showFormError(thisForm,Resources.ACCOUNT_GENERALERROR);
							break;
						case 105:
							onepage.showFormError(thisForm,Resources.ACCOUNT_PASSFAIL);
							break;
						default:
							onepage.showFormError(thisForm,Resources.ACCOUNT_GENERALERROR);	
					}
				} else {
					onepage.showFormError(thisForm,Resources.ACCOUNT_GENERALERROR);
				}
			}
		};

		progress.show($("#onepage_account_content"));
		onepage.loadJSON(sendObj);
	}
}

/**
 * @function
 * @description Account Login / Registration Functions
 */
exports.init = function () {
	account.init();

	$("#onepage_account").off('submit', '.checkoutloginform form', opcFormSubmit);
	$("#onepage_account").on('submit', '.checkoutloginform form', opcFormSubmit);

	$("#showRegForm").click(function(){
		$('#regform').slideDown();
		$(this).hide();
	});
};



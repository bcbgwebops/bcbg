var redirectToAfterPay = function(script, token) {
    if (window.AfterPay) {
        window.AfterPay.initialize({countryCode : 'US'});
        window.AfterPay.redirect({token: token.apToken});
    } 
};

exports.init = redirectToAfterPay;
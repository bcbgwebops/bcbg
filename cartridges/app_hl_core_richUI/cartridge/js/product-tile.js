'use strict';

var imagesLoaded = require('imagesloaded'),
    quickview = require('./quickview'),
    util = require('./util');


function initQuickViewButtons() {
    $(".quickviewbutton").on('click', function (e) {
        e.preventDefault();
        var utag_data = JSON.parse($(this).closest('.product-tile').find('.utag-product').val());
        quickview.show({
            callback: function() {
                if (utag_data) {
                    utag_data['event_type'] = 'product_quickview';
                    utag.view(utag_data);
                }

                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = $('#tfc-fitrec-library').attr('src');
                document.getElementById("pdpMain").appendChild(script);

                window.setTimeout(function () {
                    MagicZoom.start();
                }, 1000);
            },
            url: $(this).attr('href'),
            source: 'quickview'
        });
    });
}
function initImageFunctionality() {
    var timeout;

    $('#showOutfitView').click(function(e){
        e.preventDefault();
        util.createCookie('productView', '1', 365);
        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_type = 'outfit';
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
        setDefault();
    });
    $('#showProductView').click(function(e){
        e.preventDefault();
        util.createCookie('productView', '2', 365);
        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_type = 'product';
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
        setDefault();
    });

    setDefault();

    $('.product-image').hover(
        function(){
            var $product = $(this);
            if(!$product.hasClass('fullyLoaded')){
                $product.find('img').each(function(){
                    var dataSrc = $(this).attr('data-src');

                    if(dataSrc){
                        $(this).one('error', function() {
                              $(this).remove();
                        });
                        $(this).attr('src', dataSrc);
                    }
                })
                $product.addClass('fullyLoaded');
            }

            $product.addClass('hovering');

            if($product.find('.thumb-link img').length > 1){
                recursiveImageFade($product, 1000);
            }
        },
        function(){
            var $product = $(this);
            var $defaultImage = $product.find('.default');
            var $activeImage = $product.find('.active');

            $product.removeClass('hovering');
            clearTimeout(timeout);
            if(!$defaultImage.is($activeImage)){
                showImage($activeImage, $defaultImage, true);
            }
        }
    );

    function setDefault() {
        var productView = util.getCookie('productView');

        $('.toggle-product-view a').removeClass('active');
        $('.product-image .default').removeClass('default');
        $('.product-image .active').removeClass('active');

        if (productView == '' || productView == '1') {
            $('.toggle-product-view #showOutfitView').addClass('active');
            $('.product-image').each(function() {
                var $imageToShow = $(this).find('img:last-of-type');
                if(!$imageToShow.attr('data-src') || $imageToShow.attr('data-src').indexOf('_e.jpg') == -1){
                    $(this).find('img').css('z-index', '-1');
                    $(this).find('img:first-child').addClass('default').addClass('active').css('z-index', '1');
                    $(this).find('img').show();
                } else{
                    var imgSrc = $imageToShow.attr('data-src');
                    if($(this).hasClass('productLoadAttempted')){
                        if(!!imgSrc && imgSrc.indexOf('_e.jpg') != -1) {
                            $imageToShow.addClass('default').addClass('active');
                            showImage(null, $imageToShow, false);
                        }
                    } else {
                        $imageToShow.one('load', function() {
                            $imageToShow.addClass('default').addClass('active');
                            showImage(null, $imageToShow, false);
                            $(this).show().siblings('img').show();
                        });

                        $imageToShow.attr('src', imgSrc);
                        $(this).addClass('productLoadAttempted');
                        //$imageToShow.off('load', 'error');
                    }
                }
            });
        } else {
            $('.toggle-product-view #showProductView').addClass('active');
            $('.product-image img').css('z-index', '-1');
            $('.product-image img:first-child').addClass('default').addClass('active').css('z-index', '1');
            $('.product-image img').show();
        }
    }

    function recursiveImageFade($product, delay){
        if($product.hasClass('hovering')){
            timeout = setTimeout(function(){
                var $activeImage = $product.find('.active');
                var $nextImage = $product.find('.active').next();
                if($nextImage.length == 0){
                    $nextImage = $product.find('img:first-child');
                }
                showImage($activeImage, $nextImage, true, function(){
                    recursiveImageFade($product, delay)
                });
            }, delay);
        //Else make sure that the defualt image is shown (covers the case of unhovering while defualt image is fading to next)
        } else{
            var $defaultImage = $product.find('.default');
            var $activeImage = $product.find('.active');
            if(!$defaultImage.is($activeImage)){
                showImage($activeImage, $defaultImage, true);
            }
        }
    }

    function showImage($activeImage, $toShowImage, fade, cb){
        //bring next image to the top of hidden images.
        $toShowImage.css('z-index', '0');
        if(fade){
            $activeImage.fadeOut(function(){
                cleanUpAfterShow($activeImage, $toShowImage, cb);
            });
        } else {
            if($activeImage != null){
                $activeImage.hide(function(){
                    cleanUpAfterShow($activeImage, $toShowImage, cb);
                });
            }
            else{
                cleanUpAfterShow($activeImage, $toShowImage, cb);
            }
        }
    };

    function cleanUpAfterShow($activeImage, $toShowImage, cb){
        //bring new active image to top of stack
        $toShowImage.css('z-index', '1');
        if($activeImage != null){
            //put old active image at bottom of stack, reset it's opacity and display settings
            $activeImage.css({
                'z-index': '-1',
                'opacity': '1',
                'display': 'block'
            });
            $activeImage.removeClass('active');
        }
        $toShowImage.addClass('active');

        if(cb) cb();
    }
}

function placeOverlay() {
    jQuery('.overlay-message').each(function(idx, obj) {
        var imgObj = jQuery(obj).siblings('img');
        jQuery(obj).width(imgObj[0].width);
        jQuery(obj).css('top', imgObj[0].height - 15);
        var leftPos = (imgObj.closest('div').width() - imgObj[0].width) / 2;
        jQuery(obj).css('left', leftPos);
    });
}

/**
 * @private
 * @function
 * @description Initializes events on the product-tile for the following elements:
 * - swatches
 * - thumbnails
 */
function initializeEvents() {
    initQuickViewButtons();
    initImageFunctionality();

    $('.swatch-list').on('mouseleave', function () {
        // Restore current thumb image
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $thumb.data('current');

        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    });
    $('.swatch-list .swatch').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected')) { return; }

        var $tile = $(this).closest('.product-tile');
        $(this).closest('.swatch-list').find('.swatch.selected').removeClass('selected');
        $(this).addClass('selected');
        $tile.find('.thumb-link').attr('href', $(this).attr('href'));
        $tile.find('name-link').attr('href', $(this).attr('href'));

        var data = $(this).children('img').filter(':first').data('thumb');
        var $thumb = $tile.find('.product-image .thumb-link img').eq(0);
        var currentAttrs = {
            src: data.src,
            alt: data.alt,
            title: data.title
        };
        $thumb.attr(currentAttrs);
        $thumb.data('current', currentAttrs);
    }).on('mouseenter', function () {
        // get current thumb details
        var $tile = $(this).closest('.product-tile'),
            $thumb = $tile.find('.product-image .thumb-link img').eq(0),
            data = $(this).children('img').filter(':first').data('thumb'),
            current = $thumb.data('current');

        // If this is the first time, then record the current img
        if (!current) {
            $thumb.data('current', {
                src: $thumb[0].src,
                alt: $thumb[0].alt,
                title: $thumb[0].title
            });
        }

        // Set the tile image to the values provided on the swatch data attributes
        $thumb.attr({
            src: data.src,
            alt: data.alt,
            title: data.title
        });
    });
    
    $('.mobile-toogle-grid > .one-grid').on('click', function (e) {
		$('.search-result-content').removeClass('two-grid-tile three-grid-tile four-grid-tile').addClass('one-grid-tile');
		$('.mobile-toogle-grid').removeClass('active-two-grid').addClass('active-one-grid');
        $('.pagination').removeClass('active-three-grid active-four-grid').addClass('active-two-grid');
		//$(this).toggleClass('wide');
		util.createCookie('gridViewDefault', '1', 365);
		e.preventDefault();

        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_view = 1;
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
	});
	$('.mobile-toogle-grid > .two-grid').on('click', function (e) {
		$('.search-result-content').removeClass('one-grid-tile three-grid-tile four-grid-tile').addClass('two-grid-tile');
		$('.mobile-toogle-grid').removeClass('active-one-grid').addClass('active-two-grid');
        $('.pagination').removeClass('active-three-grid active-four-grid').addClass('active-two-grid');
		//$(this).toggleClass('wide');
		util.createCookie('gridViewDefault', '2', 365);
		e.preventDefault();

        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_view = 2;
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
	});

    $('.toggle-grid > .two-grid').on('click', function (e) {
        $('.search-result-content').removeClass('three-grid-tile four-grid-tile one-grid-tile').addClass('two-grid-tile');
        $('.pagination').removeClass('active-three-grid active-four-grid').addClass('active-two-grid');
        $('.mobile-toogle-grid').addClass('active-two-grid');
        $(this).toggleClass('wide');
        util.createCookie('gridViewDefault', '2', 365);
        e.preventDefault();
        //qubit
        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_view = 2;
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
        //placeOverlay();
    });

    $('.toggle-grid > .three-grid').on('click', function (e) {
        $('.search-result-content').removeClass('two-grid-tile four-grid-tile one-grid-tile').addClass('three-grid-tile');
        $('.pagination').removeClass('active-two-grid active-four-grid').addClass('active-three-grid');
        util.createCookie('gridViewDefault', '3', 365);
        e.preventDefault();
        //qubit
        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_view = 3;
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
        //placeOverlay();
    });

    $('.toggle-grid > .four-grid').on('click', function (e) {
        $('.search-result-content').removeClass('two-grid-tile three-grid-tile one-grid-tile').addClass('four-grid-tile');
        $('.pagination').removeClass('active-two-grid active-three-grid').addClass('active-four-grid');
        util.createCookie('gridViewDefault', '4', 365);
        e.preventDefault();
        //qubit
        if(window.universal_variable){
            window.universal_variable.listing.image_view.image_view = 4;
            window.universal_variable.events.push( { "action": "UV:updated"});
        }
        //placeOverlay();
    });
    jQuery('.pt_product-search-result .attribute-sorting-refinements ul.scrollable').enscroll({drawScrollButtons: true});
    jQuery('.pt_product-search-result .attribute-sorting-refinements ul.scrollable').next('div').addClass('scroller-div');
}

exports.init = function () {
    var $tiles = $('.tiles-container .product-tile');
    if ($tiles.length === 0) { return; }
/*    imagesLoaded('.tiles-container').on('done', function () {
        $tiles.syncHeight()
            .each(function (idx) {
                $(this).data('idx', idx);
            });
    });*/
    initializeEvents();
    // select our event that is in our cookie.
    var gridView = util.getCookie('gridViewDefault');
    var imageView = util.getCookie('productView');
    
    if (gridView == '1') {
        $('.search-result-content').removeClass('three-grid-tile four-grid-tile two-grid-tile').addClass('one-grid-tile');
        $('.mobile-toogle-grid').removeClass('active-two-grid').addClass('active-one-grid');
        $(this).toggleClass('wide');
        util.createCookie('gridViewDefault', '1', 365);
    } else if (gridView == '2') {
        $('.search-result-content').removeClass('three-grid-tile four-grid-tile').addClass('two-grid-tile');
        $('.pagination').removeClass('active-three-grid active-four-grid').addClass('active-two-grid');
        $('.mobile-toogle-grid').addClass('active-two-grid');
        $(this).toggleClass('wide');
        util.createCookie('gridViewDefault', '2', 365);
    } else if (gridView == '3') {
        $('.search-result-content').removeClass('two-grid-tile four-grid-tile').addClass('three-grid-tile');
        $('.pagination').removeClass('active-two-grid active-four-grid').addClass('active-three-grid');
        util.createCookie('gridViewDefault', '3', 365);
        gridView = 3;
    } else {
        $('.search-result-content').removeClass('two-grid-tile three-grid-tile').addClass('four-grid-tile');
        $('.pagination').removeClass('active-two-grid active-three-grid').addClass('active-four-grid');
        util.createCookie('gridViewDefault', '4', 365);
    }
    if(window.universal_variable){
        window.universal_variable.listing.image_view.image_view = gridView;

        if(imageView == '2'){
            window.universal_variable.listing.image_view.image_type = 'product';
        } else{
            window.universal_variable.listing.image_view.image_type = 'outfit';
        }
    }
};
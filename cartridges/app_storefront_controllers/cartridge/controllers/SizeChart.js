'use strict';

/**
 * Controller that renders product detail pages and snippets or includes used on product detail pages.
 * Also renders product tiles for product listings.
 *
 * @module controllers/Product
 */

var params = request.httpParameterMap;

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/**
 * Renders the product size chart.
 *
 */
function show() {

    app.getView().render('product/components/sizechart');

}

/*
 * Web exposed methods
 */
/**
 * Renders the product size chart template.
 * @see module:controllers/Product~showSizeChart
 */
exports.Show = guard.ensure(['get'], show);
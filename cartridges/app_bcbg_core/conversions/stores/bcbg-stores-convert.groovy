@Grapes([
	@Grab(group='commons-codec', module='commons-codec', version='1.9'),
	@Grab( 'org.apache.poi:poi:3.9' )
])

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import groovy.grape.*;
import org.apache.poi.ss.usermodel.*;
import groovy.xml.*;
import java.text.*;

def xmlWriter = new FileWriter('/Users/msewell/Desktop/stores-bcbg.xml');
xmlWriter.write('<?xml version="1.0" encoding="UTF-8"?>\n');

def xml = new MarkupBuilder(xmlWriter);

def FileInputStream fis = new FileInputStream('/Users/msewell/Desktop/stores_05062015.xls')
		
try {
	def workbook = new HSSFWorkbook(fis)
	def worksheet = workbook.getSheetAt(0)
	def skipFirstRow = true
	xml.stores('xmlns':'http://www.demandware.com/xml/impex/store/2007-04-30') {
		for (Row row : worksheet) {
			if (skipFirstRow) {
				skipFirstRow = false;
				continue;
			}
			store('store-id':asString(row.getCell(0, Row.RETURN_BLANK_AS_NULL))) {
				name(asString(row.getCell(5, Row.RETURN_BLANK_AS_NULL)))
				address1(asString(row.getCell(8, Row.RETURN_BLANK_AS_NULL)))
				address2(asString(row.getCell(9, Row.RETURN_BLANK_AS_NULL)))
				city(asString(row.getCell(10, Row.RETURN_BLANK_AS_NULL)))
				'postal-code'(asString(row.getCell(12, Row.RETURN_BLANK_AS_NULL)))
				'state-code'(asString(row.getCell(11, Row.RETURN_BLANK_AS_NULL)))
				'country-code'(getCountryCode(asString(row.getCell(14, Row.RETURN_BLANK_AS_NULL))))
				phone(asString(row.getCell(13, Row.RETURN_BLANK_AS_NULL)))
				fax(asString(row.getCell(15, Row.RETURN_BLANK_AS_NULL)))
				latitude(asDouble(row.getCell(3, Row.RETURN_BLANK_AS_NULL)))
				longitude(asDouble(row.getCell(2, Row.RETURN_BLANK_AS_NULL)))
				'custom-attributes'() {
					def brands = asString(row.getCell(4, Row.RETURN_BLANK_AS_NULL))
					def parts = brands.tokenize(',')
					if (parts.size() > 0) {		
						'custom-attribute'('attribute-id':'brands') {
							parts.each() {
								value(it)
							}
						}						
					}
					'custom-attribute'('attribute-id':'dateOpen', asString(row.getCell(19, Row.RETURN_BLANK_AS_NULL)))
					'custom-attribute'('attribute-id':'location', 'xml:lang':'x-default', asString(row.getCell(7, Row.RETURN_BLANK_AS_NULL)))
					'custom-attribute'('attribute-id':'phone2', asString(row.getCell(20, Row.RETURN_BLANK_AS_NULL)))
					'custom-attribute'('attribute-id':'squareFeet', asString(row.getCell(18, Row.RETURN_BLANK_AS_NULL)))
					'custom-attribute'('attribute-id':'storeNumPrefix', asString(row.getCell(16, Row.RETURN_BLANK_AS_NULL)))
					'custom-attribute'('attribute-id':'storeNumber', asString(row.getCell(17, Row.RETURN_BLANK_AS_NULL)))
					'custom-attribute'('attribute-id':'storeTypeId', asString(row.getCell(1, Row.RETURN_BLANK_AS_NULL)))
				} 
			}
		}
	}
} catch (Exception e) {
	e.printStackTrace()
} finally {
	try { 
		if (fis) {
			fis.close()
		}
	} catch (Exception e) {}
}

String getCountryCode(country) {
	switch(country) {
		case 'Curacao':
			return 'CW';
		case 'Canada':
			return 'CA';
		case 'USA':
			return 'US';
		case 'United Arab Emirates':
			return 'AE';
		case 'Thailand':
			return 'TH';
		case 'Singapore':
			return 'SG';
		case 'United Kingdom':
			return 'UK';
		case 'Australia':
			return 'AU';
		case 'France':
			return 'FR';
		case 'Switzerland':
			return 'CH';
		case 'Qatar':
			return 'QA';
		case 'Kuwait':
			return 'KW';
		case 'Hong Kong':
			return 'HK';
		case 'Morocco':
			return 'MA';
		case 'China':
			return 'CN';
		case 'Taiwan':
			return 'TW';
		case 'Japan':
			return 'JP';
		case 'Greece':
			return 'GR';
		case 'Saudi Arabia':
			return 'SA';
		case 'Bahrain':
			return 'BH';
		case 'Jordan':
			return 'JO';
		case 'Germany':
			return 'DE';
		case 'Mexico':
			return 'MX';
		case 'Portugal':
			return 'PT';
		case 'Belgium':
			return 'BE';
		case 'Spain':
			return 'ES';
		case 'Luxembourg':
			return 'LU';
		case 'Turkey':
			return 'TR';
		case 'Malaysia':
			return 'MY';
		case 'Lebanon':
			return 'LB';
		case 'Vietnam':
			return 'VN';
		case 'Indonesia':
			return 'ID';
		case 'Guam':
			return 'GU';
		case 'United States':
			return 'US';
		case 'USA':
			return 'US';
		case 'Dominican Republic':
			return 'DO';
		case 'Egypt':
			return 'EG';
		case 'Panama':
			return 'PA';
		case 'Aruba':
			return 'AW';
		case 'Bahamas':
			return 'BS';
		case 'Russia':
			return 'RU';
		case 'Chile':
			return 'CL';
		case 'Colombia':
			return 'CO';
		case 'Russian Federation':
			return 'RU';
		case 'Puerto Rico':
			return 'PR';
		case 'Italy':
			return 'IT';
		case 'Romania':
			return 'RO';
		case 'Peru':
			return 'PE';
		case 'Philippines':
			return 'PH';
		case 'Venezuela':
			return 'VE';
		case 'Brazil':
			return 'BR';
		case 'New Zealand':
			return 'NZ';
		case 'Myanmar':
			return 'MM';
		case 'Azerbaijan':
			return 'AZ';
		case 'Tunisia':
			return 'TN';
		case 'Ecuador':
			return 'EC';
		case 'Sweden':
			return 'SE';
		case 'Bulgaria':
			return 'BG';
		default:
			println(country);
			return '';	
	}
}
	
String asString(Cell cell) {
	def sdf = new SimpleDateFormat('yyyy-MM-dd');
	String v = ''
	if (cell == null) {
		return ""
	}
	switch (cell.getCellType()) {
    	case Cell.CELL_TYPE_STRING:
        	v = cell.getStringCellValue()
            break;
        case Cell.CELL_TYPE_NUMERIC:
            if (DateUtil.isCellDateFormatted(cell)) {
         		v = sdf.format(cell.getDateCellValue())
            } else {
               	v = cell.getNumericCellValue().toInteger().toString()
          	}
    		break;
    }	
    if (v == 'NULL') return ''	
	return v
}

String asDouble(Cell cell) {
	if (cell == null || cell.getNumericCellValue() == null) {
		return "";
	}
	return new Double(cell.getNumericCellValue()).toString()
}

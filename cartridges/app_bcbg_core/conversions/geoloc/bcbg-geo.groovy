def uswriter = new PrintWriter(new FileWriter("bcbg-geo-us.xml"))	
def cawriter = new PrintWriter(new FileWriter("bcbg-geo-ca.xml"))	

uswriter.println('<?xml version="1.0"?>')
cawriter.println('<?xml version="1.0"?>')

uswriter.println("<geolocations xmlns=\"http://www.demandware.com/xml/impex/geolocation/2007-05-01\" country-code="US">")
cawriter.println("<geolocations xmlns=\"http://www.demandware.com/xml/impex/geolocation/2007-05-01\" country-code="CA">")

def file = new File("zcug.txt").splitEachLine(",") {fields ->
  def thisWriter = null
  if (trim(fields[7]) == "C") {
  	thisWriter = cawriter
  } else {
    thisWriter = uswriter
  }
  thisWriter.println("  <geolocation postal-code=\"" + trim(fields[2])+ "\">")
  thisWriter.println("    <city>" + trim(fields[0]) + "</city>")
  thisWriter.println("    <state>" + trim(fields[1]) + "</state>")
  thisWriter.println("    <longitude>" + trim(fields[9]) + "</longitude>")
  thisWriter.println("    <latitude>" + trim(fields[8]) + "</latitude>")
  thisWriter.println("  </geolocation>")
}
cawriter.println("</geolocations>")
uswriter.println("</geolocations>")
cawriter.close()
uswriter.close()

def trim(sz) {
	return sz.substring(1, sz.length() - 1)
}
importScript("int_pfs:util/libPFS.ds");
importScript( "int_pfs:util/libPFSRestService.ds" ); 
importScript("util/OrderTools.ds");
importScript( "int_borderfree:common/libBorderFree.ds" );
importScript( 'bc_integrationframework:workflow/libWorkflowLogToFile.ds' );
/**
 * @module calculate.js
 *
 * This javascript file implements methods (via Common.js exports) that are needed by
 * the new (smaller) CalculateCart.ds script file.  This allows OCAPI calls to reference
 * these tools via the OCAPI 'hook' mechanism
 *
 */
var HashMap = require("dw/util").HashMap,
	PromotionMgr = require("dw/campaign").PromotionMgr,
	ShippingMgr = require("dw/order").ShippingMgr,
	ShippingLocation = require("dw/order").ShippingLocation,
	TaxMgr = require("dw/order").TaxMgr,
	Status = require('dw/system/Status');
importPackage( dw.system );

/**
 * @function calculate
 *
 * calculate is the arching logic for computing the value of a basket.  It makes
 * calls into cart/calculate.js and enables both SG and OCAPI applications to share
 * the same cart calculation logic.
 *
 * @param {object} basket The basket to be calculated
 */
exports.calculate = function(basket)
{
    // ===================================================
	// =====   CALCULATE PRODUCT LINE ITEM PRICES    =====
    // ===================================================

	calculateProductPrices(basket);
	
	// ===================================================
	// =====         CALCULATE GIFTING               =====
    // ===================================================

	var giftTotal : dw.value.Money = new dw.value.Money(0, dw.system.Site.current.defaultCurrency);
	var productLineItems : dw.util.Iterator = basket.getAllProductLineItems().iterator();
	while(productLineItems.hasNext()) {
		var productLineItem : dw.order.ProductLineItem = productLineItems.next();
		if (productLineItem.custom.isLineLevelGift) {
			giftTotal = giftTotal.add(new dw.value.Money(dw.system.Site.current.preferences.custom.giftPackagingAmount, dw.system.Site.current.defaultCurrency));
		}
	}	
	
	var pa : dw.order.PriceAdjustment = basket.getPriceAdjustmentByPromotionID('GIFTINGPROMOTION');

	if (giftTotal.value > 0) {
		if (pa == null) {
			pa = basket.createPriceAdjustment('GIFTINGPROMOTION');
		}
		pa.setPriceValue(giftTotal.value);
		pa.updateTax(0, giftTotal);
	} else {
		if (pa) {
			basket.removePriceAdjustment(pa);
		}	
	}

    // ===================================================
	// =====    CALCULATE GIFT CERTIFICATE PRICES    =====
    // ===================================================

	calculateGiftCertificatePrices(basket);

    // ===================================================
	// =====   Note: Promotions must be applied      =====
	// =====   after the tax calculation for         =====
	// =====   storefronts based on GROSS prices     =====
    // ===================================================

    // ===================================================
	// =====   APPLY PROMOTION DISCOUNTS			 =====
	// =====   Apply product and order promotions.   =====
	// =====   Must be done before shipping 		 =====
	// =====   calculation. 					     =====
    // ===================================================

	PromotionMgr.applyDiscounts(basket);

    // ===================================================
	// =====        CALCULATE SHIPPING COSTS         =====
    // ===================================================

	// apply product specific shipping costs
	// and calculate total shipping costs
	if (BorderFree.isInternational() == false) {
		ShippingMgr.applyShippingCost(basket);
		overrideShippingPrice(basket);
	}

    // ===================================================
	// =====   APPLY PROMOTION DISCOUNTS			 =====
	// =====   Apply product and order and 			 =====
	// =====   shipping promotions.                  =====
    // ===================================================

	PromotionMgr.applyDiscounts(basket);

	// since we might have bonus product line items, we need to
	// reset product prices
	calculateProductPrices(basket);

    // ===================================================
	// =====         CALCULATE TAX                   =====
    // ===================================================

	if (BorderFree.isInternational() == false) {  
		calculateTax(basket);
	} else {
		refreshLineItems(basket);	
	}

    // ===================================================
	// =====         CALCULATE BASKET TOTALS         =====
    // ===================================================

	basket.updateTotals();

    // ===================================================
	// =====            DONE                         =====
    // ===================================================

	return new Status(Status.OK);
}

/**
 * @function calculateProductPrices
 *
 * Calculates product prices based on line item quantities. Set calculates prices
 * on the product line items.  This updates the basket and returns nothing
 *
 * @param {object} basket The basket containing the elements to be computed
 */
function calculateProductPrices (basket)
{
	// get total quantities for all products contained in the basket
	var productQuantities : HashMap = basket.getProductQuantities();

	// get product prices for the accumulated product quantities
	var productPrices : HashMap = new HashMap();

	for each(var product : Product in productQuantities.keySet())
	{
		var quantity : Quantity = productQuantities.get(product);
		productPrices.put(product, product.priceModel.getPrice(quantity));
	}

	productPrices = overrideProductPrice(basket, productPrices);
	
	// iterate all product line items of the basket and set prices
	var productLineItems : Iterator = basket.getAllProductLineItems().iterator();
	while(productLineItems.hasNext())
	{
		var productLineItem : ProductLineItem = productLineItems.next();

		// handle non-catalog products
		if(!productLineItem.catalogProduct)
		{
			productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
			continue;
		}

		var product : Product = productLineItem.product;

		// handle option line items
		if(productLineItem.optionProductLineItem)
		{
			// for bonus option line items, we do not update the price
			// the price is set to 0.0 by the promotion engine
			if(!productLineItem.bonusProductLineItem)
			{
				productLineItem.updateOptionPrice();
			}
		}
		// handle bundle line items, but only if they're not a bonus
		else if(productLineItem.bundledProductLineItem)
		{
			// no price is set for bundled product line items
		}
		// handle bonus line items
		// the promotion engine set the price of a bonus product to 0.0
		// we update this price here to the actual product price just to
		// provide the total customer savings in the storefront
		// we have to update the product price as well as the bonus adjustment
		else if(productLineItem.bonusProductLineItem && product != null)
		{
			var price : Money = product.priceModel.price;
			productLineItem.setPriceValue(price.valueOrNull);
			// get the product quantity
			var quantity : Quantity = productLineItem.quantity;
			// we assume that a bonus line item has only one price adjustment
			var adjustments : Collection = productLineItem.priceAdjustments;
			if(!adjustments.isEmpty())
			{
				var adjustment : PriceAdjustment = adjustments.iterator().next();
				var adjustmentPrice : Money = price.multiply(quantity.value).multiply(-1.0);
				adjustment.setPriceValue(adjustmentPrice.valueOrNull);
			}
		}

		// set the product price. Updates the 'basePrice' of the product line item,
		// and either the 'netPrice' or the 'grossPrice' based on the current taxation
		// policy

		// handle product line items unrelated to product
		else if(product == null)
		{
			productLineItem.setPriceValue(null);
		}
		// handle normal product line items
		else
		{
			if (BorderFree.isInternational()) {
				var m : Money = BorderFree.toInternational(product, productPrices.get(product), false);
 				var usPrice : Money = BorderFree.toUS(m).value;
				productLineItem.setPriceValue(usPrice);
			} else {
				productLineItem.setPriceValue(productPrices.get(product).valueOrNull);
			}
		}
    }
}

/**
 * @function calculateGiftCertificates
 *
 * Function sets either the net or gross price attribute of all gift certificate
 * line items of the basket by using the gift certificate base price. It updates the basket in place.
 *
 * @param {object} basket The basket containing the gift certificates
 */
function calculateGiftCertificatePrices (basket)
{
	var giftCertificates : Iterator = basket.getGiftCertificateLineItems().iterator();
	while(giftCertificates.hasNext())
	{
		var giftCertificate : GiftCertificateLineItem = giftCertificates.next();
		giftCertificate.setPriceValue(giftCertificate.basePrice.valueOrNull);
	}
}

/**
 * @function calculateTax <p>
 *
 * Determines tax rates for all line items of the basket. Uses the shipping addresses
 * associated with the basket shipments to determine the appropriate tax jurisdiction.
 * Uses the tax class assigned to products and shipping methods to lookup tax rates. <p>
 *
 * Sets the tax-related fields of the line items. <p>
 *
 * Handles gift certificates, which aren't taxable. <p>
 *
 * Note that the function implements a fallback to the default tax jurisdiction
 * if no other jurisdiction matches the specified shipping location/shipping address.<p>
 *
 * Note that the function implements a fallback to the default tax class if a
 * product or a shipping method does explicitly define a tax class.
 *
 * @param {object} basket The basket containing the elements for which taxes need to be calculated
 */
function calculateTax (basket)
{
	var pliTaxes : HashMap = new HashMap();
	
	if (OrderTools.hasChanged(basket) == false) {
		
	} else {
		//pliTaxes = PFS.newCalculateTax(basket);
		
		var serviceRest : PFSRestService = new PFSRestService();
	try {
		pliTaxes = serviceRest.getVertexOTaxCalculation(basket);
		//return PIPELET_NEXT;
	}
	catch (ex) {
		useBackupCalculation = true;
		Logger.error("CalculateTax.ds (Vertex O) - failed to update tax from service - {0}", ex.message);
	  }
	}
	
	var shipments : Iterator = basket.getShipments().iterator();
	while(shipments.hasNext())
	{
		var shipment : Shipment = shipments.next();

		// first we reset all tax fields of all the line items
		// of the shipment
		var shipmentLineItems : Iterator = shipment.getAllLineItems().iterator();
		while(shipmentLineItems.hasNext())
		{
			var lineItem : LineItem = shipmentLineItems.next();
			// do not touch tax rate for fix rate items
			if(lineItem.taxClassID == TaxMgr.customRateTaxClassID)
			{
				lineItem.updateTax(lineItem.taxRate);
			}
			else
			{
				lineItem.updateTax(null);
			}
		}

		// identify the appropriate tax jurisdiction
		var taxJurisdictionID : String = null;

		// if we have a shipping address, we can determine a tax jurisdiction for it
		if(shipment.shippingAddress != null)
		{
			var location : ShippingLocation = new ShippingLocation(shipment.shippingAddress);
			taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location);
		}

		if(taxJurisdictionID == null)
		{
			taxJurisdictionID = TaxMgr.defaultTaxJurisdictionID;
		}

		// if we have no tax jurisdiction, we cannot calculate tax
		if(taxJurisdictionID == null)
		{
			continue;
		}

		// shipping address and tax juridisction are available
		var shipmentLineItems : Iterator = shipment.getAllLineItems().iterator();
		while(shipmentLineItems.hasNext())
		{
			var lineItem : LineItem = shipmentLineItems.next();
			
			if (pliTaxes.get(lineItem)) {
				var pliTaxRate : Number = pliTaxes.get(lineItem);
				var pliNum = pliTaxRate * 1;
	    		lineItem.updateTax(pliNum);
			} else {
				if ('taxRate1' in lineItem.custom && lineItem.custom.taxRate1 >= 0) {
					lineItem.updateTax(lineItem.custom.taxRate1);
				} else {
					// get the tax rate
					var taxClassID : String = lineItem.taxClassID;

					//Logger.debug("1. Line Item {0} with Tax Class {1} and Tax Rate {2}", lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);

					// do not touch line items with fix tax rate
					if(taxClassID == TaxMgr.customRateTaxClassID)
					{
						continue;
					}

					// line item does not define a valid tax class; let's fall back to default tax class
					if(taxClassID == null)
					{
						taxClassID = TaxMgr.defaultTaxClassID;
					}

					// if we have no tax class, we cannot calculate tax
					if(taxClassID == null)
					{
						//Logger.debug("Line Item {0} has invalid Tax Class {1}", lineItem.lineItemText, lineItem.taxClassID);
						continue;
					}
			
					var taxRate : Number = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID);
					
					if (lineItem instanceof dw.order.ProductLineItem || lineItem instanceof dw.order.ShippingLineItem) {
					} else {
						if (taxRate == null) {
							taxRate = 0;
						}
					}
					// w/o a valid tax rate, we cannot calculate tax for the line item
					if(taxRate == null) {
						continue;
					} else {					
						lineItem.updateTax(taxRate);
					}
				}
			}
			//Logger.debug("2. Line Item {0} with Tax Class {1} and Tax Rate {2}", lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);
		}
	}

	// besides shipment line items, we need to calculate tax for possible order-level price adjustments
    // this includes order-level shipping price adjustments
    if(!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty)
    {
	// calculate a mix tax rate from
	var basketPriceAdjustmentsTaxRate : Number = (basket.getMerchandizeTotalGrossPrice().value / basket.getMerchandizeTotalNetPrice().value) - 1;

	    var basketPriceAdjustments : Iterator = basket.getPriceAdjustments().iterator();
	    while(basketPriceAdjustments.hasNext())
	    {
			var basketPriceAdjustment : PriceAdjustment = basketPriceAdjustments.next();
			basketPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
	    }

	    var basketShippingPriceAdjustments : Iterator = basket.getShippingPriceAdjustments().iterator();
	    while(basketShippingPriceAdjustments.hasNext())
	    {
			var basketShippingPriceAdjustment : PriceAdjustment = basketShippingPriceAdjustments.next();
			basketShippingPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
	    }
	}
}

/**
 * @function refreshLineItems <p>
 *
 * Refresh lineitems;
 *
 * @param {object} basket The basket containing the elements for which taxes need to be calculated
 */
function refreshLineItems (basket)
{
	var shipments : Iterator = basket.getShipments().iterator();
	while(shipments.hasNext())
	{
		var shipment : Shipment = shipments.next();

		// first we reset all tax fields of all the line items
		// of the shipment
		var shipmentLineItems : Iterator = shipment.getAllLineItems().iterator();
		while(shipmentLineItems.hasNext())
		{
			var lineItem : LineItem = shipmentLineItems.next();
			// do not touch tax rate for fix rate items
			if(lineItem.taxClassID == TaxMgr.customRateTaxClassID)
			{
				lineItem.updateTax(lineItem.taxRate);
			}
			else
			{
				lineItem.updateTax(null);
			}
		}

		// identify the appropriate tax jurisdiction
		var taxJurisdictionID : String = null;

		// if we have a shipping address, we can determine a tax jurisdiction for it
		if(shipment.shippingAddress != null)
		{
			var location : ShippingLocation = new ShippingLocation(shipment.shippingAddress);
			taxJurisdictionID = TaxMgr.getTaxJurisdictionID(location);
		}

		if(taxJurisdictionID == null)
		{
			taxJurisdictionID = TaxMgr.defaultTaxJurisdictionID;
		}

		// if we have no tax jurisdiction, we cannot calculate tax
		if(taxJurisdictionID == null)
		{
			continue;
		}

		// shipping address and tax juridisction are available
		var shipmentLineItems : Iterator = shipment.getAllLineItems().iterator();
		while(shipmentLineItems.hasNext())
		{
			var lineItem : LineItem = shipmentLineItems.next();
			
			//Logger.debug("1. Line Item {0}", lineItem.lineItemText);
			
			// calculate the tax of the line item
			
			if ('taxRate1' in lineItem.custom && lineItem.custom.taxRate1 >= 0) {
				lineItem.updateTax(lineItem.custom.taxRate1);
				//Logger.debug("2a. Line Item {0} with Tax Rate {1}", lineItem.lineItemText, lineItem.custom.taxRate1);
			} else {
				var taxClassID : String = lineItem.taxClassID;

				// do not touch line items with fix tax rate
				if(taxClassID == TaxMgr.customRateTaxClassID)
				{
					continue;
				}

				// line item does not define a valid tax class; let's fall back to default tax class
				if(taxClassID == null)
				{
					taxClassID = TaxMgr.defaultTaxClassID;
				}

				// if we have no tax class, we cannot calculate tax
				if(taxClassID == null)
				{
					//Logger.debug("Line Item {0} has invalid Tax Class {1}", lineItem.lineItemText, lineItem.taxClassID);
					continue;
				}

				// get the tax rate
				var taxRate : Number = TaxMgr.getTaxRate(taxClassID, taxJurisdictionID);
				// w/o a valid tax rate, we cannot calculate tax for the line item
				
				if (lineItem instanceof dw.order.ProductLineItem || lineItem instanceof dw.order.ShippingLineItem) {
				} else {
					if (taxRate == null) {
						taxRate = 0;
					}
				}

				if(taxRate == null) {
					continue;
				} else {
					lineItem.updateTax(taxRate);
					
					//Logger.debug("2b. Line Item {0} with Tax Class {1} and Tax Rate {2}", lineItem.lineItemText, lineItem.taxClassID, lineItem.taxRate);
					
				}
			}
		}
	}

	// besides shipment line items, we need to calculate tax for possible order-level price adjustments
    // this includes order-level shipping price adjustments
    if(!basket.getPriceAdjustments().empty || !basket.getShippingPriceAdjustments().empty)
    {
	// calculate a mix tax rate from
	var basketPriceAdjustmentsTaxRate : Number = (basket.getMerchandizeTotalGrossPrice().value / basket.getMerchandizeTotalNetPrice().value) - 1;

	    var basketPriceAdjustments : Iterator = basket.getPriceAdjustments().iterator();
	    while(basketPriceAdjustments.hasNext())
	    {
			var basketPriceAdjustment : PriceAdjustment = basketPriceAdjustments.next();
			basketPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
	    }

	    var basketShippingPriceAdjustments : Iterator = basket.getShippingPriceAdjustments().iterator();
	    while(basketShippingPriceAdjustments.hasNext())
	    {
			var basketShippingPriceAdjustment : PriceAdjustment = basketShippingPriceAdjustments.next();
			basketShippingPriceAdjustment.updateTax(basketPriceAdjustmentsTaxRate);
	    }
	}
}

/**
 * Digital Store Solution Application
 *
 * FUNCTION: overrideProductPrice
 *
 * Function overrides the product price
 */
function overrideProductPrice(basket : Basket, productPrices : HashMap)
{

	var productLineItemsItr : Iterator = basket.getAllProductLineItems().iterator();
	var productPricesMap : HashMap = productPrices;
	while (productLineItemsItr.hasNext()) {

		var productLineItem : ProductLineItem = productLineItemsItr.next();
		if (!empty(productLineItem.custom.eaPriceOverrideType)) {
			
			productLineItem.custom.eaOriginalPrice =  productPricesMap.get(productLineItem.product);
			
			var itemPrice : dw.util.Decimal = new dw.util.Decimal (productPrices.get(productLineItem.product).value);
			var finalPrice : dw.util.Decimal = new dw.util.Decimal(0);
			
			// if the override type is none, the override is being removed
			if (productLineItem.custom.eaPriceOverrideType.equalsIgnoreCase("None")) {
				finalPrice = itemPrice;
				delete productLineItem.custom.eaPriceOverrideType;
				break;
			}
			var itemPriceOverrideValue : dw.util.Decimal = new dw.util.Decimal (productLineItem.custom.eaPriceOverrideValue);
			
			if (productLineItem.custom.eaPriceOverrideType.equalsIgnoreCase("Amount")) {
				finalPrice = itemPrice.subtract(itemPriceOverrideValue);
				
			} else if (productLineItem.custom.eaPriceOverrideType.equalsIgnoreCase("Percent")) {
				finalPrice = itemPrice.subtract(itemPrice.multiply(itemPriceOverrideValue));
				
			} else if (productLineItem.custom.eaPriceOverrideType.equalsIgnoreCase("FixedPrice")) {
				finalPrice = itemPriceOverrideValue;
			} 
			var overridePrice = new dw.value.Money (finalPrice.get(), dw.system.Site.current.defaultCurrency);
			productPricesMap.put(productLineItem.product, overridePrice);
			
		}
	}
	
	return productPricesMap;
}

/**
 * Digital Store Solution Application
 *
 * FUNCTION: overrideShippingPrice
 *
 * Function overrides the shipping price
 */
function overrideShippingPrice(basket : Basket)
{

	var allLineItems : Iterator = basket.getAllLineItems().iterator();
	while(allLineItems.hasNext())
	{
		var lineItem : LineItem = allLineItems.next();
		if (lineItem.describe().ID.equalsIgnoreCase("ShippingLineItem")) {


			if (!empty(lineItem.custom.eaPriceOverrideType)) {
				var finalPrice : dw.util.Decimal = new dw.util.Decimal(0);
				if (basket.defaultShipment.getShippingMethod()) {
					if (basket.adjustedMerchandizeTotalNetPrice.valueOrNull) { 					
						var itemPrice : dw.util.Decimal = new dw.util.Decimal((dw.order.ShippingMgr.getShippingCost(basket.defaultShipment.getShippingMethod(), basket.adjustedMerchandizeTotalNetPrice)).valueOrNull);
						
						// if the override type is none, the override is being removed
						if (lineItem.custom.eaPriceOverrideType.equalsIgnoreCase("None")) {
							finalPrice = itemPrice;
							break
						}
						var itemPriceOverrideValue : dw.util.Decimal = new dw.util.Decimal (lineItem.custom.eaPriceOverrideValue);
						
						if (lineItem.custom.eaPriceOverrideType.equalsIgnoreCase("Amount")) {
							finalPrice = itemPrice.subtract(itemPriceOverrideValue);
							
						} else if (lineItem.custom.eaPriceOverrideType.equalsIgnoreCase("Percent")) {
							finalPrice = itemPrice.subtract(itemPrice.multiply(itemPriceOverrideValue));
							
						} else if (lineItem.custom.eaPriceOverrideType.equalsIgnoreCase("FixedPrice")) {
							finalPrice = itemPriceOverrideValue;
						}
					}
					var overridePrice = new dw.value.Money (finalPrice.get(), dw.system.Site.current.defaultCurrency);
				}
				lineItem.setPriceValue(finalPrice.get());
			}
		}
	}
}
<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isscript>
	importScript( "int_borderfree:common/libBorderFree.ds" );
</isscript>
<isset name="Decorator" value="checkout/pt_checkout" scope="page" />
<isset name="isOPC" value="${'enableOPC' in dw.system.Site.current.preferences.custom && dw.system.Site.getCurrent().getCustomPreferenceValue('enableOPC')}" scope="page" />
<isset name="isInternational" value="${BorderFree.isInternational()}" scope="page" />
<iscomment>
<isif condition="${isInternational == true}">
	<isset name="isOPC" value="${false}" scope="page" />
</isif>
</iscomment>
<isif condition="${isOPC}">
	<isset name="Decorator" value="util/pt_empty" scope="page" />
</isif>
<isdecorate template="${Decorator}"/>
	<isinclude template="util/modules"/>

	<iscomment>
		This template visualizes the billing step of both checkout scenarios.
		It provides selecting a payment method, entering gift certificates and
		specifying a separate billing address.
		Depending on the checkout scenario (single or multi shipping) it is
		either the second or third checkout step.
	</iscomment>
	
	<isscript>
		var showCADisclaimer = false;
		if ('showCADisclaimer' in dw.system.Site.current.preferences.custom) {
			showCADisclaimer = dw.system.Site.current.getCustomPreferenceValue("showCADisclaimer");
		}

		var showEUDisclaimer = false;
		if ('showEUDisclaimer' in dw.system.Site.current.preferences.custom) {
			showEUDisclaimer = dw.system.Site.current.getCustomPreferenceValue("showEUDisclaimer");
		}

		var isCanada = false;
		var ii = BorderFree.getInternationalInfo();
		if (ii.CurrencyCountry == 'CA') {
			isCanada = true;
		}
		var isEU = false;
		try {
			var prefEuMap = dw.system.Site.current.getCustomPreferenceValue("euCountryMap");     
			var euMap = '';
			euMap = JSON.parse(prefEuMap);

			for (var euCode in euMap) {
				if (euMap[euCode] == ii.CurrencyCountry) {
					isEU = true;
					break;
				}
			}
		} catch(e) {
		}
		var countryList = BorderFree.getCountries(false);
		
		var firstNameLabel = '';
		var lastNameLabel = '';
		var address1Label = '';
		var address2Label = '';
		var cityLabel = '';
		var stateLabel = '';
		var stateCALabel = '';
		var regionLabel = '';
		var postalLabel = '';
		var countryLabel = '';
		var phoneLabel = '';
		var emailAddressLabel = '';
		var couponCodeLabel = '';
		var giftCertCodeLabel = '';
		var disabledAttr = null;
		var disabledAttrVal = null;
			
		if (isOPC) {
			firstNameLabel = "*" + Resource.msg('forms.contactus.firstname.label','forms',null);
			lastNameLabel = "*" + Resource.msg('forms.contactus.lastname.label','forms',null);
			address1Label = "*" + Resource.msg('forms.address1','forms',null);
			address2Label = Resource.msg('forms.address2','forms',null);
			cityLabel = "*" + Resource.msg('forms.city','forms',null);
			stateLabel = "*" + Resource.msg('forms.state','forms',null);
			stateCALabel = "*" + Resource.msg('forms.stateprovince','forms',null);
			regionLabel = Resource.msg('forms.region','forms',null);
			postalLabel = "*" + Resource.msg('forms.zipcode','forms',null);
			countryLabel = "*" + Resource.msg('forms.country','forms',null);
			phoneLabel = "*" + Resource.msg('forms.contactus.phone.label','forms',null);
			emailAddressLabel = "*" + Resource.msg('forms.contactus.email.label','forms',null);
			couponCodeLabel = Resource.msg('billingcoupon.couponenter','forms',null);
			giftCertCodeLabel = Resource.msg('billinggiftcert.giftcertlabel','forms',null);
			disabledAttr = "disabled";
			disabledAttrVal = "true";
			
			if (!isInternational) {
				pdict.CurrentForms.billing.billingAddress.addressFields.country.value = "US";
			}
		}
	</isscript>

	<isset name="showBillingAddr" value="opcField " scope="page" />
	<isset name="emailClass" value="email " scope="page" />
	<isif condition="${isOPC && (pdict.CurrentForms.singleshipping != null && pdict.CurrentForms.singleshipping.shippingAddress.useAsBillingAddress.value == true)}">
		<isset name="showBillingAddr" value="opcField opcHidden" scope="page" />
		<isset name="emailClass" value="email opcWider" scope="page" />
	</isif>
	<iscomment>Report this checkout step</iscomment>
	<isreportcheckout checkoutstep="4" checkoutname="${'Billing'}"/>

	<iscomment> ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		checkout progress indicator
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

	<isif condition="${!isOPC}">
		<ischeckoutprogressindicator step="4" multishipping="false" rendershipping="true"/>
	<iselse/>
		<script>
			var shippingFields = {
				"firstName" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.firstName.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.firstName.value : ''}",
				"lastName" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.lastName.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.lastName.value : ''}",
				"address1" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address1.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address1.value : ''}",
				"address2" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address2.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.address2.value : ''}",
				"city" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.city.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.city.value : ''}",
				"state" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.states.state.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.states.state.value : ''}",
				"stateCA" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.states.stateCA.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.states.stateCA.value : ''}",
				"region" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.region.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.region.value : ''}",
				"postal" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.postal.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.postal.value : ''}",
				"country" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.country.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.country.value : ''}",
				"phone" : "${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.phone.value != null ? pdict.CurrentForms.singleshipping.shippingAddress.addressFields.phone.value : ''}",
			}
		</script>
	</isif>
	
	<form action="${URLUtils.continueURL()}" method="post" id="${pdict.CurrentForms.billing.htmlName}" class="checkout-billing address">

	<input type="hidden" name="bfToken" id="bfToken"/>
	<input type="hidden" name="bfTokenType" id="bfTokenType"/>
	
	<iscomment> ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		billing address
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

	<isset name="afterpayError" value="${!empty(pdict.AfterpayApiError) ? pdict.AfterpayApiError : (!empty(request.httpParameterMap.get('afterpay')).stringValue ? request.httpParameterMap.get('afterpay').stringValue : null)}" scope="page"/>

	<isif condition="${!empty(afterpayError)}">
		<div class="error-form">
			<isprint value="${afterpayError}" encoding="off" />
		</div>
	</isif>

	<fieldset id="onepage_billing_fieldset">

		<iscomment>billing address area</iscomment>

		<iscomment>hidden input with action to trigger for the address form</iscomment>
		<input type="hidden" name="${pdict.CurrentForms.billing.save.htmlName}" value="true" />

		<div class="billing-title">
			${Resource.msg('minibillinginfo.billingaddress','checkout',null)}
		</div>
		
		<isif condition="${isOPC}">
			<div class="form-row fullWidth label-inline  ">
				<input class="input-checkbox " type="checkbox" <isif condition="${(pdict.CurrentForms.singleshipping != null && pdict.CurrentForms.singleshipping.shippingAddress.useAsBillingAddress.value == true)}">checked="checked"</isif> name="shipAsBill" id="shipAsBill" value="true">
				<label for="shipAsBill">${Resource.msg('checkout.shipasbill','checkout',null)}</label>
			</div>
		</isif>
		<iscomment>display select box with stored addresses if customer is authenticated and there are saved addresses</iscomment>
		<isif condition="${pdict.CurrentCustomer.authenticated && pdict.CurrentCustomer.profile.addressBook.addresses.size() > 0}">
			<div class="billing_addr_selector ${showBillingAddr}">
				<div class="select-address">
					<div class="form-row fullWidth">
						<label for="${pdict.CurrentForms.billing.addressList.htmlName}">
							${Resource.msg('billing.chooseanaddress','checkout',null)}
						</label>
						<isscript>
							importScript("cart/CartUtils.ds");
							var customerAddresses = CartUtils.getAddressList(pdict.Basket, pdict.CurrentCustomer, false);
						</isscript>
						<isaddressselectlist p_listId="${pdict.CurrentForms.billing.addressList.htmlName}" p_listaddresses="${customerAddresses}" />
					</div>
	
					<div class="form-row form-row-button">
						<button id="address-select-go-button" name="${pdict.CurrentForms.billing.selectAddress.htmlName}" type="submit" value="Go" class="simple-submit">Select</button>
					</div>
	
				</div>	
			</div>
		</isif>

		<div>
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.firstName}" type="input" rowclass="${'firstName clear ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.firstname.missing','forms',null)}" placeholdertext="${firstNameLabel}" />
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.lastName}" type="input" rowclass="${'lastName ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.lastname.missing','forms',null)}" placeholdertext="${lastNameLabel}" />
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.address1}" type="input" rowclass="${'billingaddress1 clear ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.address1.missing','forms',null)}" placeholdertext="${address1Label}" />
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.address2}" type="input" rowclass="${'address2 ' + showBillingAddr}" placeholdertext="${address2Label}" />
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.city}" type="input" rowclass="${'city clear ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.city.missing','forms',null)}" placeholdertext="${cityLabel}" />

			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.states.state}" type="select" rowclass="${'state ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.state.missing','forms',null)}" placeholdertext="${stateLabel}" />
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.states.stateCA}" type="select" rowclass="${'stateCA ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.province.missing','forms',null)}" placeholdertext="${stateCALabel}" />
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.region}" type="input" rowclass="${'region ' + showBillingAddr}" placeholdertext="${regionLabel}" />

			<iscomment>		
			<isif condition="${BorderFree.isInternational()}">
				<isif condition="${isCanada}">
					<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.states.stateCA}" type="select" rowclass="${'state ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.province.missing','forms',null)}" />
				<iselse/>
					<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.region}" type="input" rowclass="${'region ' + showBillingAddr}" />
				</isif>
			<iselse/>
	   			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.states.state}" type="select" rowclass="${'state ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.state.missing','forms',null)}" />
	    	</isif>		
	    	</iscomment>

            <isscript>
                 var postalCodeCountries = [ 
                    'DZ', 'AD', 'AM', 'AU', 'AT', 'AZ', 'BD', 'BY', 'BE', 'BR', 'BN', 'BG', 'CA', 'CN', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'GF', 'GE', 'DE', 'GR', 'GL', 'GP', 'GG', 'VA', 'HU', 'IS', 'IN', 'ID', 'IL',
                    'IT', 'JP', 'JE', 'KZ', 'KG', 'LV', 'LI', 'LT', 'LU', 'MK', 'MG', 'MY', 'MV', 'MH', 'MQ', 'MR', 'YT', 'MX',
                    'MD', 'MC', 'MN', 'ME', 'MA', 'NA', 'NL', 'NC', 'NZ', 'NO', 'PW', 'PK', 'PG', 'PH', 'PL', 'PT', 'RE', 'RO',
                    'RU', 'PM', 'RS', 'SG', 'SK', 'SI', 'ZA', 'KR', 'ES', 'SZ', 'SE', 'CH', 'TW', 'TJ', 'TH', 'TN', 'TR', 'UA',
                    'GB', 'UZ'
                ];
            </isscript>
            
            <isif condition="${pdict.CurrentSession.custom.CurrencyCountry == 'US'}">
			    <isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.postal}" type="tel" rowclass="${'postal clear ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.postal.missing','forms',null)}" placeholdertext="${postalLabel}" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{5,12}$" inputmode="text"/>
            <iselseif condition="${postalCodeCountries.indexOf(pdict.CurrentSession.custom.CurrencyCountry) != -1}"/>
			    <isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.postal}" type="input" rowclass="${'postal clear ' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.postal.missing','forms',null)}" placeholdertext="${postalLabel}" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{5,12}$" inputmode="text"/>
            <iselse/>
			    <isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.postal}" type="input" rowclass="${'postal clear ' + showBillingAddr}" placeholdertext="${postalLabel}" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{5,12}$" inputmode="text" fieldvalue="N/A" o_mandatory="false"/>
            </isif>

			<isif condition="${BorderFree.isInternational()}">
				<div class="form-row country required ${showBillingAddr}" aria-required="true">
					<isif condition="${!isOPC}">
					<label for="dwfrm_billing_billingAddress_addressFields_country">
						<span>${Resource.msg('forms.country', 'forms', null)}</span>
						<span class="required-indicator">*</span>
					</label>
					</isif>
					<isset name="currencyCountry" scope="page" value="${BorderFree.getInternationalInfo().CurrencyCountry}"/>
					<isif condition="${pdict.CurrentForms.billing.billingAddress.addressFields.country && pdict.CurrentForms.billing.billingAddress.addressFields.country.htmlValue != ''}">
						<isset name="currencyCountry" scope="page" value="${pdict.CurrentForms.billing.billingAddress.addressFields.country.htmlValue}"/>
					</isif>
					
					<select class="countryValue" name="${pdict.CurrentForms.billing.billingAddress.addressFields.country.htmlName}" value="${BorderFree.getInternationalInfo().CurrencyCountry}">
						<option value="">${countryLabel}</option>
						<isloop items="${countryList}" var="c">
							<isif condition="${c.code == currencyCountry}">
								<option selected value="${c.code+'_'+c.currency}">${c.name}</option>
							<iselse/>
								<option value="${c.code+'_'+c.currency}">${c.name}</option>
							</isif>
						</isloop>			
					</select>
				</div>		
			<iselse/>
	    		<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.country}" type="select" rowclass="${'country ' + showBillingAddr}" xhtmlclass="countryValue" requiredtext="${Resource.msg('forms.address.country.missing','forms',null)}" placeholdertext="${countryLabel}" attribute1="${disabledAttr}" value1="${disabledAttrVal}" />
	    	</isif>		

			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addressFields.phone}" type="input" rowclass="${'phone clear ' + '' + showBillingAddr}" requiredtext="${Resource.msg('forms.address.phone.missing','forms',null)}" placeholdertext="${phoneLabel}" pattern="[0-9]*" inputmode="numeric"/>

			<label class="emailLabel">${Resource.msg('billing.emaillabel','checkout',null)}</label>
			<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.email.emailAddress}" xhtmlclass="email" type="input" rowclass="${emailClass}" requiredtext="${Resource.msg('forms.address.email.invalid','forms',null)}" placeholdertext="${emailAddressLabel}" />

			<iscomment>provide option to add address to address book if customer is authenticated</iscomment>
			<isif condition="${pdict.CurrentCustomer.authenticated}">
				<isinputfield formfield="${pdict.CurrentForms.billing.billingAddress.addToAddressBook}" type="checkbox" rowclass="${'label-inline '  + showBillingAddr}" placeholdertext="${}" />
			</isif>
			
			<isif condition="${isCanada && showCADisclaimer}">
				<div class="form-row  label-inline addtoemaillist caaddto checkpos fullWidth">
					<input class="input-checkbox " type="checkbox" name="dwfrm_billing_billingAddress_addToEmailList" id="dwfrm_billing_billingAddress_addToEmailList" value="true">
					<label for="dwfrm_billing_billingAddress_addToEmailList">${Resource.msg('signup.msg','locale',null)}</label>
					<iscontentasset aid="signupmsg"/>
				</div>
			<iselseif condition="${isEU && showEUDisclaimer}">
				<div class="form-row  label-inline addtoemaillist checkpos fullWidth">
					<input class="input-checkbox " type="checkbox" name="dwfrm_billing_billingAddress_addToEmailList" id="dwfrm_billing_billingAddress_addToEmailList" value="true">
					<label for="dwfrm_billing_billingAddress_addToEmailList">${Resource.msg('signup.msg','locale',null)}</label>
				</div>
			<iselse/>
				<div class="form-row label-inline addtoemaillist fullWidth">
					<input class="input-checkbox " type="checkbox" name="dwfrm_billing_billingAddress_addToEmailList" id="dwfrm_billing_billingAddress_addToEmailList" value="true">
					<label for="dwfrm_billing_billingAddress_addToEmailList"><isprint value="${Resource.msgf('billing.addtoemaillist', 'checkout', null, URLUtils.url('Page-Show', 'cid', 'cs-privacy-policy'))}" encoding="off"/></label>
					<p><isprint value="${Resource.msg('billing.emaildisclaimer', 'checkout', null)}" encoding="off"/></p>
				</div>
			</isif>

			<isif condition="${BorderFree.isInternational() && BorderFree.getInternationalInfo().UseCheckoutGate}">
				<div class="form-row  label-inline intCheck checkpos">
					<input class="input-checkbox required" type="checkbox" name="dwfrm_billing_billingAddress_acceptInternational" id="dwfrm_billing_billingAddress_acceptInternational" value="true" required />
					<label for="dwfrm_billing_billingAddress_acceptInternational"><iscontentasset aid="eusignupmsg"/></label>
				</div>
			</isif>

			<iscomment>
			<div class="form-caption">
				<a title="${Resource.msg('global.privacypolicy','locale',null)}" href="${URLUtils.url('Page-Show','cid','cs-privacy-policy')}" class="privacy-policy" >${Resource.msg('global.privacypolicy','locale',null)}</a>
			</div>
			</iscomment>
		</div>
	</fieldset>

	<iscomment> ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		coupon / discount codes
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>

	<fieldset id="coupon_gift_codes">
		<div id="coupon_gift_area">
		<input type="hidden" name="${pdict.CurrentForms.billing.secureKeyHtmlName}" value="${pdict.CurrentForms.billing.secureKeyValue}"/>

		<isset name="gcApplicable" value="${'false'}" scope="page"/>
		<isif condition="${dw.order.PaymentMgr.getPaymentMethod(dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE).isActive() }">
			<isset name="gcApplicable" value="${'true'}" scope="page"/>
		</isif>
		
		<div class="billing-title">
			${Resource.msg('minibillinginfo.paymentmethod','checkout',null)}
		</div>		

		<isif condition="${BorderFree.isInternational() == false}">		
			<a href="#promoToggle" class="toggleSlide">${Resource.msg('checkout.haveapromocode','checkout',null)}</a>

			<div id="promoToggle">
				
				<div>
					<isinputfield formfield="${pdict.CurrentForms.billing.couponCode}" type="input" rowclass="couponCode" placeholdertext="${couponCodeLabel}" />
				
					<div class="form-row apply-button">
						<button class="checkoutCouponApply" id="add-coupon" type="submit" name="${pdict.CurrentForms.billing.applyCoupon.htmlName}" value="${Resource.msg('global.apply','locale',null)}"><span>${Resource.msg('global.apply','locale',null)}</span></button>
					</div>
					<div class="couponCodeMessaging">
						<div class="form-row coupon-error error">
							<isif condition="${pdict.CouponStatus != null && pdict.CouponStatus.error}">
								${Resource.msgf('cart.' + pdict.CouponStatus.code,'checkout', null, pdict.CurrentForms.billing.couponCode.htmlValue)}
							</isif>
						</div>
						<div class="redemption coupon form-row"><!-- container for coupon applied success messages -->
							<isif condition="${pdict.Basket.couponLineItems.length > 0}">
								<isloop items="${pdict.Basket.couponLineItems}" var="couponLineItem" status="cliloopstate">
									<isif condition="${couponLineItem.valid && couponLineItem.applied}">
										<span class="success">${Resource.msgf('billing.couponapplied', 'checkout', null, couponLineItem.couponCode)}</span>
									</isif>
								</isloop>
							</isif>
						</div>
					</div>
				</div>
				<isif condition="${isOPC}">
					<div id="coupon_gift_code_lines">
						<isloop items="${pdict.Basket.couponLineItems}" var="couponLineItem" status="cliloopstate">
							<isif condition="${couponLineItem.valid && couponLineItem.applied}">
								<div class="couponCodeLine">
									<a href="${'#'}" data-code="${couponLineItem.couponCode}" class="couponCodeLink">Remove ${couponLineItem.couponCode}</a>
								</div>
							</isif>
						</isloop>
					</div>
				</isif>
			</div>
		</isif>	
			<isset name="gcPITotal" value="${0}" scope="pdict"/>
			<isset name="OrderTotal" value="${pdict.Basket.totalGrossPrice.value}" scope="pdict"/>
			<isif condition="${gcApplicable == 'true'}">

			<iscomment>only provide gift certificate redemption, if the basket doesn't contain any gift certificates</iscomment>
			<isif condition="${pdict.Basket.giftCertificateLineItems.size() == 0}">

				<iscontentasset aid="checkout-giftcert-help"/>

				<div>
					<isinputfield formfield="${pdict.CurrentForms.billing.giftCertCode}" type="input" rowclass="label-above" placeholdertext="${giftCertCodeLabel}" />

					<div class="form-row">
						<button class="apply cancel" id="add-giftcert" type="submit" name="${pdict.CurrentForms.billing.redeemGiftCert.htmlName}" value="${Resource.msg('global.apply','locale',null)}"><span>${Resource.msg('global.apply','locale',null)}</span></button>
					</div>
					<div class="form-row">
						<button class="button-text cancel" id="check-giftcert" type="submit" name="${pdict.CurrentForms.billing.checkBalance.htmlName}" value="${Resource.msg('global.checkbalance','locale',null)}"><span>${Resource.msg('global.checkbalance','locale',null)}</span></button>
					</div>

					<div class="form-row balance"></div>
					<div class="form-row giftcert-error error">
						<isif condition="${pdict.GiftCertStatus && pdict.GiftCertStatus.error}">
							${Resource.msg('billing.' + pdict.GiftCertStatus.code, 'checkout', pdict.GiftCertStatus.code)}
						</isif>
					</div>
					<div class="form-row redemption giftcert">
						<iscomment>render gift cert redeemed success message for each gift certificate payment instrument</iscomment>
						<isset name="gcPITotal" value="${0}" scope="pdict"/>
						<isif condition="${pdict.Basket.giftCertificatePaymentInstruments.size() > 0}">
							<isloop items="${pdict.Basket.giftCertificatePaymentInstruments}" var="giftCertPI">
								<isset name="gcPITotal" value="${pdict.gcPITotal + giftCertPI.paymentTransaction.amount}" scope="pdict"/>
								<div class="success giftcert-pi" id="gc-${giftCertPI.getGiftCertificateCode()}">
									${Resource.msgf('billing.giftcertredeemed', 'checkout', null, giftCertPI.paymentTransaction.amount, giftCertPI.getMaskedGiftCertificateCode())}
									<a id="rgc-${giftCertPI.getGiftCertificateCode()}" class="remove" href="${URLUtils.https('COBilling-RemoveGiftCertificate', 'giftCertificateID', giftCertPI.getGiftCertificateCode(),'format','old')}">
										<img src="${URLUtils.staticURL('/images/interface/icon_remove.gif')}" alt="${Resource.msg('global.remove','locale',null)}"/>
										<span>${Resource.msg('global.remove','locale',null)}</span>
									</a>
								</div>
							</isloop>

							<isif condition="${pdict.Basket.totalGrossPrice!=null && pdict.Basket.totalGrossPrice.value-pdict.gcPITotal<=0}">
								<isset name="OrderTotal" value="${pdict.Basket.totalGrossPrice.value-pdict.gcPITotal}" scope="pdict"/>
							</isif>
						</isif>

					</div>
				</div>
			</isif>

		</isif>
		</div>
	</fieldset>

	<iscomment> ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		payment methods
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++</iscomment>


	<iscomment>payment method area</iscomment>
	<isif condition="${BorderFree.isInternational()}">
		<isinclude template="checkout/borderfree_paymentmethods"/>
	<iselse/>
		<isinclude template="checkout/billing/paymentmethods"/>
	</isif>
	
	<isbonusdiscountlineitem p_alert_text="${Resource.msg('billing.bonusproductalert','checkout',null)}" p_discount_line_item="${pdict.BonusDiscountLineItem}"/>

	<div class="billing-buttons">
		<a class="billing-prev-button" href="${URLUtils.url('COShipping-Start')}">${Resource.msg('global.previous','locale',null)}</a>			
		<button class="checkout-submit" type="submit" name="${pdict.CurrentForms.billing.save.htmlName}" value="${Resource.msg('global.continuecheckoutbrief','locale',null)}"><span>${Resource.msg('checkoutprogressindicator.revieworder','checkout',null)}</span></button>
		<isif condition="${!isOPC}">
			<p>${Resource.msg('checkout.placeorder','checkout',null)}</p>
		</isif>
	</div>

</form>



<isscript>
	importScript("util/ViewHelpers.ds");
	var addressForm = pdict.CurrentForms.billing.billingAddress.addressFields;
	var countries = ViewHelpers.getCountriesAndRegions(addressForm);
	var json = JSON.stringify(countries);
</isscript>
<script>
	window.Countries = <isprint value="${json}" encoding="off"/>;
</script>
<iscomment>
<isif condition="${BorderFree.isInternational()}">
	<div id="borderfree-terms">
		<isprint value="${Resource.msg('fiftyone.terms', 'fiftyone', null)}" encoding="off"/>
	</div>
</isif>
</iscomment>

</isdecorate>

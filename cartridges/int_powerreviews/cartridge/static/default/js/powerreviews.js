// ***** OLD POWER REVIEWS TEMPLATE. BCBG AND GEN USE THEIR OWN TEMPALTES NOW *******

//'use strict';

// var powerReviews = {
// 	// Setting page index on the API call to grab 5 at a time
// 	getPageIndex : function() {
// 		var pageIndex = 0;
// 	},
// 	getReviews : function(pageStart, pageSize) {
// 		getPageIndex()
// 		var pageSize = 5;
// 		var merchantIDString = $('#pr-data').data('mid'); 
// 		var apiKey = $('#pr-data').data('api');
// 		var product = $('#pid').val()
// 		var merchantID = parseInt(merchantIDString);
// 		var url = "https://readservices-b2c.powerreviews.com/m/"+merchantID+"/l/en_US/product/"+product+"/reviews?apikey="+apiKey+"&paging.from="+pageStart+"&paging.size="+pageSize
// 		var reviewAuthor = function (name, location, age, height, sizeFit, comments, recommend, headline) {		
// 			return `<div class="pr-review-author">
// 						<div class="pr-review-author-info-wrapper">
// 							<div class="name-location-container">
// 								<p class="pr-review-author-name">
// 									<span>${name}</span>
// 								</p>
// 								<p class="pr-review-author-name">
// 									<span>${location}</span>
// 								</p>
// 							</div>
// 							<div class="age-height-container">
// 								<p class="pr-review-author-name">
// 									<span>Age:<span class="review-props"> ${age || ""}</span></span>
// 								</p> 
// 								<p class="pr-review-author-name">
// 									<span>Height:<span class="review-props"> ${height || ""}</span></span>
// 								</p>
// 							</div>
// 						</div>
// 					</div>
// 					<div class="pr-review-main-wrapper">
// 						<div class="pr-review-text">
// 							<p class="pr-headline">${headline}</p>
// 							<p class="pr-comments">${comments}</p>
// 							<span class="sizefit-wrap size-and-fit">Size and Fit:<span class="review-props"> ${sizeFit || ""}</span></span><br>
// 							<span class="sizefit-wrap size-and-fit-mobile">Fits:<span class="review-props"> ${sizeFit || ""}</span></span><br>
// 							<span class="sizefit-wrap recommend"> Recommended.<span class="review-recommend"> ${recommend || ""}: </span></span>
// 						</div>
// 					</div>`
// 		};
// 		// Display the rating sprite position 
// 		var ratingPosition = function (rating) {
// 			switch (rating) {
// 				case 1:
// 					return "background-position: 0px -36px;";
// 					break;
// 				case 2:
// 					return "background-position: 0px -72px;";
// 					break;
// 				case 3:
// 					return "background-position: 0px -108px;";
// 					break;
// 				case 4:
// 					return "background-position: 0px -144px;";
// 					break;
// 				case 5:
// 					return "background-position: 0px -180px;";
// 					break;
// 				default:
// 					return ""
// 			}
// 		};
// 		// Display the rating
// 		var reviewRating = function (rating, formatDate) {
// 			var position = ratingPosition(rating);
// 			return `<div class="pr-review-rating-wrapper">
// 						<div class="pr-review-author-date pr-rounded">${formatDate}</div>
// 						<div class="pr-review-rating">
// 							<div class="pr-stars pr-stars-small pr-stars-${rating}-sm" style="${position}">&nbsp;</div>
// 								<span class="pr-rating pr-rounded"><meta content="${rating}">${rating}</span>
// 							<p class="pr-review-rating-headline></p>
// 						</div>
// 					</div>`
// 		}
// 		$.ajax({
// 			url: url,
// 			dataType: 'json',
// 			success: function (res) {
// 				// Setting API
// 				var data = res.results;
// 				pageSize = res.paging.page_size;
// 				var totalResults = res.paging.total_results;
// 				$('.pr-snapshot-average-based-on-text span').html('')
// 				$('.pr-snapshot-average-based-on-text span').append(totalResults);
// 				$('.pr-snippet-stars').append('<span class="total-reviews-upper"></span>')
// 				$('.total-reviews-upper').append(totalResults);
// 				if ( totalResults == 0 ) {
// 					$('.reviews-total').hide();
// 					$('#reviews-display').css({"position": "relative","justify-content":"center", "align-items":"center"});
// 					$('.pr-rd-no-reviews').css({"padding-left": "0"});
// 					$('.pr-rd-no-reviews').prepend('Not yet rated. Be the first to');
// 					$('.pr-snapshot-body-wrapper .pr-snapshot-footer').css({"position": "relative"});
// 					$('.pr-snippet-rating-decimal').hide();
// 					$('.total-reviews-upper').hide();
// 					$('.pr-snippet-wrapper').css({"display": "flex"});
// 					$('.pr-snippet-read-write').css({"position": "relative"});
// 					$('.pr-snapshot-body-wrapper .pr-snapshot-footer').css({"position": "relative"});
// 					$('.pr-snapshot-body-wrapper .pr-snapshot-footer').addClass('no-reviews')
// 				}

// 				var btn = $('#readmorebtn #read-more-reviews');
// 				var reviews = $('#reviewsContainer .pr-review-wrap');
// 				var btnArrow = $('#readmorebtn p');
// 				// Hide read more reviews button if total results is less than 5
// 				if(totalResults < 5){
// 					btn.addClass("hide");
// 					btnArrow.addClass("hide");
// 					reviews.removeClass("hide");
// 				}
// 				$.each(data, function (i) {
// 					var innerData = data[i].reviews;		

// 					//Looping through customer data
// 					$.each(innerData, function (i) {
// 						var userInfo = innerData[i];
// 						var userRating = userInfo.metrics.rating;
// 						var date = userInfo.details.created_date;
// 						var formatDate = new Date(date).toLocaleDateString();
// 						var userHeight;
// 						var userAge;
// 						var sizeFit;
// 						var userProps = userInfo.details.properties;
// 						var recommend = userInfo.details.bottom_line;
// 						var headline = userInfo.details.headline;

// 						var singleReview = $('<div class="pr-review-wrap"></div>');
// 						singleReview.append(reviewRating(userRating, formatDate));
// 						// Find specific questions on form
// 						$.each(userProps, function (i) {
// 							var userKeys = userProps[i].key
// 							if (userKeys == "howoldareyou" && userProps[i].value !== undefined) {
// 								userAge = userProps[i].value;
// 							}
// 							if (userKeys == "whatisyourheight" && userProps[i].value !== undefined) {
// 								userHeight = userProps[i].value;
// 							}
// 							if (userKeys == "sizeandfit" && userProps[i].value !== undefined) {
// 								sizeFit = userProps[i].value;
// 							}
// 						})
// 						singleReview.append(reviewAuthor(userInfo.details.nickname, userInfo.details.location, userAge, userHeight, sizeFit, userInfo.details.comments, recommend, headline));
// 						$('.pr-review-engine #reviewsContainer').append(singleReview);
					
// 					});
// 					// Remove button if we have shown all reviews
// 					if($('#reviewsContainer .pr-review-wrap').length === totalResults) {
// 						btn.addClass("hide");
// 						btnArrow.addClass("hide");
// 					}
// 				});
				
// 			}
// 		});	
// 		bcbgMobile();
// 		bcbgReize()
// 		bcbgOnLoad()
// 	},
// 	// $(document).ready(function(){
// 	// 	console.log('pdp ran')
// 	// 	if ( $('#pdpMain').length ) {
// 	// 		setTimeout(function(){ 
// 	// 			getReviews(pageIndex, pageSize);
// 	// 			readMoreReviews()
// 	// 		}, 1000);
// 	// 	}
// 	// 	$("#reviewssnippet").click(function() {
// 	// 		$('html, body').animate({
// 	// 			scrollTop: $("#reviewsAnswers").offset().top - $(".fixedHeader").height()
// 	// 		}, 2000);
// 	// 	});
// 	// })
// 	readMoreReviews : function() {
// 		var btn = $('<div id="read-more-reviews">READ MORE REVIEWS</div><p><i class="down"></i></p>');	
// 		if (btn) {
// 			$('#readmorebtn').append(btn);
// 			$('#reviewsAnswers-mobile #readmorebtn').append(btn)
// 			btn.click(function () {
// 				pageIndex += pageSize;
// 				getReviews(pageIndex, pageSize);
// 			})
// 		}	
// 	},
// 	// BCBG Site - Displays the reviews on the divs without cloning them
// 	bcbgMobile : function () {
// 		var reviewAnswersAdd = $('.bcbg-power-reviews');
// 	},
// 	bcbgReize : function() {
// 		$(window).resize(function() {
// 			var windowWidth = $(this).width();
// 			if (windowWidth < 767) {
// 				reviewAnswersAdd.detach();
// 				reviewAnswersAdd.appendTo('#reviewsAnswers-mobile');
// 			}
// 			else {
// 				reviewAnswersAdd.detach() ;
// 				reviewAnswersAdd .appendTo('#reviewsAnswersDesktop');
// 			}
// 		})
// 	},
// 	bcbgOnLoad : function() {
// 		if (windowWidth < 767) {
// 			var windowWidth = $(this).width();
// 			 reviewAnswersAdd.detach();
// 			 reviewAnswersAdd.appendTo('#reviewsAnswers-mobile');
// 		 }
// 		 else {
// 			 reviewAnswersAdd.detach();
// 			 reviewAnswersAdd .appendTo('#reviewsAnswersDesktop');
// 		 }
// 	 },
// };

/**
 * Resource helper
 *
 */

importPackage(dw.system);

function ResourceHelper() {}
	/**
	 * Get the client-side constants
	 * @returns {Object} An objects key key-value pairs holding the constants
	 */
	ResourceHelper.getConstants = function(pageContext) {
		var ProductAvailabilityModel = require('dw/catalog/ProductAvailabilityModel');
		return {
			AVAIL_STATUS_IN_STOCK 		: ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK,
			AVAIL_STATUS_PREORDER 		: ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER,
			AVAIL_STATUS_BACKORDER 		: ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER,
			AVAIL_STATUS_NOT_AVAILABLE 	: ProductAvailabilityModel.AVAILABILITY_STATUS_NOT_AVAILABLE,
			AFTERPAY_PORTAL				: Site.getCurrent().getCustomPreferenceValue('apJavascriptURL')
		};
	}
	/**
	 * Get the client-side resources of a given page
	 * @returns {Object} An objects key key-value pairs holding the resources
	 */
	ResourceHelper.getResources = function(pageContext) {
		var Resource = require('dw/web/Resource');
		var ProductAvailabilityModel = require('dw/catalog/ProductAvailabilityModel');

		// application resources
		var resources = {
				// Common
				I_AGREE							: Resource.msg('i_agree', 'common', null),
				CLOSE 							: Resource.msg('close', 'common', null),
				NO_THANKS						: Resource.msg('global.nothanks', 'locale', null),
				QUICKVIEW						: Resource.msg('quickview', 'common', null),
				SKIP						    : Resource.msg('skip', 'common', null),

				// Checkout
				SHIP_QualifiesFor 				: Resource.msg('shipment.qualifiesfor', 'checkout', null),
				CC_LOAD_ERROR 					: Resource.msg('billing.creditcardloaderror', 'checkout', null),

				// Registry resources
				REG_ADDR_ERROR 					: Resource.msg('global.couldntloadaddress', 'locale', null),

				// bonus products messages
				BONUS_PRODUCT 					: Resource.msg('product.bonusproduct', 'product', null),
				BONUS_PRODUCTS 					: Resource.msg('product.bonusproducts', 'product', null),
				SELECT_BONUS_PRODUCTS 			: Resource.msg('product.selectbonusproducts', 'product', null),
				SELECT_BONUS_PRODUCT 			: Resource.msg('product.selectbonusproduct', 'product', null),
				BONUS_PRODUCT_MAX 				: Resource.msg('product.bonusproductsmax', 'product', null),
				BONUS_PRODUCT_TOOMANY 			: Resource.msg('product.bonusproductstoomany', 'product', null),
				SIMPLE_SEARCH 					: Resource.msg('simplesearch.searchtext', 'search', null),
				SUBSCRIBE_EMAIL_DEFAULT			: Resource.msg('forms.subscribe.email.default', 'forms', 'Email Address'),

				CURRENCY_SYMBOL					: dw.util.Currency.getCurrency(dw.system.Site.current.defaultCurrency).symbol,
				MISSINGVAL						: Resource.msg('global.missingval', 'locale', null),
				SERVER_ERROR 					: Resource.msg('global.servererror', 'locale', null),
				MISSING_LIB 					: Resource.msg('global.missinglib', 'locale', null),
				BAD_RESPONSE					: Resource.msg('global.badresponse', 'locale', null),
				INVALID_PHONE					: Resource.msg('global.invalidphone', 'locale', null),
				INVALID_COUPON					: Resource.msg('global.invalidcoupon', 'locale', null),
				INVALID_EMAIL					: Resource.msg('forms.address.email.invalid', 'forms', null),
				REMOVE							: Resource.msg('global.remove', 'locale', null),
				QTY								: Resource.msg('global.qty', 'locale', null),
				EMPTY_IMG_ALT					: Resource.msg('global.remove', 'locale', null),
				COMPARE_BUTTON_LABEL			: Resource.msg('productcomparewidget.compareitemsbutton', 'search', null),
				COMPARE_CONFIRMATION			: Resource.msg('productcomparewidget.maxproducts', 'search', null),
				COMPARE_REMOVE_FAIL				: Resource.msg('productcomparewidget.removefail', 'search', null),
				COMPARE_ADD_FAIL				: Resource.msg('productcomparewidget.addfail', 'search', null),
				ADD_TO_CART_FAIL				: Resource.msg('cart.unableToAdd', 'checkout', null),
				ADD_TO_CART_SIZE_FAIL			: Resource.msg('cart.sizeError', 'checkout', null),
				CONTACT_US_QUESTION_FAIL		: Resource.msg('forms.contactus.category.error', 'forms', null),
				CONTACT_US_COMMENT_FAIL			: Resource.msg('forms.contactus.msg.error', 'forms', null),
				CONTACT_US_FNAME_FAIL			: Resource.msg('forms.contactus.firstname.error', 'forms', null),
				CONTACT_US_LNAME_FAIL			: Resource.msg('forms.contactus.lastname.error', 'forms', null),
				CONTACT_US_EMAIL_FAIL			: Resource.msg('forms.contactus.email.error', 'forms', null),
				CONTACT_US_PHONE_FAIL			: Resource.msg('forms.contactus.phone.error', 'forms', null),
				ADD_TO_CART_COLOR_FAIL			: Resource.msg('cart.colorError', 'checkout', null),
				REGISTRY_SEARCH_ADVANCED_CLOSE	: Resource.msg('giftregistry.closeadvanced', 'account', null),
				GIFT_CERT_INVALID				: Resource.msg('billing.giftcertinvalid', 'checkout', null),
				GIFT_CERT_BALANCE				: Resource.msg('billing.giftcertbalance', 'checkout', null),
				GIFT_CERT_AMOUNT_INVALID		: Resource.msg('giftcert.amountvalueerror', 'forms', null),
				GIFT_CERT_MISSING				: Resource.msg('billing.giftcertidmissing', 'checkout', null),
				INVALID_OWNER                   : Resource.msg('billing.ownerparseerror', 'checkout', null),
				COUPON_CODE_MISSING				: Resource.msg('cart.COUPON_CODE_MISSING', 'checkout',  null),
				COOKIES_DISABLED				: Resource.msg('browsertoolscheck.cookies', 'components', null),
				BML_AGREE_TO_TERMS				: Resource.msg('bml.termserror', 'forms', null),
				CHAR_LIMIT_MSG					: Resource.msg('forms.character.limit', 'forms', null),
				CONFIRM_DELETE					: Resource.msg('forms.confirm.delete', 'forms', null),
				TITLE_GIFTREGISTRY				: Resource.msg('forms.title.giftregistry', 'forms', null),
				TITLE_ADDRESS					: Resource.msg('forms.title.address', 'forms', null),
				TITLE_CREDITCARD				: Resource.msg('forms.title.creditcard', 'forms', null),
				SERVER_CONNECTION_ERROR 		: Resource.msg('global.serverconnection', 'locale', 'Server connection failed!'),
				IN_STOCK_DATE					: Resource.msg('global.inStockDate', 'locale', null),
				ITEM_STATUS_NOTAVAILABLE		: Resource.msg('global.allnotavailable', 'locale', null),
				INIFINITESCROLL					: Resource.msg('paginginformation.infinite-scroll', 'search', null),
				STORE_NEAR_YOU					: Resource.msg('storelist.lightbox.whatsavailable', 'storepickup', 'What\'s available at a store near you'),
				SELECT_STORE					: Resource.msg('storelist.lightbox.selectstore', 'storepickup', null),
				SELECTED_STORE					: Resource.msg('storelist.lightbox.selectedstore', 'storepickup', null),
				PREFERRED_STORE					: Resource.msg('storelist.lightbox.preferredstore', 'storepickup', null),
				SET_PREFERRED_STORE				: Resource.msg('storelist.lightbox.setpreferredstore', 'storepickup', null),
				ENTER_ZIP						: Resource.msg('storelist.lightbox.enterzip', 'storepickup', null),
				INVALID_ZIP						: Resource.msg('storelist.lightbox.invalidpostalcode', 'storepickup', null),
				SEARCH							: Resource.msg('storelist.lightbox.search', 'storepickup', null),
				CHANGE_LOCATION					: Resource.msg('storelist.lightbox.changelocation', 'storepickup', null),
				CONTINUE_WITH_STORE				: Resource.msg('storelist.lightbox.continuewithstore', 'storepickup', null),
				CONTINUE						: Resource.msg('storelist.lightbox.continue', 'storepickup', null),
				SEE_MORE						: Resource.msg('storelist.lightbox.seemore', 'storepickup', null),
				SEE_LESS						: Resource.msg('storelist.lightbox.seeless', 'storepickup', null),
				FOOTER_EMAIL_SIGNUP				: Resource.msg('footer.emailsignup', 'components', null),
				ACCOUNT_CONFIRM					: Resource.msg('forms.account.confirmemail', 'forms', null),
				ACCOUNT_PASSWORD				: Resource.msg('forms.account.confirmpassword', 'forms', null),
				ACCOUNT_EMAILEXISTS				: Resource.msg('forms.account.emailexists', 'forms', null),
				ACCOUNT_EMAILEXISTSPASSWORD		: Resource.msg('forms.account.emailexistspassword', 'forms', null),
				ACCOUNT_GENERALERROR			: Resource.msg('forms.account.generalerror', 'forms', null),
				ACCOUNT_PASSFAIL				: Resource.msg('forms.registration.password.error', 'forms', null),
				FORM_REQUIRED					: Resource.msg('global.thisisrequired', 'locale', null),
				FOOTER_EMAIL_VALIDATION 	    : Resource.msg('email.signup.validation','email',null)
				
		};

		// additional resources
		resources[ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK] = Resource.msg('global.instock', 'locale', null);
		resources["QTY_" + ProductAvailabilityModel.AVAILABILITY_STATUS_IN_STOCK] = Resource.msg('global.quantityinstock', 'locale', null);
		resources[ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER] = Resource.msg('global.allpreorder', 'locale', null);
		resources["QTY_" + ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER] = Resource.msg('global.quantitypreorder', 'locale', null);
		resources["REMAIN_" + ProductAvailabilityModel.AVAILABILITY_STATUS_PREORDER] = Resource.msg('global.remainingpreorder', 'locale', null);
		resources[ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER] = Resource.msg('global.allbackorder', 'locale', null);
		resources["QTY_" + ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER] = Resource.msg('global.quantitybackorder', 'locale', null);
		resources["REMAIN_" + ProductAvailabilityModel.AVAILABILITY_STATUS_BACKORDER] = Resource.msg('global.remainingbackorder', 'locale', null);
		resources[ProductAvailabilityModel.AVAILABILITY_STATUS_NOT_AVAILABLE] = Resource.msg('global.allnotavailable', 'locale', null);
		resources["REMAIN_" + ProductAvailabilityModel.AVAILABILITY_STATUS_NOT_AVAILABLE] = Resource.msg('global.remainingnotavailable', 'locale', null);

		return resources;
	}
	/**
	 * Get the client-side URLs of a given page
	 * @returns {Object} An objects key key-value pairs holding the URLs
	 */
	ResourceHelper.getUrls = function(pageContext) {
		var URLUtils = require('dw/web/URLUtils');
		var Resource = require('dw/web/Resource');

		// application urls
		var urls =  {
				appResources				: URLUtils.url('Resources-Load').toString(),
				pageInclude					: URLUtils.url('Page-Include').toString(),
				continueUrl 				: request.isHttpSecure() ? URLUtils.httpsContinue().toString() : URLUtils.httpContinue().toString(),
				staticPath					: URLUtils.staticURL("/").toString(),
				addGiftCert					: URLUtils.url('GiftCert-Purchase').toString(),
				minicartGC					: URLUtils.url('GiftCert-ShowMiniCart').toString(),
				addProduct					: URLUtils.url('Cart-AddProduct').toString(),
				minicart					: URLUtils.url('Cart-MiniAddProduct').toString(),
				cartShow 					: URLUtils.url('Cart-Show').toString(),
				giftRegAdd					: URLUtils.https('Address-GetAddressDetails', 'addressID', '').toString(),
				paymentsList				: URLUtils.https('PaymentInstruments-List').toString(),
				addressesList				: URLUtils.https('Address-List').toString(),
				wishlistAddress				: URLUtils.https('Wishlist-SetShippingAddress').toString(),
				wishlistAdd 				: URLUtils.https('Wishlist-Add').toString(),
				deleteAddress				: URLUtils.url('Address-Delete').toString(),
				getProductUrl 				: URLUtils.url('Product-Show').toString(),
				getBonusProducts			: URLUtils.url('Product-GetBonusProducts').toString(),
				addBonusProduct				: URLUtils.url('Cart-AddBonusProduct').toString(),
				getSetItem 					: URLUtils.url('Product-GetSetItem').toString(),
				productDetail 				: URLUtils.url('Product-Detail').toString(),
				getAvailability				: URLUtils.url('Product-GetAvailability').toString(),
				removeImg 					: URLUtils.staticURL('/images/interface/icon_remove.gif').toString(),
				searchsuggest 				: URLUtils.url('Search-GetSuggestions').toString(),
				productNav 					: URLUtils.url('Product-Productnav').toString(),
				summaryRefreshURL			: URLUtils.url('COBilling-UpdateSummary').toString(),
				billingSelectCC				: URLUtils.https('COBilling-SelectCreditCard').toString(),
				updateAddressDetails		: URLUtils.https('COShipping-UpdateAddressDetails').toString(),
				updateAddressDetailsBilling : URLUtils.https('COBilling-UpdateAddressDetails').toString(),
				shippingMethodsJSON			: URLUtils.https('COShipping-GetApplicableShippingMethodsJSON').toString(),
				shippingMethodsList			: URLUtils.https('COShipping-UpdateShippingMethodList').toString(),
				selectShippingMethodsList	: URLUtils.https('COShipping-SelectShippingMethod').toString(),
				resetPaymentForms 			: URLUtils.url('COBilling-ResetPaymentForms').toString(),
				compareShow					: URLUtils.url('Compare-Show').toString(),
				compareAdd					: URLUtils.url('Compare-AddProduct').toString(),
				compareRemove				: URLUtils.url('Compare-RemoveProduct').toString(),
				compareEmptyImage			: URLUtils.staticURL('/images/comparewidgetempty.png').toString(),
				giftCardCheckBalance		: URLUtils.https('COBilling-GetGiftCertificateBalance').toString(),
				redeemGiftCert				: URLUtils.https('COBilling-RedeemGiftCertificateJson').toString(),
				addCoupon					: URLUtils.https('Cart-AddCouponJson').toString(),
				storeLocatorPage			: URLUtils.https('StoreInventory-Find').toString(),
				storesInventory				: URLUtils.https('StoreInventory-Inventory').toString(),
				storesLookup				: URLUtils.https('StoreInventory-Lookup').toString(),
				setPreferredStore			: URLUtils.url('StoreInventory-SetPreferredStore').toString(),
				getPreferredStore			: URLUtils.url('StoreInventory-GetPreferredStore').toString(),
				setZipCode					: URLUtils.url('StoreInventory-SetZipCode').toString(),
				getZipCode					: URLUtils.url('StoreInventory-GetZipCode').toString(),
				billing						: URLUtils.url('COBilling-Start').toString(),
				currencyConverter			: URLUtils.url('Currency-SetSessionCurrency').toString(),
				addEditAddress				: URLUtils.url('COShippingMultiple-AddEditAddressJSON').toString(),
				cookieHint 					: URLUtils.url('Page-Include', 'cid', 'cookie_hint').toString(),
				registerAccount				: URLUtils.https('Account-StartRegister').toString(),
				paypalUrl					: URLUtils.https('BorderFree-PayPalURL').toString(),
				tealiumProductJson			: URLUtils.url('Tealium-GetProductJson').toString(),
				tealiumGetCustomerID		: URLUtils.url('Tealium-GetCustomerID').toString(),
				shipping					: URLUtils.https('COShipping-Start').toString(),
				account						: URLUtils.https('COCustomer-Start').toString(),
				summary						: URLUtils.https('COSummary-Start').toString(),
				confirmation				: URLUtils.https('COSummary-ShowConfirmation').toString(),
				onepagesummary				: URLUtils.https('Checkout-RefreshSummary').toString(),
				removecoupon				: URLUtils.https('Checkout-RemoveCoupon').toString(),
				headercustomer				: URLUtils.https('Home-IncludeHeaderCustomerInfo').toString(),
				refreshshipping				: URLUtils.https('Checkout-RefreshShipping').toString(),
				refreshbilling				: URLUtils.https('Checkout-RefreshBilling').toString(),
				getProductVariantID			: URLUtils.https('Product-GetVariants').toString(),
				getSizeChart		        : URLUtils.url('SizeChart-Show').toString(),
				mcSubscribeModal			: URLUtils.https('MCSubscription-Subscribe').toString(),
				mcSubscribeFooter			: URLUtils.https('MCSubscription-SubscribeFooter').toString(),
				mcSubscribeInfo				: URLUtils.https('MCSubscription-SubscribeInfo').toString(),
				mcSubscribeInitialSubmit	: URLUtils.https('MCSubscription-InitialSubmit').toString(),
				mcSubscribeSubmit			: URLUtils.https('MCSubscription-Submit').toString(),
				mcSubscribeLPSubscribe	    : URLUtils.https('MCSubscription-LPSubscribe').toString(),
				mcCheckSubscriber			: URLUtils.https('MCSubscription-CheckSubscriber').toString(),
				mcUpdateFormInfo			: URLUtils.https('MCSubscription-UpdateFormInfo').toString()

		};
		return urls;

	}
	/**
	 * Get the client-side preferences of a given page
	 * @returns {Object} An objects key key-value pairs holding the preferences
	 */
	ResourceHelper.getPreferences = function(pageContext) {
		var cookieHintAsset = dw.content.ContentMgr.getContent('cookie_hint');
		return {
			LISTING_INFINITE_SCROLL: (dw.system.Site.getCurrent().getCustomPreferenceValue('enableInfiniteScroll') ? true : false),
			LISTING_REFINE_SORT: true,
			LISTING_SEARCHSUGGEST_LEGACY: (dw.system.Site.getCurrent().getCustomPreferenceValue('enhancedSearchSuggestions') ? false : true),
			STORE_PICKUP: dw.system.Site.getCurrent().getCustomPreferenceValue('enableStorePickUp'),
			COOKIE_HINT: (cookieHintAsset && cookieHintAsset.online) || false
		};
	}
	/**
	 * Get the client-side user settings
	 * @returns {Object} An objects key key-value pairs holding the settings
	 */
	ResourceHelper.getUserSettings = function(pageContext) {
		var ProductAvailabilityModel = require('dw/catalog/ProductAvailabilityModel');
		return {
			zip: session.custom.zipcode == "null" ? null : session.custom.zipcode,
			storeId: session.custom.storeId == "null" ? null : session.custom.storeId
		};
	}

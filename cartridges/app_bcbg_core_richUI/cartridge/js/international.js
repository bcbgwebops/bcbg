'use strict';

var ajax = require('./ajax'),
	page = require('./page'),
	util = require('./util'),
	dialog = require('./dialog');

var countryCode = '';

exports.init = function () {
	
	$(document).on('click touchend', '.intl_cancel', function() {
		dialog.close();
	});

	$('.internationalShip').on('click', function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		var locale = $(this).attr('locale');
		dialog.open({
			url: url,
			options: {
				autoOpen: false,
				modal: true,
				width: "auto",
				create: function( event, ui ) {
					$(this).css("maxWidth", "640px");
				}
			}
		});		
	});
	
	$(document).on('click', '.intl_continue', function(e) {
		document.cookie = 'Lang=' + countryCode + '; expires=Fri, 31 Dec 2100 23:59:59 GMT';
	});
	
	$(document).on('click', '.intl_update', function(e) {
		e.preventDefault();
		var changeShip = ($('#intl_shipto').val() != '-1');
		if (changeShip) {
			var parts = $('#intl_shipto').val().split('_');
			if (parts.length > 1) {
				var url = $(this).attr('href') + '&currencyCountry=' + parts[0] + '&currency=' + parts[1];
				var tourl = window.location.href;
				if (tourl.indexOf('International-') == -1) {
					url += '&from=' + encodeURI(window.location.href);
				}
				window.location = url;
				return;
			}
			return;
		} 
			
	});
};


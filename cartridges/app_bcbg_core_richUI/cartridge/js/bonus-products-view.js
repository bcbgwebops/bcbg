'use strict';

var dialog = require('./dialog'),
	page = require('./page'),
	util = require('./util');

var selectedList = [];
var maxItems = 1;
var bliUUID = '';

/**
 * @private
 * @function
 * description Gets a list of bonus products related to a promoted product
 */
function getBonusProducts() {
	var o = {};
	o.bonusproducts = [];

	var i, len;
	for (i = 0, len = selectedList.length; i < len; i++) {
		var p = {
			pid: selectedList[i].pid,
			qty: selectedList[i].qty,
			options: {}
		};
		var a, alen, bp = selectedList[i];
		if (bp.options) {
			for (a = 0, alen = bp.options.length; a < alen; a++) {
				var opt = bp.options[a];
				p.options = {optionName:opt.name, optionValue:opt.value};
			}
		}
		o.bonusproducts.push({product:p});
	}
	return o;
}

var selectedItemTemplate = function (data) {
	var attributes = '';
	for (var attrID in data.attributes) {
		var attr = data.attributes[attrID];
		attributes += '<li data-attribute-id="' + attrID + '">\n';
		attributes += '<span class="display-name">' + attr.displayName + '</span>: ';
		attributes += '<span class="display-value">' + attr.displayValue + '</span>\n';
		attributes += '</li>';
	}
	attributes += '<li class="item-qty">\n';
	attributes += '<span class="display-name">Qty</span>: ';
	attributes += '<span class="display-value">' + data.qty + '</span>';
	return [
		'<li class="selected-bonus-item" data-uuid="' + data.uuid + '" data-pid="' + data.pid + '">',
		'<div class="remove-link" title="Remove this product" href="#">EDIT</div>',
		'<div class="item-name">' + data.name + '</div>',
		'<ul class="item-attributes">',
		attributes,
		'<ul>',
		'<li>'
	].join('\n');
};

// hide swatches that are not selected or not part of a Product Variation Group
var hideSwatches = function () {
	$('.bonus-product-item .swatches li').not('.selected').not('.variation-group-value').hide();
	// prevent unselecting the selected variant
	$('.bonus-product-item .swatches .selected').on('click', function () {
		return false;
	});
	$('.size-variation.selected').on('click', function () {
		return false;
	});
};

/**
 * @private
 * @function
 * @description Updates the summary page with the selected bonus product
 */
function updateSummary() {
	var $bonusProductList = $('#bonus-product-list');
	if (selectedList.length === 0) {
		$bonusProductList.find('li.selected-bonus-item').remove();
		$('#bonus-product-list-footer').removeClass("isActive");
	} else {
		var ulList = $bonusProductList.find('ul.selected-bonus-items').first();
		var i, len;
		for (i = 0, len = selectedList.length; i < len; i++) {
			var item = selectedList[i];
			var li = selectedItemTemplate(item);
			$(li).appendTo(ulList);
		}
		$('#bonus-product-list-footer').addClass("isActive");
	}

	// get remaining item count
	var remain = maxItems - selectedList.length;
	$bonusProductList.find('.bonus-items-available').text(remain);
	if (remain <= 0) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	} else {
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
	}
}

function initializeGrid () {
	var $bonusProduct = $('#bonus-product-dialog'),
		$bonusProductList = $('#bonus-product-list'),
	bliData = $bonusProductList.data('line-item-detail');
	maxItems = bliData.maxItems;
	bliUUID = bliData.uuid;

	if (bliData.itemCount >= maxItems) {
		$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
	}

	var cartItems = $bonusProductList.find('.selected-bonus-item');
	cartItems.each(function () {
		var ci = $(this);
		var product = {
			uuid: ci.data('uuid'),
			pid: ci.data('pid'),
			qty: ci.find('.item-qty').text(),
			name: ci.find('.item-name').html(),
			attributes: {}
		};
		var attributes = ci.find('ul.item-attributes li');
		attributes.each(function () {
			var li = $(this);
			product.attributes[li.data('attributeId')] = {
				displayName:li.children('.display-name').html(),
				displayValue:li.children('.display-value').html()
			};
		});
		selectedList.push(product);
	});

	$bonusProductList.on('click', '.bonus-product-item a[href].swatchanchor', function (e) {
		e.preventDefault();
		var url = this.href,
			$this = $(this);
		url = util.appendParamsToUrl(url, {
			'source': 'bonus',
			'format': 'ajax'
		});
		$.ajax({
			url: url,
			success: function (response) {
				$this.closest('.bonus-product-item').empty().html(response);
				hideSwatches();
			}
		});
	})
	.on('change', '.input-text', function () {
		$bonusProductList.find('.select-bonus-item').removeAttr('disabled');
		$(this).closest('.bonus-product-form').find('.quantity-error').text('');
	})
	.on('click', '.select-bonus-item', function (e) {
		e.preventDefault();
		if (selectedList.length >= maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			$bonusProductList.find('.bonus-items-available').text('0');
			return;
		}

		var form = $(this).closest('.bonus-product-form'),
			detail = $(this).closest('.product-detail'),
			uuid = form.find('input[name="productUUID"]').val(),
			qtyVal = form.find('input[name="Quantity"]').val(),
			qty = (isNaN(qtyVal)) ? 1 : (+qtyVal);

		if (qty > maxItems) {
			$bonusProductList.find('.select-bonus-item').attr('disabled', 'disabled');
			form.find('.quantity-error').text(Resources.BONUS_PRODUCT_TOOMANY);
			return;
		}

		var product = {
			uuid: uuid,
			pid: form.find('input[name="pid"]').val(),
			qty: qty,
			name: detail.find('.product-name').text(),
			attributes: detail.find('.product-variations').data('attributes'),
			options: []
		};

		var optionSelects = form.find('.product-option');

		optionSelects.each(function () {
			product.options.push({
				name: this.name,
				value: $(this).val(),
				display: $(this).children(':selected').first().html()
			});
		});
		selectedList.push(product);
		updateSummary();
		//auto click add to cart
		$('.add-to-cart-bonus').click();
	})
	.on('click', '.remove-link', function (e) {
		e.preventDefault();
		var container = $(this).closest('.selected-bonus-item');
		if (!container.data('uuid')) { return; }

		var uuid = container.data('uuid');
		var i, len = selectedList.length;
		for (i = 0; i < len; i++) {
			if (selectedList[i].uuid === uuid) {
				selectedList.splice(i, 1);
				break;
			}
		}
		updateSummary();
		
		$('.add-to-cart-bonus').removeAttr('disabled');
	})
	.on('click', '.add-to-cart-bonus', function (e) {
		e.preventDefault();
		
		if(selectedList.length > 0){
			
			var url = util.appendParamsToUrl(Urls.addBonusProduct, {bonusDiscountLineItemUUID: bliUUID});
			var bonusProducts = getBonusProducts();
			if (bonusProducts.bonusproducts[0].product.qty > maxItems) {
				bonusProducts.bonusproducts[0].product.qty = maxItems;
			}
			// make the server call
			$.ajax({
				type: 'POST',
				dataType: 'json',
				cache: false,
				contentType: 'application/json',
				url: url,
				data: JSON.stringify(bonusProducts)
			})
			.done(function () {
				// success
				location.reload();
				
			})
			.fail(function (xhr, textStatus) {
				// failed
				if (textStatus === 'parsererror') {
					window.alert(Resources.BAD_RESPONSE);
				} else {
					window.alert(Resources.SERVER_CONNECTION_ERROR);
				}
			})
			.always(function () {
				$bonusProduct.dialog('close');
			});
		
		}
		
	});
}

function modifyProductDisplay(){
	var displayItem = $('.bonus-product-item .product-detail').first();
	$( "#bonus-product-list .bonus-product-item" ).each(function( index ) {

	  var sizeBTN = $(this).find('.select-bonus-item span');
	  sizeBTN.text($(this).find('.size-variation.selected').text());

	});  
	$('.product-add-to-cart').appendTo( displayItem );
	$('.product-add-to-cart').find('h2.visually-hidden').parent().hide();
}

function disableAddToBagBTN(){

	if($(".select-bonus-item").is(":disabled") && selectedList.length > 0){
		$('.add-to-cart-bonus').attr('disabled', 'disabled');
	}
}

function reverseProductDisplayOrder(){
	var list = $('#bonus-product-list');
	var listItems = list.children('.bonus-product-item');
	list.append(listItems.get().reverse());
	list.append($('.bonus-product-list-footer'));
}


var bonusProductsView = {
	/**
	 * @function
	 * @description Open the list of bonus products selection dialog
	 */
	show: function (url) {
		var $bonusProduct = $('#bonus-product-dialog');
		// create the dialog
		dialog.open({
			target: $bonusProduct,
			url: url,
			options: {
				width: 595
			},
			callback: function () {
				initializeGrid();
				reverseProductDisplayOrder();
				hideSwatches();
				modifyProductDisplay();
				updateSummary();
				$("body").addClass("bonusopen");
				$("body").on( "dialogclose", function( event, ui ) { $("body").removeClass("bonusopen"); } );
			}  
		});
	},
	/**
	 * @function
	 * @description Open bonus product promo prompt dialog
	 */
	loadBonusOption: function () {
		var	self = this,
			bonusDiscountContainer = document.querySelector('.bonus-discount-container');
		if (!bonusDiscountContainer) { return; }

		// get the html from minicart, then trash it
		var bonusDiscountContainerHtml = bonusDiscountContainer.outerHTML;
		bonusDiscountContainer.parentNode.removeChild(bonusDiscountContainer);
		
		var bonuspos;
		if ($(window).width() < 768) {
			//hide the mini cart display for the special case.
			window.firstbonusopen = true;
			bonuspos = ['center',20]
		}else{
			bonuspos = {
			           my: "center center",
			           at: "center center",
			           of: window
					}
		}

		dialog.open({
			html: bonusDiscountContainerHtml,
			options: {
				width: 400,
				position: bonuspos,
				buttons: [{
					text: Resources.SELECT_BONUS_PRODUCTS,
					click: function () {
						var uuid = $('.bonus-product-promo').data('lineitemid'),
							url = util.appendParamsToUrl(Urls.getBonusProducts, {
								bonusDiscountLineItemUUID: uuid,
								source: 'bonus'
							});
						$(this).dialog('close');
						window.firstbonusopen = false;
						self.show(url);
					}
				}, {
					text: Resources.SKIP,
					click: function () {
						$(this).dialog('close');
						window.firstbonusopen = false;
					}
				}]
			},
			callback: function () {
				// show hide promo details
				$('.show-promo-details').on('click', function () {
					$('.promo-details').toggleClass('visible');
				});
				
				$('.ui-dialog-buttonpane').css("text-align", "center");
				$('.ui-dialog-buttonset').css("float", "none");
			}
		});
	}
};

function createOrder() {
	var url = util.ajaxUrl(Urls.createAfterpayOrder);
	ajax.getJson({
		url: url,
		callback: function (data) {
			if (data) {
				checkAuthorize(data);
			}
		}
	});
}

function checkAuthorize(data) {
	if (window.AfterPay) {
		AfterPay.init();
		AfterPay.display({token: data.token});
	} else {
		setTimeout(checkAuthorize.bind(null, data), 3000);
	}
}

function initScript() {
	var script = document.createElement('script');
	script.src = SitePreferences.AFTERPAY_PORTAL;
	script.async = true;
	document.body.appendChild(script);
}

function initSubmitOrderEvent() {
	var $form = $('.checkout-billing');
	var $paymentMethods = $form.find('input[name$="_selectedPaymentMethodID"]');
	var $btnContinue = $form.find('button[name$="_billing_save"]');

	$btnContinue.on('click', function(e) {
		if ($paymentMethods.filter(':checked').val() === 'AFTERPAY_PBI') {
			e.preventDefault();
			createOrder();
			return false;
		}
	});

	var $declined = $('.ap-declined'),
		$form = $('.submit-order');

	if ($declined.length > 0) {
		$form.on('submit', function (e) {
			e.preventDefault();
			window.location = window.Urls.billing;
		});
	} else {
		$form.off('submit').on('submit', function (e) {
			return true;
		});
	}
}

module.exports = bonusProductsView;

exports.init = function () {
	initScript();
	initEvent();
};

module.exports.initSubmitOrderEvent = function () {
	initSubmitOrderEvent();
};

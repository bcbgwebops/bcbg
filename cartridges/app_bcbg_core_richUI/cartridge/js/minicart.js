'use strict';

var util = require('./util'),
	bonusProductsView = require('./bonus-products-view');

var timer = {
	id: null,
	clear: function () {
		if (this.id) {
			window.clearTimeout(this.id);
			delete this.id;
		}
	},
	start: function (duration, callback) {
		this.id = setTimeout(callback, duration);
	}
};

var minicart = {
	
	init: function () {
		timer.clear();

		this.$el = $('#mini-cart');
		this.$content = this.$el.find('.mini-cart-content');
		this.$minicartLink = this.$el.find('.mini-cart-link');
		
		this.$minicartLink.on('mouseenter', function () {
			if (this.$content.not(':visible')) {
				this.slide();
			}
		}.bind(this));
		
		// Remove this event due to conflicting with edit and remove link click event.
		/*
		this.$content.on('mouseenter', function () {
			timer.clear();
		}).on('mouseleave', function () {
			timer.clear();
			timer.start(30, this.close.bind(this));
		}.bind(this));
		*/
		
		// close button event
		this.$el.find('.close').on('click', function () {
			timer.clear();
			timer.start(30, this.close.bind(this));
		}.bind(this));
	
		// jcarousel for last viewed
		$('.jc-lastviewed').jcarousel();
		
        $('.jc-lastviewed-prev')
        	.on('jcarouselcontrol:active', function() {
        		$(this).removeClass('inactive');
        	})
        	.on('jcarouselcontrol:inactive', function() {
        		$(this).addClass('inactive');
        	})
        	.jcarouselControl({
        		target: '-=1'
        });

        $('.jc-lastviewed-next')
        	.on('jcarouselcontrol:active', function() {
        		$(this).removeClass('inactive');
        	})
        	.on('jcarouselcontrol:inactive', function() {
        		$(this).addClass('inactive');
        	})
        	.jcarouselControl({
        		target: '+=1'
        });	
	},
	/**
	 * @function
	 * @description Shows the given content in the mini cart
	 * @param {String} A HTML string with the content which will be shown
	 */
	show: function (html) {
		var isQvDialog = ($('#QuickViewDialog').length > 0);
		this.$el.html(html);
		if(!isQvDialog) util.scrollBrowser(0);
		this.init();
		bonusProductsView.loadBonusOption();
		if(window.firstbonusopen){
		}else{
			this.slide();
		}
		
		if (typeof sr_updateMessages == 'function' && sr_updateMessages ) {
			sr_updateMessages();
		}
	},
	/**
	 * @function
	 * @description Slides down and show the contents of the mini cart
	 */
	slide: function () {
		timer.clear();
		// show the item
		
		if (screen.width < 768) {
			this.$content.slideDown('slow');
		} else {
			if (this.$content.size() > 0) {
				$("#mini-cart").addClass("open");
				this.$content.show("slide", { direction: "right" }, 1000);
			}
		}	
		// after a time out automatically close it
		timer.start(6000, this.close.bind(this));
	},
	/**
	 * @function
	 * @description Closes the mini cart with given delay
	 * @param {Number} delay The delay in milliseconds
	 */
	close: function (delay) {
		timer.clear();
		if (screen.width < 768) {
			this.$content.slideUp(delay);
		} else {
			this.$content.hide("slide", { direction: "right" }, 1000);
			$("#mini-cart").removeClass("open");
		}
	}	
};

module.exports = minicart;

$(document).on('click', ".remove-minicart", function (e) {
	e.preventDefault();

	var pid = $(this).attr('name');
	var deleteurl = $(this).attr('value');	
	
	// success
	var successFunc = function(req)	{
		$('#mini-cart').html(req);
		timer.clear();
		minicart.init();
		minicart.close();
		minicart.slide();
	}

	// fail
	var errFunc = function(req) {
		//console.log("Error");
	}

	jQuery.ajax({
		type	: "POST",
		url		: deleteurl,
		cache	: false,
		data	: {"pid":pid},
		success	: successFunc,
		error	: errFunc
	});
	//return false;
});


$(document).on('click', ".bodyOverlay", function(e) {
	e.preventDefault();
	minicart.init();
	timer.clear();
	timer.start(30, minicart.$content.hide("slide", { direction: "right" }, 1000));
	$("#mini-cart").removeClass("open");
});
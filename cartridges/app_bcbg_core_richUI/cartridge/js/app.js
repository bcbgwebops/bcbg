/**
 *    (c) 2009-2014 Demandware Inc.
 *    Subject to standard usage terms and conditions
 *    For all details and documentation:
 *    https://bitbucket.com/demandware/sitegenesis
 */

'use strict';

var ajax = require('./ajax'),
    countries = require('./countries'),
    dialog = require('./dialog'),
    international = require('./international'),
    minicart = require('./minicart'),
    multicurrency = require('./multicurrency'),
    page = require('./page'),
    rating = require('./rating'),
    responsys = require('./responsys'),
    searchplaceholder = require('./searchplaceholder'),
    searchsuggest = require('./searchsuggest'),
    searchsuggestbeta = require('./searchsuggest-beta'),
    tooltip = require('./tooltip'),
    util = require('./util'),
    validator = require('./validator'),
    quickview = require('./quickview');

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
    var s = document.createElement('script');
    s.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
}

require('./jquery-ext')();
require('./cookieprivacy')();
require('picturefill')();
require('lazysizes');

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function initializeEvents() {
    var controlKeys = ['8', '13', '46', '45', '36', '35', '38', '37', '40', '39'];

    //slider for header-banner (promos in header)
    $('.promo-slides').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        arrows: false,
        autoplay: true,
        cssEase: 'linear'
    });

    $('input.phone').blur(function () {
        var value = $(this).val();
        $(this).val(value.replace(/\D/g, ''));
    });

    //count the number of columns each L1 dropdown will have to determine its class to determine the width
    $('.level-1 > li').each(function () {
        var columnCount = $(this).find("ul").length;
        $(this).addClass("columns-" + columnCount);
    });
    if ($(".home-a span").length) {
        BackgroundCheck.init({
            targets: '.home-a span',
            images: '.home-a img'
        });

        BackgroundCheck.refresh();
    }

	/*//Make sure that L1 nav menu's don't extend off of right side of screen when opened
	$('.level-1 > li').mouseover(function(){
		var $levelTwo = $(this).children('.level-2');
		if($levelTwo.length){
            //reset left in case this has been run already and then the screen size changes
            //$levelTwo.css("left", "-20px");

            var vpW = $(window).width(), // Viewport Width
                right = $levelTwo.offset().left + $levelTwo.width(); //right edge of dropdown
            //if right edge flows offscreen, move to the left.
            if (right > vpW) {
                var leftPos = (right - vpW + 20) * -1;
                leftPos = leftPos + "px";
                $levelTwo.css("left", leftPos);
            }
        }
	});*/

    //Same as above, but for utility menus
    $('.upperMenus > ul > li').mouseover(function () {
        var $levelTwo = $(this).children('ul');
        if ($levelTwo.length) {
            //reset left in case this has been run already and then the screen size changes
            $levelTwo.css("left", "-10px");

            var vpW = $(window).width(), // Viewport Width
                right = $levelTwo.offset().left + $levelTwo.outerWidth(); //right edge of dropdown
            //if right edge flows offscreen, move to the left.
            if (right > vpW) {
                var leftPos = (right - vpW + 10) * -1;
                leftPos = leftPos + "px";
                $levelTwo.css("left", leftPos);
            }
        }
    });

    $('#footer .emailAlreadyUsed .close').click(function () {
        $('body').removeClass('alreadySignedUp');
    })


    $('.linkCol h4, .socialCol h4').click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass("open");
    })

    var cookiepopup = $("#cookiepopup");

    if (cookiepopup.length > 0) {
        $("#cookieclose").click(function (e) {
            e.preventDefault();

            var cookiepopup = $("#cookiepopup");

            cookiepopup.fadeOut();

            createCookie('bcbg_cookie_policy', '1', 365);
        });

        $("a", cookiepopup).click(function (e) {
            var cookiepopup = $("#cookiepopup");

            cookiepopup.fadeOut();

            createCookie('bcbg_cookie_policy', '1', 365);
        });
    }

    $('body').on('keydown', 'textarea[data-character-limit]', function (e) {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length;

            if ((charsUsed >= charsLimit) && (controlKeys.indexOf(e.which.toString()) < 0)) {
                e.preventDefault();
            }
        });

        $('body').on('change keyup mouseup', 'textarea[data-character-limit]', function () {
            var text = $.trim($(this).val()),
                charsLimit = $(this).data('character-limit'),
                charsUsed = text.length,
                charsRemain = charsLimit - charsUsed;

            if (charsRemain < 0) {
                $(this).val(text.slice(0, charsRemain));
                charsRemain = 0;
            }

            $(this).next('div.char-count').find('.char-remain-count').html(charsRemain);
        });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('body').removeClass("search-open");
            $('#sideRefinement').removeClass("open");
        }
    });


    // 'Back to Top' button
    $(window).scroll(function (e) {
        var a = $(window).scrollTop();
        a > 200 ? $("#backtotop").fadeIn() : $("#backtotop").fadeOut();

        var $body = $("body");
        var isHomePage = window.universal_variable.page.type;

        if ($(window).width() > 768) {
            var searchResultOptionsOffset = $("#search-result-options-marker").offset();

            if (isHomePage == "home") {
                var mainOffset = $(".home-a").offset();
            } else {
                var mainOffset = $("#main").offset();
            }

            var topBannerHeight = $(".header-banner").height();
            var headerHeight = $("#stickyheader").outerHeight();

            //sticky Header
            if ($body.hasClass("search-open")) {
                if ($body.hasClass("miniHeader")) {
                    if (a < 428) {
                        $(".header-banner").css("marginBottom", "0px");
                        $body.removeClass("miniHeader");
                        $(".pdp-main .product-col-2").removeClass("top40");
                        window.scrollTo(0, 0);
                    }
                } else {
                    if (a >= 428) {
                        $(".header-banner").css("marginBottom", "428px");
                        $body.addClass("miniHeader");
                        $("#stickyheader").hide().slideDown(300, 'linear');
                        $(".pdp-main .product-col-2").addClass("top40");
                    }
                }
            } else {
                if ($body.hasClass("miniHeader")) {
                    if (a < mainOffset.top) {
                        $(".header-banner").css("marginBottom", "0px");
                        $body.removeClass("miniHeader");
                        $(".pdp-main .product-col-2").removeClass("top40");
                    }
                } else {
                    if (a >= mainOffset.top) {
                        $(".header-banner").css("marginBottom", headerHeight + "px");
                        $body.addClass("miniHeader");
                        $("#stickyheader").hide().slideDown(300, 'linear');
                        $(".pdp-main .product-col-2").addClass("top40"); // product right column top:40px when miniHeader active to show product name
                    }
                }
            }


            //sticky filter
            if ($body.hasClass("searchOptionsfloating")) {
                if (a < mainOffset.top) {
                    $body.removeClass("searchOptionsfloating");
                }
            } else {
                if (a >= mainOffset.top) {
                    $body.addClass("searchOptionsfloating");
                }
            }
        }

        if (screen.width < 768) {
            if ($(this).scrollTop() > 1) {
                $('.mini-cart').addClass("active");
                $('#wrapper').addClass("sticky-active");
            } else {
                $('.mini-cart').removeClass("active");
                $('#wrapper').removeClass("sticky-active");
            }
        }
    });

    $("#backtotop").on("click", function () {
        $("html,body").animate(
            { scrollTop: 0 },
            "slow"
        );


    });

    $(document).on('click', ".quickViewOpenWithColor", function (e) {
        e.preventDefault();
        var pid = $(this).attr('product');
        var productHref = $(this).attr('href');

        ajax.getJson({
            url: util.appendParamsToUrl(Urls.getAvailability, {
                pid: pid,
                Quantity: 1
            }),
            hideParseAlert: true,
            callback: function (data) {
                if (data != null && data.levels.NOT_AVAILABLE == 0) {
                    quickview.show({
                        callback: function () {
                            window.setTimeout(function () {
                                MagicZoom.start();
                            }, 1000);

                            $(".mobile-prev-image-nav").click(function (event) {
                                event.stopImmediatePropagation();
                                MagicZoom.prev('zoom');
                            });

                            $(".mobile-next-image-nav").click(function (event) {
                                event.stopImmediatePropagation();
                                MagicZoom.next('zoom');
                            });

                        },
                        url: productHref,
                        source: 'quickview'
                    });
                }
                else {
                    alert("Item not available.");
                }
            }
        });
    });


    $(document).on('click', ".quickViewOpen", function (e) {
        e.preventDefault();

        var pid = $(this).attr('product');
        var url = util.appendParamsToUrl(Urls.getProductUrl, { pid: pid });

        ajax.getJson({
            url: util.appendParamsToUrl(Urls.getAvailability, {
                pid: pid,
                Quantity: 1
            }),
            hideParseAlert: true,
            callback: function (data) {
                if (data != null && data.levels.NOT_AVAILABLE == 0) {
                    quickview.show({
                        callback: function () {
                            window.setTimeout(function () {
                                MagicZoom.start();
                            }, 1000);
                        },
                        url: url,
                        source: 'quickview'
                    });
                }
                else {
                    alert("Item not available.");
                }
            }
        });
    });

	/**
	 * initialize search suggestions, pending the value of the site preference(enhancedSearchSuggestions)
	 * this will either init the legacy(false) or the beta versions(true) of the the search suggest feature.
	 * */
    var $searchContainer = $('.header-search');
    if (SitePreferences.LISTING_SEARCHSUGGEST_LEGACY) {
        searchsuggest.init($searchContainer, Resources.SIMPLE_SEARCH);
    } else {
        searchsuggestbeta.init($searchContainer, Resources.SIMPLE_SEARCH);
    }

    // print handler
    $('.print-page').on('click', function () {
        window.print();
        return false;
    });

    // add show/hide navigation elements
    $('.secondary-navigation .toggle').click(function () {
        $(this).toggleClass('expanded').next('ul').toggle();
    });

    // add generic toggle functionality
    $('.toggle').next('.pt_product-search-result #secondary .toggle-content').show();
    $('.pt_product-search-result #secondary .toggle').click(function () {
        $(this).toggleClass('expanded').next('.toggle-content').slideToggle('slow');
    });

    //This is for guest services toggle content
    $(function () {
        $('.guest_services_content .togglebox').hide();
        $('.guest_services_content  h2').click(function () {
            $('.guest_services_content  h2 > img').attr('src', 'http://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif');
            $(this).children('img').toggleClass('arrow_image below_arrow');
            $(this).next('div.togglebox').slideToggle('slow');
            $(this).parent().siblings().children().next().slideUp();

            return false;
        });
    });

    // subscribe email box
    var $subscribeEmail = $('.subscribe-email');
    if ($subscribeEmail.length > 0) {
        $subscribeEmail.focus(function () {
            var val = $(this.val());
            if (val.length > 0 && val !== Resources.SUBSCRIBE_EMAIL_DEFAULT) {
                return; // do not animate when contains non-default value
            }

            $(this).animate({ color: '#999999' }, 500, 'linear', function () {
                $(this).val('').css('color', '#333333');
            });
        }).blur(function () {
            var val = $.trim($(this.val()));
            if (val.length > 0) {
                return; // do not animate when contains value
            }
            $(this).val(Resources.SUBSCRIBE_EMAIL_DEFAULT)
                .css('color', '#999999')
                .animate({ color: '#333333' }, 500, 'linear');
        });
    }

    $('.privacy-policy').on('click', function (e) {
        e.preventDefault();
        dialog.open({
            url: $(e.target).attr('href'),
            options: {
                height: 600
            }
        });
    });

    // main menu toggle
    $('.menu-toggle, .close-nav').on('click', function () {
        $('#wrapper').toggleClass('menu-active');
        //$('#stickyheader').toggleClass('menu-active');
        if ($('#wrapper > .mobileOverlay').length == 0) {
            $('#wrapper').prepend("<div class='mobileOverlay'></div>");
        }
    });
    $('.menu-category li .menu-item-toggle').on('tap click', function (e) {
        //console.log("clicked");
        e.preventDefault();
        var $parentLi = $(e.target).closest('li');
        $parentLi.siblings('li').removeClass('active').find('.menu-item-toggle').removeClass('active');
        $parentLi.toggleClass('active');
        $(e.target).toggleClass('active');
    });
    $('.user-account').on('click', function (e) {
        e.preventDefault();
        $(this).parent('.user-info').toggleClass('active');
    });

    $('body').on('click', 'li.mobile-nav', function () {
        $('body').toggleClass('nav-open');
    });

    $('body').on('click', '.mobile-nav-top .close', function () {
        $('body').removeClass('nav-open');
    });

    $('body').on('click', '.search', function () {
        $('body').toggleClass("search-open");
        $('.header-search').toggleClass('search-visible');
        if ($('body').hasClass("search-open")) {
            $(".header-search #q").focus();
        } 
    });

    $('body').on('click', '.header-search .close', function () {
        $('body').toggleClass("search-open");
        $('.header-search').toggleClass('search-visible');
        $(".header-search #q").val('');
    });

	$(document).on('click', '.afterpay-link', function (e) {
		e.preventDefault();
		dialog.open({
            url: $(e.target).attr('href'),
            options: {
                dialogClass: 'afterpay-dialog'
            }
		});
    });

    $('.mobile-nav-bottom .language-dropdown').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('active');
    });

    $('.menu-category li a').on('click', function (e) {
        if (screen.width < 768) {
            e.preventDefault();
            var isThisActive = $(this).parent().hasClass('active');
            $('.menu-category li').removeClass('active');
            var $parentLi = $(e.target).closest('li');
            if (isThisActive == false) {
                $parentLi.toggleClass('active');
            }
            if ($parentLi.has('ul').length > 0 && $(this).parent().parent().parent().hasClass('level-2') == false) {
                return;
            } else {
                window.location = $(this).attr('href');
            }
        }
    });

    $(document).on('click', '.add-wishlist', function (e) {
        var utag_data = $(this).data("utag");
        utag_data['event_type'] = 'wishlist_add';
        utag.link(utag_data);
    });

    $('footer .social a').on('click', function () {
        utag_data['event_type'] = 'social_share';
        utag.link(utag_data);
    });

    $(document).on('submit', '#send-to-friend-form', function () {
        if ($(this).valid()) {
            utag_data['event_type'] = 'show_your_friend';
            utag.link(utag_data);
        }
        return true;
    });

    $(window).load(function () {
        if ($('#utag_recently_registered').val()) {
            var utag_data = { "customer_id": $('#utag_customer_id').val(), "customer_email": $('#utag_customer_email').val(), "event_type": "user_register" };
            utag.link(utag_data);
        }
        if ($('#utag_recently_login').val()) {
            var utag_data = { "customer_id": $('#utag_customer_id').val(), "customer_email": $('#utag_customer_email').val(), "event_type": "user_login" };
            utag.link(utag_data);
        }
    });

    $(".header-banner .modal").click(function (e) {
        e.preventDefault();
        var id = $(this).attr("href");

        $(id).dialog({
            autoOpen: false,
            modal: true,
            width: "auto",
            create: function (event, ui) {
                $(this).css("maxWidth", "860px");
            }
        });
        $(id).dialog('open');
    });

    //*************************Product dot nav page scroll*************************//
    $(document).ready(function () {
        if ($(".par").length) {
            $(function () {
                $.scrollIt({ topOffset: $('.par').first().offset().top * -1 });
            });
        }
    });

    if (screen.width >= 768) {
        $('.sticky').Stickyfill();
    }
}


/**
 * @private
 * @function
 * @description Adds class ('js') to html for css targeting and loads js specific styles.
 */
function initializeDom() {
    // add class to html for css targeting
    $('html').addClass('js');
    if (SitePreferences.LISTING_INFINITE_SCROLL) {
        $('html').addClass('infinite-scroll');
    }
    // load js specific styles
    util.limitCharacters();

}

var pages = {
    account: require('./pages/account'),
    cart: require('./pages/cart'),
    checkout: require('./pages/checkout'),
    compare: require('./pages/compare'),
    product: require('./pages/product'),
    registry: require('./pages/registry'),
    search: require('./pages/search'),
    storefront: require('./pages/storefront'),
    wishlist: require('./pages/wishlist'),
    storelocator: require('./pages/storelocator'),
    orderconfirmation: require('./pages/orderconfirmation'),
    onepage: require('./pages/onepage')
};

var app = {
    init: function () {
        if (document.cookie.length === 0) {
            $('<div/>').addClass('browser-compatibility-alert').append($('<p/>').addClass('browser-error').html(Resources.COOKIES_DISABLED)).appendTo('#browser-check');
        }
        initializeDom();
        initializeEvents();

        // init specific global components
        countries.init();
        tooltip.init();
        minicart.init();
        validator.init();
        rating.init();
        searchplaceholder.init();
        multicurrency.init();
        international.init();
        responsys.init();

        // execute page specific initializations
        $.extend(page, window.pageContext);

        var ns = page.ns;
        if (ns && pages[ns] && pages[ns].init) {
            pages[ns].init();
        }
    }
};

// general extension functions
(function () {
    String.format = function () {
        var s = arguments[0];
        var i, len = arguments.length - 1;
        for (i = 0; i < len; i++) {
            var reg = new RegExp('\\{' + i + '\\}', 'gm');
            s = s.replace(reg, arguments[i + 1]);
        }
        return s;
    };
})();

// initialize app
$(document).ready(function () {
    app.init();
});

// for mobile footer accordion menu	ECOM-586 	
$('.product-accordion').accordion({
    header: "pre",
    collapsible: true,
    autoHeight: false,
    heightStyle: "content",
    animate: 200
});

// ECOM-586
$(".wrap_topfooter").clone().appendTo(".footer-copy-mobile");
$(".footer-logos").clone().appendTo(".footer-copy-mobile");


//*******Mobile Refinement *******//
//Filter accordion
$(function () {
    $('.refinement-accordion').show().accordion({
        header: "pre",
        collapsible: true,
        active: false,
        autoHeight: false,
        navigation: true,
        heightStyle: "content"
    });
});

$("#color-select").chosen({
    width: "100%",
    disable_search: true,
    inherit_select_classes: true,
    placeholder_text_multiple: "Color",
    'autoheight': false,
    'clearStyle': true
});

$("#size-select").chosen({
    width: "100%",
    disable_search: true,
    inherit_select_classes: true,
    placeholder_text_multiple: "Size",
    'autoheight': false,
    'clearStyle': true
});

//***** mobile response for Next/Previous Button DEV-560 *****//
if ($("#product-nav-container").length > 0) {
    $(window).on("load resize", function (e) {
        if (window.innerWidth < 768) {
            $(".breadcrumb").before($("#product-nav-container"));
        } else {
            $(".stickypdp").before($("#product-nav-container"));
        }
    });
}
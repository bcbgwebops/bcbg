'use strict';
exports.init = function () {
	$('#vertical-carousel')
		.jcarousel({
			vertical: true
		})
		.jcarouselAutoscroll({
			interval: 5000
		});
	$('#vertical-carousel .jcarousel-prev')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '-=1'
		});

	$('#vertical-carousel .jcarousel-next')
		.on('jcarouselcontrol:active', function () {
			$(this).removeClass('inactive');
		})
		.on('jcarouselcontrol:inactive', function () {
			$(this).addClass('inactive');
		})
		.jcarouselControl({
			target: '+=1'
		});
};

'use strict';
var dialog = require('../dialog'),
    ajax = require('../ajax'),
    util = require('../util');

var geocoder;
var map;
var latlngbounds;
var mapResized = false;
var marker = [];

exports.init = function () {
    $(".back-storelocator").click(function(e) {
        var storeResults = $("#storeresults");
        var storeforms = $("#storeforms");
        var storeData = $("#storeresults tbody");

        storeforms.show();
        storeResults.hide();
        $("tr.storeresult",storeData).remove();
    });

    ajax.load({
        url: "/on/demandware.store/Sites-BCBG-Site/default/Stores-countryJSON",
        callback: function (data) {
            $("#dwfrm_storelocator_country").children(":gt(0)").remove();
            $("#dwfrm_storelocator_country").append(data);
        }
    });

    ajax.load({
        url: "/on/demandware.store/Sites-BCBG-Site/default/Stores-stateJSON",
        callback: function (data) {
            $("#dwfrm_storelocator_address_states_stateUSCA").children(":gt(0)").remove();
            $("#dwfrm_storelocator_address_states_stateUSCA").append(data);
            $("#dwfrm_storelocator_address_states_stateUSCA").children().each(function(i){
                if ($(this).text() == "" || $(this).text() == "x") {
                    $(this).remove();
                }
            });
        }
    });
    
    $(function() {
    	if (navigator && navigator.geolocation) {
	        var fallback = setTimeout(function() { fail('10 seconds expired'); }, 10000);
	        navigator.geolocation.getCurrentPosition(
	            function (pos) {
	                clearTimeout(fallback);

	                var point = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
	                new google.maps.Geocoder().geocode({'latLng': point}, function (res, status) {
	                    if(status == google.maps.GeocoderStatus.OK && typeof res[0] !== 'undefined') {
	                        var address = res[4].formatted_address;
	                        var getState = address.split(",");
	                        var state = getState[1];
	                        
	                        var zipStr = res[0].formatted_address.match(/,\s\w{2}\s(\d{5})/);
	                        var zipStr = zipStr[1];
	                        
	                        if (state) {
	                        
						        var loading = $(this).siblings(".loader-indicator");
						
						        var stateProvStr = state;
						        var caArray = ["AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"];
						
						        if (stateProvStr.length > 0) {
						            var stateProvinceURL = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";
						
						            var url = util.appendParamToURL(stateProvinceURL, 'brand', storebrand);
						
						            if (caArray.indexOf(stateProvStr) > -1) {
						                url = util.appendParamToURL(url, 'country', "CANADA");
						            } else {
						                url = util.appendParamToURL(url, 'country', "USA");
						            }
						
						            url = util.appendParamToURL(url, 'state', stateProvStr);
						            url = util.appendParamToURL(url, 'type', 'state');
						
						            loading.show();
						            ajax.load({
						                url: url,
						                callback: function (data) {
						                	storeDataGeolocation(pos.coords.latitude, pos.coords.longitude, data);
						                    loading.hide();
						                    document.body.scrollTop = document.documentElement.scrollTop = 0;
						                    showStoresGeoLocation(pos.coords.latitude, pos.coords.longitude, data);
						                    google.maps.event.addDomListener(window, 'load', showStoresGeoLocation);
						                }
						            });
						            
						        }
						        
	                  		} else fail('Failed to parse');
	                        
	                    } else {
	                        fail('Failed to reverse');
	                    }
	                });
	            }, function(err) {
	                fail(err.message);
	            }
	        );
	    }

	    function fail(err) {
	        $(".storemaps").html('Error ' + err);
	    }
	});
    
    

    $("#intSubmit").on('click', function(e) {
        e.preventDefault();

        var loading = $(this).siblings(".loader-indicator");

        var country = $("#dwfrm_storelocator_country").val();

        if (country.length > 0) {
            var countryURL = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";

            var url = util.appendParamToURL(countryURL, 'brand', storebrand);
            url = util.appendParamToURL(url, 'country', country);
            url = util.appendParamToURL(url, 'type', 'international');

            loading.show();
            ajax.load({
                url: url,
                callback: function (data) {
                    storeData(data);
                    loading.hide();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                    google.maps.event.addDomListener(window, 'load', showStoresGoogleMap('country', country, data));
                }
            });
        }
    });

    $("#usCaStateSubmit").click(function(e) {
        e.preventDefault();

        var loading = $(this).siblings(".loader-indicator");

        var stateProvStr = $("#dwfrm_storelocator_address_states_stateUSCA").val();
        var caArray = ["AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"];

        if (stateProvStr.length > 0) {
            var stateProvinceURL = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";

            var url = util.appendParamToURL(stateProvinceURL, 'brand', storebrand);

            if (caArray.indexOf(stateProvStr) > -1) {
                url = util.appendParamToURL(url, 'country', "CANADA");
            } else {
                url = util.appendParamToURL(url, 'country', "USA");
            }

            url = util.appendParamToURL(url, 'state', stateProvStr);
            url = util.appendParamToURL(url, 'type', 'state');

            loading.show();
            ajax.load({
                url: url,
                callback: function (data) {
                    storeData(data);
                    loading.hide();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                    google.maps.event.addDomListener(window, 'load', showStoresGoogleMap('state', stateProvStr, data));
                }
            });
        }
    });

    $("#usCaZipSubmit").click(function(e) {
        e.preventDefault();

        var loading = $(this).siblings(".loader-indicator");

        var radiusStr = $("#dwfrm_storelocator_maxdistance").val();
        var zipStr = $("#dwfrm_storelocator_postalCode").val();

        if (zipStr.length == 6) {
            var part1 = zipStr.substr(0, 3);
            var part2 = zipStr.substr(3, 3);
            zipStr = part1 + " " + part2;
        }

        if (radiusStr.length > 0 && zipStr.length > 0) {
            var zipUrl = "/on/demandware.store/Sites-BCBG-Site/default/Stores-storeJSON";

            var url = util.appendParamToURL(zipUrl, 'brand', storebrand);

            var reg = new RegExp('^[0-9]+$');

            if (reg.test(zipStr)) {
                url = util.appendParamToURL(url, 'country', "USA");
            } else {
                url = util.appendParamToURL(url, 'country', "CANADA");
            }

            url = util.appendParamToURL(url, 'radius', radiusStr);
            url = util.appendParamToURL(url, 'zip', zipStr);
            url = util.appendParamToURL(url, 'type', 'zip');

            loading.show();
            ajax.load({
                url: url,
                callback: function (data) {
                    storeData(data);
                    loading.hide();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                    showStoresGoogleMap('zip', zipStr, data);
                    google.maps.event.addDomListener(window, 'load', showStoresGoogleMap);
                }
            });
        }
    });


    function storeData(data) {
        var obj = jQuery.parseJSON(data);

        var storeT = $("#storetemplate");
        var storeResults = $("#storeresults");
        var storeData = $("#storeresults tbody");
        var storeforms = $("#storeforms");

        var foundData = false;

        $.each(obj, function(key,value) {
             var storeTR = storeT.clone();
             storeTR.attr("id","");

             foundData = true;

             var storename = value.storename;
             if (value.storename == "null" || value.storename == null) {
                 storename = "";
             }

             var storephone = value.phone;
             if (value.phone == "null" || value.phone == null) {
                 storephone = "";
             } else {
            	if (typeof window.orientation !== 'undefined') {
            		storephone = "<a href='tel:" + storephone + "'>" + storephone + "</a>";
            	} else {
            		storephone = storephone;
            	}
             }
             

             //storetype boutique, partner, outlet
             var storetype = value.storetype;
             if (value.storetype == "null" || value.storetype == "" || value.storetype == null) {
            	 storetype = "";
             } else {
            	 storetype = storetype;
             }
             
             var storeaddress = value.address;
             if (value.address == "null" || value.address == null) {
                 storeaddress = "";
             }
             var storecity = value.city;
             if (value.city == "null" || value.city == null) {
                 storecity = "";
             }
             var storestate = value.state;
             if (value.state == "null" || value.state == null) {
                 storestate = "";
             }
             var storezip = value.zip;
             if (value.zip == "null" || value.zip == null) {
                 storezip = "";
             }
             var storecountry = value.country;
             if (value.country == "null" || value.country == null) {
                 storecountry = "";
             }

             var qAttr = "";

             if (storeaddress != "") qAttr += storeaddress + ", ";
             if (storecity != "") qAttr += storecity + ", ";
             if (storezip != "") qAttr += storezip + ", ";
             if (storestate != "") qAttr += storestate + ", ";
             if (storecountry != "") qAttr += storecountry;

             //store-map
             var storelink = "http://maps.google.com/maps?hl=en&f=q&q=" + encodeURI(qAttr);
             var storemap = "<a class='google-map' href='"+ storelink +"' target='_blank'>Map</a>";

             if ((storecity != "" && storecity != "null") && (storestate != "" && storestate != "null")) {
                 storecity = storecity + ", ";
             }

             $(".storename", storeTR).html(storename);
             $(".storephone", storeTR).html(storephone);
             
             $(".storeaddress", storeTR).html(storeaddress);
             $(".storecity", storeTR).html(storecity);
             $(".storestate", storeTR).html(storestate);
             $(".storezip", storeTR).html(storezip);
             $(".storecountry", storeTR).html(storecountry);

             $(".store-map", storeTR).html(storemap);

             storeTR.attr("style","").addClass("storeresult");
             storeData.append(storeTR);
        });

        $(".store-locator-no-results").hide();

        if (foundData) {
            storeResults.show();
            storeforms.hide();
        } else {
            $(".store-locator-no-results").show();
        }
    }
    
    function storeDataGeolocation(lat, long, data) {
        var obj = jQuery.parseJSON(data);

        var storeT = $("#storetemplate");
        var storeResults = $("#storeresults");
        var storeData = $("#storeresults tbody");
        var storeforms = $("#storeforms");

        var foundData = false;
        var point = new google.maps.LatLng(lat, long);

        $.each(obj, function(key,value) {
        	
        	var latlng = new google.maps.LatLng(value.lat, value.long);
        	var dist = google.maps.geometry.spherical.computeDistanceBetween(point, latlng);
        	
        	if (dist < 24000) {	// meter
	             var storeTR = storeT.clone();
	             storeTR.attr("id","");
	
	             foundData = true;
	
	             var storename = value.storename;
	             if (value.storename == "null" || value.storename == null) {
	                 storename = "";
	             }
	
	             var storephone = value.phone;
	             if (value.phone == "null" || value.phone == null) {
	                 storephone = "";
	             } else {
	            	 if (typeof window.orientation !== 'undefined') {
	             		storephone = "<a href='tel:" + storephone + "'>" + storephone + "</a>";
	             	} else {
	             		storephone = storephone;
	             	}
	             }
	
	             //storetype boutique, partner, outlet
	             var storetype = value.storetype;
	             if (value.storetype == "null" || value.storetype == "" || value.storetype == null) {
	            	 storetype = "";
	             } else {
	            	 storetype = storetype;
	             }
	             
	             var storeaddress = value.address;
	             if (value.address == "null" || value.address == null) {
	                 storeaddress = "";
	             }
	             var storecity = value.city;
	             if (value.city == "null" || value.city == null) {
	                 storecity = "";
	             }
	             var storestate = value.state;
	             if (value.state == "null" || value.state == null) {
	                 storestate = "";
	             }
	             var storezip = value.zip;
	             if (value.zip == "null" || value.zip == null) {
	                 storezip = "";
	             }
	             var storecountry = value.country;
	             if (value.country == "null" || value.country == null) {
	                 storecountry = "";
	             }
	
	             var qAttr = "";
	
	             if (storeaddress != "") qAttr += storeaddress + ", ";
	             if (storecity != "") qAttr += storecity + ", ";
	             if (storezip != "") qAttr += storezip + ", ";
	             if (storestate != "") qAttr += storestate + ", ";
	             if (storecountry != "") qAttr += storecountry;
	
	             //store-map
	             var storelink = "http://maps.google.com/maps?hl=en&f=q&q=" + encodeURI(qAttr);
	             var storemap = "<a class='google-map' href='"+ storelink +"' target='_blank'>Map</a>";
	
	             if ((storecity != "" && storecity != "null") && (storestate != "" && storestate != "null")) {
	                 storecity = storecity + ", ";
	             }
	
	             $(".storename", storeTR).html(storename);
	             $(".storephone", storeTR).html(storephone);
	             
	             $(".storeaddress", storeTR).html(storeaddress);
	             $(".storecity", storeTR).html(storecity);
	             $(".storestate", storeTR).html(storestate);
	             $(".storezip", storeTR).html(storezip);
	             $(".storecountry", storeTR).html(storecountry);
	
	             $(".store-map", storeTR).html(storemap);
	
	             storeTR.attr("style","").addClass("storeresult");
	             storeData.append(storeTR);
        	}
        });

        $(".store-locator-no-results").hide();

        if (foundData) {
            storeResults.show();
            storeforms.hide();
        } else {
            $(".store-locator-no-results").show();
        }
    }
    
    function showStoresGoogleMap(type, address, data) {
    	
    	var infoWindows = [];
    	var contentString = "";
    	var obj = jQuery.parseJSON(data);
    	latlngbounds = new google.maps.LatLngBounds();
    	geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                var centerlatlng = new google.maps.LatLng(latitude, longitude);

                var mapOptions = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                
                var count = 0;
                map = new google.maps.Map(document.getElementById('GoogleMap'), mapOptions);
                $.each(obj, function(key,store) {
                	var latlng = new google.maps.LatLng(store.lat, store.long); 
                	latlngbounds.extend(latlng);
              	  
                marker[count] = new google.maps.Marker({
              	    position: latlng,
              	    map: map,
    	      	    optimized: false,
		      	    clickable: true
              	  });
                
                
                contentString = storeContent(store);
    	      	  
    	      	infoWindows[count] = new google.maps.InfoWindow({
    	            content: contentString
    	        });
    	      	
    	      	google.maps.event.addListener(marker[count], 'click', function(innerKey) {
    	      		return function() {
    	      			infoWindows[innerKey].open(map, marker[innerKey]);
    	      			map.setZoom(14);
    	      			map.setCenter(marker[innerKey].getPosition());
    	      	    }
    	  	    }(count));	

    	      	 count++;

           	 	});
               
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);
                
                
            }
        });
    }
    
    function showStoresGeoLocation(lat, long, data) {
    	
    	var infoWindows = [];
    	var contentString = "";
    	var obj = jQuery.parseJSON(data);
    	var point = new google.maps.LatLng(lat, long);
    	latlngbounds = new google.maps.LatLngBounds();
    	
    	var mapOptions = {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

    	map = new google.maps.Map(document.getElementById('GoogleMap'), mapOptions);
        var count = 0;
    	
        $.each(obj, function(key,store) {
        	var storeLatlng = new google.maps.LatLng(store.lat, store.long);
        	var dist = google.maps.geometry.spherical.computeDistanceBetween(point, storeLatlng);
        	
        	if (dist < 24000) {	// meter
        	
	        	var latLng = new google.maps.LatLng(store.lat, store.long); 
	        	
	        	marker[count] = new google.maps.Marker({
		      	    position: latLng,
		      	    map: map,
		      	    optimized: false,
		      	    clickable: true
		      	});
		      	  latlngbounds.extend(latLng);
		      	  contentString = storeContent(store);
	      	  
		      	infoWindows[count] = new google.maps.InfoWindow({
		            content: contentString
		        });
		      	
		      	google.maps.event.addListener(marker[count], 'click', function(innerKey) {
		      		return function() {
		      			infoWindows[innerKey].open(map, marker[innerKey]);
		      			map.setZoom(14);
		      			map.setCenter(marker[innerKey].getPosition());
		      	    }
		  	    }(count));	

	      	 count++;
        	}
	    });	
        
        if (count > 0) {
        	map.setCenter(latlngbounds.getCenter());
        	map.fitBounds(latlngbounds);
        } else {
        	map.setCenter(point);
        }
   
    }
    
    function storeContent(store){
    	
    	var storeInfo = "";
    	
		var storeaddress = store.address;
		if (store.address == "null" || store.address == null) {
		storeaddress = "";
		}
		var storecity = store.city;
		if (store.city == "null" || store.city == null) {
		storecity = "";
		}
		var storestate = store.state;
		if (store.state == "null" || store.state == null) {
		storestate = "";
		}
		var storezip = store.zip;
		if (store.zip == "null" || store.zip == null) {
		storezip = "";
		}
		var storecountry = store.country;
		if (store.country == "null" || store.country == null) {
		storecountry = "";
		}
	
    	var qAttr = "";
        if (storeaddress != "") qAttr += storeaddress + ", ";
        if (storecity != "") qAttr += storecity + ", ";
        if (storezip != "") qAttr += storezip + ", ";
        if (storestate != "") qAttr += storestate + ", ";
        if (storecountry != "") qAttr += storecountry;

        var storeDirectionlink = "https://www.google.com/maps/dir//" + encodeURI(qAttr); 
        
    	storeInfo = '<div><b>'+ store.storename + '</b></div>';
    	if(store.monday != null) storeInfo = storeInfo + '<div>Monday:' + store.monday + '</div>';
    	if(store.tuesday != null) storeInfo = storeInfo + '<div>Tuesday' + store.tuesday + '</div>';
    	if(store.wednesday != null) storeInfo = storeInfo + '<div>Wednesday' + store.wednesday + '</div>';
    	if(store.thursday != null) storeInfo = storeInfo + '<div>Thursday' + store.thursday + '</div>';
    	if(store.friday != null) storeInfo = storeInfo + '<div>Friday' + store.friday + '</div>';
    	if(store.saturday != null) storeInfo = storeInfo + '<div>Saturday' + store.saturday + '</div>';
    	if(store.sunday != null) storeInfo = storeInfo + '<div>Sunday' + store.sunday + '</div>';
    	storeInfo = storeInfo + '<div><a href="' + storeDirectionlink + '" target="_blank"><b>Get directions</b></a></div>';
   
    	return storeInfo;
    }
  
    
};

'use strict';

var progress = require('../../progress'),
	util = require('../../util'),
	dialog = require('../../dialog'),
	account = require('./account'),
	shipping = require('./shipping'),
	billing = require('./billing'),
	summary = require('./summary'),
	international = require('../../international');

/**
 * @function
 * @description Update hash in URL
 */
var updateUrl = function (stepVal, fromVal) {
	location.hash = '#' + stepVal;

	if (utag_data) {
		var pageName = "";

		switch(stepVal) {
			case "account":
				pageName = "Checkout: Account";
				break;
			case "shipping":
				pageName = "Checkout: Shipping Address";
				break;
			case "billing":
				pageName = "Checkout: Billing Address";
				break;
			case "summary":
				pageName = "Checkout: Summary";
				break;
			default:
				pageName = "";
		}

		if (pageName != "") {
			utag.view({
				"page_type": stepVal,
				"page_name": pageName
			});
		}
	}

	util.createCookie('checkoutStep',stepVal,0);
	util.createCookie('checkoutStepFrom',fromVal,0);
	checkSummary(stepVal);
}


/**
 * @function
 * @description Scroll to ID
 */
var checkoutScroll = function (scrollId) {
	var scrollTop = $(scrollId).position().top;
	$("html,body").animate({
			scrollTop : scrollTop
		}, 400
	);
}

var removeActive = function(steps, checksteps) {
	for (var i in steps) {
		$("#onepage_" + steps[i]).removeClass("active");
		if(checksteps != null && (checksteps.indexOf(steps[i]) < 0)){
			$("#onepage_"+ steps[i] +"_content").html('');
		}
	}

	for (var i in checksteps) {
		var thisStep = $("#onepage_" + checksteps[i]);
		if ($(".onepage_mini_summary", thisStep).length <= 0) {
			$("#onepage_"+ checksteps[i] +"_content").html('');
		}
	}
}

/**
 * @function
 * @description Reload Loqate 
 */
var refreshLoqate = function () {
	if (typeof pca !== 'undefined') { 
		pca.on("options", function(type, key, options){
		    if(type == "capture+"){
		        options.suppressAutocomplete = false;
		    }
		});

		pca.on("load", function(type, id, control){
		    for (i=0; control.fields.length > i; i++){
		        document.getElementById(control.fields[i].element).autocomplete = "PCATEST";
		    };
		});
		
		pca.load();
	}	
};

/**
 * @function
 * @description Activate Account Panel
 */
var activateAccount = function () {
	removeActive(["shipping","billing","summary"]);
	$("#onepage_account").addClass("active");
	$("#onepage").removeClass().addClass("account");
	progress.show($("#onepage_account_content"));
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.account, "format", "ajax"),
		dataType: 'html',
		success: function(data) {
			if (data != null) {
				$("#onepage_account_content").html('');
				
				if (displayOutput(data, $("#onepage_account_content"), "There has been an error submitting your information. Please return to the cart and try again.")) {
					account.init();
				}
				
				refreshLoqate();

				checkoutScroll("#onepage_account");

				$("#edit_account").removeClass("visible");
				$("#onepage_breadcrumbs li").removeClass("active");
				$(".account_breadcrumb").addClass("active");

				progress.hide();
				refreshMiniSummary(false);
			} else {
				showGeneralError($("#onepage_account_content"),"There has been an error submitting your information. Please return to the cart and try again.", timestamp());
			}
		}
	});
};

/**
 * @function
 * @description Activate Shipping Panel
 */
var activateShipping = function () {
	removeActive(["account","summary","billing"],[]);
	$("#onepage_shipping").addClass("active");
	$("#onepage").removeClass().addClass("shipping");
	progress.show($("#onepage_shipping_content"));
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.shipping, "format", "ajax"),
		dataType: 'html',
		success: function(data) {
			if (data != null) {
				$("#onepage_shipping_content").html('');
				
				if (displayOutput(data, $("#onepage_shipping_content"), "There has been an error submitting your information. Please return to the cart and try again.")) {
					shipping.init();
				}
				
				refreshLoqate();

				checkoutScroll("#onepage_shipping");

				$("#edit_shipping").removeClass("visible");
				$("#onepage_breadcrumbs li").removeClass("active");
				$(".shipping_breadcrumb").addClass("active");

				var $shippingMethodList = $('#shipping-method-list');
				if ($shippingMethodList.find('.input-radio:checked').length === 0) {
					$shippingMethodList.find('.input-radio:first').prop('checked', 'checked').parents(".form-row").addClass("active");
				}
				else{
					$shippingMethodList.find('.input-radio:checked').parents(".form-row").addClass("active");
				}

				progress.hide();
				refreshMiniSummary(false);
			} else {
				showGeneralError($("#onepage_shipping_content"),"There has been an error submitting your information. Please return to the cart and try again.", timestamp());
			}
		}
	});
};

var refreshShipping = function () {
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.refreshshipping, "format", "ajax"),
		dataType: 'html'
	}).done(function(data){
		if (data != null) {
			$("#onepage_shipping_content").html('');
			$("#onepage_shipping_content").html(data);
			refreshLoqate();
		}
	}).fail(function(data){
	}).always(function(data){
		updateUrl('billing',"");
		$("#edit_shipping").addClass("visible");
	});
};

/**
 * @function
 * @description Activate Billing Panel
 */
var activateBilling = function () {
	removeActive(["account","summary","shipping"],["shipping"]);
	$("#onepage_billing").addClass("active");
	$("#onepage").removeClass().addClass("billing");
	progress.show($("#onepage_billing_content"));
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.billing, "format", "ajax"),
		dataType: 'html',
		success: function(data) {
			if (data != null) {
				$("#onepage_billing_content").html('');
				
				if (displayOutput(data, $("#onepage_billing_content"), "There has been an error submitting your information. Please return to the cart and try again.")) {
					billing.init();
				}
				
				refreshLoqate();
				
				checkoutScroll("#onepage_billing");

				$("#edit_billing").removeClass("visible");
				$("#onepage_breadcrumbs li").removeClass("active");
				$(".billing_breadcrumb").addClass("active");

				progress.hide();
				refreshMiniSummary(false);
			} else {
				showGeneralError($("#onepage_billing_content"),"There has been an error submitting your information. Please return to the cart and try again.", timestamp());
			}
		}
	});
};

var refreshBilling = function () {
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.refreshbilling, "format", "ajax"),
		dataType: 'html'
	}).done(function(data){
		if (data != null) {
			$("#onepage_billing_content").html('');
			$("#onepage_billing_content").html(data);
			refreshLoqate();
		}
	}).fail(function(data){
	}).always(function(data){
		updateUrl('summary',"");
		$("#edit_billing").addClass("visible");
	});
};


/**
 * @function
 * @description Activate Summary Panel
 */
var activateSummary = function () {
	removeActive(["account", "shipping", "billing"],["shipping","billing"]);
	$("#onepage_summary").addClass("active");
	$("#onepage").removeClass().addClass("summary");
	progress.show($("#onepage_summary_content"));
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.summary, "format", "ajax"),
		dataType: 'html',
		success: function(data) {
			if (data != null) {
				$("#onepage_summary_content").html('');
				
				if (displayOutput(data, $("#onepage_summary_content"), "There has been an error submitting your information. Please return to the cart and try again.")) {
					summary.init();
				}
				if(CbtManager != undefined) {
					$('#pbSessionID').val(CbtManager.getDeviceFingerprintId());
				}
				checkoutScroll("#onepage_summary");
				$("#onepage_breadcrumbs li").removeClass("active");
				$(".summary_breadcrumb").addClass("active");

				progress.hide();
				refreshMiniSummary(false);
			} else {
				showGeneralError($("#onepage_summary_content"),"There has been an error submitting your information. Please return to the cart and try again.", timestamp());
			}
		}
	});
};

var opcHandleEstimateResponse = function (payment_estimate, a) {
	var dollars = payment_estimate.payment_string;

	var logo = '<img src="data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzEuNjQgMTY2LjA0Ij48ZGVmcz48c3R5bGU+LmNscy0xe2ZpbGw6IzIzMWYyMDt9PC9zdHlsZT48L2RlZnM+PHRpdGxlPmxvZ290eXBlX2JsazwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMjk4LjM5LTEuOTJBMTcuNTcsMTcuNTcsMCwxLDAsMzE2LDE1LjY1LDE3LjU5LDE3LjU5LDAsMCwwLDI5OC4zOS0xLjkyWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxLjkyKSIvPjxyZWN0IGNsYXNzPSJjbHMtMSIgeD0iMjgzLjMiIHk9IjQ2LjY4IiB3aWR0aD0iMjkuOTkiIGhlaWdodD0iMTE5LjMxIi8+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNNDAzLjQ3LDQxLjY5Yy0xNSwwLTMyLjI1LDEwLjgtMzcuOTMsMjQuMzRWNDQuNzZIMzM3LjA5VjE2NC4wOGgzMFYxMDguNjdjMC0yMy40NSw5LTM2LjU0LDI4LjYxLTM2LjkxbDE2Ljc1LTI5LjM4QTY0LjE3LDY0LjE3LDAsMCwwLDQwMy40Nyw0MS42OVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMS45MikiLz48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik01MjQsNDEuNzdjLTEyLjg3LDAtMjQuNDEsNC44NC0zMi41LDEzLjYybC0wLjQyLjQ1LTAuNDEtLjQ1Yy04LTguNzgtMTkuNDgtMTMuNjItMzIuMzYtMTMuNjItMjcuNTgsMC00Ny42LDIwLjExLTQ3LjYsNDcuODF2NzQuNTNoMjkuNTJWODguOTRjMC0xMS42Myw3LjEtMTkuMTQsMTguMDgtMTkuMTRzMTguMDksNy41MSwxOC4wOSwxOS4xNHY3NS4xN0g1MDZWODguOTRjMC0xMS42Myw3LjEtMTkuMTQsMTguMDktMTkuMTRzMTguMDksNy41MSwxOC4wOSwxOS4xNHY3NS4xN2gyOS41MVY4OS41OEM1NzEuNjQsNjEuODgsNTUxLjYyLDQxLjc3LDUyNCw0MS43N1oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMS45MikiLz48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik0yNDcsMzMuNzdjMC0zLjk0LjUzLTguNjUsMy45LTExLjI0LDMuNjktMi44Nyw5LjA5LTIuMzQsMTMuNDYtMi4xMmw1LjgyLTIxLjcyLTMuMTEtLjE2Yy0xMi41Mi0uNjctMjUuNjktMS42LTM2LjY3LDUuNjYtOS4zMSw2LjE1LTEzLjQ1LDE2Ljc0LTEzLjQ1LDI3LjYxdjEzSDE4MS4xOHYtMTFjMC0zLjkyLjUyLTguNTgsMy44Mi0xMS4yLDMuNjktMi45Myw5LjE2LTIuMzgsMTMuNTMtMi4xNmw1LjgyLTIxLjcyLTMuMTEtLjE2Yy0xMi42Mi0uNjgtMjUuOTItMS42LTM2LjkxLDUuODYtOS4xMiw2LjE4LTEzLjE2LDE2LjY4LTEzLjE2LDI3LjQydjEzSDEzNy42NVY2Ni40NWgxMy41M3Y5Ny42M2gzMFY2Ni40NWgzNS43NnY5Ny42M2gzMFY2Ni40NWgyMC43N1Y0NC43NkgyNDd2LTExWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxLjkyKSIvPjxwYXRoIGNsYXNzPSJjbHMtMSIgZD0iTTEyMi40MywxNjQuMDlsMC0xMDUuMzZhMTcuMzIsMTcuMzIsMCwwLDAtMTUuMzItMTdjLTUuNzMtLjM3LTExLjgyLDEuNzMtMTUuNDEsNi4zOEwwLDE2NC4wOEgyMi42YzksMCwxNi4xOC00LjY5LDIxLjYxLTExLjdMOTUsODguMTV2NzUuOTRoMjcuNDVaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDEuOTIpIi8+PC9zdmc+" style="height:1em; margin:0 .3em .15em;vertical-align:bottom;">';

	// Define the text surrounding the monthly payment price
	var content = "or as low as $" + dollars + "/mo with" + logo + "<br /><span>Learn more</span>";

	a.innerHTML = content;
	a.onclick = payment_estimate.open_modal;
	a.style.visibility = "visible";
};

/**
 * @function
 * @description Refresh MiniSummary Panel
 */
var refreshMiniSummary = function (doLoad) {
	if (doLoad) {
		$("#onepage_review_initial_load").attr("id","onepage_review_loaded");
	}

	if ($("#onepage_review_initial_load").length <= 0) {
		progress.show($("#onepage_review_content"));

		$.ajax({
			type: 'GET',
			url: util.appendParamToURL(Urls.onepagesummary, "format", "ajax"),
			dataType: 'html',
			success: function(data) {
				if (data != null) {
					$("#onepage_review_content").html('');

					displayOutput(data, $("#onepage_review_content"), "There has been an error submitting your information. Please return to the cart and try again.");
				} else {
					showGeneralError($("#onepage_review_content"),"There has been an error submitting your information. Please return to the cart and try again.", timestamp());
				}

				progress.hide();

				if (typeof sr_updateMessages == 'function' && sr_updateMessages ) {
					sr_updateMessages();
				}

				var amtDiv = $(data).find("#affirm-price");

				if (typeof(affirm) !== 'undefined' && affirm) {
					var amount = amtDiv.attr("data-affirm-price");

					var a = document.getElementById('affirm-learn-more');

					// Only display as low as for items over $50 and less than $17500  
					if ( ( amount == null ) || ( amount > 1750000 ) || (amount < 50) ) { return; }

					// Define payment estimate options
					var options = {
						apr: "0.10", // percentage assumed APR for loan
						months: 12, // can be 3, 6, or 12
						amount: amount // USD cents
					}

					affirm.ui.payments.get_estimate(options, function(response){ opcHandleEstimateResponse(response, a); });
				}
			}
		});
	} else {
		$("#onepage_review_initial_load").attr("id","onepage_review_loaded");
	}
};

/**
 * @function
 * @description Refresh Header Customer Info
 */
var refreshHeaderCustomerInfo = function () {
	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.headercustomer),
		dataType: 'html',
		success: function(data) {
			if (data != null) {
				$("#onepage_header_info").html(data);
			}
			international.init();
		}
	});
};


/**
 * @function
 * @description Remove Coupon Code
 */
var removeCoupon = function (couponCode, parent) {
	var $error = $("#coupon_gift_codes", parent).find('.coupon-error'),
		$redemption = $("#coupon_gift_codes", parent).find('.redemption.coupon');

	$error.html('');
	$redemption.html('');

	progress.show($("#coupon_gift_codes", parent));

	$.ajax({
		type: 'GET',
		url: util.appendParamToURL(Urls.removecoupon, "couponCode", couponCode),
		dataType: 'json',
	}).done(function(data){
		progress.hide();

		var dataJson;
		try {
			if (typeof data === 'object') {
				dataJson = data;
			} else {
				dataJson = $.parseJSON(data.toString().trim());
			}
		} catch(err) {
			// Nothing
		}

		if (dataJson != null && !dataJson.success) {
			$error.html("There was an error removing this code, please try again or return to the cart.");
		} else if (dataJson != null && dataJson.success) {
			if (dataJson.success) {
				$redemption.html("<span class='success'>" + dataJson.message + "</span>");
				$("a[data-code='"+couponCode+"']", parent).remove();
				refreshMiniSummary(true);
			} else {
				$error.html(dataJson.message);
			}
		} else {
			$error.html("There was an error removing this code, please try again or return to the cart.");
		}
	}).fail(function(data){
		progress.hide();
		$error.html("There was an error removing this code, please try again or return to the cart.");
	});
};

/**
 * @function
 * @description Set Checkout Step
 * @param {String} Checkout step value
 */
var setStep = function(stepVal) {
	switch(stepVal) {
		case "account":
			if (window.accountLoaded != true) {
				activateAccount();
			} else {
				account.init();
			}

			window.accountLoaded = true;
			window.shippingLoaded = false;
			window.billingLoaded = false;
			window.summaryLoaded = false;

			break;
		case "shipping":
			if (window.shippingLoaded != true) {
				activateShipping();
			} else {
				shipping.init();
			}

			window.accountLoaded = false;
			window.shippingLoaded = true;
			window.billingLoaded = false;
			window.summaryLoaded = false;

			break;
		case "billing":
			if (window.billingLoaded != true) {
				activateBilling();
			} else {
				billing.init();
			}

			window.accountLoaded = false;
			window.shippingLoaded = false;
			window.billingLoaded = true;
			window.summaryLoaded = false;

			break;
		case "summary":
			if (window.summaryLoaded != true) {
				activateSummary();
			} else {
				summary.init();
			}

			window.accountLoaded = false;
			window.shippingLoaded = false;
			window.billingLoaded = false;
			window.summaryLoaded = true;

			break;
		default:
			account.init();
	}
}

var checkSummary = function(stepVal) {
	if (stepVal == "summary") {
		$("#onepage_checkout_review_wrapper").addClass("summary");
	} else {
		$("#onepage_checkout_review_wrapper").removeClass("summary");
	}
}

/**
 * @function
 * @description ajax request to load json with callbacks
 * @param {Object} options for the ajax request
 */
var loadJSON = function (options) {
	var formaction = util.appendParamToURL(options.thisForm.attr('action'), "format", "ajax");
	var formData = options.thisForm.serialize();
	
	formData += "&"+ options.formBtn +"=";

	$.ajax({
		type: 'POST',
		url: formaction,
		data: formData,
		dataType: 'json',
		success: options.successCallback,
		error: JSONError
	});
};

var JSONError = function(jqXHR,error, errorThrown) {
			console.log(jqXHR);
			console.log(error);
			console.log(errorThrown);
		};

/**
 * @function
 * @description ajax request to load html with callbacks
 * @param {Object} options for the ajax request
 */
var loadHTML = function (options) {
	var formaction = util.appendParamToURL(options.thisForm.attr('action'), "format", "ajax");
	var formData = options.thisForm.serialize();
	
	formData += "&"+ options.formBtn +"=";

	$.ajax({
		type: 'POST',
		url: formaction,
		data: formData,
		dataType: 'html',
		success: options.successCallback
	});
};

var timestamp = Date.now || function() {
  return +new Date;
};

/**
 * @function
 * @description Populate form with error at the top
 * @param {Object} Form to show the message on
 * @param {String} message to show
 */
var showFormError = function (thisForm, errormsg) {
	thisForm.prepend($("<div>").html("<p>"+ errormsg + "</p>").addClass("error errorMsg"));
};

/**
 * @function
 * @description Populate response with error message
 * @param {Object} Div to show the message on
 * @param {String} message to show
 */
var showGeneralError = function (div, errormsg, timestamp) {
	div.html($("<div>").html("<p>"+ errormsg + "</p>").addClass("error errorMsg").attr("id","error_" + timestamp));
	div.append($("<a/>").html("Return to Cart").attr("href","#").addClass("return-to-cart"));

	checkoutScroll("#error_" + timestamp);
};

/**
 * @function
 * @description Populate response with output or error message
 * @param {Object} Response Data
 * @param {Object} Div to show the message on
 * @param {String} message to show
 */
var displayOutput = function (data, div, errormsg) {
	var dataJson;
	try {
		dataJson = $.parseJSON(data.toString().trim());
	} catch(err) {
		// Nothing
	}

	if (dataJson != null && !dataJson.success) {
		div.html($("<div>").html("<p>"+ errormsg + "</p>").addClass("error errorMsg"));
		div.append($("<a/>").html("Return to Cart").attr("href","#").addClass("return-to-cart"));
		return false;
	} else if (dataJson != null && dataJson.success) {
		if (dataJson.step != null) {
			setStep(dataJson.step);
		}
		return false;
	} else {
		div.html(data);
		return true;
	}
};

/**
 * @function
 * @description Populate form with error at the top
 * @param {Object} Form to show the message on
 * @param {String} message to show
 */
var clearFormError = function (thisForm) {
	$("div.errorMsg", thisForm).remove()
};

exports.loadJSON = loadJSON;
exports.loadHTML = loadHTML;
exports.showFormError = showFormError;
exports.clearFormError = clearFormError;
exports.activateAccount = activateAccount;
exports.activateShipping = activateShipping;
exports.activateBilling = activateBilling;
exports.activateSummary = activateSummary;
exports.updateUrl = updateUrl;
exports.checkoutScroll = checkoutScroll;
exports.setStep = setStep;
exports.displayOutput = displayOutput;
exports.refreshMiniSummary = refreshMiniSummary;
exports.removeCoupon = removeCoupon;
exports.refreshHeaderCustomerInfo = refreshHeaderCustomerInfo;
exports.refreshShipping = refreshShipping;
exports.refreshBilling = refreshBilling;
exports.refreshLoqate = refreshLoqate;

window.updateUrl = updateUrl;
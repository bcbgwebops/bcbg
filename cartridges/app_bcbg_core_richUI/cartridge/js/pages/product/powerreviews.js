'use strict';

// var counter = 0;
module.exports = function () {
	var averageRating;
	var averageRatingNotRounded;
	var totalResults;
	var starPosition;
	// Setting page index on the API call to grab 5 at a time
	var pageIndex = 0;
	var pageSize = 5;
	function getReviews(pageStart, pageSize){ 
		// Get merchant ID, API key from ID attached to div on reviewsrender.isml
		var merchantIDString = $('#pr-data').data('mid'); 
		var apiKey = $('#pr-data').data('api');
        var product = $('#masterProductPid').val()
		var merchantID = parseInt(merchantIDString);
		var url = "https://readservices-b2c.powerreviews.com/m/"+merchantID+"/l/en_US/product/"+product+"/reviews?apikey="+apiKey+"&paging.from="+pageStart+"&paging.size="+pageSize
		// Create template for each author of a review
		var reviewAuthor = function (name, location, age, height, sizeFit, comments, recommend, headline) {		
			return `<div class="pr-review-author">
						<div class="pr-review-author-info-wrapper">
							<div class="name-location-container">
								<p class="pr-review-author-name">
									<span>${name}</span>
								</p>
								<p class="pr-review-author-name">
									<span>${location}</span>
								</p>
							</div>
							<div class="age-height-container">
								<p class="pr-review-author-name">
									<span>Age:<span class="review-props"> ${age || ""}</span></span>
								</p> 
								<p class="pr-review-author-name">
									<span>Height:<span class="review-props"> ${height || ""}</span></span>
								</p>
							</div>
						</div>
					</div>
					<div class="pr-review-main-wrapper">
						<div class="pr-review-text">
							<p class="pr-headline">${headline}</p>
							<p class="pr-comments">${comments}</p>
							<span class="sizefit-wrap size-and-fit">Size and Fit:<span class="review-props"> ${sizeFit || ""}</span></span><br>
							<span class="sizefit-wrap size-and-fit-mobile">Fits:<span class="review-props"> ${sizeFit || ""}</span></span><br>
							<span class="sizefit-wrap recommend"> Recommended.<span class="review-recommend"> ${recommend || ""}: </span></span>
						</div>
					</div>`
		};
		// Display the rating sprite position for lower reviews
		var ratingPosition = function (rating) {
			switch (rating) {
				case 1:
					return "background-position: 0px -35px;";
					break;
				case 2:
					return "background-position: 0px -71px;";
					break;
				case 3:
					return "background-position: 0px -107px;";
					break;
				case 4:
					return "background-position: 0px -143px;";
					break;
				case 5:
                    return "background-position: 0px -177px;";
                    break;
            default:
                return ""
        }
	};
	// Display stars in position depending on rating after we round the number
    var headerPosition = function (rating) {
        switch (rating) {
            case "0":
                return "background-position: 0px 0px;";
                break;
            case "0.5":
                return "background-position: 0px -23px;";
                break;
            case "1.0":
                return "background-position: 0px -46px;";
                break;
            case "1.5":
                return "background-position: 0px -69px;";
                break;
            case "2.0":
                return "background-position: 0px -92px;";
                break;
            case "2.5":
                return "background-position: 0px -115px;";
                break;
            case "3.0":
                return "background-position: 0px -138px;";
                break;
            case "3.5":
                return "background-position: 0px -161px;";
                break;
            case "4.0":
                return "background-position: 0px -184px;";
                break;
            case "4.5":
                return "background-position: 0px -208px;";
                break;
            case "5.0":
                return "background-position: 0px -230px;";
                break;
            default:
                return ""
            }
        };
		// Build out the template to display the ratings with position and rating
		var reviewRating = function (rating, formatDate) {
			var position = ratingPosition(rating);
			return `<div class="pr-review-rating-wrapper">
						<div class="pr-review-author-date pr-rounded">${formatDate}</div>
						<div class="pr-review-rating">
							<div class="pr-stars pr-stars-small pr-stars-${rating}-sm" style="${position}">&nbsp;</div>
								<span class="pr-rating pr-rounded"><meta content="${rating}">${rating}</span>
							<p class="pr-review-rating-headline></p>
						</div>
					</div>`
		}
		$.ajax({
			url: url,
			dataType: 'json',
			success: function (res) {
				// Setting API to data to be drilled into for more data
				var data = res.results;
				pageSize = res.paging.page_size;
				totalResults = res.paging.total_results;
				// Binding Quickview.js and Variant.js to be able to draw out the reviews on page refresh
				if (totalResults > 0) {
					$(window).bind('topContentReplaced', paintUpperReviewRatings);
					$(window).bind('quickViewRating', paintUpperReviewRatings);
				} else {

					$(window).bind('topContentReplaced', paintUpperNoReviews);
					$(window).bind('quickViewRating', paintUpperNoReviews);
				}
				// Set button and reviews
				var btn = $('#readmorebtn #read-more-reviews');
				var reviews = $('#reviewsContainer .pr-review-wrap');
				var btnArrow = $('#readmorebtn p');
				// Hide read more reviews button if total results is less than 5
				if(totalResults < 5){
					btn.addClass("hide");
					btnArrow.addClass("hide");
					reviews.removeClass("hide");
				}
				$.each(data, function (i) {
					//Only grab average rating on 1st page load, 
					//otherwise it comes up undefined due to avergerating not being on next page index
					var innerData = data[i].reviews;
					if ( pageIndex === 0 && totalResults > 0) {
						averageRating = data[i].rollup.average_rating;
						averageRating = (Math.round(averageRating * 2) / 2).toFixed(1);
						starPosition = headerPosition(averageRating);
						averageRatingNotRounded = data[i].rollup.average_rating;
						averageRatingNotRounded = Math.round(averageRatingNotRounded * 10) / 10;
					}
					if ( totalResults > 0) {
						paintUpperReviewRatings();
						paintLowerReviewRatings();
					} else {
						paintLowerNoReviews();
						paintUpperNoReviews();
					} 
					//Appending Write A Review button to the quickview snippet
					var quickviewLink = $('#quickview-scroll');
					$('#QuickViewDialog .pr-snapshot-rating').wrap(quickviewLink);
					$('#QuickViewDialog .pr-snapshot-body-wrapper').removeAttr('id');
					$.each(innerData, function (i) {
						//Looping through customer inputs/data
						var userInfo = innerData[i]; 
						var userRating = userInfo.metrics.rating;
						var date = userInfo.details.created_date;
						var formatDate = new Date(date).toLocaleDateString();
						var userHeight;
						var userAge;
						var sizeFit;
						var userProps = userInfo.details.properties;
						var recommend = userInfo.details.bottom_line;
						var headline = userInfo.details.headline;

						var singleReview = $('<div class="pr-review-wrap"></div>');
						singleReview.append(reviewRating(userRating, formatDate));				
						// Find specific questions on form
						$.each(userProps, function (i) {
							var userKeys = userProps[i].key
							if (userKeys == "howoldareyou" && userProps[i].value !== undefined) { 
								userAge = userProps[i].value;
							}
							if (userKeys == "whatisyourheight" && userProps[i].value !== undefined) {
								userHeight = userProps[i].value;
							}
							if (userKeys == "sizeandfit" && userProps[i].value !== undefined) {
								sizeFit = userProps[i].value;
							}
						})
						singleReview.append(reviewAuthor(userInfo.details.nickname, userInfo.details.location, userAge, userHeight, sizeFit, userInfo.details.comments, recommend, headline));
						$('.pr-review-engine #reviewsContainer').append(singleReview);
					});
					// Remove button if we have shown all reviews
					if($('#reviewsContainer .pr-review-wrap').length === totalResults) {
						btn.addClass("hide");
						btnArrow.addClass("hide");
					}
				});				
			}
		});	
	}
	// Wait for page to load before calling to API
	$(document).ready(function(){  
		setTimeout(function(){
			getReviews(pageIndex, pageSize);
			readMoreReviews();
		},2000)		
	}); 
	// If more than 5 reviews exist, add READ MORE REVIEWS button. If clicked, change page index and grab next 5 reviews from api
	function readMoreReviews(){
		var btn = $('<div id="read-more-reviews">READ MORE REVIEWS</div><p><i class="down"></i></p>');	
		if (btn) {
            $('#readmorebtn').append(btn);
			$('#reviewsAnswers-mobile #readmorebtn').append(btn)
			btn.click(function () {
                pageIndex += pageSize;
				getReviews(pageIndex, pageSize);
			})
		}	
    }
	// BCBG Site - Displays the reviews on mobile without cloning them
	var reviewAnswersAdd = $('.bcbg-power-reviews');
    $(window).resize(function() {
		var windowWidth = $(window).width();
		if (windowWidth < 767) {
			reviewAnswersAdd.detach();
			reviewAnswersAdd.appendTo('#reviewsAnswers-mobile');
		}
		else {
			reviewAnswersAdd.detach() ;
			reviewAnswersAdd.appendTo('#reviewsAnswersDesktop');
		}
    })
    var windowWidth = $(window).width();
    if (windowWidth < 767) {
        reviewAnswersAdd.detach();
        reviewAnswersAdd.appendTo('#reviewsAnswers-mobile');
    }
    else {
        reviewAnswersAdd.detach();
        reviewAnswersAdd.appendTo('#reviewsAnswersDesktop');
	}
	// Template for Upper reviews snippet
	function paintUpperReviewRatings() {
			$('#stars-wrapper').empty();
			var upperReviews =`<div id="reviewssnippet" class="upper-snippet">
				<div id="variant-header" class="reviews-header-container pr-review-engine">
					<div class="pr-header">
						<div id="scroll-down" class="pr-snapshot-body-wrapper">
							<div class="pr-snapshot-rating">
								<div class="reviews-stars-top">
									<div class="pr-stars pr-stars-${averageRating}" style="${starPosition}">&nbsp;</div>
								</div>
								<p class="reviews-total">
									<span class="average-rating">${averageRatingNotRounded}</span>${totalResults}<span>&nbsp;REVIEWS</span>
								</p>
							</div>
						</div>
						<div class="append-write-review" id="reviews-display-top"></div>
					</div>
				</div>
			</div>`
			var writeReview = $('#lower-write-review');
			$('#stars-wrapper').append(upperReviews);
			$('.append-write-review').append(writeReview);
			//Add on click scroll to lower reviews section from upper reviews snippet
			$("#scroll-down").click(function() {
				$('html, body').animate({
					scrollTop: $("#reviewsAnswers").offset().top - $(".stickyheader").height()
				}, 2000);
			});
	}
	// Template for Lower reviews snippet header 
	function paintLowerReviewRatings() {
			$('#lower-reviews-rating').empty();
			var lowerReviews = `<div class="reviews-header-container pr-review-engine lower-reviews">
				<h1>REVIEWS</h1>
				<div class="pr-header">
					<div class="pr-snapshot-body-wrapper">
						<div class="pr-snapshot-rating">
							<div class="reviews-stars">
								<div class="pr-stars pr-stars-${averageRating}" style="${starPosition}">&nbsp;</div>
							</div>
							<p class="reviews-total">
								<span class="average-rating">${averageRatingNotRounded}</span>${totalResults}<span>&nbsp;REVIEWS</span>
							</p>
						</div>
					</div>
					<div class="append-write-review" id="reviews-display"></div>
				</div>
			</div>`
			var writeReview = $('#lower-write-review');
			$('#lower-reviews-rating').append(lowerReviews);
			$('.append-write-review').append(writeReview);
	}
	// Template for lower reviews snippet header if no reviews exist
	function paintLowerNoReviews() {
			$('#lower-reviews-rating').empty();
			var lowerReviews = `<div class="reviews-header-container pr-review-engine lower-reviews">
				<h1>REVIEWS</h1>
				<div class="pr-header">
					<div class="pr-snapshot-body-wrapper">
						<div class="pr-snapshot-rating">
							<div class="no-reviews">
								<div class="not-yet-rated">Not yet rated. Be the first to&nbsp;</div>
								<div class="append-write-review" id="reviews-display"></div>
							</div>
						</div>
					</div>
				</div>
			</div>`
			$('#lower-reviews-rating').append(lowerReviews);
			$('#QuickViewDialog .pr-snippet-write-review-link').html('Write a Review');
			var writeReview = $('#lower-write-review');
			$('.append-write-review').append(writeReview);
			$('.pr-snippet-write-review-link').html('Write a Review');
	}
	// Template for upper reviews snippet header if no reviews exist
	function paintUpperNoReviews() {
			$('#stars-wrapper').empty();
			var upperReviews =`<div id="reviewssnippet" class="upper-snippet">
				<div id="variant-header" class="reviews-header-container pr-review-engine">
					<div class="pr-header">
						<div class="pr-snapshot-body-wrapper">
						<div class="pr-snapshot-rating no-reviews">
							<div id="scroll-down">
								<div class="reviews-stars">
									<div class="pr-stars pr-stars-0" style="background-position: 0px 0px;">&nbsp;</div>
								</div>
							</div>
							<div class="append-write-review" id="reviews-display-top"></div>
						</div>
						</div>
					</div>
				</div>
			</div>`
			$('#stars-wrapper').append(upperReviews)
			var writeReview = $('#lower-write-review');
			$('.append-write-review').append(writeReview);
			//Add on click scroll to lower reviews section from upper reviews snippet
			$("#scroll-down").click(function() {
				$('html, body').animate({
					scrollTop: $("#reviewsAnswers").offset().top - $(".stickyheader").height()
				}, 2000);
			});
			var quickviewLink = $('#quickview-scroll')
			$('#quickviewDialog #scroll-down').wrap(quickviewLink)
		}
};
// module.exports = powerReviews;
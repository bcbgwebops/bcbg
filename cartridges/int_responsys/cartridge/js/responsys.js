'use strict';

var dialog = require('./dialog');
var util = require('./util');

exports.init = function () {

	if (util.getCookie('ResponsysSession')=='') {
		var options = {};
		options.url = Urls.pageInclude + '?cid=responsys-first-session';
		
		dialog.open(options);
	    
	    var expireDate = new Date(new Date().getTime() + (30 * 24 * 60 * 60 * 1000));
		document.cookie = 'ResponsysSession=true; expires='+ expireDate +'; path=/;';
	}
	
    $(document).on('submit', '#promotional-signup-email-container,#dwfrm_emailsubscription', function(e) {
    	e.preventDefault();
    	if ($(this).valid() == true) {
    		$.post($(this).attr('action'), $(this).serialize(), function(data) {
    			$('#dialog-container').html(data);
    		});
    	}  	
    });	  

    $('#email-alert-signup-footer').submit( function(e) {
    	e.preventDefault();
		if ($(this).valid() == true) {
			var options = {};
			options.url = util.appendParamToURL($(this).attr('action'), 'email', $('#es-email-address-footer').val());
			
			dialog.open(options);
		}   
    });	
}	

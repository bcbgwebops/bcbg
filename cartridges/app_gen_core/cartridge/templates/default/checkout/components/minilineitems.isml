<iscontent type="text/html" charset="UTF-8" compact="true"/>
<iscomment>
    Renders mini lineitems for order summary and mini cart.

    Parameters:
    p_showreverse     	: boolean to render the line items in reverse order
    p_lineitemctnr     	: the line item container to render (this could be either an order or a basket as they are both line item containers)
    p_productlineitem	: in case of mini cart this is the product lineitem just got added/changed so it should be expanded and at the top of the list
    p_giftcertlineitem	: in case of mini cart this is the gift certificate line item just got added/changed so it should be expanded and at the top of the list

</iscomment>

<iscomment>Create page varibale representing the line item container</iscomment>
<isset name="LineItemCtnr" value="${pdict.p_lineitemctnr}" scope="page"/>

<isif condition="${LineItemCtnr != null}">
	<isscript>
		importScript( "int_borderfree:common/libBorderFree.ds" );
		importScript("app_gen_core:util/ProductImageMgr.ds");

		var pliList : dw.util.Collection = new dw.util.ArrayList(LineItemCtnr.productLineItems);
		if( pdict.p_showreverse )
		{
			// order of items is reverse in case of mini cart display
			pliList.reverse();

			// remove the bonus item from the mini cart display
			var tempList : dw.util.ArrayList = new dw.util.ArrayList();

			// add the recently touched/added product line item at the top of the list
			if( pdict.p_productlineitem )
			{
				tempList.add( pdict.p_productlineitem );
			}

			for( var ind in pliList )
			{
				var li = pliList[ind];

				// skip recently touched/added item, its already added before the loop
				if( empty(pdict.p_productlineitem) || (pdict.p_productlineitem && li.position != pdict.p_productlineitem.position))
				{
					tempList.add( li );
				}
			}
			pliList = tempList;
		}
	</isscript>

	<iscomment>the item count is incremented after each display of a line item</iscomment>
	<isset name="itemCount" value="${1}" scope="page"/>

	<iscomment>render a newly added gift certificate line item at the top</iscomment>
	<isif condition="${pdict.p_giftcertlineitem != null}">

		<div class="mini-cart-product">
			<div class="mini-cart-image">
				<img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="<isprint value="${pdict.p_giftcertlineitem.lineItemText}"/>" />
			</div>

			<div class="mini-cart-name">
				<span><isprint value="${pdict.p_giftcertlineitem.lineItemText}"/></span>
			</div>

			<div class="mini-cart-pricing">
				${Resource.msg('global.qty','locale',null)}: 1
				<isprint value="${pdict.p_giftcertlineitem.price}"/>
			</div>

		</div>

		<isset name="itemCount" value="${itemCount+1}" scope="page"/>

	</isif>

	<iscomment>product line items (only rendered if we haven't add a gift certificate to the basket)</iscomment>
		<isloop items="${pliList}" var="productLineItem" status="loopstate">

			<div class="mini-cart-product">
				<div class="mini-cart-image">
					<img src="${ProductImageMgr.detail(productLineItem.product)}" alt="${productLineItem.product.name}" title="${productLineItem.product.name}"/>
				</div>
				<div class="mini-cart-rightside">

					<div class="mini-cart-name">
						<isset name="itemUrl" value="${empty(productLineItem.categoryID) ? URLUtils.http('Product-Show','pid', productLineItem.productID) : URLUtils.http('Product-Show','pid', productLineItem.productID, 'cgid', productLineItem.categoryID)}" scope="page"/>
	
						<a href="${itemUrl}"><isprint value="${productLineItem.productName}"/></a>
					</div>

					<div class="mini-cart-attributes">
						<isdisplayvariationvalues product="${productLineItem.product}"/>
					</div>
					
					<isdisplayproductavailability p_productli="${productLineItem}" p_displayinstock="${false}" p_displaypreorder="${true}" p_displaybackorder="${true}"/>
					
					<div class="mini-cart-qty">				
						<span class="label">${Resource.msg('global.qty','locale',null)}:</span>
						<span class="value"><isprint value="${productLineItem.quantity}"/></span>				
					</div>	
					
					<div class="mini-cart-pricing">
	
						<isif condition="${productLineItem.bonusProductLineItem}">
							${Resource.msg('global.bonus','locale',null)}
						<iselse/>
						
							<isset name="PriceModel" value="${productLineItem.product.getPriceModel()}" scope="page"/>
							<isinclude template="product/components/standardprice"/>
							<span class="mini-cart-price">
	
								<isset name="listPrice" value="${StandardPrice}" scope="page"/>
							
								<isif condition="${BorderFree.isInternational()}">
									<isset name="salePrice" value="${PriceModel.getPrice()}" scope="page"/>
								<iselse/>
									<isset name="salePrice" value="${productLineItem.adjustedPrice}" scope="page"/>
								</isif>
								
								<isif condition="${listPrice.value > salePrice.value}">
									<span class="price-standard">
										${Resource.msg('pricing.original','product',null)} <isprint value="${BorderFree.toInternational(productLineItem.product, StandardPrice.multiply(productLineItem.quantityValue), false)}"/>
									</span>
									<span class="price-sales">
										${Resource.msg('pricing.now','product',null)} <isprint value="${BorderFree.toInternational(productLineItem.product, productLineItem.getAdjustedPrice(), true)}"/>
										<isif condition="${productLineItem.product.custom.isFinalSale == true}"><a class="final-sale">${Resource.msg('pricing.minicartfinalsale','product',null)}</a></isif>
									</span>
								<iselse/>
									<isprint value="${BorderFree.toInternational(productLineItem.product, productLineItem.getAdjustedPrice(), true)}"/>
								</isif>					
							</span>					
						
							<iscomment>					
							<isset name="productTotal" value="${productLineItem.adjustedPrice}" scope="page"/>
							<isif condition="${productLineItem.optionProductLineItems.size() > 0}">
								<isloop items="${productLineItem.optionProductLineItems}" var="optionLI">
									<isset name="productTotal" value="${productTotal.add(optionLI.adjustedPrice)}" scope="page"/>
								</isloop>
							</isif>
							<span class="mini-cart-price">
								<isprint value="${BorderFree.toInternational(null, productTotal, true)}"/>
							</span>
							</iscomment>
						</isif>
					</div>
					<isif condition="${!empty(productLineItem.product.ID)}">
						<isshoprunner p_divname="sr_catalogProductGridDiv" p_pid="${productLineItem.product.ID}" />
					</isif>
				</div>
			</div>
			<isset name="itemCount" value="${itemCount+1}" scope="page"/>
		</isloop>

	<iscomment>gift certificate line items (only rendered if we haven't add a product to the basket)</iscomment>
	
	<isloop items="${LineItemCtnr.giftCertificateLineItems}" var="gcLI" status="loopstate">
		<iscomment>
			Omit showing the gift certificate that was just added (in case we render the minicart).
			This gift certificate has already been rendered at the top before the product line items.
		</iscomment>
		<isif condition="${!(pdict.p_giftcertlineitem != null && pdict.p_giftcertlineitem.UUID.equals(gcLI.UUID))}">

			<div class="mini-cart-product <isif condition="${loopstate.first}"> first <iselseif condition="${loopstate.last}"> last</isif>">

				<div class="mini-cart-image">
					<img src="${URLUtils.staticURL('/images/gift_cert.gif')}" alt="<isprint value="${gcLI.lineItemText}"/>" />
				</div>

				<div class="mini-cart-name">
					<isprint value="${gcLI.lineItemText}"/>
				</div><!-- END: name -->

				<div class="mini-cart-pricing">
					<span class="label">${Resource.msg('global.qty','locale',null)}: 1</span>
					<span class="value"><isprint value="${BorderFree.toInternational(null, gcLI.price true)}"/></span>
				</div>

			</div>
			<isset name="itemCount" value="${itemCount+1}" scope="page"/>
		</isif>
	</isloop>

</isif>

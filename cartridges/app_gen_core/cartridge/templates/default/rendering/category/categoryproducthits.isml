<iscontent type="text/html" charset="UTF-8" compact="true"/>
<isdecorate template="search/pt_productsearchresult_content">
	<isscript>
		importScript("app_bcbg_core:product/ProductUtils.ds");
		var compareEnabled = false;
		if (!empty(pdict.CurrentHttpParameterMap.cgid.value)) {
			compareEnabled = ProductUtils.isCompareEnabled(pdict.CurrentHttpParameterMap.cgid.value);
		}
	</isscript>

	<iscomment>
		Use the decorator template based on the requested output. If
		a partial page was requested an empty decorator is used.
		The default decorator for the product hits page is
		search/pt_productsearchresult.
	</iscomment>

	<iscache type="relative" minute="30" varyby="price_promotion"/>

	<isinclude template="util/modules"/>

	<iscomment>get the current paging model for convenient reuse</iscomment>
	<isset name="pagingModel" value="${pdict.ProductPagingModel}" scope="page"/>

	<iscomment>
		Configured as default template for the product search results.
		Displays a global slot with static html and the product search
		result grid.
	</iscomment>

	<iscomment>create reporting event</iscomment>
	<isinclude template="util/reporting/ReportSearch.isml"/>
	
	<iscomment>suggest similar search phrase below the breadcrumb</iscomment>
	<isif condition="${!empty(pdict.ProductSearchResult.suggestedSearchPhrase)}">
		<p class="did-you-mean">
			${Resource.msg('search.nohits.didyoumeanex','search',null)}
			<a title="${pdict.ProductSearchResult.suggestedSearchPhrase}" href="<isprint value="${URLUtils.http('Search-Show','q',pdict.ProductSearchResult.suggestedSearchPhrase)}"/>"><isprint value="${pdict.ProductSearchResult.suggestedSearchPhrase}"/></a>
		</p>
	</isif>

	<iscomment>
		Render promotional content at the top of search results as global slot.
		This content is only displayed if the search result is refined by a category.
		If the search result is not refined by a category a global default is displayed.
	</iscomment>
	
	<div class="attribute-sorting-refinements clearfix">
		<div class="center-refinebar">
			<isinclude template="search/components/productsearchrefinebar"/>
		</div>
	</div>

	<isif condition="${!empty(pdict.ProductSearchResult) && !empty(pdict.ProductSearchResult.searchPhrase)}">

		<isif condition="${empty(pagingModel)}">
			<isset name="breadcrumbHref" value="${URLUtils.url('Search-Show','q',pdict.ProductSearchResult.searchPhrase)}" scope="page" />
		<iselse/>
			<isset name="breadcrumbHref" value="${pagingModel.appendPaging(URLUtils.url('Search-Show','q',pdict.ProductSearchResult.searchPhrase), 0)}" scope="page" />
		</isif>
		<span class="breadcrumb-element breadcrumb-result-text breadcrumb-element">
			${Resource.msg('searchbreadcrumbs.resultstext','search',null)}
		</span>
		<span class="breadcrumb-element">
			<a href="${breadcrumbHref}" title="${pdict.ProductSearchResult.searchPhrase}">${pdict.ProductSearchResult.searchPhrase}</a>
		</span>
	</isif>

	<iscomment>REFINEMENT INFO </iscomment>
	<isif condition="${!empty(pdict.ProductSearchResult) && (pdict.ProductSearchResult.refinedByPrice || pdict.ProductSearchResult.refinedByAttribute)}">
		
		<iscomment>
			<isif condition="${(pdict.ProductSearchResult.refinements.priceRefinementDefinition && pdict.ProductSearchResult.refinedByPrice) || (pdict.ProductSearchResult.refinements.refinementDefinitions.size() > 0)}">
				<span class="breadcrumb-refined-by">${Resource.msg('searchbreadcrumbs.refinedby', 'search', null)}</span>
			</isif>
		</iscomment>

		<iscomment>Price</iscomment>
		<isif condition="${pdict.ProductSearchResult.refinements.priceRefinementDefinition && pdict.ProductSearchResult.refinedByPrice}">
			<isscript>
				// for price refinements, we use the display value of the price refinement definition's value
				var breadcrumbLabel = null

				var refinements = pdict.ProductSearchResult.refinements;
				var prdValues = refinements.getRefinementValues(refinements.priceRefinementDefinition);
				if (prdValues.iterator().hasNext()) {
					breadcrumbLabel = prdValues.iterator().next().getDisplayValue();
				}
			</isscript>
			<div class="breadcrumb-refinement-bar">
			<span class="breadcrumb-refinement" data-divider="${Resource.msg('searchbreadcrumbs.attributedivider','search',null)}">
				<span class="breadcrumb-refinement-name">
					<isprint value="${pdict.ProductSearchResult.refinements.priceRefinementDefinition.displayName}"/>
				</span>

				<span class="breadcrumb-refinement-value">
				<a class="breadcrumb-relax" href="${unsanitizeOR(breadcrumbHref)}" title="${Resource.msg('global.remove', 'locale', null)}">x</a>
					<isif condition="${!empty(breadcrumbLabel)}">
						<isprint value="${breadcrumbLabel}"/>
					<iselse/>
						<isprint value="${pdict.ProductSearchResult.priceMin}"/>
						${Resource.msg('searchbreadcrumbs.to','search',null)}
						<isprint value="${pdict.ProductSearchResult.priceMax}"/>
					</isif>

					<isif condition="${empty(pagingModel)}">
						<isset name="breadcrumbHref" value="${pdict.ProductSearchResult.urlRelaxPrice('Search-Show')}" scope="page" />
					<iselse/>
						<isset name="breadcrumbHref" value="${pagingModel.appendPaging(pdict.ProductSearchResult.urlRelaxPrice('Search-Show'), 0)}" scope="page" />
					</isif>
				</span>
			</span>
			</div>

		</isif>

		<iscomment>attributes</iscomment>
		<isset name="hasAttributes" value="${false}" scope="page" />
		<isloop items="${pdict.ProductSearchResult.refinements.refinementDefinitions}" var="definition" status="attributes">
			<isif condition="${definition.isAttributeRefinement() && pdict.ProductSearchResult.isRefinedByAttribute(definition.attributeID)}">
				<div class="breadcrumb-refinement-bar">
				<span class="breadcrumb-refinement" data-divider="${Resource.msg('searchbreadcrumbs.attributedivider','search',null)}">
					<span class="breadcrumb-refinement-name">
						<isprint value="${definition.displayName}"/>
					</span>
					<isset name="hasAttributes" value="${true}" scope="page" />
					<isloop items="${pdict.ProductSearchResult.refinements.getRefinementValues(definition)}" var="value" status="values">
						<isif condition="${pdict.ProductSearchResult.isRefinedByAttributeValue(definition.attributeID, value.value)}">
							<span class="breadcrumb-refinement-value">
								<isif condition="${empty(pagingModel)}">
									<isset name="breadcrumbHref" value="${pdict.ProductSearchResult.urlRelaxAttributeValue('Search-Show', definition.attributeID, value.value)}" scope="page" />
								<iselse/>
									<isset name="breadcrumbHref" value="${pagingModel.appendPaging(pdict.ProductSearchResult.urlRelaxAttributeValue('Search-Show', definition.attributeID, value.value), 0)}" scope="page" />
								</isif>
								<a class="breadcrumb-relax" href="${unsanitizeOR(breadcrumbHref)}" title="${Resource.msg('global.remove', 'locale', null)}">x</a>
								<isprint value="${value.displayValue}"/>
								
								<script type="text/javascript">
									window.universal_variable.listing.filter.${definition.displayName.toLowerCase()}.push("${value.displayValue}");
								</script>
							</span>
						</isif>
					</isloop>
				</span>
				</div>
			</isif>
		</isloop>
	</isif>

	<isif condition="${!pdict.ProductSearchResult.refinedSearch && !empty(pdict.ContentSearchResult) && pdict.ContentSearchResult.count > 0}">

		<div class="search-result-bookmarks">
			${Resource.msg('topcontenthits.yoursearch','search',null)}
			<a href="${'#results-products'}" class="first">${Resource.msg('search.producthits.006','search',null)}</a>
			<a href="${'#results-content'}">${Resource.msg('topcontenthits.goto','search',null)}</a>
		</div>

		<h1 class="content-header" id="results-products">${Resource.msgf('search.producthitscount','search',null,pdict.ProductSearchResult.count)}</h1>

	</isif>
	
	<iscomment>MYBUYS TAG LOGIC </iscomment>
	<isinclude template="MyBuys/modules"/>
	
	<isif condition="${pdict.ProductSearchResult.category != null && 'myBuysPageType' in pdict.ProductSearchResult.category.custom && !empty(pdict.ProductSearchResult.category.custom.myBuysPageType)}">
		<ismybuys pagetype="${pdict.ProductSearchResult.category.custom.myBuysPageType}">
	<iselseif condition="${!empty(pdict.ContentSearchResult) && !empty(pdict.ContentSearchResult.searchPhrase)}">
		<ismybuys pagetype="searchResults" searchstring="${pdict.ProductSearchResult.searchPhrase}">
	<iselseif condition="${(pdict.ProductSearchResult.category.parent.ID == 'root')}">
		<ismybuys pagetype="highLevelCategory">
	<iselse>
		<ismybuys pagetype="leafCategory">
	</isif>
	
	<isif condition="${!(pdict.ProductPagingModel == null) && !pdict.ProductPagingModel.empty}">

		<div class="top-search-options search-result-options clearfix">
		
		<iscomment>SEARCH PHRASE INFO </iscomment>
			<isif condition="${!empty(pdict.ContentSearchResult) && !empty(pdict.ContentSearchResult.searchPhrase)}">
				<span class="top-resultstext">
					${Resource.msg('global.search', 'locale', null)}:&nbsp;<a class="searchphraselink" title="${pdict.ContentSearchResult.searchPhrase}">${pdict.ContentSearchResult.searchPhrase}</a>
				</span>
			</isif>
			
			<iscomment>Print Category Name</iscomment>
			<iscomment>
			<isif condition="${pdict.ProductSearchResult.category != null && !empty(pdict.ProductSearchResult.category.getDisplayName())}">
				<h4>${pdict.ProductSearchResult.category.getDisplayName()}</h4>
			</isif>
			</iscomment>
			<iscomment>pagination</iscomment>
			
			<ispagingbar pageurl="${pdict.ProductSearchResult.url('Search-Show')}" pagingmodel="${pdict.ProductPagingModel}"/>

			<iscomment>render compare controls if we present in a category context</iscomment>
			<isif condition="${!empty(pdict.ProductSearchResult) && !empty(pdict.ProductSearchResult.category) && compareEnabled}">
				<iscomparecontrols category="${pdict.ProductSearchResult.category}"/>
			</isif>

		</div>

		<div class="search-result-content two-grid-tile">
			<isproductgrid pagingmodel="${pdict.ProductPagingModel}" category="${pdict.ProductSearchResult.category}"/>
		</div>
		


		<div class="bottom-search-options search-result-options">

			
			<iscomment><isproductsortingoptions productsearchmodel="${pdict.ProductSearchResult}" pagingmodel="${pdict.ProductPagingModel}" uniqueid="grid-sort-footer"/></iscomment>

			<iscomment>
				<ispaginginformation pagingmodel="${pdict.ProductPagingModel}" pageurl="${pdict.ProductSearchResult.url('Search-Show')}" uniqueid="grid-paging-footer"/>
			</iscomment>
			<ispagingbar pageurl="${pdict.ProductSearchResult.url('Search-Show')}" pagingmodel="${pdict.ProductPagingModel}"/>

		</div>
		
		

		<iscomment>show top content hits</iscomment>
		<isif condition="${!pdict.ProductSearchResult.refinedSearch && !empty(pdict.ContentSearchResult) && pdict.ContentSearchResult.count > 0}">

			<h1 class="content-header" id="results-content">${Resource.msgf('topcontenthits.articlesfound','search',null,pdict.ContentSearchResult.count)}</h1>

			<div class="search-results-content">
				<isinclude template="search/topcontenthits"/>
			</div>

		</isif>

	<iselse/>

		<iscomment>display no results</iscomment>
		<div class="no-results">
			${Resource.msg('productresultarea.noresults','search',null)}
		</div>

	</isif>

	<iscomment>Render promotional content at the bottom of search results as global slot</iscomment>
	<div class="search-promo"><isslot id="search-promo" description="Promotional Content at the bottom of Search Results" context="global"/></div>
	
	<iscomment>Adding SEO Content Slots To Category and Sub-Category Pages</iscomment>
	<isif condition="${empty(pdict.CurrentHttpParameterMap.q.stringValue)}">
	<div class="seo-content-slots"><isslot id="seo-content-slots" description="Promotional Content at the bottom of Search Results" context="category" context-object="${pdict.ProductSearchResult.category}"/></div>
    </isif>
	<isif condition="${!empty(pdict.ProductSearchResult.category)}">
		<iscomment>
		<isslot id="category-left-rail" context="category" description="Left rail category page" context-object="${pdict.ProductSearchResult.category}"/>

		<isslot id="cat-banner" context="category" description="Category Banner" context-object="${pdict.ProductSearchResult.category}"/>

		</iscomment>
		<iscomment>
		<isslot id="mega-menu-cat-slot" context="category" description="Slot for category images in the mega menu" />
		<isslot id="mega-menu-img-slot" context="category" context-object="${pdict.ProductSearchResult.category}" description="Slot for category images in the mega menu" />
		</iscomment>
	</isif>

</isdecorate>

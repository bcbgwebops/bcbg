'use strict';
var PRODUCTION = 'production';
var DEVELOPMENT = 'development';

var through = require('through2');
var request = require('request');
var watch = require('gulp-watch');
var url = require('url');
var path = require('path');
var fs = require('fs');
var es = require('event-stream');
var browserify = require('browserify'),
	buffer = require('vinyl-buffer'),
	connect = require('gulp-connect'),
	deploy = require('gulp-gh-pages'),
	gif = require('gulp-if'),
	gulp = require('gulp'),
	gutil = require('gulp-util'),
	jscs = require('gulp-jscs'),
	jshint = require('gulp-jshint'),
	merge = require('merge-stream'),
	minimist = require('minimist'),
	mocha = require('gulp-mocha'),
    rsync = require('gulp-rsync'),
	sass = require('gulp-sass'),
	source = require('vinyl-source-stream'),
	sourcemaps = require('gulp-sourcemaps'),
	stylish = require('jshint-stylish'),
	watchify = require('watchify'),
	notify = require('gulp-notify'),
	prefix = require('gulp-autoprefixer'),
	xtend = require('xtend');

var prefixerOpts = {
		cascade: true,
		browsers: ['last 2 versions', 'ie >= 10', 'iOS 7']
	};

var paths = require('./package.json').paths;
var projectName = require('./package.json').name;

var env = gutil.env.type === 'prod' ? PRODUCTION : DEVELOPMENT;

gutil.log("Using node environment: " + env);

require('babel/register');

var watching = false;
gulp.task('enable-watch-mode', function () {watching = true;});

gulp.task('css', function () {
	var streams = merge();
	var nodePath = path;
	paths.css.forEach(function (path) {
		streams.add(gulp.src(path.src)
			.pipe(sourcemaps.init())
			.pipe(sass({
					includePaths: require('node-normalize-scss').with(
                        // paths.scssIncludePaths.concat(nodePath.dirname(require.resolve('mathsass')))
					),
					outputStyle: (env === PRODUCTION) ? 'compressed' : 'nested',
					errLogToConsole: env !== PRODUCTION
				}).on('error', sass.logError)
			)
			.pipe(prefix(prefixerOpts))
			.pipe(sourcemaps.write('./'))
			.pipe(gulp.dest(path.dest)))
			.pipe(notify({ title: 'SCSS Complete', message: 'SASS Compiled', onLast: true }));
	});
	return streams;
});

gulp.task('js', function(done) {
	var uglify = require('gulp-uglify');
	var babel = require('gulp-babel');
	var tasks = paths.js.map(function(path) {
		function createBundler(path) {
			var opts = {
				entries: path.src,
				debug: env !== PRODUCTION
			};
			if (watching) {
				opts = xtend(opts, watchify.args);
			}
			var bundler = browserify(opts);
			if (watching) {
				bundler = watchify(bundler);
			}
			bundler.on('update', function(ids) {
				gutil.log('File(s) changed:' + gutil.colors.cyan(ids));
				gutil.log('Rebundling');
				rebundle(bundler, path);
			});
			bundler.on('log', gutil.log);
			return bundler;
		}
		function rebundle (bundler, path) {
			return bundler.bundle()
				.pipe(source('app.js'))
					.pipe(buffer())
					.pipe(gif(env === PRODUCTION, babel({
						presets: [
							[
							  'es2015',
							  {
								'targets': {
									'browsers': [
										'ie 11',
										'ie_mob 11',
									]
								}, 'useBuiltins': true
							}
						]
					],
					compact: false
					})))
					.pipe(gif(env === PRODUCTION, uglify({
											compress:true
										})
										.on('error', function(e) {
											gutil.log(gutil.colors.red(e));
										})
					))
					.pipe(gif(env === PRODUCTION, sourcemaps.init({loadMaps:true})))
					.pipe(gif(env === PRODUCTION, sourcemaps.write('./')))
				.pipe(gulp.dest(path.dest))
    	        .pipe(notify({ title: 'JS Complete', message: 'JS Compiled', onLast: true }));
		}
		var b = createBundler(path);
		return rebundle(b, path);
	});
	return es.merge.apply(null, tasks);
});

// Task below updated above

//gulp.task('js', function () {
//	var tasks = paths.js.map(function(jsPath) {
//		var opts = {
//			entries: jsPath.src,
//			debug: env !== PRODUCTION
//		};
//		if (watching) {
//			opts = xtend(opts, watchify.args);
//		}
//		var bundler = browserify(opts);
//		if (watching) {
//			bundler = watchify(bundler);
//		}
//		// optionally transform
//		// bundler.transform('transformer');
//
//		bundler.on('update', function (ids) {
//			gutil.log('File(s) changed: ' + gutil.colors.cyan(ids));
//			gutil.log('Rebundling...');
//			rebundle();
//		});
//
//		bundler.on('log', gutil.log);
//
//		function rebundle () {
//			// minify on non-sandboxes
//			if (env === PRODUCTION) {
//				//bundler = bundler.transform({global: true}, 'uglifyify');
//			}
//
//			return bundler
//				.bundle()
//				.on('error', function (e) {
//					gutil.log('Browserify Error', gutil.colors.red(e));
//					if (!watching) {
//						process.exit(1);
//					}
//				})
//				.pipe(source(path.basename(jsPath.src)))
//				.pipe(gulp.dest(jsPath.dest));
//		}
//		return rebundle();
//	});
//	return es.merge.apply(null, tasks);
//});

gulp.task('jscs', function () {
	return gulp.src('**/*.js')
		.pipe(jscs());
});

gulp.task('jshint', function () {
	return gulp.src('./app_storefront_richUI/cartridge/js/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter(stylish));
});

gulp.task('test:application', function () {
	var opts = minimist(process.argv.slice(2));
	// default option to all
	var suite = opts.suite || '*';
	if (suite === 'all') {
		suite = '*';
	}
	// default reporter to spec
	var reporter = opts.reporter || 'spec';
	// default timeout to 10s
	var timeout = opts.timeout || 10000;
	return gulp.src(['test/application/' + suite + '/**/*.js', '!test/application/webdriver/*'], {read: false})
		.pipe(mocha({
			reporter: reporter,
			timeout: timeout
		}));
});

gulp.task('test:unit', function () {
	var opts = minimist(process.argv.slice(2));
	var reporter = opts.reporter || 'spec';
	var timeout = opts.timeout || 10000;
	var suite = opts.suite || '*';
	return gulp.src(['test/unit/' + suite + '/**/*.js'], {read: false})
		.pipe(mocha({
			reporter: reporter,
			timeout: timeout
		}));
});

function getUserHome() {
  return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}
function uploadFile(file, destDir) {
	var credentialsFile = path.join(getUserHome(), '.dwre.json');
	if (!fs.existsSync(credentialsFile)) {
		gutil.log('[Upload] skipping... (no credentials file)');
		return;
	}
	var dwreFile = JSON.parse(fs.readFileSync(credentialsFile));
	var project = dwreFile.projects[projectName];

	if (!project) {
		gutil.log('[Upload] skipping... (no project)');
		return;
	}
	var defaultEnvironment = project.defaultEnvironment;
	var creds = project.environments[defaultEnvironment];
	var serverUrl = url.format({
		protocol : 'https',
		host : creds.server,
		pathname : '/on/demandware.servlet/webdav/Sites/Cartridges/' + creds.codeVersion + '/'
	});
	var destination = url.resolve(serverUrl, destDir + file.relative);

	file.pipe(request.put(destination).auth(creds.username, creds.password)
	.on('response', function(response) {
		if (response.statusCode >= 300) {
			gutil.log('[Upload] error -- status code: ' + response.statusCode);
		} else {
			gutil.log('[Upload] uploaded ' + destDir + file.relative);
		}
	})
	.on('error', function(err) {
		gutil.log('[Upload] error ', gutil.colors.red(err));
	}));
}
gulp.task('upload', [], function () {

	// Paths override from .dwre.json file to allow
	// devs to work on same sandbox by isolating uploads
	var dwreFile = path.join(getUserHome(), '.dwre.json');
	if (fs.existsSync(dwreFile)) {
		var data = JSON.parse(fs.readFileSync(dwreFile));
		data.paths = data.paths || {};
		if (data.paths.upload) {
			gutil.log('[Upload] overriding upload paths from .dwre.json file...');
			paths.upload = data.paths.upload;
		}
	}

	paths.upload.forEach(function(path) {
		gutil.log('watching', path.src);
		watch(path.src, function(file) {
			gutil.log('[Upload] path changed ' + file.relative);
			uploadFile(file, path.dest);
		});
	});
});

gulp.task('default', ['enable-watch-mode', 'upload', 'js', 'css'], function () {
	gulp.watch(paths.css.map(function (path) {
        return path.src;
	}), ['css']);
	gulp.watch(paths.js.map(function (path) {
		return path.src;
	}), ['js']);
});

gulp.task('build', ['js', 'css']);
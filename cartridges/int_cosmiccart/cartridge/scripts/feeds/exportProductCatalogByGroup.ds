/**
* exportProductCatalogByGroup.ds
*
* Prepares the product catalog for Tmall.
*
* @input GroupIndex: Number
* @input TempCatalogFile: String 
*
* @output Message : String
*/
importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.catalog );
importPackage( dw.customer );
importPackage( dw.io );
importPackage( dw.net );
importPackage( dw.object );
importPackage( dw.campaign );
importPackage( dw.web );

importScript( "app_bcbg_core:/util/ProductImageMgr.ds" );

// Writes the product catalog records contained in the current group to the temp catalog file identified in the PipelineDictionary
function execute( pdict : PipelineDictionary ) : Number
{
	var groupIndex: String = pdict.GroupIndex;

	var site : Site = Site.getCurrent();

    var localFilePath : String = pdict.TempCatalogFile;
    
	var file : File = new File(localFilePath + ".txt");
    var fw : FileWriter = new FileWriter(file, "UTF-8", true);
	
	var productIterator : SeekableIterator = ProductMgr.queryAllSiteProducts();	
	productIterator.forward(groupIndex,1000);
	
    while(productIterator.hasNext()) {
        var product : Product = productIterator.next();
        
		if (!product.isVariant()) {
			var variants : Collection = product.getVariants();
			
			if (variants.isEmpty()) {
            	writeProduct(product, fw);
			} else {
				writeProduct(product, fw);
				
				var iter : Iterator = variants.iterator();

				while (iter != null && iter.hasNext()) {
					var variant : Variant = iter.next();
					writeProduct(variant, fw);
				}
			}
		}
    }
    
	productIterator.close();
	
 	fw.close();
	return PIPELET_NEXT;
}

function writeProduct(theProduct : Product, theFileWriter : FileWriter) {
	var theProductJSON = null;
	
	var active = 0;
	
	if(theProduct.isMaster()){
		active = (theProduct.online && theProduct.searchable && theProduct.categorized && theProduct.availabilityModel.orderable) ? "1" : "0";  // 1-true, 0-false	
	} else {
		var active_master = (theProduct.masterProduct.online && theProduct.masterProduct.searchable && theProduct.masterProduct.categorized && theProduct.masterProduct.availabilityModel.orderable) ? "1" : "0";  // 1-true, 0-false	
		active = ((active_master == "1") && theProduct.online && theProduct.availabilityModel.orderable) ? "1" : "0";  // 1-true, 0-false
	}
	
	if(active == 1 && !theProduct.bundle && !theProduct.productSet && !theProduct.optionProduct){
		theProductJSON = {};
		
		theProductJSON = getGenericProductData(theProduct);
		
		var str = null;
		var prepend = null;

		str = JSON.stringify(theProductJSON);
		append = ",";
		str = StringUtils.format('{0}{1}',str,append);
		
		theFileWriter.write(str);
		theFileWriter.flush();
	}
}

function getGenericProductData(theProduct : dw.catalog.Product){
	var theProductJSON = {};
	
	var listPricebookID = dw.system.Site.getCurrent().getCustomPreferenceValue('listPriceDefault');
	var storeId = dw.system.Site.getCurrent().getCustomPreferenceValue('cosmicCartStoreId');
	var currencyCode = dw.system.Site.getCurrent().getDefaultCurrency();
	var custAttrs = dw.system.Site.getCurrent().getCustomPreferenceValue('cosmicCartRegCustomAttributes');
	var viewTypeSize = dw.system.Site.getCurrent().getCustomPreferenceValue('cosmicCartImageViewTypeSize');
	var pvm = null;
	var rawItemType = null;

	theProductJSON.storeId = storeId;
	
	if(theProduct.isMaster()){
		rawItemType = "PRODUCT";
	}else if(theProduct.isVariant()){
		rawItemType = "VARIANT";
	}else{
		rawItemType = "STANDALONE";
	}
	
	theProductJSON.rawItemType = rawItemType;
	
	if(!empty(theProduct.getEAN())){
		theProductJSON.ean = theProduct.getEAN();
	}
	theProductJSON.sku = !empty(theProduct.getUPC()) ? theProduct.getUPC() : theProduct.getID();
	
	if(!empty(theProduct.getName())){
		theProductJSON.name = theProduct.getName();
	}
	if(!empty(theProduct.getID())){
		theProductJSON.productId = theProduct.getID();
	}
	if(!empty(theProduct.getLongDescription())){
		theProductJSON.description = theProduct.getLongDescription().toString();
	}
	if(!empty(theProduct.getBrand())){
		theProductJSON.brand = theProduct.getBrand();
	}
	
	theProductJSON.available = theProduct.getAvailabilityModel().isInStock();
	
	//images
	theProductJSON.photos = [];
	var imgs = new Array();
	var image_url = ProductImageMgr.detail(theProduct);
	var image_url_a = ProductImageMgr.detailAlt(theProduct,"a");
	var image_url_b = ProductImageMgr.detailAlt(theProduct,"b");
	var image_url_c = ProductImageMgr.detailAlt(theProduct,"c");
	var image_url_d = ProductImageMgr.detailAlt(theProduct,"d");
	var image_url_e = ProductImageMgr.detailAlt(theProduct,"e");
	
	imgs.push(image_url);
	if (image_url_a != "") { imgs.push(image_url_a); }
	if (image_url_b != "") { imgs.push(image_url_b); }
	if (image_url_c != "") { imgs.push(image_url_c); }
	if (image_url_d != "") { imgs.push(image_url_d); }
	if (image_url_e != "") { imgs.push(image_url_e); }
	
	for (let i = 0;i< imgs.length; i++) {
		var obj = {
			"url" : imgs[i].toString(),
			"position" : i
		}
		theProductJSON.photos.push(obj);
	}
	
	var priceModel : ProductPriceModel = theProduct.getPriceModel();
	var listprice : Money = null;
	var saleprice : Money = null;
	
    if (priceModel != null) {
    	var salePriceBook = Site.getCurrent().getCustomPreferenceValue('salePriceDefault');
		saleprice = priceModel.getPriceBookPrice(salePriceBook);

		var priceBook = Site.getCurrent().getCustomPreferenceValue('listPriceDefault');
		listprice = priceModel.getPriceBookPrice(priceBook);
    }
	theProductJSON.pricing = {
		price : listprice.value.toFixed(2),
		salePrice : saleprice.value.toFixed(2),
		currency : currencyCode
	}
	
	if(!empty(custAttrs)){
		theProductJSON.custom = {};
		for each(let ca in custAttrs){
			if(ca in theProduct.custom && !empty(theProduct.custom[ca])){
				theProductJSON.custom[ca] = theProduct.custom[ca];
			}else if(!empty(theProduct[ca])){
				theProductJSON.custom[ca] = theProduct[ca];
			}
		}
	}
	
	if(theProduct.master){
		theProductJSON.options = [];
		pvm = theProduct.getVariationModel();
		for each(let prodVarAttr in pvm.getProductVariationAttributes()){
			var obj = { 
				"attribute" : prodVarAttr.getDisplayName()
			};
			arr = [];
			for each(let prodVarAttrVal in pvm.getAllValues(prodVarAttr)){
				if (prodVarAttrVal != null) {
					arr.push(prodVarAttrVal.getDisplayValue());
				}
			}
			obj.values = arr;
			theProductJSON.options.push(obj);
		}
		
	}else if(theProduct.variant){
		theProductJSON.product = theProduct.getVariationModel().getMaster().getID();
		theProductJSON.attributes = [];
		pvm = theProduct.getVariationModel();
		for each(let prodVarAttr in pvm.getProductVariationAttributes()){
			var vv = pvm.getVariationValue(theProduct, prodVarAttr);
			var dv = "";
			if (vv != null) {
				dv = vv.getDisplayValue()
			}
			var obj = {
				"attribute" : prodVarAttr.getDisplayName(),
				"value" : dv
			};
			theProductJSON.attributes.push(obj);
		}
	}
	
	return theProductJSON;
}

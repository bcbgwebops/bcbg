var StringUtils = require('dw/util').StringUtils;
/** @module int_cosmiccart/cartridge/scripts/cosmiccart/utils/CosmicCartUtils */
/**
* CosmicCartUtils
* Common Utilities for Demandware Development
* @namespace
*/
function CosmicCartUtils () {}

/**
* Get the request body as a JSON object
*/
CosmicCartUtils.getRequestBodyAsJSON = function(r){
	var o = {};
	try{
		o = JSON.parse(r);
	}catch(e){
		dw.system.Logger.getLogger('CosmicCart').error('[CosmicCartUtils.ds] Error parsing request body');
	}
	return o;
}

/**
* Get the product line item by ID
*/
CosmicCartUtils.getProductLineItemByUUID = function(theBasket, theID){
	var plis = theBasket.getProductLineItems();
	for(let i = 0; i < plis.length; i++){
		if(theID == plis[i].getUUID()){
			return plis[i];
		}
	}
	return null;
}

CosmicCartUtils.getApplicableShippingMethods = function(theBasket : dw.order.Basket){
	var shipment = theBasket.getDefaultShipment();
	var theAddress = shipment.getShippingAddress();

	// country code
	var countryCode : String = theAddress.getCountryCode().getValue();
	if (countryCode != null ){
		countryCode = StringUtils.trim(countryCode);
	}
	
	// state code
	var stateCode : String = theAddress.getStateCode();
	if (stateCode != null ){
		stateCode = StringUtils.trim(stateCode);
	}
	
	
	// postal code
	var postalCode : String	= theAddress.getPostalCode();
	if (postalCode != null)
	{
		postalCode = StringUtils.trim(postalCode);	// no need for defaults
	}
	
	// city
	var city : String = theAddress.getCity();
	if (city != null)
	{
		city = StringUtils.trim(city);  // no need for defaults
	}	
	
	// address1
	var address1 : String = theAddress.getAddress1();
	if (address1 != null)
	{
		address1 = StringUtils.trim(address1);  // no need for defaults
	}
	
	// address2
	var address2 : String = theAddress.getAddress2();
	if (address2 != null)
	{
		address2 = StringUtils.trim(address2);  // no need for defaults
	}
	
	// Construct an address from request parameters.
	var addressObj = new Object();
	addressObj.address1 = address1;
	addressObj.address2 = address2;
	addressObj.countryCode = countryCode;
	addressObj.stateCode = stateCode;
	addressObj.postalCode = postalCode;
	addressObj.city = city;
	
	// Retrieve the list of applicabnle shipping methods for the given shipment and address.
	var shippingModel : dw.order.ShipmentShippingModel = dw.order.ShippingMgr.getShipmentShippingModel(shipment);
	var ret = shippingModel.getApplicableShippingMethods(addressObj);
	var arr = [];
	var obj = {};
	for each(let method in ret){
		if(method.isOnline()){
			obj = {};
			obj.id = method.getID();
			obj.name = method.getDisplayName();
			obj.isDefault = method.isDefaultMethod();
			obj.description = method.getDescription();
			obj.price = shippingModel.getShippingCost(method).getAmount().getValue();
			arr.push(obj);
		}
	}
	return {"shippingMethods":arr};
}

/** 
 * CosmicCart Utility namespace
 * @type {CosmicCartUtils}
 */
exports.CosmicCartUtils = CosmicCartUtils;
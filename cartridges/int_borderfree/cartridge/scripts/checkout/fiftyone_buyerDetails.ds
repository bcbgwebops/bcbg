/**
*   Generates the buyer profile portion of the BTP based on the logged in customer
*	
*	@input 	Customer : dw.customer.Customer
*	@output BuyerXml : XML
*/

importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.customer);

importScript( "common/fiftyone_site.ds" );

//Generates the buyer profile portion of the BTP based on the logged in customer
function execute( args : PipelineDictionary ) : Number
{
    try
	{
		var customer : dw.customer.Customer = args.Customer;
		var profile : Profile = customer.getProfile();
		var addressBook : AddressBook = profile.getAddressBook();
		var primaryAddress : CustomerAddress = addressBook.getPreferredAddress();
		
		if(primaryAddress==null)
		{
			args.BuyerXml = null;
		}
		else
		{
			//build xml
			var xmlBuilder : ArrayList = new ArrayList();
			xmlBuilder.add1("<BuyerProfile><BuyerProfileType type='Billing'>");
		    xmlBuilder.add1(StringUtils.format("<MerchantProfileID>{0}</MerchantProfileID>",customer.getID()));
	        xmlBuilder.add1(StringUtils.format("<FirstName>{0}</FirstName>",encodeXml(profile.getFirstName())));
	        xmlBuilder.add1(StringUtils.format("<MiddleInitials>{0}</MiddleInitials>",encodeXml(profile.getSecondName())));
	        xmlBuilder.add1(StringUtils.format("<LastName>{0}</LastName>",encodeXml(profile.getLastName())));
	        xmlBuilder.add1(StringUtils.format("<AddressLine1>{0}</AddressLine1>",encodeXml(primaryAddress.getAddress1())));
	        xmlBuilder.add1(StringUtils.format("<AddressLine2>{0}</AddressLine2>",encodeXml(primaryAddress.getAddress2()||'')));
	        xmlBuilder.add1(StringUtils.format("<City>{0}</City>",encodeXml(primaryAddress.getCity())));
	        xmlBuilder.add1(StringUtils.format("<Region>{0}</Region>",primaryAddress.getStateCode()));
	        xmlBuilder.add1(StringUtils.format("<Country>{0}</Country>",primaryAddress.getCountryCode().value));
	        xmlBuilder.add1(StringUtils.format("<PostalCode>{0}</PostalCode>",primaryAddress.getPostalCode()));
	        xmlBuilder.add1(StringUtils.format("<Email>{0}</Email>",encodeXml(profile.getEmail())));
	        xmlBuilder.add1(StringUtils.format("<PrimaryPhone>{0}</PrimaryPhone>",primaryAddress.getPhone()));
	        xmlBuilder.add1("</BuyerProfileType></BuyerProfile>");
			
			var buyerXml : XML = xmlBuilder.join('\n');
	    	args.BuyerXml = buyerXml;
		} 
	}
	catch(e)
	{
		logError(e);
		return PIPELET_ERROR;
	}
	
    return PIPELET_NEXT;
}
/**
*   Calls the FiftyOne order API to retrieve the FiftyOne order ID for a given basket
*	E4X order number will be stored in custom attribute for DW order 
*
*	@input Basket : dw.order.Basket
*	@output FiftyOneOrderID : String
*	
*/

importPackage( dw.order );
importPackage( dw.util );

importScript( "common/fiftyone_site.ds" );
importScript( "common/fiftyone_orderAPI.ds" );

//Calls the FiftyOne order API to retrieve the FiftyOne order ID for a given basket
function execute( args : PipelineDictionary ) : Number
{
	var basket :dw.order.Basket = args.Basket;
	try
	{
		var fiftyoneOrderId = getOrderId(basket.custom.fiftyone_referenceId); //Pass the same UUID set in checkout request
		if(!fiftyoneOrderId)
			throw new Error(StringUtils.format("An empty FiftyOne Order ID was returned for merchant ref id:{0}",basket.custom.fiftyone_referenceId)); 
		
		args.FiftyOneOrderID = fiftyoneOrderId;
	}
	catch(e)
	{
		logError(e);
		return PIPELET_ERROR;
	}
	return PIPELET_NEXT;
}

//given a merchantRefId (a UUID passed to Borderfree at the time checkout is envoked), return Borderfree order number (E4X)
function getOrderId(merchantRefId : String)
{
	var orderId = '';
	try
	{
		var orderXml = getOrderStateXml(merchantRefId, true);
		var orderId = parseOrderId(orderXml);
	}
	catch(ex)
	{
		throw new Error(StringUtils.format("FiftyOne Order ID could not be retrived for merchant ref id:{0}",merchantRefId));
	}
	return orderId;
}

//parse XML response and return E4X order number
function parseOrderId(orderXml)
{
	var nsAPI = new Namespace("http://services.fiftyone.com/ws/merchantAPI/v1.0");
	var nsSOAP = new Namespace('http://www.w3.org/2003/05/soap-envelope');
	var body = orderXml.nsSOAP::Body;
	var orderResponse = body.nsAPI::getOrderDetailsResponse;
	var mapNode = orderResponse.merchOrderState.map;
	var entryNode = mapNode.entry.(key == 'E4XOrderNum');
	
	return entryNode["value"].toString();
}
/**
*	Retrieves the order details from the specified manifest file and populates the basket to 
*	match the missing order details.
*
*	@input Basket : dw.order.Basket
*	@input OrderId : String
*	@input ManifestFile : String
*   @output OrderState : String
*/

importPackage( dw.system );
importPackage( dw.util );
importPackage( dw.order );
importPackage( dw.io );


importScript( "common/fiftyone_site.ds" );
importScript( "common/fiftyone_xmlFeed.ds" );

//executed during 51AuditOrders-Synchronize
//Retrieves the order details from the specified manifest file and populates the basket to match the missing order details
function execute( args : PipelineDictionary ) : Number
{	
	try
	{
    	var basket : Basket = args.Basket;
		var manifestFilePath : String = args.ManifestFile;
		var orderId :String = args.OrderId;
		
		var orderObj = getOrder(manifestFilePath, orderId);
		args.OrderState = orderObj.state;
		buildBasket(basket, orderObj);
		if(orderObj.orderDiscount)
		{
			var adjustment : LineItem = basket.createPriceAdjustment(UUIDUtils.createUUID());
			adjustment.setPriceValue(orderObj.orderDiscount);
		}
		
		normalizeBasket(basket);
	}
    catch(e)
	{
		logError(e);
		return PIPELET_ERROR;
	}
		
	return PIPELET_NEXT;
}

//get line item price if it doesn't exist
function normalizeBasket(basket : Basket)
{
	var lineItems : Collection = basket.allProductLineItems;
	for(var i=0;i<lineItems.length;i++)
	{
		if(!lineItems[i].priceValue)
			lineItems[i].setPriceValue(0);
	}
}

//accessor method to get order based on orderId
function getOrder(manifestFilePath, orderId)
{
	var fileReader : FileReader = null;
	var xmlStreamReader : XMLStreamReader = null;
	
	try
	{
		var manifestFile = new File(manifestFilePath);
		fileReader = new FileReader(manifestFile, 'UTF-8');
		xmlStreamReader= new XMLStreamReader(fileReader);
		
		while(findNextNode(xmlStreamReader, "Order")!=null)
		{
			var orderNode : XML = xmlStreamReader.getXMLObject();
			if(orderNode.OrderId.E4XOrderId.toString()==orderId)
			{
				return mapOrderObject(orderNode);
			}
		}
		return null;
	}
	finally
	{
		if(xmlStreamReader!=null) xmlStreamReader.close();
		if(fileReader!=null) fileReader.close();
	}
}

//Add order information (Borderfree's E4X order number and order state) and set basket items
function mapOrderObject(orderNode : XML)
{
	var orderObj = {};
	orderObj.id=orderNode.OrderId.E4XOrderId.toString();
	orderObj.state = orderNode.FraudState.toString();
	orderObj.orderDiscount = orderNode.DomesticBasket.OrderDetails.TotalOrderLevelDiscounts * 1;
	
	orderObj.orderItems = [];
	
	var basketItems : XMLList = orderNode.DomesticBasket.BasketDetails.BasketItem;
	for(var i=0;i<basketItems.length();i++)
	{
		orderObj.orderItems.push(
			{
				sku:basketItems[i].MerchantSKU.toString(),
				quantity:basketItems[i].ProductQuantity.toString()*1,
				price:basketItems[i].ProductSalePrice.toString()*1
			}
		);
	}
	return orderObj;
}

//assign prices and quantity to basket items
function buildBasket(basket : Basket, orderObj)
{
	var shipment = basket.getDefaultShipment();
	for(var i=0;i<orderObj.orderItems.length;i++)
	{
		var lineItem : ProductLineItem = basket.createProductLineItem(orderObj.orderItems[i].sku,shipment);
		lineItem.setPriceValue(orderObj.orderItems[i].price * orderObj.orderItems[i].quantity);
		lineItem.setQuantityValue(orderObj.orderItems[i].quantity);
	}	
}




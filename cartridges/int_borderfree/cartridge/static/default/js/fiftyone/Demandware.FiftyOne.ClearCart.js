/*******************************************************************
 * 
 *  Used after Envoy checkout to clear the mini-cart display without
 *  requiring a page refresh. 
 * 
 ******************************************************************/

if(typeof(Demandware)=='undefined')Demandware = {};
if(!Demandware.FiftyOne)Demandware.FiftyOne = {};

Demandware.FiftyOne.Checkout = new function()
{
	//constructor
	new function()
	{
		$(document).bind("ready", clearCart);
	}
	
	function clearCart()
	{
		var parentDocument = parent.document;
		var miniCart = parent.$('#mini-cart');
		var cartTotal = miniCart.find('div.mini-cart-total');
		cartTotal.replaceWith('<div class="mini-cart-total"><span class="mini-cart-label">Cart</span><span class="mini-cart-empty">(0)</span></div>');
		
		var cartContent = miniCart.find('div.mini-cart-content');
		cartContent.remove();
	}
	
	
}
Dependancies
1. verify that node is installed on your machine, if it isn't install it.
2. open terminal to root project folder
3. run: sudo npm install
4. run: grunt (will watch css and js files and build when something changes)


Eclipse Set up:
1. Open Eclipse
2. click browse, and choose/create a place to save your workspace (outside of the repository).
3. File -> Import -> General -> Existing Projects into Workspace
4. Select Root Directory: choose your checked our repository folder.
5. Verify that all projects are selected
6. Verify that "Copy projects into workspace" is NOT checked.
7. Click Finish
8. File -> New -> Demandware Server Connection
9. Choose a name for your server connection
10. Enter your sandbox host (for example: dev07-web-bcbg.demandware.net) and credentials.
11. Select the correct version directory (probably version1).
12. Click finish.



Pushing changes to sandbox:
- Changes made within Eclipse should be automatically uploaded to the configured server.
- Changes made outside of Eclipse (like css or js files built by grunt) will need to be manually uploaded. Find the folder that holds the changed files, right click, and choose refresh.
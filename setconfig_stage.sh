#!/usr/bin/env bash
_CODE_VERSION=$(date -u +"%Y-%m-%dT%H%M%SZ")

# build config
cat > build_settings.sh <<CONFIGEND

CODE_VERSION=${_CODE_VERSION}
EXCLUDE_CARTRIDGES=()

CONFIGEND

cat > deploy_settings.sh <<CONFIGEND

SERVER="cert.staging.web.bcbg.demandware.net"

TWO_FACTOR_ENABLED="1"

PEM_FILE="bcbg.pem"
PEM_PASSWORD="0414webbcbg1600"
CODE_PACKAGE="build/${_CODE_VERSION}.zip"
CODE_VERSION="${_CODE_VERSION}"

DEPLOYMENT_USER=$staging_user
DEPLOYMENT_PASSWORD=$staging_pass

CURL_EXEC="curl"
CURL_OPTS="-v -k"

CONFIGEND

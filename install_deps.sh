#!/usr/bin/env bash

cd cartridges && npm install --unsafe-perm && npm run build && cd ..
#!/usr/bin/env bash
#
# Demandware Deploy Shell Script
# Copyright (c) 2015 PixelMedia, Inc
#

set -e
#set -x

if [ ! -f deploy_settings.sh ]; then
  echo "Error: 'deploy_settings.sh' deployment settings file not found"
  exit 1
fi

# Import settings
source deploy_settings.sh

_CURL_OPTS="${CURL_OPTS}"

if [ $TWO_FACTOR_ENABLED -eq 1 ]; then
  echo "Enabling two factor authentication client certificates..."
  _CURL_OPTS="${_CURL_OPTS} --cert ${PEM_FILE}:${PEM_PASSWORD} "
fi

function finish {
  echo "Cleaning up..."
  echo "Removing cookie jar..."
  rm -f cookies.txt
}
trap finish EXIT

echo "Uploading code package ${CODE_PACKAGE}..."
_CMD="https://${SERVER}/on/demandware.servlet/webdav/Sites/Cartridges/2016-11-03T002432Z-ECOM-DSP2-85.zip \
  --silent \
  -T ${CODE_PACKAGE} --user ${DEPLOYMENT_USER}:${DEPLOYMENT_PASSWORD}" 
$CURL_EXEC $_CURL_OPTS $_CMD

echo "Unzipping code package..."
_CMD="https://${SERVER}/on/demandware.servlet/webdav/Sites/Cartridges/2016-11-03T002432Z-ECOM-DSP2-85.zip \
  --silent \
  --data method=UNZIP --user ${DEPLOYMENT_USER}:${DEPLOYMENT_PASSWORD}"
$CURL_EXEC $_CURL_OPTS $_CMD

echo "Logging in with deployment user ${DEPLOYMENT_USER}..."
_CMD="https://${SERVER}/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin \
  --data-urlencode LoginForm_Login=${DEPLOYMENT_USER} \
  --data-urlencode LoginForm_Password=${DEPLOYMENT_PASSWORD} \
  --data-urlencode LocaleID= \
  --data-urlencode login= \
  --data-urlencode LoginForm_RegistrationDomain=Sites \
  --data-urlencode ForgotPassword=false \
  --cookie cookies.txt --cookie-jar cookies.txt"
$CURL_EXEC $_CURL_OPTS $_CMD

echo "Activating code version ${CODE_VERSION}..."
_CMD="https://${SERVER}/on/demandware.store/Sites-Site/default/ViewCodeDeployment-Activate \
  --silent \
  --data CodeVersionID=${CODE_VERSION} \
  --cookie cookies.txt --cookie-jar cookies.txt"
$CURL_EXEC $_CURL_OPTS $_CMD


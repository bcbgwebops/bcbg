#!/usr/bin/env bash
_CODE_VERSION=$(date -u +"%Y-%m-%dT%H%M%SZ")

# build config
cat > build_settings.sh <<CONFIGEND

CODE_VERSION=${_CODE_VERSION}
EXCLUDE_CARTRIDGES=()

CONFIGEND

cat > deploy_settings.sh <<CONFIGEND

SERVER="dev02-web-bcbg.demandware.net"

TWO_FACTOR_ENABLED="0"

PEM_FILE="keystore.pem"
PEM_PASSWORD=""

CODE_PACKAGE="build/${_CODE_VERSION}.zip"
CODE_VERSION="${_CODE_VERSION}"

DEPLOYMENT_USER=$dev02_user
DEPLOYMENT_PASSWORD=$dev02_pass

CURL_EXEC="curl"
CURL_OPTS="-k"

CONFIGEND
